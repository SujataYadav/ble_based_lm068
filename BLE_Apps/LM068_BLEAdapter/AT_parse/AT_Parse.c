

#include "../CSR_LIB.h"
#include "../common.h"
#include "AT_Parse.h"
#include "at_Parser_Internal.h"

#include "../debug.h"

#include "../utils/utils.h"
#include "../config/config.h"


/* ************ LOCAL define for LM_DEBUG_PARSE ********************  
#define     LM_AT_PARSE  */

#ifdef      LM_AT_PARSE
#define     LM_DEBUG_PARSE(x)   printf x
#else
#define     LM_DEBUG_PARSE(x)            
#endif




/**
 * AT parser state data structure.
 */
typedef struct at_parser_state_st
{
    /** Current FSM state */
    enum
    {
        /** Awaiting "AT" */
        STATE_AWAIT_AT,
        /** Awaiting "\r\n" */
        STATE_AWAIT_TERMINATE,
        /** Awaiting "\r\n" after a parser error */
        STATE_ERR_AWAIT_TERMINATE
    } fsm_state;

    /** Offset into the received buffer */
    uint16 offset;

} at_parser_state_st;




static at_parser_state_st state;

void at_parser_init(void)
{
    memset(&state, 0, sizeof(state));

    /* Connect the UART Sink to the application Task so that we receive
     * messages from it. */
    MessageSinkTask(at_get_sink(), app_data.task);
}


bool Is_Response_Set ( void )
{
    if ( app_config.resp )
        return TRUE;
    else
        return FALSE;
}


void at_parser_send_err_response(void)
{
    if ( app_config.resp )
    {
        uart_tx("ERR\r\n", lstrlen("ERR\r\n"));
    }
}

void at_parser_send_STATEerr_response(void)
{
    if ( app_config.resp )
    {
        uart_tx("State ERR\r\n", lstrlen("State ERR\r\n"));
    }
}
void at_parser_send_ok_response(void)
{
    if ( app_config.resp )
    {
        uart_tx("OK\r\n", lstrlen("OK\r\n"));
    }
}



/**
 * Function invoked when data is received in state \ref STATE_AWAIT_AT.
 * \param data     Data buffer
 * \param length   Length of data
 *
 * \return number of characters to consume, or -1 to indicate that processing
 * is blocked on the reception of more data.
 */
static int32 process_data_in_state_await_at(const uint8 *data, uint16 length)
{
    int32 pos = find_word(data, length, "AT", lstrlen("AT"));
    if (pos < 0)
    {
        /* If the last character is A and leave it in the buffer,
         * since it could be the start of AT. */
        if (MATCH_LETTER(data[length - 1], 'A'))
        {
            LM_DEBUG_PARSE(("No match looking for AT. Leaving last A in buffer\n"));

            /* If there is more data than just the 'A' in the buffer then we
             * remove it, otherwise we return -1 to indicate that we are done
             * parsing and need more data before we can proceed. */
            if ((length - 1) > 0)
                return length - 1;
            else
                return -1;
        }
        else
        {
            LM_DEBUG_PARSE(("No match looking for AT.\n"));
            return length;
        }
    }

    LM_DEBUG_PARSE(("AT matched. Awaiting *\n"));
    state.fsm_state = STATE_AWAIT_TERMINATE;
    /* Start scanning for \r\n from offset 1, since we are looking for a
     * two character pattern. */
    state.offset = 1;
    return pos + lstrlen("AT");
}



/**
 * Function invoked when data is received in state \ref STATE_AWAIT_TERMINATE.
 * \param data     Data buffer
 * \param length   Length of data
 * \return number of characters to consume, or -1 to indicate that processing
 * is blocked on the reception of more data.
 */
static int32 process_data_in_state_await_terminate(const uint8 *data,
                                                   uint16 length)
{
    LM_DEBUG_PARSE(("Looking for terminator (starting @ offset %u)\n", state.offset));
    while (state.offset < length)
    {
        /* If we have received too much data to be a valid command then
         * go into the error state. */
        if (state.offset > AT_PARSER_MAX_PACKET_LENGTH)
        {
            LM_DEBUG_PARSE(("Command packet overflow\n"));
            state.fsm_state = STATE_ERR_AWAIT_TERMINATE;
            return state.offset;
        }

        /* If we have received \r\n then activate the subparser
         * (leaving the data in the buffer for it to use) */
        if (data[state.offset - 1] == '\r' && data[state.offset] == '\n')
        {
            /* Terminator found. "Get out". */

            MOCK(at_parser_invoke_subparser(data, state.offset - 1));
            state.fsm_state = STATE_AWAIT_AT;
            return state.offset + 1;
        }

        state.offset++;
    }

    /* We still haven't got \r\n yet, so we need to buffer more data */
    return -1;
}


/**
 * Function invoked when data is received in state
 * \ref STATE_ERR_AWAIT_TERMINATE.
 *
 * \param data     Data buffer
 * \param length   Length of data
 * \return number of characters to consume, or -1 to indicate that processing
 * is blocked on the reception of more data.
 */
static int32 process_data_in_state_err_await_terminate(const uint8 *data,
                                                       uint16 length)
{
    uint16 i;
    for (i = 1; i < length; i++)
    {
        if (data[i - 1] == '\r' && data[i] == '\n')
        {
            /* End of the command received. Send ERR response. */
            MOCK(at_parser_send_err_response());

            state.fsm_state = STATE_AWAIT_AT;
            return i;
        }
    }

    /* If the last character in the buffer is \r then we need to keep it
     * around in case the next character is \n, we discard everything else.
     */
    if (data[length - 1] == '\r')
    {
        /* If there is more data than just the '\r' in the buffer then we
         * remove it, otherwise we return -1 to indicate that we are done
         * parsing and need more data before we can proceed. */
        if ((length - 1) > 0)
            return length - 1;
        else
            return -1;
    }
    else
    {
        return length;
    }
}


/**
 * This function is responsible for invoking the appropriate state-specific
 * data handling function for the current state.
 *
 * \param data     Data buffer
 * \param length   Length of data
 * \return number of characters to consume, or -1 to indicate that processing
 * is blocked on the reception of more data.
 */
static int32 process_data(const uint8 *data, uint16 length)
{
    LM_DEBUG_PARSE(("process_data(length=%u)\n", length));

    if (length == 0)
    {
        LM_DEBUG_PARSE(("process_data: unexpected zero length\n"));
        return -1;
    }

    switch (state.fsm_state)
    {
        case STATE_AWAIT_AT:
            return process_data_in_state_await_at(data, length);

        case STATE_AWAIT_TERMINATE:
            return process_data_in_state_await_terminate(data, length);

        case STATE_ERR_AWAIT_TERMINATE:
            return process_data_in_state_err_await_terminate(data, length);


        default:
            LM_DEBUG_PARSE(("AT parser process_data in invalid state %u\n",
                      state.fsm_state));
            Panic();
            return -1;
    }
}


uint16 at_parser_process_data(const uint8 *data, uint16 length)
{
    uint16 consumed = 0;

    while (consumed < length)
    {
        int32 to_consume = process_data(data, length - consumed);

        /* A negative return value is a special case indicating that we have
         * finished processing what it is in the buffer for now and need
         * to wait for more data. */
        if (to_consume < 0)
            return consumed;

        data += to_consume;
        consumed += to_consume;

        LM_DEBUG_PARSE(("After process_data total conusmed=%u (length=%u)\n",
                  consumed, length));
    }

    return consumed;
}









