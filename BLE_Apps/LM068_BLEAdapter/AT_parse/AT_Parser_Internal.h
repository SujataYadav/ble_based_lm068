#ifndef AT_PARSER_INTERNAL_H__
#define AT_PARSER_INTERNAL_H__





#ifndef AT_PARSER_MAX_PACKET_LENGTH
/**
 * Maximum length of a packet to receive (in octets). Note that this
 * excludes \\r\\n, so it is necessary to buffer
 * (AT_PARSER_MAX_PACKET_LENGTH + 2).
 */
#define AT_PARSER_MAX_PACKET_LENGTH             (512)
#endif


/*
 * -------------------------------------------------------------------------
 *                           Utility Functions
 * -------------------------------------------------------------------------
 */

/**
 * Get the Sink being used by the AT module to send data to the host.
 *
 * \return the Sink being used by the AT module.
 */
Sink at_get_sink(void);


/**
 * Get the Source being used by the AT module to receive data from the host.
 *
 * \return the Source being used by the AT module.
 */
Source at_get_source(void);


/*
 * -------------------------------------------------------------------------
 *                     Parser internal functions
 * -------------------------------------------------------------------------
 *
 * These functions are exposed here for unit testing purposes.
 */


/**
 * Invoke the appropriate subparser for the given AT packet.
 *
 * \param at_packet         The AT packet to hand to the subparser (note that
 *                          the "AT" will already have been stripped from the
 *                          start of the packet.
 * \param at_packet_length  Length of at_packet.
 */
void at_parser_invoke_subparser(const uint8 *at_packet,
                                uint16 at_packet_length);


/*
 * -------------------------------------------------------------------------
 *                    Subparser utility functions
 * -------------------------------------------------------------------------
 *
 * These functions are for subparsers to use for parsing input.
 */

/**
 * Match a boolean type: (either "OFF" or "ON", case insensitive).
 *
 * \param val           Pointer to a bool type to store the result in
 *                      (only updated if return value is TRUE).
 * \param packet        Pointer to the packet buffer reference.  If the
 *                      return value is TRUE, the packet buffer reference
 *                      will be updated to point to the data immediately
 *                      after the value.
 * \param packet_length Pointer to an integer for the packet length.  If
 *                      the return value is TRUE, the length value
 *                      will be updated to remove the length of the value.
 *
 * \return TRUE if a valid value was matched, else FALSE.
 */
bool at_parser_match_boolean(bool *val,
                             const uint8 **packet, uint16 *packet_length);


/* 
checks if the packet has word "ALL" 
returns TRUE if present else False
*/
bool at_parser_match_boolean_ALL(bool *val,
                             const uint8 **packet, uint16 *packet_length);



/**
 * Match a IOTYPE type: (either "keboard_only" or "display_only", "Display_YN", case insensitive).
 *
 * \param val           Pointer to a bool type to store the result in
 *                      (only updated if return value is TRUE).
 * \param packet        Pointer to the packet buffer reference.  If the
 *                      return value is TRUE, the packet buffer reference
 *                      will be updated to point to the data immediately
 *                      after the value.
 * \param packet_length Pointer to an integer for the packet length.  If
 *                      the return value is TRUE, the length value
 *                      will be updated to remove the length of the value.
 *
 * \return TRUE if a valid value was matched, else FALSE.
 */
bool at_parser_match_iotype(cl_sm_io_capability *val,
                             const uint8 **packet, uint16 *packet_length);


bool at_parser_match_GapRole_type(bool *val,
                             const uint8 **packet, uint16 *packet_length) ;      

/**
 * Match a ACCEPT/REJECT type: (either "ACCEPT" or "REJECT", case insensitive).
 *
 * \param val           Pointer to a bool type to store the result in
 *                      (only updated if return value is TRUE).
 * \param packet        Pointer to the packet buffer reference.  If the
 *                      return value is TRUE, the packet buffer reference
 *                      will be updated to point to the data immediately
 *                      after the value.
 * \param packet_length Pointer to an integer for the packet length.  If
 *                      the return value is TRUE, the length value
 *                      will be updated to remove the length of the value.
 *
 * \return TRUE if a valid value was matched, else FALSE.
 */
bool at_parser_match_accept_reject(bool *val, const uint8 **packet, uint16 *packet_length);


bool at_parser_match_UartConfigType_type(uint16 *val, const uint8 **packet, uint16 *packet_length);
        
bool at_parser_match_UpgardeIntf_type(uint16 *val, const uint8 **packet, uint16 *packet_length);




/**
 * Match a decimal unsigned integer type.
 *
 * \param val           Pointer to an uint32 to store the result in
 *                      (the value will be undefined if this function returns
 *                      FALSE).
 * \param length        The length of the integer in the packet. A value
 *                      of zero indicates that the length is unknown and
 *                      the integer will be parsed until the next non-numeral
 *                      character is reached.
 * \param packet        Pointer to the packet buffer reference.  If the
 *                      return value is TRUE, the packet buffer reference
 *                      will be updated to point to the data immediately
 *                      after the value.
 * \param packet_length Pointer to an integer for the packet length.  If
 *                      the return value is TRUE, the length value
 *                      will be updated to remove the length of the value.
 *
 * \return TRUE if a valid value was matched, else FALSE.
 */
bool at_parser_match_dec_uint(uint32 *val, uint16 length,
                              const uint8 **packet, uint16 *packet_length);


/**
 * Match a BD_ADDR.
 *
 * \param addr          Pointer to a bdaddr to store the result in
 *                      (the value will be undefined if this function returns
 *                      FALSE).
 * \param packet        Pointer to the packet buffer reference.  If the
 *                      return value is TRUE, the packet buffer reference
 *                      will be updated to point to the data immediately
 *                      after the value.
 * \param packet_length Pointer to an integer for the packet length.  If
 *                      the return value is TRUE, the length value
 *                      will be updated to remove the length of the value.
 *
 * \return TRUE if a valid value was matched, else FALSE.
 */
bool at_parser_match_bdaddr(bdaddr *addr,
                            const uint8 **packet, uint16 *packet_length);
#endif
