

#include "../CSR_LIB.h"
#include "../common.h"
#include "../config/config.h"
#include "../events.h"
#include "../utils/utils.h"

#include "../debug.h"

#include "AT_Parse.h"
#include "at_Parser_Internal.h"

#include "../uart/uart.h"

#include "connection_no_ble.h"

#include "../BLE/BLE_Cen_core.h"
#include "../BLE_Periph/BLE_Peri.h"


extern const uint8     iotypedsponly[10] ;
extern const uint8     iotypedspyn[8] ;
extern const uint8     iotypekbonly[8] ;
extern const uint8     iotypenoinout[9] ;
extern const char      *upgrade_password ;


extern  uint16   upgade_debug;


void at_subparser_STATE(const uint8 *at_packet, uint16 at_packet_length);


void at_subparser_VER(const uint8 *at_packet, uint16 at_packet_length);

void at_subparser_DEL(const uint8 *at_packet, uint16 at_packet_length);



void at_subparser_UPGRADEINT(const uint8 *at_packet, uint16 at_packet_length);


void at_subparser_ADDR(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_NAME(const uint8 *at_packet, uint16 at_packet_length);

void at_subparser_DCOV(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_PAIR(const uint8 *at_packet, uint16 at_packet_length);


void at_subparser_PIN(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_DPIN(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_MITM(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_IOTYPE(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_PASSKEY(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_PASSCFM(const uint8 *at_packet, uint16 at_packet_length);



void at_subparser_RESET(const uint8 *at_packet, uint16 at_packet_length);


void at_subparser_UPGRADE(const uint8 *at_packet, uint16 at_packet_length);


void at_subparser_STOPPAIR(const uint8 *at_packet, uint16 at_packet_length);

void at_subparser_PAIRLIST(const uint8 *at_packet, uint16 at_packet_length);

void at_subparser_SETTINGS(const uint8 *at_packet, uint16 at_packet_length);


void at_subparser_OTADEBUG(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_STACKVER(const uint8 *at_packet, uint16 at_packet_length);        

void at_subparser_UPDATENAME (const uint8 *at_packet, uint16 at_packet_length);






/******************************************************************************/
/* 1= UART, 2=OTA */
void at_subparser_UPGRADEINT(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_UpgradeIntf_st *msg = NULL;
            
    LOG_INFO(("at_subparser_UPGRADEINT\n"));	
        msg = (event_at_UpgradeIntf_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
            goto err;

    if(*at_packet != '=' )
        goto err;

    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

        msg->type = OP_SET;
    
        LOG_INFO(("packet length =%u\n", at_packet_length ));

        if (!at_parser_match_UpgardeIntf_type((uint16*)&msg->interface, &at_packet,
                                      &at_packet_length))
        {
            LOG_INFO(("IOTYPE packet: parser error  match %u\n", 2));
            goto err;
        }
    
        LOG_INFO(("msg->interface %d (1=UART,2=OTA)", msg->interface));
        
        
        if (at_packet_length != 0)
        {
            LOG_INFO(("IOTYPE packet: parser error at_packet_length %u\n", 5));
            goto err;
        }

send:
    /* Success ... pass message to application Task */
    send_message(EventAtUpgradeInterface, (void *)msg);
    return;

err:
    free(msg);
    MOCK(at_parser_send_err_response());       
        
}

/******************************************************************************/
/******************************************************************************/

void at_subparser_STACKVER(const uint8 *at_packet, uint16 at_packet_length)
{

    LOG_INFO(("\n at_subparser_STACKVER "));	
   
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
            MOCK(at_parser_send_ok_response());
            ConnectionReadBtVersion (app_data.task);
            ConnectionReadLocalVersion (app_data.task);
            return;
    }

err:
    MOCK(at_parser_send_err_response());
}

/*******************************************************************************/


static void Print_Device_Firmware_Version ( uint8 caller_id )
{
    /*
    if caller_id=1 i.e. called from Settings commnad (AT_EVENT_TYPE_INFO) 
        and 
    if caller_id=2 i.e. called from AT*VER=? commnad (AT_EVENT_TYPE_REP) 
    */
    
    const char *AT_RESPONSE_MARKUP =NULL ;
    const char *report = "REP*:";
    const char *info =  " ";
    
    if(caller_id ==1)
      AT_RESPONSE_MARKUP = info;
    else if(caller_id ==2 )
      AT_RESPONSE_MARKUP =  report;
    
        

       
        if(app_config.ver.major < 10) 
            MOCK(at_parser_send_event_message(AT_RESPONSE_MARKUP, "VER", "su.2", 
                                          "068LM_BLE_CEN_0", strlen("068LM_BLE_CEN_0"), 
                                          (uint32)app_config.ver.major, (uint32)app_config.ver.minor)); 
        else    
            MOCK(at_parser_send_event_message(AT_RESPONSE_MARKUP, "VER", "s_u.2", 
                                          "068LM_BLE_CEN", strlen("068LM_BLE_CEN"), 
                                          (uint32)app_config.ver.major, (uint32)app_config.ver.minor)); 


        
}        



void at_subparser_SETTINGS(const uint8 *at_packet, uint16 at_packet_length)
{
    uint16 value = 0;
   
    app_devname_t       updated_name;
    bdaddr              addr;
    char *string_ptr[4] = {"write_static", "generate_static", "non_resolvable", "resolvable" };  
    char *adr_typr[2] = {"Public", "Random"};
    
    
	LOG_INFO((" at_subparser_SETTINGS \n"));	
    
    /* The only thing in the packet should be '=?' */
    if (at_packet_length != 2 || memcmp(at_packet, "=?", 2))
    {
        MOCK(at_parser_send_err_response());
        return;
    }

    MOCK(at_parser_send_ok_response()); 

    if( app_config.AddressBytesInName == TRUE )
        get_updated_name(&updated_name);
    else
        get_device_name(&updated_name);
  
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "NAME", "s", updated_name.dev_name, updated_name.name_len));
    
    
    BdaddrSetZero (&addr); 
    read_BDaddr( LCN_LOCAL_BD_ADDRESS,  &addr);
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "ADDR", "B", &addr, sizeof(addr) ));
        
	/**/
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "PAIR", "b", app_config.pair));	
	MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "DCOV", "b", app_config.dcov));	
										
        value = get_UART_BAUD_value();
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "BAUD", "s",  baud_array[value] , strlen(baud_array[value])  ));
        value = get_UART_StopBit_value();
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "STOP", "s",  StopBit_array[value] , strlen(StopBit_array[value])  ));								
        value = get_UART_PARITY_value();
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "PARITY", "s",  ParityBit_array[value] , strlen(ParityBit_array[value])  ));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "FLOW", "b", app_config.flow ));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "ECHO", "b", app_config.echo));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "RESP", "b", app_config.resp));

        
		MOCK(at_parser_send_event_message( AT_EVENT_TYPE_INFO, "DPIN", "b", app_config.dpin ));		

                if( app_config.dpin == TRUE )		
                {
                    MOCK(at_parser_send_event_message( AT_EVENT_TYPE_INFO, "MITM", "b", app_config.mitm ));	
               
                    if( app_config.iotype == cl_sm_io_cap_display_only  )
                        MOCK(at_parser_send_event_message( AT_EVENT_TYPE_INFO, "IOTYPE", "s", iotypedsponly , sizeof(iotypedsponly) ));  
                    else 
                    if( app_config.iotype == cl_sm_io_cap_display_yes_no   )
                        MOCK(at_parser_send_event_message( AT_EVENT_TYPE_INFO, "IOTYPE", "s", iotypedspyn , sizeof(iotypedspyn) ));  
                    else 
                    if( app_config.iotype == cl_sm_io_cap_no_input_no_output     )
                        MOCK(at_parser_send_event_message( AT_EVENT_TYPE_INFO, "IOTYPE", "s", iotypenoinout , sizeof(iotypenoinout) ));  
                    else 
                    if( app_config.iotype == cl_sm_io_cap_keyboard_only    )
                        MOCK(at_parser_send_event_message( AT_EVENT_TYPE_INFO, "IOTYPE", "s", iotypekbonly , sizeof(iotypekbonly) ));
                }
                else
                {
                    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "PIN", "s", app_config.PIN.PIN, app_config.PIN_len));	
                }
        
        
                if( stBLEInfoData->BLE_Role == GAP_CENTRAL_ROLE )
                {
                            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "GAPROLE", "s",  "GAP_CENTRAL", strlen("GAP_CENTRAL")   ));
                            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "LE_AUTOCONN_PERIPH", "b", stBLEInfoData->BLEautoConnect));
                            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "LE_AUTOSCAN_SERVER", "b", stBLEInfoData->BLEautoScan));
                }
                else if( stBLEInfoData->BLE_Role == GAP_PERIPHERAL_ROLE )
                {
                            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "GAPROLE", "s",  "GAP_PERIPHERAL", strlen("GAP_PERIPHERAL")   )); 
                            
                            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "Peri_ADV_ADR", "s,s,B",
                                              string_ptr[stBLEInfoData->Peri_addr_type],
                                              strlen(string_ptr[stBLEInfoData->Peri_addr_type]),
                                              adr_typr[stBLEInfoData->Peri_addr_type],
                                              strlen(adr_typr[stBLEInfoData->Peri_addr_type]),
                                              &addr ));
                }
                
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "LE_SEC", "b", stBLEInfoData->WhiteListEnable));
                
                BdaddrSetZero (&addr); 
                read_BDaddr( LCN_LE_BOND_BDADDR,  &addr);
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "LEBOND", "B", &addr, sizeof(addr) ));
                
                                
    Print_Device_Firmware_Version(1);
            
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "SETTINGS", "s", "END", lstrlen("END")));
    
}

void at_subparser_VER(const uint8 *at_packet, uint16 at_packet_length)
{
/*
    bool Analog_read=FALSE;
    static uint8 count=0;
 */   
	LOG_INFO(("at_subparser_VER\n"));	

    /* The only thing in the packet should be '=?' */
    if (at_packet_length != 2 || memcmp(at_packet, "=?", 2))
    {
        MOCK(at_parser_send_err_response());
        return;
    }

    MOCK(at_parser_send_ok_response());
	    Print_Device_Firmware_Version(2);
	
	
/*
if(count==0)   
    {
        Analog_read = AdcRequest ( app_data.task, (vm_adc_source_type) adcsel_aio0 );
        count++;
    }
else if(count==1)
    {
       Analog_read = AdcRequest ( app_data.task, (vm_adc_source_type) adcsel_aio1 );
       count++;
    }
else if(count==2)   
    {
        Analog_read = AdcRequest ( app_data.task, (vm_adc_source_type) adcsel_aio2 );
        count++;
    }
else if(count==3)
    {
       Analog_read = AdcRequest ( app_data.task, (vm_adc_source_type) adcsel_aio3 );
       count++;
    }
else if(count==4)
    {
       Analog_read = AdcRequest ( app_data.task, (vm_adc_source_type) adcsel_vref );
       count++;
    }
else if(count==5)   
    {
        Analog_read = AdcRequest ( app_data.task, (vm_adc_source_type) adcsel_vdd_bat );
        count++;
    }
else if(count==6)
    {
       Analog_read = AdcRequest ( app_data.task, (vm_adc_source_type) adcsel_byp_vregin );
       count++;
    }
else if(count==7)
    {
       Analog_read = AdcRequest ( app_data.task, (vm_adc_source_type) adcsel_vdd_sense );
       count++;
    }
else if(count==8)
{
    Analog_read = AdcRequest ( app_data.task, (vm_adc_source_type) adcsel_vregen );
    count=0;
}
    LOG_INFO(("\n AioGet = %s ", (Analog_read)?("ACCEPT"):("REJECT") ));
*/
  
    
/*
  adcsel_vref = 4, 
  adcsel_vdd_bat, 
  adcsel_byp_vregin, 
  adcsel_vdd_sense, 
  adcsel_vregen 

*/    
    
    
/*    
    if(app_config.ver.major < 10) 
    {
        LOG_INFO(("\n  app_config.ver.major < 10"));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "VER", "su.2", 
                                      "961LM_SPP+GAPCEN_0", strlen("961LM_SPP+GAPCEN_0"), 
                                      (uint32)app_config.ver.major, (uint32)app_config.ver.minor)); 
    }
    else
    {
        LOG_INFO(("\n  app_config.ver.major >= 10"));
        
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "VER", "s_u.2", 
                                      "961LM_SPP+GAPCEN", strlen("961LM_SPP+GAPCEN"), 
                                      (uint32)app_config.ver.major, (uint32)app_config.ver.minor));         
    }
	*/
}



void at_subparser_ADDR(const uint8 *at_packet, uint16 at_packet_length)
{
    bdaddr              addr;
    BdaddrSetZero (&addr); 
     
    LOG_INFO(("at_subparser_ADDR\n   PRINT_BDADDR  ==  "));
    
    /* The only thing in the packet should be '=?' */
    if (at_packet_length != 2 || memcmp(at_packet, "=?", 2))
    {
        MOCK(at_parser_send_err_response());
        return;
    }



    /*ConnectionReadLocalAddr(app_data.task);      */
    read_BDaddr( LCN_LOCAL_BD_ADDRESS,  &addr);
    PRINT_BDADDR(( addr ));
    
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "ADDR", "B", &addr, sizeof(addr) ));
    
    MOCK(at_parser_send_ok_response()); 
}



void at_subparser_NAME(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_name_st *msg = NULL;

    LOG_INFO(("at_subparser_NAME \n"));
    
    msg = (event_at_name_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

        msg->type = OP_SET;
    
        /* Ensure that we won't overflow the pin buffer */
        if (at_packet_length > NUM_ELEMENTS(msg->name))
        {
            LM_DEBUG(("NAME packet: Name would overflow the event buffer\n"));
            goto err;
        }
    
        /* Copy the name */
        memcpy(msg->name, at_packet, at_packet_length);
        msg->name_length = at_packet_length;

send:
    /* Success ... pass message to application Task */
    send_message(EventAtName, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());     
}




void at_subparser_STATE(const uint8 *at_packet, uint16 at_packet_length)
{
    LOG_INFO(("at_subparser_STATE\n"));	
        
    /* The only thing in the packet should be '=?' */
    if (at_packet_length != 2 || memcmp(at_packet, "=?", 2))
    {
        MOCK(at_parser_send_err_response());
        return;
    }
    /* Success ... pass message to application Task */
    send_message(EventAtGetState, NULL);        
        
}


/******************************************************************************
 ******************************************************************************/

void at_subparser_PIN(const uint8 *at_packet, uint16 at_packet_length)
{ 
    event_at_pin_st *msg = NULL;
    
    LOG_INFO(("\n at_subparser_PIN "));
    
    msg = (event_at_pin_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

    /* Ensure that we won't overflow the pin buffer */
    if (at_packet_length > NUM_ELEMENTS(msg->pin))
    {
        LOG_INFO(("NAME packet: Name would overflow the event buffer\n"));
        goto err;
    }

    /* Copy the name */
    memcpy(msg->pin, at_packet, at_packet_length);
    msg->pin_length = at_packet_length;

send:
    /* Success ... pass message to application Task */
    send_message(EventAtPin, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
    
}


void at_subparser_DPIN(const uint8 *at_packet, uint16 at_packet_length)
{        
    event_at_dpin_st *msg = NULL;
    
    LOG_INFO(("at_subparser_DPIN\n"));	

    msg = (event_at_dpin_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

    /* Expect either a 1 or a 2 */
    if (!at_parser_match_boolean(&msg->dpin, &at_packet,
                                  &at_packet_length))
    {
        LOG_INFO(("DPIN packet: parser error %u\n", 2));
        goto err;
    }

    if (at_packet_length != 0)
    {
        LOG_INFO(("DPIN packet: parser error %u\n", 5));
        goto err;
    }

send:
    /* Success ... pass message to application Task */
    send_message(EventAtdpin, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());        
        
}


void at_subparser_MITM(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_mitm_st *msg = NULL;
    
    LOG_INFO(("at_subparser_MITM \n"));	

    msg = ( event_at_mitm_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

    /* Expect either a 1 or a 2 */
    if (!at_parser_match_boolean(&msg->mitm, &at_packet,
                                  &at_packet_length))
    {
        LOG_INFO(("MITM packet: parser error %u\n", 2));
        goto err;
    }

    if (at_packet_length != 0)
    {
        LOG_INFO(("MITM packet: parser error %u\n", 5));
        goto err;
    }

send:
    /* Success ... pass message to application Task */
    send_message(EventAtMitm, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());        
        
}

void at_subparser_IOTYPE(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_iotype_st *msg = NULL;
    
    LOG_INFO(("at_subparser_IOTYPE \n"));	

    msg = ( event_at_iotype_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

    /* Expect either a 1 or a 2 */
    if (!at_parser_match_iotype(&msg->iotype, &at_packet,
                                  &at_packet_length))
    {
        LOG_INFO(("IOTYPE packet: parser error  match %u\n", 2));
        goto err;
    }

    if (at_packet_length != 0)
    {
        LOG_INFO(("IOTYPE packet: parser error at_packet_length %u\n", 5));
        goto err;
    }

send:
    /* Success ... pass message to application Task */
    send_message(EventAtIotype, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());        
        
   
}


/*
  If IOtype of Local device is Display_YN, then stack gives the passkey.
  User has to provide confirmation, Yes/No if the passkey matches to remote device
*/
void at_subparser_PASSCFM(const uint8 *at_packet, uint16 at_packet_length)
{    
    event_Passk_CFM_yn_st *passcfm_msg = (event_Passk_CFM_yn_st *)malloc(sizeof(*passcfm_msg));
    
    LOG_INFO(("at_subparser_PASSCFM \n  "));   
    
    if ( passcfm_msg == NULL )
    goto err;

    
    if (at_packet[0] == '=')
    {
        /* Move past '=' */
        at_packet += 1;
        at_packet_length -= 1;
    
        if ( !at_parser_match_bdaddr(&(passcfm_msg->peer_bdaddr.addr), &at_packet, &at_packet_length) )
        {
            LM_DEBUG(("  PASSCFM addr-error \n  "));
            free(passcfm_msg);
            goto err;
        }
    }

    if( at_packet_length != 0 )
    {
        if (at_packet_length < 1 || at_packet[0] != ',')
        {
             LM_DEBUG(("  PASSCFM Syntax Comma error \n  "));                
            free(passcfm_msg);
            goto err;
        }

        at_packet++;
        at_packet_length--;

        if (at_packet_length == lstrlen("YES"))
        {
            if (match_word(at_packet, at_packet_length,
                           "YES", lstrlen("YES")))
            {
                passcfm_msg->cfm_yn = TRUE ;
            }
            else
            {
                free(passcfm_msg);
                goto err;
            }
        }
        else if(at_packet_length == lstrlen("NO"))
        {
            if(match_word(at_packet, at_packet_length,
                                "NO", lstrlen("NO")))
            {
                passcfm_msg->cfm_yn = FALSE ;
            }
            else
            {
                free(passcfm_msg);
                goto err;
            }
        }
        else
        {
            LOG_INFO(("PAIR packet: parser error %u (len=%u)\n", 4, at_packet_length));
            free(passcfm_msg);
            goto err;
        }

    }
    
    LOG_INFO(("Sending EventAtPassCFM with cmd_type %u\n", passcfm_msg->cfm_yn ));
    /* Success ... pass message to application Task */
    send_message(EventAtPassCFM, (void *)passcfm_msg);
    return;

err:
    MOCK(at_parser_send_err_response());
    
}
        


void at_subparser_PASSKEY(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_passkey_st *msg = NULL;

    LOG_INFO(("at_subparser_PASSKEY \n  "));    
    
    if (at_packet_length < 2 || at_packet[0] != '=')
    {
        LOG_INFO(("PASSK packet: parser error %u\n", 1));
        goto err;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg = (event_at_passkey_st *)malloc(sizeof(*msg));

    if (!at_parser_match_dec_uint(&(msg->passkey), at_packet_length,
                                  &at_packet, &at_packet_length))
    {
        LOG_INFO(("PASSK packet: parser error %u\n", 2));
        goto err;
    }

    /* Success ... pass message to application Task */
    send_message(EventAtPasskey, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());      
}


static bool at_subparser_PAIR_get_pairable(void)
{
    event_at_pairable_st *pairable_msg =
            (event_at_pairable_st *)malloc(sizeof(*pairable_msg));

    LOG_INFO(("\n at_subparser_PAIR_get_pairable "));
        
    if (pairable_msg == NULL)
        return FALSE;

    pairable_msg->type = OP_GET;

    /* Success ... message to application Task */
    send_message(EventAtPairable, (void *)pairable_msg);

    return TRUE;
}

static bool at_subparser_PAIR_get_paired(const uint8 *at_packet,
                                         uint16 at_packet_length)
{

    event_at_paired_st *paired_msg =
            (event_at_paired_st *)malloc(sizeof(*paired_msg));

    LOG_INFO(("\n at_subparser_PAIR_get_paired "));
        
    if (paired_msg == NULL)
        return FALSE;

    if (!at_parser_match_bdaddr(&(paired_msg->peer_bdaddr),
            &at_packet, &at_packet_length))
    {
        LOG_INFO(("PAIR packet: parser error %u\n", 1));
        free(paired_msg);
        return FALSE;
    }

    if (at_packet_length != 0)
    {
        LOG_INFO(("PAIR packet: parser error %u\n", 2));
        free(paired_msg);
        return FALSE;
    }

    /* Success ... pass message to application Task */
    send_message(EventAtPaired, (void *)paired_msg);
    return TRUE;
}


static bool at_subparser_PAIR_set_pairable(const uint8 *at_packet,
                                           uint16 at_packet_length)
{
    event_at_pairable_st *pairable_msg =
            (event_at_pairable_st *)malloc(sizeof(*pairable_msg));

    LOG_INFO(("\n at_subparser_PAIR_set_pairable "));
    
    if (pairable_msg == NULL)
        return FALSE;

    pairable_msg->type = OP_SET;

    /* Expect either a 1 or a 2 */
    if (!at_parser_match_boolean(&pairable_msg->pairable, &at_packet,
            &at_packet_length))
    {
        LOG_INFO(("PAIR packet: parser error %u\n", 3));
        free(pairable_msg);
        return FALSE;
    }

    if (at_packet_length != 0)
    {
        LOG_INFO(("PAIR packet: parser error %u\n", 4));
        free(pairable_msg);
        return FALSE;
    }

    /* Success ... pass message to application Task */
    send_message(EventAtPairable, (void *)pairable_msg);
    return TRUE;
}


static bool at_subparser_PAIR_pair(const uint8 *at_packet,
                                   uint16 at_packet_length)
{
    event_at_pair_st *pair_msg = (event_at_pair_st *)malloc(sizeof(*pair_msg));
    
    LOG_INFO(("\n at_subparser_PAIR_pair "));
    
    if (pair_msg == NULL)
        return FALSE;

    if (!at_parser_match_bdaddr(&(pair_msg->peer_bdaddr),
                                &at_packet, &at_packet_length))
    {
        free(pair_msg);
        return FALSE;
    }

    /* If we have reached the end of the packet then this was a command
     * to initiate pairing (since there was no ,REJECT or ,ACCEPT at the
     * end. */
    if (at_packet_length == 0)
    {
        pair_msg->cmd_type = PAIR_INITIATE;
    }
    else
    {
        /* Next we expect a comma */
        if (at_packet_length < 1 || at_packet[0] != ',')
        {
            free(pair_msg);
            return FALSE;
        }

        at_packet++;
        at_packet_length--;

        /* We expect either ACCEPT or REJECT (and rely on the fact that
         * they are the same length). */
        if (at_packet_length == lstrlen("ACCEPT"))
        {
            if (match_word(at_packet, at_packet_length,
                           "ACCEPT", lstrlen("ACCEPT")))
            {
                pair_msg->cmd_type = PAIR_ACCEPT;
            }
            else if (match_word(at_packet, at_packet_length,
                                "REJECT", lstrlen("REJECT")))
            {
                pair_msg->cmd_type = PAIR_REJECT;
            }
            else
            {
                free(pair_msg);
                return FALSE;
            }
        }
        else
        {
            LOG_INFO(("PAIR packet: parser error %u (len=%u)\n", 4, at_packet_length));
            free(pair_msg);
            return FALSE;
        }
    }

    LOG_INFO(("Sending EventAtPair with cmd_type %u\n", pair_msg->cmd_type));
    /* Success ... pass message to application Task */
    send_message(EventAtPair, (void *)pair_msg);
    return TRUE;
}



/*
AT*PAIR=ON/OFF   				- modify pairable setting
AT*PAIR=?						- query pairable setting
AT*PAIR=00126F356fc8,Accept		- Accept pairing request from remote device
AT*PAIR=00126f357125			- Request local device to start pairing prcocess with remote device
*/
void at_subparser_PAIR(const uint8 *at_packet, uint16 at_packet_length)
{
    bool ok = FALSE;
        
    LOG_INFO(("at_subparser_PAIR\n"));	

    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
    {
        MOCK(at_parser_send_err_response());
        return;
    }

    /* Check the packet starts with '=?' */
    if (memcmp(at_packet, "=?", 2) == 0)
    {
        /* Move past '=?' */
        at_packet += 2;
        at_packet_length -= 2;

        /* The packet might or might not contain a BD_ADDR after =?.
         * When there is a BD_ADDR after the =? then this is logically
         * a different request, so we use the EventAtPaired event. */
        if (at_packet_length == 0)
        {
            ok = at_subparser_PAIR_get_pairable();
        }
        else
        {
            ok = at_subparser_PAIR_get_paired(at_packet, at_packet_length);
        }
    }
    else if (at_packet[0] == '=')
    {
        /* Move past '=' */
        at_packet += 1;
        at_packet_length -= 1;

        /* If the packet is less than the length of a BD_ADDR then
         * we must be enabling/disabling pairing (or it is a malformed
         * packet). */
        if (at_packet_length <= 3)
        {
            ok = at_subparser_PAIR_set_pairable(at_packet, at_packet_length);
        }
        else
        {
            ok = at_subparser_PAIR_pair(at_packet, at_packet_length);
        }
    }

    if (!ok)
        MOCK(at_parser_send_err_response());

}


/******************************************************************************
 ******************************************************************************/
void at_subparser_DEL(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_del_st *msg = (event_at_del_st *)malloc(sizeof(*msg));
        
    LOG_INFO(("\n at_subparser_DEL"));	
    
    if (msg == NULL)
    {
        LOG_INFO(("DEL packet: allocation failure\n"));
        goto err;
    }

    if (at_packet_length != 13 /* Length of "={bdaddr}" */)
    {
        LOG_INFO(("DEL packet: parser error %u\n", 1));
        goto err;
    }

    if (at_packet[0] != '=')
    {
        LOG_INFO(("DEL packet: parser error %u\n", 2));
        goto err;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;


    if (!at_parser_match_bdaddr(&(msg->addr),
                                &at_packet, &at_packet_length))
    {
        LOG_INFO(("DEL packet: parser error %u\n", 3));
        goto err;
    }

    /* Success ... pass message to application Task */
    send_message(EventAtDel, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
}


void at_subparser_PAIRLIST(const uint8 *at_packet, uint16 at_packet_length)
{
    LOG_INFO(("at_subparser_PAIRLIST\n"));	
    
    /* if(*at_packet != '=' )
        goto err; */                /* PAIRLIST is always query command */
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        send_message(EventAtPairList, NULL);
    }
    else
    {
        MOCK(at_parser_send_err_response());
    }        
        
}



void at_subparser_STOPPAIR(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_stoppair_st *msg = (event_at_stoppair_st *)malloc(sizeof(*msg));
    
    LOG_INFO(("at_subparser_STOPPAIR"));
    
    if (msg == NULL)
    {
        LOG_INFO(("STOPPAIR packet: allocation failure\n"));
        goto err;
    }

    if (at_packet_length != 13)
    {
        LOG_INFO(("STOPPAIR packet: parser error %u\n", 1));
        goto err;
    }

    if (at_packet[0] != '=')
    {
        LOG_INFO(("STOPPAIR packet: parser error %u\n", 2));
        goto err;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    if (!at_parser_match_bdaddr(&msg->peer_bdaddr,
                                &at_packet, &at_packet_length))
    {
        LOG_INFO(("STOPPAIR packet: parser error %u\n", 3));
        goto err;
    }

    /* Success ... pass message to application Task */
    send_message(EventAtStopPair, (void *)msg);
    return;


err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
}




/******************************************************************************
 ******************************************************************************/




void at_subparser_DCOV(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_discovery_st *msg = NULL;

    LOG_INFO(("at_subparser_DCOV\n"));	
   
    msg = (event_at_discovery_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

    /* Expect either a 1 or a 2 */
    if (!at_parser_match_boolean(&msg->discoverable, &at_packet,
                                  &at_packet_length))
    {
        LOG_INFO(("DCOV packet: parser error %u\n", 2));
        goto err;
    }

    if (at_packet_length != 0)
    {
        LOG_INFO(("DCOV packet: parser error %u\n", 5));
        goto err;
    }

send:
    /* Success ... pass message to application Task */
    send_message(EventAtDiscovery, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
    
}


/*******************************************************************************
	AT*RESET = 1	soft reset, device restarts without power on/off  	
	AT*RESET = 2    hard reset, deleting all paired device and loading default settings
  ******************************************************************************/
void at_subparser_RESET(const uint8 *at_packet, uint16 at_packet_length)
{
	uint8 value= 0; 

    event_at_reset_st *msg = NULL;

	LOG_INFO(("at_subparser_RESET\n"));	
   
    msg = (event_at_reset_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
		goto err;

	/* Check if the only thing in the packet is '=1' or '=2'*/
    if (at_packet_length == 2 )
    {
		if( *at_packet == '=' )
		{
			/* Move past '=' */
			at_packet += 1;
			at_packet_length -= 1;
		
			LOG_INFO(("reset packet LENGTH =  %d\n", at_packet_length));
			
			value = (*at_packet) - 0x30 ;
				
				if( ( value == 1)  ||  ( value == 2) )
				{    
					msg->reset_level = value;
                    goto send;
                }
				else
					goto err;
		}
		else 
			goto err; 
	}
    else
        goto err;    
	
send:
	MOCK(at_parser_send_ok_response());  /* send OK response here*/
    send_message(EventAtReset, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
}

   
/*******************************************************************************
  ******************************************************************************/

void at_subparser_UPGRADE(const uint8 *at_packet, uint16 at_packet_length)
{
    event_firmware_upgrade_st *msg = NULL;

    LOG_INFO(("at_subparser_UPGRADE\n"));
   
    msg = (event_firmware_upgrade_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
		goto err;    
    
    /* Check that the packet contains = and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    /*AT*UPGRADE=LMupdatev123*/
    if (at_packet_length != 0 )/*AT*UPGRADE*/ /*AT*UPGRADE=*/
    {  
        if( *at_packet == '=' )/*AT*UPGRADE=*/
        {
                /* Move past '=' */
                at_packet += 1;
                at_packet_length -= 1;
                
            LM_DEBUG(("\n Packet_length = %d  ", at_packet_length ));

            if( strlen(upgrade_password) == at_packet_length )
            {
                if ( memcmp(upgrade_password, at_packet, at_packet_length ) == 0x00 )  
                {
                        LM_DEBUG(("\n Correct password enterd "));
                        MOCK(at_parser_send_ok_response()); 
                        msg->upgrade = TRUE;
                        goto send;
                }
                else
                {
                    LM_DEBUG(("\n Wrong password "));
                         goto err; 
                }
            }
            else
            {
               LM_DEBUG(("\n Insufficient/incorrect passwod length ")); 
                    goto err; 
            }
        }/* if packet starts with = */
        goto err;
    }/*if packet length */
     goto err;   
  
send:
    /* Success ... pass message to application Task */
    send_message(EventAtUpgrade, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    
    MOCK(at_parser_send_err_response());
}




void at_subparser_OTADEBUG(const uint8 *at_packet, uint16 at_packet_length)
{
    uint16  otadebug_local =0xff;

    LOG_INFO(("\n at_subparser_OTADEBUG "));	
   
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
            MOCK(at_parser_send_ok_response()); 
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "OTADEBUG", "b", upgade_debug ));
            return;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;
  
        if( at_packet_length == 2  || at_packet_length == 3 )
        {
            /* Expect either a 1 or a 2 */
            if (!at_parser_match_boolean(&otadebug_local, &at_packet,
                                      &at_packet_length))
            {
                LOG_INFO(("otadebug packet: parser error %u\n", 2));
                goto err;
            }
            
            if( at_packet_length != 0)
                goto err;    
            else if( otadebug_local != upgade_debug )
                goto send;
            else
                goto err;
            
        }
        

    
err:
    MOCK(at_parser_send_err_response());
    return;

        
send:
       /* if( app_config.FW_upgrade_interface == 2 ) */

            upgade_debug = otadebug_local ;    
            MOCK(at_parser_send_ok_response()); 

}







void at_subparser_UPDATENAME (const uint8 *at_packet, uint16 at_packet_length)
{        
    event_at_updatename_st *msg = NULL;
    
    LOG_INFO(("at_subparser_UPDATENAME\n "));	

    msg = (event_at_updatename_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

    /* Expect either a 1 or a 2 */
    if (!at_parser_match_boolean(&msg->updatename, &at_packet,
                                  &at_packet_length))
    {
        LOG_INFO(("updatename packet: parser error %u\n", 2));
        goto err;
    }

    if (at_packet_length != 0)
    {
        LOG_INFO(("updatename packet: parser error %u\n", 5));
        goto err;
    }

send:
    /* Success ... pass message to application Task */
    send_message(EventAtUpdateName, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());        
        
}







