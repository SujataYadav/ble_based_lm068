
#include "../CSR_LIB.h"
#include "../common.h"
#include "../config/config.h"
#include "../events.h"
#include "../utils/utils.h"

#include "../debug.h"

#include "../uart/uart.h"

#include "AT_Parse.h"
#include "at_Parser_Internal.h"





void at_subparser_STOP(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_PAR(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_BAUD(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_ECHO(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_RESP(const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_FLOW(const uint8 *at_packet, uint16 at_packet_length);



void at_subparser_UARTCONF(const uint8 *at_packet, uint16 at_packet_length);

/************************************************************************
 ************************************************************************/


void at_subparser_FLOW(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_flow_st *msg = NULL;

    LOG_INFO(("at_subparser_FLOW\n"));
   
    msg = (event_at_flow_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

    /* Expect either a 1 or a 2 */
    if (!at_parser_match_boolean(&msg->flow, &at_packet,
                                  &at_packet_length))
    {
        LOG_INFO(("DCOV packet: parser error %u\n", 2));
        goto err;
    }

    if (at_packet_length != 0)
    {
        LOG_INFO(("DCOV packet: parser error %u\n", 5));
        goto err;
    }

send:
    /* Success ... pass message to application Task */
    send_message(EventAtFlow, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());

}


/************************************************************************
 ************************************************************************/

void at_subparser_STOP(const uint8 *at_packet, uint16 at_packet_length)
{
    uint8 value= 0; 
    
    event_at_stopbit_st *msg = NULL;

    LOG_INFO(("at_subparser_STOP \n"));
   
    msg = (event_at_stopbit_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

        
        if(at_packet_length == 1) /* baud-number 0 to 9 only */
        {
            value = (*at_packet) - 0x30 ;
            
            if( ( value >= VALID_STOPBIT_MIN ) && ( value <= VALID_STOPBIT_MAX ) )   
            {    
                msg->stopbit = value;
            }
            else
            {
                LOG_INFO(("StopBit packet: parser error %u\n", 2));
                goto err;
            }   
        }
        else
        goto err;     


send:
    /* Success ... pass message to application Task */
    send_message(EventAtStopbit, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
    
}



void at_subparser_PAR(const uint8 *at_packet, uint16 at_packet_length)
{
    uint8 value= 0; 
    
    event_at_paritybit_st *msg = NULL;
    
    LOG_INFO(("at_subparser_PAR\n"));
   
    msg = (event_at_paritybit_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    if (at_packet_length < 2)
            goto err;

    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

        
        if(at_packet_length == 1) /* baud-number 0 to 9 only */
        {
            value = (*at_packet) - 0x30 ;
            
            if( ( value >= VALID_PARITY_MIN ) && ( value <= VALID_PARITY_MAX ) )   
            {    
                msg->paritybit = value;
            }
            else
            {
                LOG_INFO(("SParityBit packet: parser error %u\n", 2));
                goto err;
            }   
        }
        else
        goto err;     

send:
    send_message(EventAtParitybit, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
}

/*valid command is AT*BAUD=1(9600), 
                   AT*Baud=9(1382400)
*/
void at_subparser_BAUD(const uint8 *at_packet, uint16 at_packet_length)
{
    uint8 value= 0; 

    event_at_baud_st *msg = NULL;
    
    LOG_INFO(("at_subparser_BAUD\n"));	
    
    msg = (event_at_baud_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

        
        if(at_packet_length == 1) /* baud-number 0 to 9 only */
        {
            value = (*at_packet) - 0x30 ;
            
            if( ( value >= VALID_BAUD_MIN ) && ( value <= VALID_BAUD_MAX ) )   
            {    
                msg->baud = value;
            }
            else
            {
                LOG_INFO(("SParityBit packet: parser error %u\n", 2));
                goto err;
            }   
        }
        else
        goto err;     


send:
    send_message(EventAtuartBaud, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
}


 
/************************************************************************
 ************************************************************************/

void at_subparser_ECHO(const uint8 *at_packet, uint16 at_packet_length)
{
    bool echo_enable;
    status_et status = STATUS_OK;

    LOG_INFO(("at_subparser_ECHO\n"));    
    
    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        /* Return response */
        MOCK(at_parser_send_ok_response());

        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "ECHO", "b",
                                         app_config.echo));
        return;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    if (!at_parser_match_boolean(&echo_enable, &at_packet, &at_packet_length))
    {
        LM_DEBUG(("ECHO packet: parser error %u\n", 2));
        goto err;
    }

    if (at_packet_length != 0)
    {
        LM_DEBUG(("ECHO packet: parser error %u\n", 5));
        goto err;
    }

        app_config.echo = echo_enable;

        status = Store_memory_Location ( LCN_ECHO, (const uint16*)&app_config.echo, sizeof(app_config.echo) );
        
        if (status != TRUE)
        goto err;

        MOCK(at_parser_send_ok_response());
        return;

err:
    MOCK(at_parser_send_err_response());  
}




void at_subparser_RESP(const uint8 *at_packet, uint16 at_packet_length)
{
    bool resp_enable;
    status_et status = STATUS_OK;
   /* app_config_st *app_config; */

    LOG_INFO(("at_subparser_RESP \n"));    
    
    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        /* Return response */
        MOCK(at_parser_send_ok_response());

        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "RESP", "b",
                                         app_config.resp));
        return;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    if (!at_parser_match_boolean(&resp_enable, &at_packet, &at_packet_length))
    {
        LM_DEBUG(("RESP packet: parser error %u\n", 2));
        goto err;
    }

    if (at_packet_length != 0)
    {
        LM_DEBUG(("RESP packet: parser error %u\n", 5));
        goto err;
    }

        app_config.resp = resp_enable;

        status = Store_memory_Location ( LCN_RESP, (const uint16*)&app_config.resp, sizeof(app_config.resp) );
        
        if (status != TRUE)
        goto err;

        MOCK(at_parser_send_ok_response());
        return;

err:
    MOCK(at_parser_send_err_response()); 
}



/************************************************************************
 ************************************************************************/

void at_subparser_UARTCONF(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_uartconfig_st *msg = NULL;
            
    LOG_INFO(("at_subparser_UARTCONFIG\n"));	
        msg = (event_at_uartconfig_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;

    if(*at_packet != '=' )
        goto err;

    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

        msg->type = OP_SET;
    
        LOG_INFO(("packet length =%u\n", at_packet_length ));

        if (!at_parser_match_UartConfigType_type((uint16*)&msg->uartconfig, &at_packet,
                                      &at_packet_length))
        {
            LOG_INFO(("IOTYPE packet: parser error  match %u\n", 2));
            goto err;
        }
    
        if (at_packet_length != 0)
        {
            LOG_INFO(("IOTYPE packet: parser error at_packet_length %u\n", 5));
            goto err;
        }

send:
    /* Success ... pass message to application Task */
    send_message(EventAtUartConfigType, (void *)msg);
    return;

err:
    free(msg);
    MOCK(at_parser_send_err_response());       
           
}




