#ifndef AT_PARSER_H__
#define AT_PARSER_H__


/**
 * Initialise the AT command parser, resetting its initial state.
 */
void at_parser_init(void);

/**
 * This function is invoked upon reception of data to pass it to the AT
 * parser.  This function is then responsible for processing the data and
 * returns the amount of data that it wishes to consume.  Consumed data
 * will be removed from the start of the data buffer. Any data not
 * consumed will remain in the buffer to be passed to the next invocation
 * of this function.
 *
 * \param data      Pointer to the buffer containing data to process.
 * \param length    Length of the data available for processing.
 * \return          The number of octets of data to consume from the buffer.
 */
uint16 at_parser_process_data(const uint8 *data, uint16 length);


/** \ref at_parser_send_event_message event_type for reports */
#define AT_EVENT_TYPE_REP "REP*:"

#define AT_EVENT_TYPE_INFO  " " 

/** \ref at_parser_send_event_message event_type for indications */
#define AT_EVENT_TYPE_IND "IND*:"

/**
 * Send an event message (report/indication) from module to host.
 *
 * \param event_type        The type of event message (must be
 *                          \ref AT_EVENT_TYPE_REP for a report or
 *                          \ref AT_EVENT_TYPE_IND for an indication).
 * \param code              The event code.
 * \param params            Event parameters format string. Note that this
 *                          is not a typical printf format string.  See
 *                          \ref at_vsprintf for details.
 *                          Can be NULL if not required.
 * \param ...               Variadic args for params format string.
 *
 * \return STATUS_OK on success, else an appropriate error code.
 *
 * \note This function will unmap the currently mapped Sink.
 */
status_et at_parser_send_event_message(const char *event_type,
                                      const char *code,
                                      const char *params,
                                      ...);


/**
 * This function is invoked to send an error response ("ERR") to an AT command.
 */
void at_parser_send_err_response(void);

/**
 * This function is invoked to send an error response ("StateERR") to an AT command.
 * issuing connect command in Master acon+ mode
 * issuing ACON +/- command, in slave or dual role
 */
void at_parser_send_STATEerr_response(void);
/**
 * This function is invoked to send an OK response ("OK") to an AT command.
 */
void at_parser_send_ok_response(void);


/**
 * Take a boolean and convert it to ON or OFF as appropriate for the SWD v4.0
 * specification.
 *
 * \param value     The boolean value to convert
 *
 * \return a string that is the boolean in textual form (ON or OFF)
 */
const char *at_parser_bool_to_str(bool value);




bool Is_Response_Set ( void );



#endif








