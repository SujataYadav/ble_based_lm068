
#include "../CSR_LIB.h"

#include "../utils/utils.h"
#include "../common.h"
#include "../events.h"

#include "../debug.h"
#include "AT_Parser_Internal.h"
#include "AT_Parse.h"

#include "../config/config.h"


/*
#include <stream.h>
#include <stdarg.h>
#include <vm.h>
*/



Sink at_get_sink(void)
{
    return StreamUartSink();
}


Source at_get_source(void)
{
    return StreamUartSource();
}


/**
 * Custom version of vsprintf used by the AT parser.
 *
 * Note that this function is quite different to the standard library
 * version of vsprintf. Differences include:
 *
 *  - buffer may be NULL, in which case the length of the data that would
 *    be put in the buffer is returned, without actually taking any action.
 *  - % characters are not used for format specifiers.  Rather, format
 *    specifiers stand alone (e.g., a 'b' in the format string will always
 *    be turned into a boolean value).
 *  - The list of format specifiers is different.
 *
 *  Format specifiers:
 *  - 2: a 2 digit unsigned integer (must be passed as a uint32, but will
 *       be modulo 100; will be zero padded to 2 digits if necessary)
 *  - b: boolean value (TRUE->"ON", FALSE->"OFF")
 *  - B: BD_ADDR (passed in as a bdaddr *)
 *  - c: a character
 *  - r: a response code (boolean parameter where TRUE->"OK", FALSE->"ERR")
 *  - s: a string with length; this takes two parameters: the pointer
 *       and the length (the type for length is int)
 *  - u: an unsigned long decimal integer (must be passed as a uint32)
 *  - x: an unsigned long hexadecimal integer (must be passed as a uint32)
 *       lower case
 *  - X: an unsigned long hexadecimal integer (must be passed as a uint32)
 *       upper case
 */
static uint16 at_vsprintf(uint8 *buffer, const char *format, va_list args)
{
    uint16 length = 0;
    for (; *format != 0; format++)
    {
        switch (*format)
        {
                     
            case '2':
            {
                uint32 u = va_arg(args, uint32);
                if (buffer)
                {
                    *buffer++ = '0' + ((u / 10) % 10);
                    *buffer++ = '0' + ((u)      % 10);
                }
                length += 2;
                break;
            }

            case 'b':
            {
                unsigned int b = va_arg(args, unsigned int);
                if (b != 0)
                {
                    length += 2;
                    if (buffer)
                    {
                        memcpy(buffer, "ON", lstrlen("ON"));
                        buffer += 2;
                    }
                }
                else
                {
                    length += 3;
                    if (buffer)
                    {
                        memcpy(buffer, "OFF", lstrlen("OFF"));
                        buffer += 3;
                    }
                }
                break;
            }

            case 'B':
            {
                bdaddr *addr = va_arg(args, bdaddr *);

                if (buffer)
                {
                    sprintf((char *)buffer, "%04x-%02x-%06lx", addr->nap, addr->uap,
                            addr->lap);
                    buffer += 14;
                }

                length += 14;
                break;
            }

            case 'c':
            {
                char c = (char)va_arg(args, unsigned int);

                if (buffer)
                {
                    *buffer++ = c;
                }

                length++;
                break;
            }

            case 'r':
            {
                unsigned int r = va_arg(args, unsigned int);
                if (r != 0)
                {
                    length += 2;
                    if (buffer)
                    {
                        memcpy(buffer, "OK", lstrlen("OK"));
                        buffer += 2;
                    }
                }
                else
                {
                    length += 3;
                    if (buffer)
                    {
                        memcpy(buffer, "ERR", lstrlen("ERR"));
                        buffer += 3;
                    }
                }
                break;
            }


            case 's':
            {
                uint8 *str = va_arg(args, uint8 *);
                int str_len = va_arg(args, int);

                if (buffer)
                {
                    memcpy(buffer, str, str_len);
                    buffer += str_len;
                }

                length += str_len;
                break;
            }

            case 'u':
            {
                uint32 u = va_arg(args, uint32);
                uint32 tmp = u;

                if (u == 0)
                {
                    length++;
                }
                else
                {
                    for (; tmp != 0; tmp = tmp / 10)
                    {
                        length++;
                    }
                }

                if (buffer)
                {
                    tmp = sprintf((char *) buffer, "%lu", u);
                    buffer += tmp;
                }
                break;
            }
            case 'z':/* print 8 bit number in hex format, 0xff as ff */
            case 'Z':
            {
                uint8 u = va_arg(args, uint32);
                uint8 tmp = u;
                if (buffer)
                {
                    tmp = sprintf((char *) buffer, "%02x", u);
                    buffer += tmp;
                }
                length += 2;
                break;
            }
            
            
            case 'y':/* print 16 bit number in hex format, 4-nibbles */
            case 'Y':
            {
                uint16 u = va_arg(args, uint32);
                uint16 tmp = u;

            /* All hex numbers are to be 8 characters wide */
                if (buffer)
                {
                    tmp = sprintf((char *) buffer, "%04x", u);
                    buffer += tmp;
                }
                length += 4;
                break;
            }
            
            case 'x':
            case 'X':
            {
                uint32 u = va_arg(args, uint32);
                uint32 tmp = u;

            /* All hex numbers are to be 8 characters wide */
                if (buffer)
                {
                    if (*format == 'x')
                        tmp = sprintf((char *) buffer, "%08lx", u);
                    else
                        tmp = sprintf((char *) buffer, "%08lX", u);
                    buffer += tmp;
                }
                length += 8;
                break;
            }

            default:
            {
                if (buffer)
                {
                    *buffer = *format;
                    buffer++;
                }
                length++;
                break;
            }
        }
    }
    return length;
}


status_et at_parser_send_event_message(const char *event_type,
                                      const char *code,
                                      const char *params,
                                      ...)
{
    Sink sink = at_get_sink();
    int sink_space;
    int sink_claim;
    uint16 offset;
    uint8 *sink_buffer;
    int params_len = 0;
   
    va_list args;
   
    uint16_t code_length = 0;    
    
    /* Note: this assumes AT_EVENT_TYPE_REP and AT_EVENT_TYPE_IND are
     * the same length, which they are. */
    /*uint16_t event_type_length = lstrlen(AT_EVENT_TYPE_REP);*/
    uint16_t event_type_length = strlen(event_type);

    
    PanicNull(sink);

    /* Find out how much space is available in the Sink */
    sink_space = SinkSlack(sink);

    if (code != NULL)
    {
        /* Add 1 for the '=' that will be included at the end of the code */
        code_length = strlen(code) + 1;
    }

    if (params != NULL)
    {
        va_start (args, params);
        params_len = at_vsprintf(NULL, params, args);
        va_end(args);
    }

    /* Find out how much space to try and claim */
    sink_claim = (int)(event_type_length + code_length + params_len + 2);

    /* Check whether the available space is less than what is to be claimed */
    if (sink_space < sink_claim)
    {
        return STATUS_INSUFFICIENT_SPACE;
    }

    /* If can't claim the SinkSpace (indicated by 0xffff), then return with an
     * error
     */
    offset = SinkClaim(sink, sink_claim);

    if (offset == 0xffff)
    {
        return STATUS_OTHER_ERROR;
    }

    /* Map the sink into memory space */
    sink_buffer = SinkMap(sink);
    (void) PanicNull(sink_buffer);

    /* The sink_buffer must be offset by the value returned by SinkClaim() */
    sink_buffer += offset;

    /* Copy event type into buffer */
    memcpy(sink_buffer, event_type, event_type_length);
    sink_buffer += event_type_length;

    /* Copy command code followed by '=' into buffer if the code is non-NULL */
    if (code != NULL)
    {
        /* Subtract 1 from code_length, because earlier it was computed
         * accounting for the '=' */
        memcpy(sink_buffer, code, (code_length - 1));
        sink_buffer += (code_length - 1);
        *sink_buffer = '=';
        sink_buffer++;
    }

    /* sprintf parameters into buffer */
    if (params != NULL)
    {
        va_start (args, params);
        params_len = at_vsprintf(sink_buffer, params, args);
        va_end(args);
        sink_buffer += params_len;
    }

    *sink_buffer = '\r';
    sink_buffer++;

    *sink_buffer = '\n';
    sink_buffer++;

    /* Flush the data out to the uart */
    if (!SinkFlush(sink, sink_claim))
        Panic();

    return STATUS_OK;
}


bool at_parser_match_boolean(bool *val,
                             const uint8 **packet, uint16 *packet_length)
{
    if (match_word(*packet, *packet_length, "ON", lstrlen("ON")))
    {
        *val = TRUE;
        *packet += lstrlen("ON");
        *packet_length -= lstrlen("ON");
        return TRUE;
    }
    
    if (match_word(*packet, *packet_length, "OFF", lstrlen("OFF")))
    {
        *val = FALSE;
        *packet += lstrlen("OFF");
        *packet_length -= lstrlen("OFF");
        return TRUE;
    }

    return FALSE;
}

bool at_parser_match_boolean_ALL(bool *val,
                             const uint8 **packet, uint16 *packet_length)
{
    if (match_word(*packet, *packet_length, "ALL", lstrlen("ALL")))
    {
        *val = TRUE;
        *packet += lstrlen("ALL");
        *packet_length -= lstrlen("ALL");
        return TRUE;
    }
    
    return FALSE;
}


bool at_parser_match_GapRole_type(bool *val,
                             const uint8 **packet, uint16 *packet_length)
{
    
    if (match_word(*packet, *packet_length, "CENTRAL", lstrlen("CENTRAL")))
    {
        *val = 1 ;
        *packet += lstrlen("CENTRAL");
        *packet_length -= lstrlen("CENTRAL");
        return TRUE;
    }

    if (match_word(*packet, *packet_length, "PERI", lstrlen("PERI")))
    {
        *val = 2 ;
        *packet += lstrlen("PERI");
        *packet_length -= lstrlen("PERI");
        return TRUE;
    }
    
    return FALSE;
}        
        
bool at_parser_match_iotype(cl_sm_io_capability *val,
                             const uint8 **packet, uint16 *packet_length)
{


    if (match_word(*packet, *packet_length, "DISP_ONLY", lstrlen("DISP_ONLY")))
    {
        *val = cl_sm_io_cap_display_only ;
        *packet += lstrlen("DISP_ONLY");
        *packet_length -= lstrlen("DISP_ONLY");
        return TRUE;
    }

    if (match_word(*packet, *packet_length, "DISP_YN", lstrlen("DISP_YN")))
    {
        *val = cl_sm_io_cap_display_yes_no ;
        *packet += lstrlen("DISP_YN");
        *packet_length -= lstrlen("DISP_YN");
        return TRUE;
    }
    
    if (match_word(*packet, *packet_length, "KB_ONLY", lstrlen("KB_ONLY")))
    {
        *val = cl_sm_io_cap_keyboard_only ;
        *packet += lstrlen("KB_ONLY");
        *packet_length -= lstrlen("KB_ONLY");
        return TRUE;
    }
    
    if (match_word(*packet, *packet_length, "NO_INOUT", lstrlen("NO_INOUT")))
    {
        *val = cl_sm_io_cap_no_input_no_output  ;
        *packet += lstrlen("NO_INOUT");
        *packet_length -= lstrlen("NO_INOUT");
        return TRUE;
    }  
    return FALSE;
}


bool at_parser_match_accept_reject(bool *val, const uint8 **packet,
                                   uint16 *packet_length)
{
    if (match_word(*packet, *packet_length, "ACCEPT", lstrlen("ACCEPT")))
    {
        *val = TRUE;
        *packet += lstrlen("ACCEPT");
        *packet_length -= lstrlen("ACCEPT");
        return TRUE;
    }

    if (match_word(*packet, *packet_length, "REJECT", lstrlen("REJECT")))
    {
        *val = FALSE;
        *packet += lstrlen("REJECT");
        *packet_length -= lstrlen("REJECT");
        return TRUE;
    }

    return FALSE;
}



bool at_parser_match_UpgardeIntf_type(uint16 *val,
                             const uint8 **packet, uint16 *packet_length)
{
    if (match_word(*packet, *packet_length, "UART", lstrlen("UART")))
    {
        *val =  1 ;
        *packet += lstrlen("UART");
        *packet_length -= lstrlen("UART");
        return TRUE;
    }

    if (match_word(*packet, *packet_length, "OTA", lstrlen("OTA")))
    {
        *val =  2 ;
        *packet += lstrlen("OTA");
        *packet_length -= lstrlen("OTA");
        return TRUE;
    }

    return FALSE;
}



bool at_parser_match_UartConfigType_type(uint16 *val,
                             const uint8 **packet, uint16 *packet_length)
{
  /*    static const char* const UARTconfig_array[2] = { "UART_LATENCY", "UART_THROUGHPUT" };*/  
    if (match_word(*packet, *packet_length, "LATENCY", lstrlen("LATENCY")))
    {
        *val =  0 ;
        *packet += lstrlen("LATENCY");
        *packet_length -= lstrlen("LATENCY");
        return TRUE;
    }

    if (match_word(*packet, *packet_length, "THROUGHPUT", lstrlen("THROUGHPUT")))
    {
        *val =  1 ;
        *packet += lstrlen("THROUGHPUT");
        *packet_length -= lstrlen("THROUGHPUT");
        return TRUE;
    }

    return FALSE;
}

/**
 * Read a numeral from a string and update the given integer.
 * This will advance the pointer into the string.
 *
 * \param val       Pointer to the (unsigned) integer to update.
 * \param packet    Pointer to a string. This pointer will be advanced
 *                  if the numeral was valid.
 *
 * \return TRUE if the numeral was valid, else FALSE.
 */
static bool read_int_numeral(uint32 *val, const uint8 **packet)
{
    char c = **packet;

    if (c < '0' || c > '9')
        return FALSE;

    (*packet)++;
    *val *= 10;
    *val += (c - '0');
    return TRUE;
}


bool at_parser_match_dec_uint(uint32 *val, uint16 length,
                              const uint8 **packet, uint16 *packet_length)
{
    *val = 0;
    if (length != 0)
    {
        if (length > *packet_length)
            return FALSE;

        while (length--)
        {
            if (!read_int_numeral(val, packet))
                return FALSE;
        }
        *packet_length -= length;
    }
    else
    {
        bool first = TRUE;
        while (*packet_length > 0)
        {
            /* For the value to be valid the first character must be a valid
             * numeral (i.e., the integer must contain at least one numeral).
             */
            if (!read_int_numeral(val, packet))
                return !first;
            (*packet_length)--;
            first = FALSE;
        }
    }

    return TRUE;
}


/**
 * Convert an hexadecimal number in ASCII form and convert to binary form.
 *
 * \param val           Pointer to the uint8 integer to update. The value
 *                      will be OR'd into the least significant nibble.
 * \param c             Character to convert.
 *
 * \return TRUE if successful, FALSE on failure (i.e., invalid character).
 */
static bool read_hex_nibble(uint8 *val, char c)
{
    if (c >= '0' && c <= '9')
        *val |= c - '0';
    else if (c >= 'a' && c <= 'f')
        *val |= c - 'a' + 0x0A;
    else if (c >= 'A' && c <= 'F')
        *val |= c - 'A' + 0x0A;
    else
        return FALSE;

    return TRUE;
}


/**
 * Read an ASCII hexadecimal encoded octet from a string and update the
 * given integer.  This will advance the packet data/length references.
 *
 * \param val               Pointer to the uint8 integer to update.
 * \param packet            Pointer to a string. This pointer will be advanced.
 * \param packet_length     Pointer to string length. This will be updated.
 *
 * \return TRUE if the numeral was valid, else FALSE. On FALSE return
 *         the packet and packet_length values will be undefined.
 */
static bool read_hex_octet(uint8 *val,
                           const uint8 **packet, uint16 *packet_length)
{
    *val = 0;

    if (*packet_length < 2)
        return FALSE;

    if (!read_hex_nibble(val, (char)**packet))
        return FALSE;

    (*val) <<= 4;
    (*packet)++;

    if (!read_hex_nibble(val, (char)**packet))
            return FALSE;

    (*packet)++;
    (*packet_length) -= 2;

    return TRUE;
}


bool at_parser_match_bdaddr(bdaddr *addr,
                            const uint8 **packet, uint16 *packet_length)
{
    uint8 tmp;
    uint8 i;

    addr->nap = 0;
    addr->uap = 0;
    addr->lap = 0;

    /* Read NAP */
    for (i = 0; i < 2; i++)
    {
        if (!read_hex_octet(&tmp, packet, packet_length))
            return FALSE;
        addr->nap <<= 8;
        addr->nap |= tmp;
    }

    /* Read UAP */
    if (!read_hex_octet(&(addr->uap), packet, packet_length))
        return FALSE;


    /* Read LAP */
    for (i = 0; i < 3; i++)
    {
        if (!read_hex_octet(&tmp, packet, packet_length))
            return FALSE;
        addr->lap <<= 8;
        addr->lap |= tmp;
    }

    return TRUE;
}


const char *at_parser_bool_to_str(bool value)
{
    if (value)
        return "ON";
    else
        return "OFF";
}





