#include "../CSR_LIB.h"
#include "../common.h"
#include "../config/config.h"
#include "../events.h"
#include "../utils/utils.h"

#include "../debug.h"

#include "../BT_core/bt_core.h"


#include "AT_Parse.h"
#include "at_Parser_Internal.h"

#include "../uart/uart.h"

#include "../BLE/BLE_Cen_core.h"


void at_subparser_LEAUTOCONN  (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_LEAUTOSCAN  (const uint8 *at_packet, uint16 at_packet_length);

void at_subparser_LEFILT    (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_LEFIND    (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_LECONN    (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_LEDROP    (const uint8 *at_packet, uint16 at_packet_length);


void at_subparser_FINDCHAR  (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_FINDSERV  (const uint8 *at_packet, uint16 at_packet_length);


void at_subparser_CLRWHITE  (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_LESEC     (const uint8 *at_packet, uint16 at_packet_length);


void at_subparser_BLECONN    (const uint8 *at_packet, uint16 at_packet_length);



void at_subparser_BLEADDR (const uint8 *at_packet, uint16 at_packet_length); 

  
/* this two function shares the validity check */
void at_subparser_RDCHARVAL (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_ENBCCFG (const uint8 *at_packet, uint16 at_packet_length);




/* these three functions shares the validity check */
void at_subparser_WRCHARVAL (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_WRWORESP (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_SIWRWORESP (const uint8 *at_packet, uint16 at_packet_length);


    
    
void at_subparser_INDRESP (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_RDMULTCHAR ( const uint8 *at_packet, uint16 at_packet_length );
void at_subparser_RDLONGCHAR (const uint8 *at_packet, uint16 at_packet_length);

/*void at_subparser_GETCID    (const uint8 *at_packet, uint16 at_packet_length);*/

void at_subparser_LEBOND    (const uint8 *at_packet, uint16 at_packet_length);
void at_subparser_LECONFIG  (const uint8 *at_packet, uint16 at_packet_length);


void at_subparser_GAPROLE  (const uint8 *at_packet, uint16 at_packet_length);
/*******************************************************************************
  ******************************************************************************/


void at_subparser_GAPROLE  (const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_GapRoleType_st *msg = NULL;
            
    LOG_INFO(("at_subparser_GAP-role\n"));	
        msg = (event_at_GapRoleType_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    /* Check that the packet contains at least two characters, so that '=?' can
     * be checked for and that '=' can be moved past without ending up in memory
     * that can not be accessed
     */
    if (at_packet_length < 2)
            goto err;

    if(*at_packet != '=' )
        goto err;

    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

        msg->type = OP_SET;
    
        LOG_INFO(("packet length =%u\n", at_packet_length ));

        if (!at_parser_match_GapRole_type((uint16*)&msg->GapRole, &at_packet,
                                      &at_packet_length))
        {
            LOG_INFO(("IOTYPE packet: parser error  match %u\n", 2));
            goto err;
        }
    
        if (at_packet_length != 0)
        {
            LOG_INFO(("IOTYPE packet: parser error at_packet_length %u\n", 5));
            goto err;
        }

send:
    /* Success ... pass message to application Task */
    send_message(EventAtGapRoleType, (void *)msg);
    return;

err:
    free(msg);
    MOCK(at_parser_send_err_response());       
        
}

/*******************************************************************************
  ******************************************************************************/

void at_subparser_LECONFIG  (const uint8 *at_packet, uint16 at_packet_length)
{
    bdaddr              addr;
    
	LOG_INFO((" at_subparser_LECONFIG \n"));	
    
    /* The only thing in the packet should be '=?' */
    if (at_packet_length != 2 || memcmp(at_packet, "=?", 2))
    {
        MOCK(at_parser_send_err_response());
        return;
    }

    MOCK(at_parser_send_ok_response()); 

    BdaddrSetZero (&addr); 
    read_BDaddr( LCN_LOCAL_BD_ADDRESS,  &addr);
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "ADDR", "B", &addr, sizeof(addr) ));
										
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "LE_SEC", "b",stBLEInfoData->WhiteListEnable));
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "LE_AUTOCONN_PERIPH", "b", stBLEInfoData->BLEautoConnect));
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "LE_AUTOSCAN_SERVER", "b", stBLEInfoData->BLEautoScan));
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "LE_FILTER", "b",stBLEInfoData->BLEApplyFilter));
    
    BdaddrSetZero (&addr);
    read_BDaddr( LCN_LE_BOND_BDADDR,  &addr);
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "LEBOND", "B", &addr ));
    
    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LECONFIG", "s", "END", lstrlen("END")));
}


void at_subparser_BLEADDR (const uint8 *at_packet, uint16 at_packet_length)
{
    LOG_INFO(("\n at_subparser_BLEADDR"));   

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;

    /* The only thing in the packet should be '=?' */
    if (at_packet_length != 2 || memcmp(at_packet, "=?", 2))
    {
        MOCK(at_parser_send_err_response());
        goto err;
    }    

   /* Success ... pass message to application Task, OK will be sent later */
   send_message(EventAtBLEaddr, NULL );
   return;

err:
   MOCK(at_parser_send_err_response());

}


/*******************************************************************************
  ******************************************************************************/


/* AT*BOND=0012-6f-356fc8 Bond ON, Auto connect to only mentioned device
   AT*BOND=?  Query command   ==>> reply should be "REP*:BOND=0012-6f-357fc8" or "REP*:BOND=0000-00-000000"
   AT*BOND=0000-00-000000   bond off, auto connect to any device
*/
void at_subparser_LEBOND(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_BondAddr_st *msg = (event_at_BondAddr_st *)malloc(sizeof(*msg));
    
    LOG_INFO(("at_subparser_LEBOND\n"));	
    
    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;

    
    /* Check if the only thing in the packet is '=?' */
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    msg->type = OP_SET;

    if (at_packet_length == lstrlen("=001122334455"))
    {
    }
    else
    {
        LOG_INFO(("CONNECT packet: parser error %u\n", 1));
        goto err;
    }

    if (at_packet[0] != '=')
    {
        LOG_INFO(("CONNECT packet: parser error %u\n", 2));
        goto err;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

        if (!at_parser_match_bdaddr(&(msg->addr),
                &at_packet, &at_packet_length))
        {
            LOG_INFO(("LEBOND packet: parser error %u\n", 4));
            goto err;
        }

    if( at_packet_length != 0 )
    goto err;
    
send:    
        /* Success ... pass message to application Task */
        send_message(EventAtLEBondAddr, (void *)msg);
        return;

    
err:
    free(msg);
    MOCK(at_parser_send_err_response());
        
}






void at_subparser_CLRWHITE  (const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_start_stop_discovery_st *msg = NULL; 
	/*this is dummy messsage here */
    
    LOG_INFO(("\n at_subparser_CLRWHITE")); 
    
        msg = (event_at_start_stop_discovery_st *)malloc(sizeof(*msg));
        /*this is dummy messsage here */
       
    
        LOG_INFO(("packet_length=%u ", at_packet_length));  
     
       
        if( at_packet_length == 0 )
        {
            /* Success ... pass message to application Task */
            send_message(EventAtLEClearWhitelist, (void *)msg);
            return;
        }
        
        free(msg);
        MOCK(at_parser_send_err_response());    
        
}


void at_subparser_LEAUTOCONN  (const uint8 *at_packet, uint16 at_packet_length)
{
    event_BLE_AutoScan_st *msg = NULL;

    LE_INFO(("\n at_subparser_LESEC"));   

        
        msg = (event_BLE_AutoScan_st *)malloc(sizeof(*msg));
    
        if (msg == NULL)
            goto err;
    
        if (at_packet_length < 2)
                goto err;
        
        if(*at_packet != '=' )
            goto err;
        
        if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
        {
            msg->type = OP_GET;
            goto send;
        }
    
        /* Move past '=' */
        at_packet += 1;
        at_packet_length -= 1;
    
        msg->type = OP_SET;
    
       if (!at_parser_match_boolean(&msg->enable, &at_packet,
                                     &at_packet_length))
       {
           goto err;
       }
    
        if (at_packet_length == 0)
        {
                goto send;
        }
        else
            goto err;
    
send:
   /* Success ... pass message to application Task, OK will be sent later */
   send_message(EventAtLEAutoConnectPeripheral, (void *)msg);
   return;

err:
   if (msg != NULL)
       free(msg);
   MOCK(at_parser_send_err_response());
    
}



void at_subparser_LEAUTOSCAN  (const uint8 *at_packet, uint16 at_packet_length)
{
    event_BLE_AutoScan_st *msg = NULL;

    LE_INFO(("\n at_subparser_LESEC"));   

        msg = (event_BLE_AutoScan_st *)malloc(sizeof(*msg));
    
        if (msg == NULL)
            goto err;
    
        if (at_packet_length < 2)
                goto err;
        
        if(*at_packet != '=' )
            goto err;
        
        if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
        {
            msg->type = OP_GET;
            goto send;
        }
    
        /* Move past '=' */
        at_packet += 1;
        at_packet_length -= 1;
    
        msg->type = OP_SET;
    
       if (!at_parser_match_boolean(&msg->enable, &at_packet,
                                     &at_packet_length))
       {
           goto err;
       }
    
        if (at_packet_length == 0)
        {
                goto send;
        }
        else
            goto err;
    
send:
   /* Success ... pass message to application Task, OK will be sent later */
   send_message(EventAtLEAutoScanServer, (void *)msg);
   return;

err:
   if (msg != NULL)
       free(msg);
   MOCK(at_parser_send_err_response());
    
}



void at_subparser_LESEC     (const uint8 *at_packet, uint16 at_packet_length)
{
    event_BLE_WList_st *msg = NULL;

    LE_INFO(("\n at_subparser_LESEC"));   

    msg = (event_BLE_WList_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

   if (!at_parser_match_boolean(&msg->enable, &at_packet,
                                 &at_packet_length))
   {
       goto err;
   }

    if (at_packet_length == 0)
    {
            goto send;
    }
    else
        goto err;
    
send:
   /* Success ... pass message to application Task, OK will be sent later */
   send_message(EventAtLEWhitelist, (void *)msg);
   return;

err:
   if (msg != NULL)
       free(msg);
   MOCK(at_parser_send_err_response());
    
}


void at_subparser_LEFILT(const uint8 *at_packet, uint16 at_packet_length)
{
     event_BLE_WList_st *msg = NULL;

    LE_INFO(("\n at_subparser_LEFILT  "));   
        
    msg = (event_BLE_WList_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    if (at_packet_length == 2 && memcmp(at_packet, "=?", 2) == 0)
    {
        msg->type = OP_GET;
        goto send;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    msg->type = OP_SET;

   if (!at_parser_match_boolean(&msg->enable, &at_packet,
                                 &at_packet_length))
   {
       goto err;
   }

    if (at_packet_length == 0)
    {
            goto send;
    }
    else
        goto err;
send:
   /* Success ... pass message to application Task, OK will be sent later */
   send_message(EventAtLEaddFilter, (void *)msg);
   return;

err:
   if (msg != NULL)
       free(msg);
   MOCK(at_parser_send_err_response());

}

/*******************************************************************************
  ******************************************************************************/

void at_subparser_LEFIND(const uint8 *at_packet, uint16 at_packet_length)
{
    event_BLE_discovery_st *msg = NULL;

    LE_INFO(("\n at_subparser_LEFIND"));   
        
    msg = (event_BLE_discovery_st *)malloc(sizeof(*msg));

   if (msg == NULL)
       goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;   
   
   /* Move past '=' */
   at_packet += 1;
   at_packet_length -= 1;

   if (!at_parser_match_boolean(&msg->enable, &at_packet,
                                 &at_packet_length))
   {
       goto err;
   }

    if (at_packet_length == 0)
    {
            goto send;
    }
    else
        goto err;
send:
   /* Success ... pass message to application Task, OK will be sent later */
   send_message(EventAtLEdiscovery, (void *)msg);
   return;

err:
   if (msg != NULL)
       free(msg);
   MOCK(at_parser_send_err_response());

}

/*******************************************************************************
  ******************************************************************************/

void at_subparser_LECONN(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_ble_ConDiscon_st *msg = (event_at_ble_ConDiscon_st *)malloc(sizeof(*msg));
    
    LE_INFO(("\n at_subparser_LECONN"));   

    if (msg == NULL)
    {
        LOG_INFO(("CONNECT packet: allocation failure\n"));
        goto err;
    }
    
    if (at_packet_length == lstrlen("=001122334455"))
    {
    }
    else
    {
        LOG_INFO(("CONNECT packet: parser error %u\n", 1));
        goto err;
    }

    if (at_packet[0] != '=')
    {
        LOG_INFO(("CONNECT packet: parser error %u\n", 2));
        goto err;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

        if (!at_parser_match_bdaddr(&(msg->addr),
                &at_packet, &at_packet_length))
        {
            LOG_INFO(("CONNECT packet: parser error %u\n", 4));
            goto err;
        }

    if( at_packet_length == 0 )
    {
        /* Success ... pass message to application Task */
        send_message(EventAtLEconn, (void *)msg);
        return;
    }
    
err:
    free(msg);
    MOCK(at_parser_send_err_response());  
}

void at_subparser_LEDROP(const uint8 *at_packet, uint16 at_packet_length)
{
    event_at_ble_ConDiscon_st *msg = (event_at_ble_ConDiscon_st *)malloc(sizeof(*msg));
    
    LE_INFO(("\n at_subparser_LEDROP"));   
  
    if (msg == NULL)
    {
        LOG_INFO(("CONNECT packet: allocation failure\n"));
        goto err;
    }

    if (at_packet_length == lstrlen("=001122334455"))
    {
    }
    else
    {
        LOG_INFO(("CONNECT packet: parser error %u\n", 1));
        goto err;
    }

    if (at_packet[0] != '=')
    {
        LOG_INFO(("CONNECT packet: parser error %u\n", 2));
        goto err;
    }

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

        if (!at_parser_match_bdaddr(&(msg->addr),
                &at_packet, &at_packet_length))
        {
            LOG_INFO(("CONNECT packet: parser error %u\n", 4));
            goto err;
        }

    if( at_packet_length == 0 )
    {
        /* Success ... pass message to application Task */
        send_message(EventAtLEdisconn, (void *)msg);
        return;
    }
    
err:
    free(msg);
    MOCK(at_parser_send_err_response());
}

/*******************************************************************************
  ******************************************************************************/
void at_subparser_FINDSERV(const uint8 *at_packet, uint16 at_packet_length)
{
    event_BLE_findServ_st *msg = NULL;

    LE_INFO(("\n at_subparser_FINDSERV"));

    msg = (event_BLE_findServ_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    /* Expect either a 1 or a 2 */
    if( ( *at_packet > '0' ) && 
        ( *at_packet < ('0' + 1) ) ) 
        msg->connID = *at_packet - '1' ;
    else
        goto err;    

    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;
    
    if (at_packet_length != 0)
    {
        LOG_INFO(("DCOV packet: parser error %u\n", 5));
        goto err;
    }

    /* Success ... pass message to application Task */
    send_message(EventAtLEfindServ, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
    
}



/* "at*findchar=1,0000,ffff" */
void at_subparser_FINDCHAR  ( const uint8 *at_packet, uint16 at_packet_length )
{
    uint8 value= 0; 
    bool valid = FALSE;
    event_BLE_findchar_st *msg = NULL;
    
    /* uint16 i=0;*/

    LE_INFO(("\n at_subparser_FINDCHAR"));    
    
    msg = (event_BLE_findchar_st *)malloc(sizeof(*msg));

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;
    
    LE_DEBUG(("\n at_packet_length = %d  :: " , at_packet_length ));
    /* for (i=0;i<at_packet_length; i++)
      LE_DEBUG(("%c" , *(at_packet+i) ));   */
    
    
    if( at_packet_length==11 &&
        *(at_packet+1)== ',' &&
        *(at_packet+6)== ',' )
        ;
    else
        goto err;

    
    value = *at_packet;
    /* ConnectionID 0 to 1 only, for 0th connection, at*findchar=1,all */
    if( ( *at_packet > '0' ) && 
        ( *at_packet < ('0' + 1) ) )    
    {    
         msg->connID = value - '1' ;
    }
    else
    {
        LOG_INFO(("StopBit packet: parser error %u\n", 2));
        goto err;
    }   
                    
    /* 1 for connID and one for ',' */
    at_packet += 2;
    at_packet_length -= 2;
    
    valid = convert_string_to_uint16( at_packet , &msg->start ); 
    if(valid == FALSE)
    goto err;
    
    at_packet += 5;   /*4 for 0000 and one for ',' */
    at_packet_length -= 5;
    valid = convert_string_to_uint16( at_packet , &msg->end ); 
    if(valid == FALSE)
    goto err;

    
    /* Success ... pass message to application Task */
    send_message(EventAtLEfindChar, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
    
    
}


static bool chk_validity_rdcharval_enbCCFG (const uint8 *at_packet, uint16 at_packet_length, event_BLE_rdcharval_st *msg)
{
    
    uint8 value= 0; 
    bool valid = FALSE;

    /*uint16 i=0;*/

    LE_INFO(("\n chk_validity_rdchar_enbCCFG")); 

    if (msg == NULL)
        goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;
    
    LE_DEBUG(("\n at_packet_length = %d  :: " , at_packet_length ));
    /*for (i=0;i<at_packet_length; i++)
      LE_DEBUG(("%c" , *(at_packet+i) )); */ 
    
    
    if( at_packet_length==6 &&
        *(at_packet+1)== ',' )
        ;
    else
        goto err;

    
    value = *at_packet;
    /* ConnectionID 0 to 1 only, for 0th connection, at*findchar=1,all */
    if( ( *at_packet > '0' ) && 
        ( *at_packet < ('0' + 1) ) )    
    {    
         msg->connID = value - '1' ;
    }
    else
    {
        LOG_INFO(("StopBit packet: parser error %u\n", 2));
        goto err;
    }   
                    
    /* 1 for connID and one for ',' */
    at_packet += 2;
    at_packet_length -= 2;
    
    valid = convert_string_to_uint16( at_packet , &msg->char_handle ); 
    if(valid == FALSE)
    goto err;    
  
    return TRUE;

err:
    return FALSE;
    
}




/*
    read char value: cid and Characteristsic handle
    AT*rdCHARval=1,0045  (connection 1, char handle 0x45)
*/
void at_subparser_RDCHARVAL (const uint8 *at_packet, uint16 at_packet_length)
{
    bool valid = FALSE;
    event_BLE_rdcharval_st *msg = NULL;

    LE_INFO(("\n at_subparser_FINDCHAR")); 
    
    msg = (event_BLE_rdcharval_st *)malloc(sizeof(*msg));

    valid= chk_validity_rdcharval_enbCCFG( at_packet, at_packet_length, msg);    
    if(valid == FALSE)
    goto err;
    
   
    /* Success ... pass message to application Task */
    send_message(EventAtLEReadCharValue, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
   
}

/*  */
void at_subparser_RDLONGCHAR (const uint8 *at_packet, uint16 at_packet_length)
{
    bool valid = FALSE;
    event_BLE_rdcharval_st *msg = NULL;

    LE_INFO(("\n at_subparser_FINDCHAR")); 

    msg = (event_BLE_rdcharval_st *)malloc(sizeof(*msg));

    valid= chk_validity_rdcharval_enbCCFG( at_packet, at_packet_length, msg);    
    if(valid == FALSE)
    goto err;
    
   
    /* Success ... pass message to application Task */
    send_message(EventAtLEReadLongCharValue, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
   
}


void at_subparser_ENBCCFG (const uint8 *at_packet, uint16 at_packet_length)
{
    bool valid = FALSE;
    event_BLE_rdcharval_st *msg = NULL;
    
    LE_INFO(("\n at_subparser_ENBCCFG")); 

    msg = (event_BLE_rdcharval_st *)malloc(sizeof(*msg));

    valid= chk_validity_rdcharval_enbCCFG( at_packet, at_packet_length, msg);    
    if(valid == FALSE)
    goto err;
    
   
    /* Success ... pass message to application Task */
    send_message(EventAtLEnbCCFG, (void *)msg);
    return;

err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());
   
}

/*******************************************************************************
  ******************************************************************************/

/*
  Accepts the inputs form At packet and 
  updated the parameters of msg ptr.
  
  returns FALSE if found error otherwise TRUE.
*/
static bool check_gatt_write_packet ( const uint8 *at_packet, uint16 at_packet_length, event_BLE_wrcharval_st *msg )
{
    uint8 value = 0; 
    bool valid = FALSE;
    uint8   buff[4] = {'0','0','0','0'};
    
    /* uint16 i=0;*/

    LE_DEBUG(("\n check_gatt_write_packet "));    
    
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;
    
    LE_DEBUG(("\n at_packet_length = %d  :: " , at_packet_length ));
    /* for (i=0;i<at_packet_length; i++)
      LE_DEBUG(("%c" , *(at_packet+i) )) ; */  
    
    
    if( *(at_packet+1) == ','  &&
        *(at_packet+6) == ','  &&
        *(at_packet+9) == ',' )
    ;
    else
    goto err;
    
    value = *at_packet;
    /* ConnectionID 0 to 1 only, for 0th connection, at*findchar=1,all */
    if( ( *at_packet > '0' ) &&
        ( *at_packet < ( '0' + 1) ) )    
    {    
         msg->connID = value - '1' ;
    }
    else
    {
        LOG_INFO(("StopBit packet: parser error %u\n", 2));
        goto err;
    }   
                    
    /* 1 for connID and one for ',' */
    at_packet += 2;
    at_packet_length -= 2;
    
    
    valid = convert_string_to_uint16( at_packet , &msg->char_handle ); 
    if(valid == FALSE)
    goto err;
    
    at_packet += 5;   /*4 for 0000 and one for ',' */
    at_packet_length -= 5;
    buff[2] =  *at_packet; 
    buff[3] =  *(at_packet+1);
    valid = convert_string_to_uint16( buff , &msg->size ); 
    if( valid==FALSE || (msg->size>20) )
    goto err;
       
    at_packet += 3;   
    at_packet_length -= 3;
    
    LE_DEBUG(("length=%d  value=%d ", at_packet_length, *at_packet ));    

    if(at_packet_length != msg->size)
    goto err;
    
    memcpy( msg->val_buff, at_packet, msg->size );

    return TRUE;

err:
    return FALSE;  
}


/*******************************************************************************
  ******************************************************************************/
/*  at*wrcharval=1,0003,05,12345
    at*wrcharval=connID, char-handle, size, actual-data */
void at_subparser_WRCHARVAL (const uint8 *at_packet, uint16 at_packet_length)
{
    event_BLE_wrcharval_st *msg = NULL;
    bool valid = FALSE;
                 
    LE_INFO(("\n at_subparser_WRCHARVAL"));     

    msg = (event_BLE_wrcharval_st *)malloc(sizeof(*msg));
        
    if (msg == NULL)
        goto err;
    
    valid = check_gatt_write_packet( at_packet, at_packet_length, msg);
    if(valid != TRUE)
    goto err;
       
    /* Success ... pass message to application Task */
    send_message(EventAtLEWriteCharValue, (void *)msg);
    return;  
    
err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());    
}
/*******************************************************************************
  ******************************************************************************/

void at_subparser_WRWORESP (const uint8 *at_packet, uint16 at_packet_length)
{
    event_BLE_wrcharval_st *msg = NULL;
    bool valid = FALSE;
                 
    LE_INFO(("\n at_subparser_WRWORESP "));     

    msg = (event_BLE_wrcharval_st *)malloc(sizeof(*msg));
        
    if (msg == NULL)
        goto err;
    
    valid = check_gatt_write_packet( at_packet, at_packet_length, msg);
    if(valid != TRUE)
    goto err;
       
    /* Success ... pass message to application Task */
    send_message(EventAtLEWriteWoResp, (void *)msg);
    return;  
    
err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());    
}



void at_subparser_SIWRWORESP (const uint8 *at_packet, uint16 at_packet_length)
{
    event_BLE_wrcharval_st *msg = NULL;
    bool valid = FALSE;
                 
    LE_INFO(("\n at_subparser_SIWRWORESP"));     

    msg = (event_BLE_wrcharval_st *)malloc(sizeof(*msg));
        
    if (msg == NULL)
        goto err;
    
    valid = check_gatt_write_packet( at_packet, at_packet_length, msg);
    if(valid != TRUE)
    goto err;
       
    /* Success ... pass message to application Task */
    send_message(EventAtLEsignedWriteWoResp, (void *)msg);
    return;  
    
err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());    
}

/*******************************************************************************
  function displaying details for BLE connections
  AT*BLECONN=?
  *****************************************************************************/
void at_subparser_BLECONN    (const uint8 *at_packet, uint16 at_packet_length)
{
	LOG_INFO(("\n at_subparser_BLECONN"));	
 
    /* The only thing in the packet should be '=?' */
    if (at_packet_length != 2 || memcmp(at_packet, "=?", 2))
    {
        MOCK(at_parser_send_err_response());
        return;
    }

    send_message(EventAtLEConnInfo, (void *)NULL); /* this message does not have anything to pass only request should be triggered, hence NULL*/
    return; 
    
}



/*******************************************************************************
    server sends GATT_INDICATION_IND with "cid, handle, size and data"
    LM961 shows this as indication.
    
    LM961 expects user to provide "IndicationResponose"  as 
    "AT*INDResp=1" till "AT*INDResp=4"
  
  *****************************************************************************/
void at_subparser_INDRESP (const uint8 *at_packet, uint16 at_packet_length)
{    
    event_BLE_IndicationResp_st *msg = NULL;
                 
    LOG_INFO(("\n at_subparser_INDRESP"));	  
     
    msg = (event_BLE_IndicationResp_st *)malloc(sizeof(*msg));
        
    if (msg == NULL)
        goto err;
            
/** >>>> **/
    
    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;
    
    LE_DEBUG(("\n at_packet_length = %d  :: " , at_packet_length ));
    /* for (i=0;i<at_packet_length; i++)
      LE_DEBUG(("%c" , *(at_packet+i) )) ; */  

    /* ConnectionID 0 to 1 only, for 0th connection, at*findchar=1,all */
    if( ( *at_packet > '0' ) &&
        ( *at_packet < ( '0' + 1) ) )    
    {    
         msg->connID = *at_packet - '1' ;
    }
    else
    {
        LOG_INFO(("StopBit packet: parser error %u\n", 2));
        goto err;
    }   
                    
    /* 1 for connID and one for ',' */
    at_packet += 1;
    at_packet_length -= 1;
    
    if(at_packet_length != 0)
        goto err;
    
/**  >>>>   */    
    
    send_message(EventAtLEIndicationResp, (void  *)msg);
    return; 
    
err:
    if (msg != NULL)
        free(msg);
    MOCK(at_parser_send_err_response());  
    
}




/*******************************************************************************
  Read multiple characteristics at a time
    AT*RdMultChar=1,03,0001,0003,0005
    AT*RdMultChar=3,05,0001,0003,0005,0008,000c
    Maximum 5 chars read supported 
  ******************************************************************************/
void at_subparser_RDMULTCHAR ( const uint8 *at_packet, uint16 at_packet_length )
{
    event_BLE_RdMultChar_st *msg = NULL;
    bool valid = FALSE;
	uint8 i=0,j=0;
                 
    LE_INFO(("\n at_subparser_RDMULTCHAR"));     

    msg = (event_BLE_RdMultChar_st *)malloc(sizeof(*msg));
        
    if(msg == NULL)
      goto err;

    if (at_packet_length < 2)
            goto err;
    
    if(*at_packet != '=' )
        goto err;
    
    /* Move past '=' */
    at_packet += 1;
    at_packet_length -= 1;

    LE_DEBUG(("\n at_packet_length = %d  :: " , at_packet_length ));
	for (i=0;i<at_packet_length; i++)
      LE_DEBUG(("%c" , *(at_packet+i) )) ;	

   
    /* ConnectionID 0 to 1 only, for 0th connection, at*findchar=1,all */
    if( ( *at_packet > '0' ) &&
        ( *at_packet < ( '0' + 1) ) )    
    {    
         msg->connID = (*at_packet) - '1' ;
		 LE_DEBUG(("\n Conn_ID = %d  :: " , msg->connID ));
    }
    else
    {
        LOG_INFO(("StopBit packet: parser error %u\n", 2));
        goto err;
    }

	
	/* Move past 'conn_ID' */
    at_packet += 1;
    at_packet_length -= 1;

	
	if( (*at_packet == ',') && ((*(at_packet+3) == ',')) )
	;
	else
	goto err;


   /*move past ',' after conn_ID */
    at_packet += 1;
    at_packet_length -= 1;
	if( (*at_packet == '0') &&
	   ( (*(at_packet+1)>'0') && (*(at_packet+1)<'6')   )   
	   )
	   {
		   msg->size= (*(at_packet+1)) - '0' ;
		   LE_DEBUG(("\n msg->size = %d  :: " , msg->size ));
	   }
    else
	 goto err;
	
    /*move past '05' after conn_ID comma */
    at_packet += 2;
    at_packet_length -= 2;
	
	if( *at_packet != ',')
	goto err;
    
	at_packet += 1;
    at_packet_length -= 1;
    
    
    
    LE_DEBUG(("\n at_packet_length = %d  :: " , at_packet_length ));
	for (i=0;i<at_packet_length; i++)
      LE_DEBUG(("%c" , *(at_packet+i) )) ;

    
    
	if(at_packet_length != (4*msg->size)+(msg->size-1))
    {
		LE_DEBUG(("\n Invald packet length " ));
        valid = FALSE ;		
        goto err;
	}
	
    
    for(i=0;i<msg->size;i++)
    {
            LE_DEBUG(("\n for i=%d  at_packet_length = %d  :: " , i,  at_packet_length ));
	        for(j=0;j<at_packet_length; j++)
               LE_DEBUG(("%c" , *(at_packet+j) )) ;
            
        valid = convert_string_to_uint16( at_packet , &msg->char_handle[i] );
                
        if(valid == FALSE)
        goto err;
        
        LE_DEBUG(("\n msg->char_handle[%d]=%d" , i, msg->char_handle[i] )) ;
        
        if(i<((msg->size-1)))/* this condition is must, at end there is no comma */
        if((*(at_packet+4)!=','))
        {
            LE_DEBUG(("\n error for , in loop i=%d" , i )) ;
            goto err;
        }
        at_packet += (5);   /*4 for 0000 and one for ',' */
        at_packet_length -= (5);
    }
    
    LE_DEBUG(("\n for ConnID=%d read_%d_Values for handles 1=%d, 2=%d, 3=%d, 4=%d, 5=%d", 
                msg->connID,                 
                 msg->size, 
                 msg->char_handle[0], msg->char_handle[1],msg->char_handle[2], msg->char_handle[3], msg->char_handle[4] ));
    
    
    
    send_message(EventAtLERdMultCharVal, (void  *)msg);
    return; 
    
err:
    if (msg != NULL)
        free(msg);
    
    MOCK(at_parser_send_err_response());  
}

