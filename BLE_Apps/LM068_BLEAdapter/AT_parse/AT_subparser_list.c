#include <stdlib.h>

#include "../CSR_LIB.h"
#include "../common.h"
#include "AT_Parse.h"
#include "at_Parser_Internal.h"
#include "../utils/utils.h"

/* #include "../debug.h"*/

/*
 * -------------------------------------------------------------------------
 *                       AT command declarations
 * -------------------------------------------------------------------------
 *
 * We use X macros to define the AT commands that we need to be able to
 * handle.  We have separate X macro lists for each different command
 * length.
 *
 * Note: when declaring a command, the command identifier in X_COMMAND()
 *       must not be enclosed in quotes.
 */

/** List of AT commands where the command identifier is made up of
 *  three letters. */
#define X_COMMANDS_3 \
    X_COMMAND(PAR) \
    X_COMMAND(VER) \
    X_COMMAND(PIN)

    
/** List of AT commands where the command identifier is made up of
 *  four letters. */
#define X_COMMANDS_4 \
    X_COMMAND(BAUD) \
    X_COMMAND(ECHO) \
    X_COMMAND(RESP) \
    X_COMMAND(FLOW) \
    X_COMMAND(STOP) \
    X_COMMAND(ADDR) \
    X_COMMAND(NAME) \
    X_COMMAND(DCOV) \
    X_COMMAND(PAIR) \
    X_COMMAND(DPIN) \
    X_COMMAND(MITM)
     
/** List of AT commands where the command identifier is made up of
 *  five letters. */
#define X_COMMANDS_5 \
    X_COMMAND(STATE) \
    X_COMMAND(RESET) \
    X_COMMAND(LESEC)

/** List of AT commands where the command identifier is made up of
 *  five letters. */
#define X_COMMANDS_6 \
    X_COMMAND(IOTYPE) \
    X_COMMAND(LEFIND) \
    X_COMMAND(LECONN) \
    X_COMMAND(LEBOND) \
    X_COMMAND(LEFILT) \
    X_COMMAND(LEDROP)

#define X_COMMANDS_7 \
    X_COMMAND(ENBCCFG) \
    X_COMMAND(PASSKEY) \
    X_COMMAND(PASSCFM) \
    X_COMMAND(BLECONN) \
    X_COMMAND(INDRESP) \
    X_COMMAND(GAPROLE) \
    X_COMMAND(BLEADDR) \
    X_COMMAND(UPGRADE)
    
#define X_COMMANDS_8 \
    X_COMMAND(UARTCONF) \
    X_COMMAND(FINDSERV) \
    X_COMMAND(FINDCHAR) \
    X_COMMAND(STOPPAIR) \
    X_COMMAND(PAIRLIST) \
    X_COMMAND(SETTINGS) \
    X_COMMAND(CLRWHITE) \
    X_COMMAND(OTADEBUG) \
    X_COMMAND(LECONFIG) \
    X_COMMAND(WRWORESP) \
    X_COMMAND(STACKVER)

#define X_COMMANDS_9 \
    X_COMMAND(RDCHARVAL) \
    X_COMMAND(WRCHARVAL)

#define X_COMMANDS_10 \
    X_COMMAND(LEAUTOSCAN) \
    X_COMMAND(LEAUTOCONN) \
    X_COMMAND(RDMULTCHAR) \
    X_COMMAND(RDLONGCHAR) \
    X_COMMAND(UPGRADEINT) \
    X_COMMAND(UPDATENAME) \
    X_COMMAND(SIWRWORESP)


/** The minimum subparser cmdid length */
#define AT_SUBPARSER_MIN_CMDID_LENGTH           (3)

/** The maximum subparser cmdid length */
#define AT_SUBPARSER_MAX_CMDID_LENGTH           (10)   
    

    
/*
 * -------------------------------------------------------------------------
 *                        Subparser prototypes
 * -------------------------------------------------------------------------
 *
 * Here we use the X macros from above to generate the function prototypes
 * for the subparser functions.
 */

/* Subparser prototypes from X macro */
#define X_COMMAND(cmdid) \
    void at_subparser_##cmdid(const uint8 *at_packet, uint16 at_packet_length);

X_COMMANDS_3
X_COMMANDS_4
X_COMMANDS_5
X_COMMANDS_6
X_COMMANDS_7
X_COMMANDS_8
X_COMMANDS_9
X_COMMANDS_10
#undef X_COMMAND


/*
 * -------------------------------------------------------------------------
 *                        Subparser executor
 * -------------------------------------------------------------------------
 *
 * The following function is responsible for executing the appropriate
 * subparser for a given AT packet. It uses the X macros above to generate
 * the code responsible for evaluating each command.
 */

void at_parser_invoke_subparser(const uint8 *at_packet,
                                uint16 at_packet_length)
{
    /* Verify packet is long enough to hold the '*' and the minimum
     * command ID length */

    if (at_packet_length < (AT_SUBPARSER_MIN_CMDID_LENGTH + 1))
    {
        MOCK(at_parser_send_err_response());
        return;
    }

    /* Verify packet starts with '*' ("AT" has already been
     * stripped at this point). */
    if (at_packet[0] != '*')
    {
        MOCK(at_parser_send_err_response());
        return;
    }

    at_packet++;
    at_packet_length--;


    /* This definition of X_COMMAND will generate a block of code that tests
     * for cmdid match, and if it is found invokes the corresponding
     * subparser function then returns. */
#define X_COMMAND(cmdid)                                                    \
    if (match_word(at_packet, at_packet_length, #cmdid, lstrlen(#cmdid)))   \
    {                                                                       \
        at_subparser_##cmdid(at_packet + lstrlen(#cmdid),                   \
                             at_packet_length - lstrlen(#cmdid));           \
        return;                                                             \
    }

    /* We process the list in reverse length order, since a longer command
     * identifier may contain a shorter command identifier. */
    if (at_packet_length >= 10)
    {
        X_COMMANDS_10
    }
    
    if (at_packet_length >= 9)
    {
        X_COMMANDS_9
    }
    
    if (at_packet_length >= 8)
    {
        X_COMMANDS_8
    }

    if (at_packet_length >= 7)
    {
        X_COMMANDS_7
    }

    /* If packet is long enough, test for 5 character command IDs */
    if (at_packet_length >= 6)
    {
        X_COMMANDS_6
    }
    
    /* If packet is long enough, test for 5 character command IDs */
    if (at_packet_length >= 5)
    {
        X_COMMANDS_5
    }

    /* If packet is long enough, test for 4 character command IDs */
    if (at_packet_length >= 4)
    {
        X_COMMANDS_4
    }

    /* Test if it matches any of the 3 character long command IDs */
    X_COMMANDS_3

#undef X_COMMAND

    /* If nothing matched then send an error response */
    MOCK(at_parser_send_err_response());
    
}






