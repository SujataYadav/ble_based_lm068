
#ifndef BT_PRIVATE_H__
#define BT_PRIVATE_H__





/** Maximum number of BT connections to support */
#define MAX_BT_CONNECTIONS                      (2)


/** The minimum possible value for the discovery timeout */
#define DISCOVERY_MIN_TIME (10)


/** The maximum possible value for the discovery timeout */
#define DISCOVERY_MAX_TIME (48)


/** The inquiry filter value, used to filter devices based on class */
#define BT_INQUIRY_FILTER (0)


/** The maximum number of results to discover */
#define BT_INQUIRY_MAX_RESULTS (10)




/**
 * This data structure is used when storing remote device attributes in
 * persistent store.
 */
typedef struct {
    /** Device name (we limit this to a maximum of 32 characters */
    uint8  device_name[MAX_DEVICE_NAME_ATTRIB_LENGTH];
    /** Length of \ref device_name */
    uint16 device_name_length;
} remote_device_attributes_st;




/** Enumeration of states */
typedef enum bt_state
{
    /** Powered off */
    STATE_OFF,

    /** Ready to do something (what can be done depends on
     *  \ref bt_data_t::discoverable and \ref bt_data_t::pairable */
    STATE_READY,

    /** In the processing of pairing. The BD_ADDR of the device being
     *  paired with is stored in \ref bt_data_t::pairing_addr. */
    STATE_PAIRING,

    /** In the process of pairing, initiated by the local device. This
     *  is before we have dealt with IO capabilities. */
    STATE_PAIRING_INITIATED,

    /** In the process of pairing, awaiting acceptance from the host */
    STATE_PAIRING_AWAIT_ACCEPT,

    /** In the process of pairing, and a User Passkey has been requested */
    STATE_PAIRING_AWAIT_PASSKEY,

    /** In the process of pairing, and a PIN code has been requested */
    STATE_PAIRING_AWAIT_PIN,

    /** In the process of pairing, awaiting confirmation */
    STATE_PAIRING_AWAIT_CONFIRM,

    /** In the process of reading details from the paired device */
    STATE_READING_REMOTE_NAME
} bt_state_t;



/** Bluetooth global data structure */
typedef struct 
{
    /** Current state */
    bt_state_t state;

    /** Inquiry TX power */
    int8 inquiry_tx;

    /** Address of the device that we are currently pairing with then
     *  in any of the pairing states (STATE_PAIRING*). */
    typed_bdaddr 	pairing_addr;

    
    /** Indicates whether a 'Paired' request is in progress. */
    bool paired_requested;

    /** BD_ADDR used for 'Paired' requests (check if a device is paired).
     *  Only valid if \ref paired_requested is TRUE. */
    bdaddr paired_addr;

    /** Indicates whether a 'trusted' request is in progress. */
    bool trusted_requested;

    /** BD_ADDR used for 'trusted' requests (check if a device is trusted).
     *  Only valid if \ref trusted_requested is TRUE. */
    bdaddr trusted_addr;

    
} bt_data_st;


/** Global BT data */
extern bt_data_st bt_data;






#endif

