#ifndef BT_CORE_H
#define BT_CORE_H


#define CLASS_OF_DEVICE		0x1F00
#define INQUIRY_COD			0x00  /*class of device filter */
#define INQUIRY_TIMEOUT     0x4  /*0x08 Inquiry timeout(sec) = 1.28 * INQUIRY_TIMEOUT */ 
#define GIAC_INQUIRY		0x9E8B33 /* as per specification */
enum { MIN_INQUIRE_DEVICES=1,MAX_INQUIRE_DEVICES=8};

typedef enum {
    inquiryIdle,
    inquiryPending,
    inquiryComplete,
    inquiryCancel
}inquiryState;


struct inquiryBlock
{
    uint16 devInquireIndex;
    uint16 devResolveIndex;
    inquiryState inquiryStatus;
};
        
struct inquireDevInfo 
{
	bool valid;
	bdaddr addr;
};


void clearInquiredAddress(void);

void Stop_inquiry ( void );
bool is_Inquiring ( void );


/** Handle Messages in the CL range */
void handle_cl_message (Task task, MessageId id, Message message);


/** Handle an event message from the AT parser destined for the BT subsystem */
void handle_at_bt_event_message (Task task, MessageId id, Message message);

void Delete_Pairing_and_Settings ( void );

void    Stop_advertising_after_connect      ( void );
void    Start_DualAdvert_AfterDisconnect    ( void );


#endif /* BT_CORE_H */


