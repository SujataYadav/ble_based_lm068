
#include <connection.h>
#include "../CSR_LIB.h"
#include "../common.h"
#include "../events.h"
#include "../AT_parse/AT_Parse.h"
#include "../config/config.h"

#include "../debug.h"

#include "../spp/spp_common.h" 

#include "bt_core.h"
#include "bt_private.h"


#include "../BLE/BLE_Cen_core.h"
#include "../BLE_Periph/BLE_Peri.h"



#include "../LED/LED.h"
#include <connection.h>




/* Scan activity interval and window values: interval of 1024ms and a window of 512ms (ON for 0.5s OFF for 0.5s) */
#define US_SCAN_ACT_INTERVAL_MS             ((uint16)0x0400)
#define US_SCAN_ACT_WINDOW_MS               ((uint16)0x0200)

#define DEV_RESET_DELAY_TIME_MS         ((uint16)300)    



const uint8     iotypedsponly[10] = {"Disp_Only"};
const uint8     iotypedspyn[8] = {"Disp_YN"};
const uint8     iotypekbonly[8] = {"KB_Only"};
const uint8     iotypenoinout[9] = {"NO_InOut"};




bt_data_st bt_data;



/* __________________________________________________________________________ */

void bt_power_on ( void );

/* __________________________________________________________________________ */

/**
 * Invoked when we receive a get state event from the user.
 */
static void handle_at_get_state(void)
{
    MOCK(at_parser_send_err_response());
/*    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "STATE-DP", "b,b",
                                      app_config.dcov ,
                                      app_config.pair ));

*/}

/**
 * This function checks if we are currently in a pairing state.
 *
 * \return TRUE if we are currently in a pairing state (STATE_PAIR*)
 *         else FALSE.
 */
static bool in_pairing_state(void)
{
    switch (bt_data.state)
    {
        case STATE_PAIRING:
        case STATE_PAIRING_INITIATED:
        case STATE_PAIRING_AWAIT_ACCEPT:
        case STATE_PAIRING_AWAIT_PASSKEY:
        case STATE_PAIRING_AWAIT_PIN:
        case STATE_PAIRING_AWAIT_CONFIRM:
            return TRUE;

        default:
            return FALSE;
    }
}



/******************************************************************************
  BT core state changes due to SPP connection and disconnection
  *****************************************************************************/


void bt_power_on(void)
{
    bt_data.state = STATE_READY;
    
	app_data.spp_online_state = SPP_ONLINE_COMMAND;
    
    Dev_Initiated = TRUE;
    
    LOG_INFO(("\n connected slave = hci_scan_enable_off "));
    ConnectionWriteScanEnable( hci_scan_enable_off ); 
            
    BLE_Initialise();
   
}





/**
 * Invoked when we receive an EventAtDel event from the user.
 *
 * \param event     Del (unpair) event received from user.
 */
static void handle_at_del(event_at_del_st *event)
{
    ConnectionSmDeleteAuthDeviceReq(TYPED_BDADDR_PUBLIC, &(event->addr));
    MOCK(at_parser_send_ok_response());
}


/**
 * Invoked when we receive an AT*PAIRLIST command from the user.
 */
static void handle_at_pairlist(void)
{
    uint16 i;
    uint16 trusted_device_list_size = ConnectionTrustedDeviceListSize();
    remote_device_attributes_st attribs;
    typed_bdaddr taddr;

    MOCK(at_parser_send_ok_response());

    LOG_INFO(("Trusted device list size: %u\n", trusted_device_list_size));
    LOG_INFO(("\n Sizeof remote_device_attributes_st = %d ", sizeof(remote_device_attributes_st) ));
    
    for (i = 0; i < trusted_device_list_size; i++)
    {
        bool success =
                ConnectionSmGetIndexedAttributeNowReq(0, i,
                                                      sizeof(attribs),
                                                      (uint8 *)&attribs,
                                                      &taddr);
        if (success)
        {
            LOG_INFO(("%u: bd_addr=%04x-%02x-%06lx)\n",
                      i,
                      taddr.addr.nap,
                      taddr.addr.uap,
                      taddr.addr.lap));
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP,
                                              "PAIRLIST",
                                              "B,s",
                                              &(taddr.addr),
                                              attribs.device_name,
                                              attribs.device_name_length));
        }
        else
        {
            LOG_INFO(("Failed to read trusted device list index %u\n", i));
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP,
                                              "PAIRLIST",
                                              "B,s",
                                              &(taddr.addr),
                                              "", 0));
        }
    }

    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "PAIRLIST",
                                      "s", "END", lstrlen("END")));
}


/**
 * Invoked when we receive a discovarable event from the user.
 *
 * \param event     discovarable get/set event received from user.
 */



/**
 * Invoked when we receive a pairable event from the user.
 *
 * \param event     Pairability get/set event received from user.
 */
static void handle_at_pairable(event_at_pairable_st *event)
{


    LOG_INFO(("\n handle_at_pairable "));
    
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "PAIR", "b",
                                          app_config.pair));
    }
    else
    {
        bool status = FALSE; 
        
        /*bt_data.pairable = event->pairable;*/
        
        app_config.pair = event->pairable;
        status = Store_memory_Location ( LCN_PAIR, (const uint16*)&app_config.pair, sizeof(app_config.pair) );

       /* update_state(); */
        MOCK(at_parser_send_ok_response());
    }
}




/**
 * Invoked when we receive a passkey from the user.
 *
 * \param passk     Passkey received from user.
 */
static void passkey_response(event_at_passkey_st *passk)
{
    if (bt_data.state == STATE_PAIRING_AWAIT_PASSKEY)
    {
        LOG_INFO(("Sending passkey response (passkey=%lu)\n", passk->passkey));
        ConnectionSmUserPasskeyResponse(&(bt_data.pairing_addr), FALSE,
                                        passk->passkey);
        bt_data.state = STATE_PAIRING_AWAIT_CONFIRM;
        MOCK(at_parser_send_ok_response());
    }
    else
    {
        MOCK(at_parser_send_err_response());
        LOG_INFO(("Spurios PASSK message\n"));
    }
}


static void passkey_cfm_byUser ( event_Passk_CFM_yn_st *passcfm)
{
    LOG_INFO(("passkey_cfm_byUser \n"));
    
    if (bt_data.state == STATE_PAIRING_AWAIT_CONFIRM )
    {  
        if ( !memcmp(&(bt_data.pairing_addr.addr),
                   &(passcfm->peer_bdaddr.addr ),
                   sizeof(bt_data.pairing_addr.addr))            
            )
        {
            ConnectionSmUserConfirmationResponse( &passcfm->peer_bdaddr , passcfm->cfm_yn );
            MOCK(at_parser_send_ok_response());
            LM_DEBUG(("\n asseretd user conf resp "));
        }
        else
        {
          ConnectionSmUserConfirmationResponse( &passcfm->peer_bdaddr , FALSE );
          MOCK(at_parser_send_err_response());
          LM_DEBUG(("\n Rejected user confi resp ERR "));
        }  
    }
    else
    {
        MOCK(at_parser_send_err_response());
        LOG_INFO(("Spurios PASSK message\n"));
    }  
}


/**
 * Invoked when we receive an AT*PIN command.
 *
 * \param event     Event data structure (see \ref event_at_pin_st).
 */



static void handle_at_dpin ( event_at_dpin_st *event )
{
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message( AT_EVENT_TYPE_REP, "DPIN", "b",
                                              app_config.dpin ));
    }
    else
    {
        /* Store the config into the persistent store */
        bool status = FALSE; 

        app_config.dpin = event->dpin;

        status = Store_memory_Location ( LCN_DPIN, (const uint16*)&app_config.dpin, sizeof(app_config.dpin) );         

        if (status != TRUE)
        {
            MOCK(at_parser_send_err_response());
            return;
        }

        MOCK(at_parser_send_ok_response());        
    }    
}



static void handle_at_mitm ( event_at_mitm_st  *event) 
{
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message( AT_EVENT_TYPE_REP, "MITM", "b",
                                              app_config.mitm ));
    }
    else
    {
        /* Store the config into the persistent store */
        bool status = FALSE; 

        app_config.mitm = event->mitm;

        status = Store_memory_Location ( LCN_MITM, (const uint16*)&app_config.mitm, sizeof(app_config.mitm) );       

        if (status != TRUE)
        {
            MOCK(at_parser_send_err_response());
            return;
        }

        MOCK(at_parser_send_ok_response());        
    }    
} 


static void handle_at_iotype( event_at_iotype_st  *event)
{
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        if( app_config.iotype == cl_sm_io_cap_display_only  )
                MOCK(at_parser_send_event_message( AT_EVENT_TYPE_REP, "IOTYPE", "s", iotypedsponly , sizeof(iotypedsponly) ));  
        else 
            if( app_config.iotype == cl_sm_io_cap_display_yes_no   )
                MOCK(at_parser_send_event_message( AT_EVENT_TYPE_REP, "IOTYPE", "s", iotypedspyn , sizeof(iotypedspyn) ));  
        else 
            if( app_config.iotype == cl_sm_io_cap_no_input_no_output     )
                MOCK(at_parser_send_event_message( AT_EVENT_TYPE_REP, "IOTYPE", "s", iotypenoinout , sizeof(iotypenoinout) ));  
        else 
            if( app_config.iotype == cl_sm_io_cap_keyboard_only    )
                MOCK(at_parser_send_event_message( AT_EVENT_TYPE_REP, "IOTYPE", "s", iotypekbonly , sizeof(iotypekbonly) ));
    }
    else
    {
        /* Store the config into the persistent store */
        bool status = FALSE; 

        app_config.iotype = event->iotype;

        status = Store_memory_Location ( LCN_IOTYPE, (const uint16*)&app_config.iotype, sizeof(app_config.iotype) );        

        if (status != TRUE)
        {
            MOCK(at_parser_send_err_response());
            return;
        }

        MOCK(at_parser_send_ok_response());        
    }    
}            
            
            
static void handle_at_updateNameSetting ( event_at_updatename_st *event )
{
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message( AT_EVENT_TYPE_REP, "UpdateName", "b",
                                              app_config.AddressBytesInName ));
    }
    else
    {
        /* Store the config into the persistent store */
        bool status = FALSE; 

        if( app_config.AddressBytesInName == event->updatename) 
        {
            MOCK(at_parser_send_err_response());
            return;
        }    

        app_config.AddressBytesInName = event->updatename ;
        status = Store_memory_Location ( LCN_ADD_ADDRESS_BYTES_IN_NAME, (const uint16*)&app_config.AddressBytesInName, sizeof(app_config.AddressBytesInName) );       

        if (status != TRUE)
        {
            MOCK(at_parser_send_err_response());
            return;
        }

        MOCK(at_parser_send_ok_response());        
    }     
}
            
/**
 * Invoked when we receive a name event from the user.
 *
 * \param event     Name get/set event received from user.
 */
static void handle_at_name(event_at_name_st *event)
{
    app_devname_t       updated_name;
        
        
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        
    if( app_config.AddressBytesInName == TRUE )
        get_updated_name(&updated_name);
    else
        get_device_name(&updated_name);
  
        
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "NAME", "s", updated_name.dev_name, updated_name.name_len));        
        /* ConnectionReadLocalName(app_data.task);*/
    }
    else
    {
        bool status = FALSE; 
               
        status = Store_memory_Location ( LCN_NAME_LENGTH, (const uint16*)&event->name_length, sizeof(event->name_length) );
        status = Store_memory_Location ( LCN_NAME_STRING, (const uint16*)event->name, event->name_length);
        
        if (status != TRUE)
        {
            MOCK(at_parser_send_err_response());
            return;
        }

    if( app_config.AddressBytesInName == TRUE )
        get_updated_name(&updated_name);

        ConnectionChangeLocalName(updated_name.name_len, updated_name.dev_name );
        
        MOCK(at_parser_send_ok_response());
    }
}


/**
 * Invoked when we receive a paired event from the user (querying
 * whether a given device is paired or not).
 *
 * \param event     Paired event received from user.
 */
static void handle_at_paired(event_at_paired_st *event)
{
    bt_data.paired_requested = TRUE;
    memcpy(&(bt_data.paired_addr), &(event->peer_bdaddr),
           sizeof(bt_data.paired_addr));

    /* Test whether we are paired with the given device */
    ConnectionSmGetAuthDevice(app_data.task, &(event->peer_bdaddr));
    MOCK(at_parser_send_ok_response());
}


/**
 * Invoked when we receive a pair event from the user (either
 * triggering pairing or accepting/rejecting in progress pairing).
 *
 * \param event     Pair event received from user.
 */
static void handle_at_pair(event_at_pair_st *event)
{
    if (event->cmd_type == PAIR_INITIATE)
    {
        LOG_INFO(("Initiate pairing\n"));
        ConnectionSmAuthenticate(app_data.task, &(event->peer_bdaddr),
                                 90 );

        /* Stash BD_ADDR of device we are connecting to and update state. */
        memcpy(&(bt_data.pairing_addr.addr), &(event->peer_bdaddr),
               sizeof(bt_data.pairing_addr.addr));        

        bt_data.state = STATE_PAIRING_INITIATED;
        
        app_data.device_state = PAIRING_STATE ;
        Update_LED_state();   
        
        MOCK(at_parser_send_ok_response());
    }
    else
    {
        if (bt_data.state != STATE_PAIRING_AWAIT_ACCEPT ||
            memcmp(&(bt_data.pairing_addr.addr),
                   &(event->peer_bdaddr),
                   sizeof(bt_data.pairing_addr.addr)))
        {
            LOG_INFO(("Spurios PAIR message (or invalid address)\n"));
            MOCK(at_parser_send_err_response());
            return;
        }

        if (event->cmd_type == PAIR_ACCEPT)
        {
            LOG_INFO(("Pairing accepted by host\n"));
            ConnectionSmIoCapabilityResponse(&(bt_data.pairing_addr.addr),
                                             app_config.iotype,
                                             app_config.mitm ,/* TRUE, MITM required*/
                                             TRUE,  /* bonding */
                                             0, NULL, NULL);

            bt_data.state = STATE_PAIRING;
            return;
        }
        else
        {
            LOG_INFO(("Pairing rejected by host\n"));
            ConnectionSmIoCapabilityResponse(&(bt_data.pairing_addr.addr),
                                             cl_sm_reject_request,
                                             TRUE, /* MITM required */
                                             TRUE, /* bonding */
                                             0, NULL, NULL);
            bt_data.state = STATE_READY;
        }
        MOCK(at_parser_send_ok_response());
    }
}


/**
 * Invoked when we receive an EventAtStopPair event from the user.
 *
 * \param event     Event structure received from user.
 */
static void handle_at_stoppair(event_at_stoppair_st *event)
{
    if (in_pairing_state())
    {
        if (memcmp(&(bt_data.pairing_addr.addr), &(event->peer_bdaddr),
                   sizeof(bt_data.pairing_addr.addr)))
        {
            LOG_INFO(("STOPPAIR message for unexpected BD_ADDR\n"));
            MOCK(at_parser_send_err_response());
            return;
        }
    }
    else
    {
        LOG_INFO(("STOPPAIR message in invalid state\n"));
        MOCK(at_parser_send_err_response());
        return;
    }

    LOG_INFO(("Stopping pairing\n"));
    ConnectionSmCancelAuthenticate(app_data.task, FALSE);
}


static void handle_at_LEBondAddress ( event_at_BondAddr_st *event )
{
    bdaddr addr;
    BdaddrSetZero (&addr);
    read_BDaddr( LCN_LE_BOND_BDADDR, &addr);
        LOG_INFO(("\n read_BDaddr from LE_BoND_ADDR location is (bd_addr=%04x-%02x-%06lx) ",
              addr.nap,
              addr.uap,
              addr.lap ));

    if (event->type == OP_GET)
    {      
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LEBOND", "B", &addr ));
    }
    else
    {
       if( TRUE ==  BdaddrIsSame( (const bdaddr *) &(addr) ,  (const bdaddr *) &(event->addr) )  )
       {
          LOG_INFO(("\n written and current address same ")); 
       }
       else
       {
               if ( BdaddrIsZero( (const bdaddr *)&(event->addr))	== TRUE ) /* TRUE if the address passed is zero */
                {
                        LOG_INFO(("\n clear BDaddress "));
                        clear_BDaddr( LCN_LE_BOND_BDADDR );                  
                } 
               else 
                {
                    LOG_INFO(("\n Write new BDaddress "));
                    write_BDaddr(LCN_LE_BOND_BDADDR,  &(event->addr));
                }
           }
        MOCK(at_parser_send_ok_response());
        
        
        if(stBLEInfoData->BLE_Role == GAP_CENTRAL_ROLE )
            handleGapCentralAutoConnect_AfterDisconnet();
        
    }
    
}



/*******************************************************************************
  ******************************************************************************
  Functions for  Connection_Library_messages
  ******************************************************************************
  ******************************************************************************/

/**
 * Handle a CL_SM_PIN_CODE_IND event.
 *
 * This is sent during legacy pairing to retrieve the pin code.
 *
 * \param ind       The Message data structure.
 */
static void handle_cl_sm_pin_code_ind(CL_SM_PIN_CODE_IND_T *ind)
{
    LOG_INFO(("Pincode request (bd_addr=%04x-%02x-%06lx)\n",
              ind->taddr.addr.nap,
              ind->taddr.addr.uap,
              ind->taddr.addr.lap));
        LM_DEBUG((" provided PIN to above-device as len=%u ", app_config.PIN_len ));
        
        /* check here, if SPP conneted ===>reject pairing request */
             
        /* Stash BD_ADDR of device we are connecting to and update state. */
        memcpy(&(bt_data.pairing_addr), &(ind->taddr),
               sizeof (bt_data.pairing_addr));
        /* bt_data.pairing_addr.transport = TRANSPORT_BREDR_ACL;*/
        bt_data.pairing_addr.type = ind->taddr.type; 
        bt_data.state = STATE_PAIRING_INITIATED;
        
        app_data.device_state = PAIRING_STATE ;
        Update_LED_state();   
        
        ConnectionSmPinCodeResponse( &ind->taddr, app_config.PIN_len, (uint8*)&app_config.PIN.PIN );

}


/**
 * Handle a CL_SM_IO_CAPABILITY_REQ_IND event.
 *
 * This is sent when our IO capabilities are being requested and we must
 * invoke ConnectionSmIoCapabilityResponse().
 *
 * \param ind       The Message data structure.
 */
static void handle_cl_sm_io_capability_req_ind( CL_SM_IO_CAPABILITY_REQ_IND_T *ind)
{
    event_at_pair_st *pair_accept = (event_at_pair_st *)malloc(sizeof(*pair_accept));
    LOG_INFO(("SM IO capability request\n"));
    /* UI_DEBUG_LEDS_BT_CONN_STATE(UI_DEBUG_LEDS_BT_PAIRING); */

    if (app_config.pair && bt_data.state == STATE_READY)
    {
        /* Stash pairing address and update state 
        bt_data.pairing_addr.transport = TRANSPORT_BREDR_ACL;*/
        
        memcpy(&(bt_data.pairing_addr.addr), &(ind->bd_addr),
               sizeof(bt_data.pairing_addr.addr));
        
        bt_data.state = STATE_PAIRING_AWAIT_ACCEPT;

        /* Send a PAIR indication to the host to request it to
         * accept/reject pairing. 
        at_parser_send_event_message(AT_EVENT_TYPE_IND, "PAIR",
                                     "B", &(ind->bd_addr));  */
        
        memcpy(&(pair_accept->peer_bdaddr), &(ind->bd_addr), sizeof(ind->bd_addr));
        pair_accept->cmd_type = PAIR_ACCEPT;
 
        send_message(EventAtPair, (void *)pair_accept);

        app_data.device_state = PAIRING_STATE ;
        Update_LED_state(); 
            return;
    }
    else if (bt_data.state == STATE_PAIRING_INITIATED)
    {  
        if (!memcmp(&(bt_data.pairing_addr.addr), &(ind->bd_addr),
                    sizeof(ind->bd_addr))
            )             
        {
            LOG_INFO(("Pairing initiated by local device... sending IO capabilities\n"));
            ConnectionSmIoCapabilityResponse(&(bt_data.pairing_addr.addr),
                                             app_config.iotype,
                                             app_config.mitm,
                                             TRUE, /*bonding*/
                                             FALSE,
                                             NULL, NULL);
        }
        else
        {
            LOG_INFO(("IO capabilities request received from invalid device\n"));
            ConnectionSmIoCapabilityResponse(&ind->bd_addr,
                                             cl_sm_reject_request,
                                             app_config.mitm,
                                             TRUE, /*bonding*/
                                             FALSE,
                                             NULL, NULL);
        }
    }
    else
    {
        LOG_INFO(("Pairing disabled or in wrong state, rejecting IO capability request\n"));
            ConnectionSmIoCapabilityResponse(&ind->bd_addr,
                                             cl_sm_reject_request,
                                             app_config.mitm,
                                             FALSE, /*bonding*/
                                             FALSE,
                                             NULL, NULL);
    }
    
    free(pair_accept);
    
}


/**
 * Handle a CL_SM_REMOTE_IO_CAPABILITY_IND event.
 *
 * This is sent during pairing to indicate to us what the IO capabilities
 * of the remote device are.
 *
 * \param ind       The Message data structure.
 */
static void handle_cl_sm_remote_io_capability_ind( CL_SM_REMOTE_IO_CAPABILITY_IND_T *ind)
{
    LOG_INFO(("Remote IO capability ind (auth_req=%u, io_cap=%u, oob_data=%s)\n",
              ind->authentication_requirements,
              ind->io_capability,
              ind->oob_data_present ? "TRUE" : "FALSE"));
}

static void handle_cl_sm_user_passkey_Notif_Ind( CL_SM_USER_PASSKEY_NOTIFICATION_IND_T *ind )
{
    LM_DEBUG((" CL_SM_USER_PASSKEY_NOTIFICATION_IND_T \n "));
    
    LOG_INFO(("User passkey Notif_Ind (bd_addr=%04x-%02x-%06lx),  Passkey = %ld \n",
              ind->taddr.addr.nap,
              ind->taddr.addr.uap,
              ind->taddr.addr.lap,
              ind->passkey));

    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_IND, "PASSKEY", "u", ind->passkey));
    
}



static void handle_cl_sm_user_Passkey_CfmReq_Ind( CL_SM_USER_CONFIRMATION_REQ_IND_T *ind )
{
    LM_DEBUG((" handle_cl_sm_user_Passkey_CfmReq_Ind \n ")); 
       
    LOG_INFO(("User passkey request (bd_addr=%04x-%02x-%06lx),  Passkey = %ld , Resp_req = %s \n",
        ind->taddr.addr.nap,
        ind->taddr.addr.uap,
        ind->taddr.addr.lap,
        ind->numeric_value, 
        ind->response_required ? "TRUE" : "FALSE"
        ));
        
        /* Send here response if Yes/No
           ConnectionSmUserConfirmationResponse( &remote_addr, confirm ); */ 
           
    if ( !memcmp(&(bt_data.pairing_addr), &(ind->taddr), sizeof(bt_data.pairing_addr))  )           
    {   
        if( bt_data.state == STATE_PAIRING ) 
        {
            /* Send a PAIR indication to the host to request it to
            * accept/reject pairing. */
            at_parser_send_event_message(AT_EVENT_TYPE_IND, "PASSCFM_YN",
                                                 "B", &(ind->taddr.addr)); 
            
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_IND, "PASSKEY", "u", ind->numeric_value));
                    
            bt_data.state = STATE_PAIRING_AWAIT_CONFIRM ;
        }
    }
}


/**
 * Handle a CL_SM_USER_PASSKEY_REQ_IND event.
 *
 * This is sent during pairing to request entry of the passkey.  We
 * inform the stack of the entered passkey by invoking
 * ConnectionSmUserPasskeyResponse().
 *
 * \param ind       The Message data structure.
 */
static void handle_cl_sm_user_passkey_req_ind( CL_SM_USER_PASSKEY_REQ_IND_T *ind)
{
    LOG_INFO(("User passkey request (bd_addr=%04x-%02x-%06lx)\n",
              ind->taddr.addr.nap,
              ind->taddr.addr.uap,
              ind->taddr.addr.lap));

    /* We only accept this request if: pairing was initiated by the remote
     * device and we are pairable, or pairing was initiated by the local
     * device. */
    if ((app_config.pair && bt_data.state == STATE_PAIRING) ||
        (bt_data.state == STATE_PAIRING_INITIATED))
    {
        if (memcmp(&(bt_data.pairing_addr), &(ind->taddr),
                   sizeof(bt_data.pairing_addr)))
        {
            LOG_INFO(("Received User Passkey request for unexpected device\n"));
            ConnectionSmUserPasskeyResponse(&ind->taddr, TRUE, 0);
            return;
        }

        bt_data.state = STATE_PAIRING_AWAIT_PASSKEY;
        at_parser_send_event_message(AT_EVENT_TYPE_IND, "PASSK", "?");
    }
    else
    {
        LOG_INFO(("Pairing disabled, rejecting user passkey request\n"));
        ConnectionSmUserPasskeyResponse(&ind->taddr, TRUE, 0);
    }
}


static void handle_cl_sm_set_trust_level_cfm( CL_SM_SET_TRUST_LEVEL_CFM_T *message )
{
    LOG_INFO(("CL_SM_SET_TRUST_LEVEL_CFM_T  \n"));

    if (message->status == success)
    {
        LOG_INFO(("\n CL_SM_SET_TRUST_LEVEL_CFM_T = success "));
        /**/MOCK(at_parser_send_ok_response());
    }
    else
    {
         LOG_INFO(("\n CL_SM_SET_TRUST_LEVEL_CFM_T = error "));
        /**/MOCK(at_parser_send_err_response());
    }
}


static void handle_cl_sm_get_auth_device_cfm( CL_SM_GET_AUTH_DEVICE_CFM_T *message)
{
    LOG_INFO(("CL_SM_GET_AUTH_DEVICE_CFM_T  \n"));
    LOG_INFO(("Cfm bd_addr=%04x-%02x-%06lx)\n",
              message->bd_addr.nap,
              message->bd_addr.uap,
              message->bd_addr.lap));
    
    if (bt_data.paired_requested && memcmp(&(bt_data.paired_addr),
                                           &(message->bd_addr),
                                           sizeof(bt_data.paired_addr)) == 0)
    {
        LOG_INFO((" Address Match  \n"));
        if ( TRUE == Is_Response_Set() )
        {
            if (message->status == success)
            {
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "PAIR", "B,b",
                                                  &(message->bd_addr),
                                                  TRUE));
                /**/MOCK(at_parser_send_ok_response());
            }
            else
            {
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "PAIR", "B,b",
                                                  &(message->bd_addr),
                                                  FALSE));
                /**/MOCK(at_parser_send_err_response());
            }
        }

        bt_data.paired_requested = FALSE;
        return;
    }
    else if (bt_data.trusted_requested && memcmp(&(bt_data.trusted_addr),
                                         &(message->bd_addr),
                                         sizeof(bt_data.trusted_addr)) == 0)
    {
         LOG_INFO((" Address MisMatch  \n"));
         
        if ( TRUE == Is_Response_Set() )
        {
            if (message->status == success && message->trusted)
            {
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "TRUST", "B,b",
                                                  &(message->bd_addr),
                                                  TRUE));
                MOCK(at_parser_send_ok_response());
            }
            else
            {
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "TRUST", "B,b",
                                                  &(message->bd_addr),
                                                  FALSE));
                /**/MOCK(at_parser_send_err_response());
            }
        }
        
        bt_data.trusted_requested = FALSE;
        return;
    }
}

static void handle_cl_Authorise_Ind( CL_SM_AUTHORISE_IND_T *ind )
{
		LOG_INFO(("handle_cl_Authorise_Ind \n"));
        ConnectionSmAuthoriseResponse (	&(ind->bd_addr), ind->protocol_id , ind->channel, ind->incoming, TRUE );    
}

static void handle_cl_sm_authenticate_cfm(CL_SM_AUTHENTICATE_CFM_T *cfm)
{
    bool  Pair_bool = FALSE;
    
    LOG_INFO(("Authenticate cfm (status=%u, key type=%u, bonded=%s)\n",
              cfm->status, cfm->key_type, cfm->bonded ? "TRUE" : "FALSE")); 
 
    if (in_pairing_state())
    {
        if( memcmp(&(bt_data.pairing_addr.addr), &(cfm->bd_addr), sizeof(bt_data.pairing_addr.addr)) )
        {
            LOG_INFO(("Authenticate Cfm message for unexpected BD_ADDR\n"));
            return;
        }
    }
    else
    {
        LOG_INFO(("Suprious Authenticate Cfm message\n"));
        return;
    }

    
    if (cfm->status == auth_status_success)
    {      
        ConnectionSmSetTrustLevel( app_data.task, &cfm->bd_addr, TRUE );    
        Pair_bool = TRUE;   
    }

    
    if ( TRUE == Is_Response_Set() )
    {
        if(Pair_bool == TRUE)
        {
            at_parser_send_event_message(AT_EVENT_TYPE_IND, "PAIR","r,B", TRUE, &(cfm->bd_addr));
            /*MOCK(at_parser_send_ok_response());*/
        }
        else
        {
            at_parser_send_event_message(AT_EVENT_TYPE_IND, "PAIR","r,B", FALSE, &(cfm->bd_addr));
           /* MOCK(at_parser_send_err_response());*/
        }
    }
    
    bt_data.state = STATE_READY;

}




/**
 * Handle a CL_DM_LOCAL_NAME_COMPLETE event.
 *
 * This is sent in response to invocation of ConnectionReadLocalName().
 *
 * \param msg       The Message data structure.
 */
static void handle_cl_dm_local_name_complete(CL_DM_LOCAL_NAME_COMPLETE_T *msg)
{
    
    if (msg->status != hci_success)
    {
        LM_DEBUG(("Failed to read local name \n"));
        return;
    }
    else
    {
        LM_DEBUG(("Local NameRead Success \n"));
        /*
        app_config.name.name_len = MIN( msg->size_local_name, BT_AT_DEVICE_NAME_LENGTH );
        memcpy( app_config.name.dev_name, msg->local_name, app_config.name.name_len);
        */
        
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "NAME", "s",
                                          msg->local_name,
                                          msg->size_local_name));
    }
}



/**
 * Handle a CL_DM_ACL_OPENED_IND indication.
 *
 * \param ind       The indication Message data structure.
 */
static void handle_cl_dm_acl_opened_ind(CL_DM_ACL_OPENED_IND_T *ind)
{
    LOG_INFO(("\n  ACL opened indication (dir=%s, class=%08lx), status_code=0x%x ",
              ind->incoming ? "in" : "out",
              ind->dev_class ,
              ind->status));

    PRINT_BDADDR(ind->bd_addr.addr);
    if (ind->status == hci_success)
    {
      /*  UI_DEBUG_LEDS_BT_CONN_STATE(UI_DEBUG_LEDS_BT_CONNECTED); */
    }
    else
    {
        LOG_INFO(("ACL open failed with status %u\n", ind->status));
    }
    
}


/**
 * Handle a CL_DM_ACL_CLOSED_IND indication.
 *
 * \param ind       The indication Message data structure.
 */
static void handle_cl_dm_acl_closed_ind(CL_DM_ACL_CLOSED_IND_T *ind)
{
    LOG_INFO(("ACL closed indication (status = %u)\n", ind->status));
    /* UI_DEBUG_LEDS_BT_CONN_STATE(UI_DEBUG_LEDS_BT_UNCONNECTED); */
    
    /* 19 = remote device terminated the connection 
        8 = Indicates that the link supervision timeout has expired for a given connection.
       22 = hci_error_conn_term_local_host, Indicates that the local device terminated the connection
    */
}





/**
 * Handle the CL_INIT_CFM message, sent in response to ConnectionInitEx2()
 * (invoked from \ref bt_init()).  This will verify that initialisation was
 * successful, then continue the initialisation process.
 */
static void handle_cl_init_cfm(CL_INIT_CFM_T *cfm)
{
    
    if (cfm->status != success)
    {
        LM_DEBUG(("Failed to initialise BT Connection library\n"));
        Panic();
        return;
    }

    LM_DEBUG(("Connection library initialised (v %u)\n", cfm->version));
    
    ConnectionWriteClassOfDevice(CLASS_OF_DEVICE);
    ConnectionReadLocalAddr(app_data.task);
    Read_DevName_FromPSkey ();
    
    ConnectionSmSetSecurityMode(app_data.task, sec_mode4_ssp, hci_enc_mode_pt_to_pt);

    /* WAE - no ACL, Debug keys - off, Legacy pair key missing - on */
    ConnectionSmSecModeConfig(app_data.task, cl_sm_wae_acl_none, FALSE, TRUE);
    
        
        ConnectionDmBleSetAdvertiseEnable (FALSE);
        ConnectionDmBleSetScanEnable(FALSE);

        
    bt_power_on();
    
}

static void handle_readLocalVersionCFM ( CL_DM_LOCAL_VERSION_CFM_T *cfm )
{
    
/* 
---- 12:35:04.482 ------------------ 
DM_HCI_READ_LOCAL_VER_INFO_CFM 
type = 0x1201 (4609) 
phandle = 0x0010 (16) 
status = 0x00 (0) 
hci_version = 0x09 (9) 
hci_revision = 0x291d (10525) 
lmp_version = 0x09 (9) 
manuf_name = 0x000a (10) 
lmp_subversion = 0x291d (10525) 
------------------------------------ 
*/
    
   LOG_INFO (("\n CL_DM_LOCAL_VERSION_CFM_T == ")); 
   
    if(cfm->status == hci_success )
    {
        LOG_INFO((" \n HCI-version = %d  \n HCI-Revision=%d  \n ImpVersion=%d \n ImpSubVersion=%d ", cfm->hciVersion, cfm->hciRevision , cfm->lmpVersion , cfm->lmpSubVersion ));
        
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "HCI-Ver", "u:s", (uint32) cfm->hciVersion,  "HCI-Ver=9=BT5.0", lstrlen("HCI-Ver=9=BT5.0") ));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "HCI_Rev", "u:s", (uint32) cfm->hciRevision, "HCI-Ver=10525=BT5.0", lstrlen("HCI-Ver=10525=BT5.0") ));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "IMP-Ver", "u:s", (uint32) cfm->lmpVersion , "IMP-Ver=9=BT5.0", lstrlen("IMP-Ver=9=BT5.0") ));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "Manuf-Name", "u", (uint32) cfm->manufacturerName ));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "IMP-SubVer", "u:s", (uint32) cfm->lmpSubVersion, "IMP-SubVer=10525=BT5.0", lstrlen("IMP-SubVer=10525=BT5.0") ));
        
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "STACKVER", "s", "END", lstrlen("END")));
    }
    else
         LOG_INFO((" == Veriosn read error "));
     
}


static void handle_readBTversionCFM (CL_DM_READ_BT_VERSION_CFM_T *cfm )
{
     LOG_INFO (("\n CL_DM_READ_BT_VERSION_CFM_T == ")); 
     
     if(cfm->status == hci_success )
         LOG_INFO((" BT version on the device is %d  ", cfm->version));
     else
         LOG_INFO((" == Veriosn read error "));
     
}
            
            
            
            

/********************************************************************************/



void get_device_name( app_devname_t  *pname )
{
    uint8    i=0;

            
    PsRetrieve( LCN_NAME_LENGTH, &(pname->name_len), sizeof(pname->name_len) );
    PsRetrieve( LCN_NAME_STRING, &(pname->dev_name), pname->name_len ); 
            for (i=0; i<( pname->name_len); i++ )
            {
                LOG_INFO((" %c ", pname->dev_name[i] ));    
            }            
}




void get_updated_name( app_devname_t  *pname )
{
    
    uint8 lo=0, hi=0; 
    bdaddr              addr;    

    uint8    i=0;
    app_devname_t       name;

    
    BdaddrSetZero (&addr); 
    PsRetrieve( LCN_LOCAL_BD_ADDRESS, &addr, sizeof(addr) );       
        lo = addr.lap & 0x0f;       
           if(lo >= 0x0A && lo <= 0x0F)
                lo = lo-0x0A + 'A';
            else if(lo >= 0 && lo <= 9)
                lo = lo +'0';  

        hi = ( (addr.lap & 0x00f0) >> 4);
            if(hi >= 0x0A && hi <= 0x0F)
                hi = hi-0x0A + 'A';
            else if(hi >= 0 && hi <= 9)
                hi = hi +'0';     

            
    PsRetrieve( LCN_NAME_LENGTH, &name.name_len, sizeof(name.name_len) );
    PsRetrieve( LCN_NAME_STRING, &name.dev_name, name.name_len ); 
            for (i=0; i<( name.name_len); i++ )
            {
                pname->dev_name[i] = name.dev_name[i] ;
                LOG_INFO((" %c ", name.dev_name[i] ));    
            }
            pname->dev_name[name.name_len] = hi;            
            pname->dev_name[name.name_len+1] = lo;       
            pname->name_len = (name.name_len) + 2;
                        
            LOG_INFO((" \n Adjusted_Name is %d length = ", pname->name_len ));  
            for (i=0; i<(pname->name_len); i++ )
            {
                LOG_INFO((" %c ",  pname->dev_name[i] ));    
            }
            
}


static void Handle_Local_BTAdress_Cfm (CL_DM_LOCAL_BD_ADDR_CFM_T *cfm )
{     
    app_devname_t       updated_name;
   
    bdaddr              addr;
    BdaddrSetZero (&addr); 

    
    LOG_INFO(("\n CL_DM_LOCAL_BD_ADDR_CFM_T "));     
    
    if(cfm->status == hci_success  )
    {  
        LOG_INFO(("\n read_addr_from_stack_cfm is =  ")); PRINT_BDADDR( (cfm->bd_addr) );
        write_BDaddr(LCN_LOCAL_BD_ADDRESS,  &(cfm->bd_addr) );

        read_BDaddr( LCN_LOCAL_BD_ADDRESS,  &addr);
        LOG_INFO(("\n read_above addr from PS-store is =  ")); PRINT_BDADDR(( addr )); 

        if( app_config.AddressBytesInName == TRUE )
            get_updated_name(&updated_name);
        else
            get_device_name(&updated_name);


        LOG_INFO((" \n Send Request to update the local name" ));  
        ConnectionChangeLocalName(updated_name.name_len, updated_name.dev_name );
            
    }
    
}

/** Handler for Bluetooth Classic Messages on the Application Task */
void handle_cl_message(Task task, MessageId id, Message message)
{
    switch (id)
    {     
        case CL_INIT_CFM:
            LM_BLE_DEBUG(("\n CL_INIT_CFM "));
            handle_cl_init_cfm((CL_INIT_CFM_T *)message);
            break;
        case CL_DM_LOCAL_BD_ADDR_CFM :
            Handle_Local_BTAdress_Cfm ((CL_DM_LOCAL_BD_ADDR_CFM_T*) message );
            break;
        case CL_DM_LOCAL_NAME_COMPLETE:
            handle_cl_dm_local_name_complete(
                    (CL_DM_LOCAL_NAME_COMPLETE_T *)message);
            break;

        case CL_DM_LOCAL_VERSION_CFM:
            handle_readLocalVersionCFM ( (CL_DM_LOCAL_VERSION_CFM_T *) message);
            break;
        case CL_DM_READ_BT_VERSION_CFM:
            handle_readBTversionCFM ( (CL_DM_READ_BT_VERSION_CFM_T *) message);
            break;
            
        case CL_DM_INQUIRE_RESULT:
            break;            
        case CL_DM_REMOTE_NAME_COMPLETE: 
            break;
            
            
        case CL_DM_ACL_OPENED_IND:
            handle_cl_dm_acl_opened_ind((CL_DM_ACL_OPENED_IND_T *)message);
            break;
        case CL_DM_ACL_CLOSED_IND:
            handle_cl_dm_acl_closed_ind((CL_DM_ACL_CLOSED_IND_T *)message);
            break;
  
        case CL_SM_PIN_CODE_IND:
            handle_cl_sm_pin_code_ind((CL_SM_PIN_CODE_IND_T *)message);
            break;
        case CL_SM_IO_CAPABILITY_REQ_IND:
            handle_cl_sm_io_capability_req_ind((CL_SM_IO_CAPABILITY_REQ_IND_T *)message);
            break;
        case CL_SM_REMOTE_IO_CAPABILITY_IND:
            handle_cl_sm_remote_io_capability_ind((CL_SM_REMOTE_IO_CAPABILITY_IND_T *)message);
            break;
        case CL_SM_USER_PASSKEY_REQ_IND:
            handle_cl_sm_user_passkey_req_ind((CL_SM_USER_PASSKEY_REQ_IND_T *)message);
            break;            
        case CL_SM_USER_PASSKEY_NOTIFICATION_IND:
            handle_cl_sm_user_passkey_Notif_Ind((CL_SM_USER_PASSKEY_NOTIFICATION_IND_T *)message);
            break;
        case CL_SM_USER_CONFIRMATION_REQ_IND:
            handle_cl_sm_user_Passkey_CfmReq_Ind((CL_SM_USER_CONFIRMATION_REQ_IND_T *)message);
           break; 
        case CL_SM_AUTHENTICATE_CFM :
            handle_cl_sm_authenticate_cfm((CL_SM_AUTHENTICATE_CFM_T *)message);
            break;
        case CL_SM_GET_AUTH_DEVICE_CFM:
            handle_cl_sm_get_auth_device_cfm((CL_SM_GET_AUTH_DEVICE_CFM_T *)message); 
            break;
        case CL_SM_SET_TRUST_LEVEL_CFM:
            handle_cl_sm_set_trust_level_cfm((CL_SM_SET_TRUST_LEVEL_CFM_T *)message);
            break;            
        case CL_RFCOMM_PORTNEG_IND:
            LM_DEBUG(("\n CL_RFCOMM_PORTNEG_IND "));
            break;
            
        case CL_RFCOMM_CONTROL_IND :
            LM_DEBUG(("\n CL_RFCOMM_CONTROL_IND "));
            break;
  
        case CL_DM_RSSI_CFM :
            LM_DEBUG(("\n CL_DM_RSSI_CFM "));
            break;
        case CL_DM_MODE_CHANGE_EVENT:
            LM_DEBUG(("\n CL_DM_MODE_CHANGE_EVENT"));
            break;
            
        case CL_DM_READ_EIR_DATA_CFM:
            LM_DEBUG(("\n CL_DM_READ_EIR_DATA_CFM"));
            break;
            
        case CL_DM_ROLE_CFM:
            LM_DEBUG(("\n CL_DM_ROLE_CFM"));
            break;
            
        case CL_DM_ROLE_IND:
            LM_DEBUG(("\n CL_DM_ROLE_IND"));
            break;
            
        case CL_DM_SYNC_CONNECT_IND:
            LM_DEBUG(("\n CL_DM_SYNC_CONNECT_IND"));
            break;
            
        case CL_SM_ADD_AUTH_DEVICE_CFM:
            LM_DEBUG(("\n CL_SM_ADD_AUTH_DEVICE_CFM"));
            break;
           
        case CL_SM_AUTHORISE_IND:
            LM_DEBUG(("\n CL_SM_AUTHORISE_IND"));
            handle_cl_Authorise_Ind((CL_SM_AUTHORISE_IND_T *)message);
            break;

            case CL_DM_BLE_SECURITY_CFM :
            case CL_DM_BLE_SET_SCAN_RESPONSE_DATA_CFM:
            case CL_DM_BLE_ADVERTISING_REPORT_IND:
            case CL_DM_BLE_SET_ADVERTISING_PARAMS_CFM:
            case CL_DM_BLE_SET_ADVERTISING_DATA_CFM:
            case CL_DM_BLE_SET_CONNECTION_PARAMETERS_CFM:
            case CL_DM_BLE_CONNECTION_PARAMETERS_UPDATE_CFM:
            case CL_DM_BLE_SET_SCAN_PARAMETERS_CFM:
            case CL_DM_BLE_READ_WHITE_LIST_SIZE_CFM:
            case CL_DM_BLE_REMOVE_DEVICE_FROM_WHITE_LIST_CFM :
            case CL_DM_BLE_CLEAR_WHITE_LIST_CFM:
            case CL_DM_BLE_ADD_DEVICE_TO_WHITE_LIST_CFM: 
            case CL_SM_BLE_SIMPLE_PAIRING_COMPLETE_IND:
            case CL_DM_BLE_CONFIGURE_LOCAL_ADDRESS_CFM:
            case CL_DM_BLE_ACCEPT_CONNECTION_PAR_UPDATE_IND:
            case CL_SM_BLE_IO_CAPABILITY_REQ_IND:
                  /*  LM_BLE_DEBUG(("CL DM BLE (id=0x%x)\n", id));  */              
                handle_DM_BLE_message(id, message);
                 LE_DEBUG(("CL BLE (id=%x)\n", id));
            break; 
            
            
        default:
            LE_DEBUG(("CL unmanaged (id=%x)\n", id));
            break;
    }
}














/*******************************************************************************
 *******************************************************************************
 
 *******************************************************************************
 *******************************************************************************/

void handle_at_bt_event_message(Task task, MessageId id, Message message)
{
    switch (id)
    {	
        case EventAtGetState:
            handle_at_get_state();
            break;
            
        case EventAtPasskey:
            passkey_response((event_at_passkey_st *)message);
            break;
        case EventAtPassCFM:
            passkey_cfm_byUser (( event_Passk_CFM_yn_st *)message);
            break;
        case EventAtName:
            handle_at_name((event_at_name_st *)message);
            break;
            
        case EventAtUpdateName:
            handle_at_updateNameSetting((event_at_updatename_st *) message);
            break;
            
        case EventAtPin:
            break;
        case EventAtdpin:
            handle_at_dpin((event_at_dpin_st *)message);
            break;
        case EventAtMitm:
            handle_at_mitm((event_at_mitm_st *)message);            
            break;
        case EventAtIotype:
            handle_at_iotype((event_at_iotype_st *)message); 
            break;


        case EventAtLEBondAddr:
            handle_at_LEBondAddress((event_at_BondAddr_st *)message); 
            break;
            
            
        case EventAtPairList:
            handle_at_pairlist();
            break;
        case EventAtDel:
            handle_at_del((event_at_del_st *)message);
            break;


            
        case EventAtPairable:
            handle_at_pairable((event_at_pairable_st *)message);
            break;

        case EventAtPaired:
            handle_at_paired((event_at_paired_st *)message);
            break;

        case EventAtPair:
           handle_at_pair((event_at_pair_st *)message);
            break;
        case EventAtStopPair:
            handle_at_stoppair((event_at_stoppair_st *)message);
            break;            
        case EventAtGetRSSI:
            break;            
            
        default:
            LM_DEBUG(("Invalid AT BT Event message (id=%u)\n", id));
            break;
    }
}










/********************************************************************************
  Handler for reset request.
  reset can be initiated from 
  "AT+reset=1"
  "AT+reset=2"
  "AT*FLOW=on/OFF" - If flow change required
  *********************************************************************************/

void Initiate_device_reset_delay( void )
{
    MessageCancelAll( app_data.task, KE_SPP_APP_RESET_DELAY_MSG);
    
    MessageSendLater( app_data.task, KE_SPP_APP_RESET_DELAY_MSG, 0, DEV_RESET_DELAY_TIME_MS );    
}

void handle_at_Reset(event_at_reset_st *event)
{
	if(event->reset_level == 1)/* soft reset*/
	{
        LOG_INFO(("\n Calling WarmBoot "));
        BootWarm();
	}
	else if(event->reset_level == 2)/* hard reset*/
	{
        Delete_Pairing_and_Settings ();
        
        LOG_INFO(("\n deleted paired dev and user settings "));
        Initiate_device_reset_delay();
	}
}


void Delete_Pairing_and_Settings ( void )
{
		/* delete all paired/authenticated devices */
        ConnectionSmDeleteAllAuthDevices(0); 
	    ConnectionDmBleClearWhiteListReq ();
        
        /* clear all memory locations for user settings */
        Clear_memory_Location( LCN_NAME_LENGTH );  
        Clear_memory_Location( LCN_NAME_STRING );                           
        
        Clear_memory_Location( LCN_PIN );  
        Clear_memory_Location( LCN_IOTYPE );  
        Clear_memory_Location( LCN_MITM );  
        Clear_memory_Location( LCN_DPIN );  
        Clear_memory_Location( LCN_UART_CONFIG );  
        Clear_memory_Location( LCN_FLOW );  
        Clear_memory_Location( LCN_ECHO );  
        Clear_memory_Location( LCN_RESP );  
        Clear_memory_Location( LCN_DCOV );  
        Clear_memory_Location( LCN_PAIR );  
        Clear_memory_Location( LCN_FW_VERSION ); 
        
        Clear_memory_Location( LCN_PIN_LENGTH );
        
        Clear_memory_Location( LCN_ADD_ADDRESS_BYTES_IN_NAME );
        
        Clear_memory_Location( LCN_BLE_WHITE_LIST_ENB );
        Clear_memory_Location( LCN_BLE_CLIENT_AUTOSCAN_SERVER );
        Clear_memory_Location( LCN_LE_BOND_BDADDR );
        Clear_memory_Location( LCN_BLE_CLIENT_AUTOCONN_PERIPHERAL );
        Clear_memory_Location( LCN_LE_ADD_FILTER );
        
        Clear_memory_Location( LCN_BLE_ROLE );
        
}


void resetSppApp ( void )
{
    LM_DEBUG(("\n+resetSppApp "));
    
    /*
    LedConfigure(LED_0, LED_ENABLE, 1);     
    LedConfigure(LED_1, LED_ENABLE, 1); 
    LedConfigure(LED_2, LED_ENABLE, 1); 
    LedConfigure(LED_3, LED_ENABLE, 1); 
    LM_DEBUG(("\n+ TURN on ALL THE LEDs..... "));     
   */
    
    if( app_config.flow == FALSE )
    {
        LM_DEBUG(("\n call to BootSetmode with Flow Control OFF"));
        BootSetMode(BOOT_MODE_FLOW_CONTROL_OFF);
    }
	else 
    {
        LM_DEBUG(("\n call to BootSetmode with Flow Control ON"));
        BootSetMode(BOOT_MODE_FLOW_CONTROL_ON);
    }
    
} 




/*************************************************************************************
  Implementation Controlling Dual advertisements
  ************************************************************************************/

void  Stop_advertising_after_connect      ( void )
{
    LM_DEBUG(("\n Stop_advertising_after_connect "));  

    ConnectionDmBleSetAdvertiseEnable( FALSE ); 
}


void  Start_DualAdvert_AfterDisconnect    ( void )
{
    LM_DEBUG(("\n Start_DualAdvert_AfterDisconnect "));  

    ConnectionDmBleSetAdvertiseEnable( TRUE );
    
    Update_LED_state();
}
