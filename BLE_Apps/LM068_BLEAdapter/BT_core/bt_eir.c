
#include "../CSR_LIB.h"
#include "../common.h"

#include "../debug.h"

#include "bt_core.h"
#include "bt_private.h"

/* EIR tags */
#define EIR_TYPE_UUID16_PARTIAL             (0x02)
#define EIR_TYPE_UUID16_COMPLETE            (0x03)
#define EIR_TYPE_UUID32_PARTIAL             (0x04)
#define EIR_TYPE_UUID32_COMPLETE            (0x05)
#define EIR_TYPE_UUID128_PARTIAL            (0x06)
#define EIR_TYPE_UUID128_COMPLETE           (0x07)
#define EIR_TYPE_LOCAL_NAME_SHORT           (0x08)
#define EIR_TYPE_LOCAL_NAME_COMPLETE        (0x09)
#define EIR_TYPE_INQUIRY_TX                 (0x0A)



