#include <connection.h>
#include "../CSR_LIB.h"
#include "../common.h"
#include "../events.h"
#include "../AT_parse/AT_Parse.h"
#include "../config/config.h"

#include "../BT_core/bt_core.h"

#include "../debug.h"

#include "../LED/LED.h"

#include "BLE_Cen_core.h"

#include <stdio.h> 



/*
#define MATH_DEBUG  1
*/
#ifdef  MATH_DEBUG  
#define math_DEBUG(x)             printf x
#else
#define math_DEBUG(x)
#endif











/* assumes the string is only 4 character wide,
   checks 0 to 9 and A to F , and a to f chars   
   updates the numer in num_ptr  
   returns TRUE if valid number found
   returns FALSE if in-valid string found
   0 to 9 >> 0x30 to 0x39
   A to F >> 0x41 to 0x46
   a to f >> 061  to 0x66
*/
bool convert_string_to_uint16 ( const uint8 *str_ptr, uint16* num_ptr )
{
    uint16  i=0;
    uint8 val[4] = {0,0,0,0};    
    
    for(i=0; i<4; i++)
    {
        math_DEBUG(("\n value=%x", *(str_ptr+i)));
        
            if( ((*(str_ptr+i)) >= '0') &&  ((*(str_ptr+i)) <= '9') ) 
               val[i] = (*(str_ptr+i)) - '0' ;
            else if( ((*(str_ptr+i)) >= 'A') && ((*(str_ptr+i)) <= 'F') )
                val[i] = (*(str_ptr+i)) - 'A' + 10;
            else if( ((*(str_ptr+i)) >= 'a') && ((*(str_ptr+i)) <= 'f') )
                val[i] = (*(str_ptr+i)) - 'a' + 10;   
            else
            {
                  math_DEBUG(("\n break at %d ", i));    
                return FALSE;
            }
    }
    
    if(i==4)
    {
           *num_ptr =  val[3] + 
                    (val[2] * 16 ) + 
                    (val[1] * 256 ) + 
                    (val[0] * 4096 );
        
        math_DEBUG(("\n number = %x ", *num_ptr ));
        
        return  TRUE;
    }
    
   return FALSE;
}





/*end of file*/

