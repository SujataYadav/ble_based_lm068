
#ifndef BLEcore_h
#define BLEcore_h

#include "../CSR_LIB.h"

#include "../events.h"

/*******************************************************************************
  ******************************************************************************/

/* --------------- Local definitions -------------- */

#define         GAP_CENTRAL_ROLE        1
#define         GAP_PERIPHERAL_ROLE     2


#define     UC_MAX_NUM_OF_MANAGED_SERVICES          ((uint8)5)
#define     UC_MAX_NUM_OF_MANAGED_CHARACTERISTICS   ((uint8)5)



#define		MAX_BLEin_DATALENGTH                     ((uint8)30)



/* Max num of found devices that can be saved in a list */
#define     UC_MAX_NUM_OF_BLE_SCAN_DEVICES              ((uint8)10)     /* as this bridge only, 10 here, else 8*/



/* properties of the characteristic, this is read by maaster in CHAR_RAD_CFM_T structure from BLE slave */
#define     CHAR_PROP_BROADCAST     0x01
#define     CHAR_PROP_READ          0x02
#define     CHAR_PROP_WRITECMD      0x04
#define     CHAR_PROP_WRITEREQ      0x08
/*#define CHAR_PROP_WRITE     0x4c*/   /* threee types of write */
#define     CHAR_PROP_CCFG          0x10
#define     CHAR_PROP_INDICATION    0x20
#define     CHAR_PROP_WRITE_SIGNED  0x40
#define     CHAR_PROP_EXTENDED      0x80






/*  Desciption obtained from U-Energy documentation for attribute 
*   "CS-317146-SP_CSRuEnergyFirmwareLibraryDocumentationAPIDescription.pdf" Section 47.1.2
#define ATT_PERM_WRITE_CMD              0x04        If set, permit writes of the Characteristic Value without response.
#define ATT_PERM_WRITE_REQ              0x08        If set, permits writes of the Characteristic Value with response.
#define ATT_PERM_WRITE_SIGNED           0x40        If set, permits signed writes to the Characteristic Value.
#define ATT_PERM_CONFIGURE_BROADCAST    0x01        If set, permits broadcasts of the Characteristic Value using Characteristic Configuration Descriptor
#define ATT_PERM_EXTENDED               0x80        If set, additional characteristic properties are defined in the Characteristic Extended Properties Descriptor.
#define ATT_PERM_INDICATE               0x20        If set, permits indications of a Characteristic Value with acknowledgement.
#define ATT_PERM_NOTIFY                 0x10        If set, permits notifications of a Characteristic Value without acknowledgement.
#define ATT_PERM_READ                   0x02        If set, permits reads of the Characteristic Value.
*/






/* --------------- Local enums declaration -------------- */


/*! BLE advertising types enum */
typedef enum
{
    AD_TYPE_FLAGS                       = 0x01,   /*!< Advertising Data Flags as defined in CSS v4 Part A Section 1.3.*/
    AD_TYPE_SERVICE_UUID_16BIT          = 0x02,   /*!< Incomplete list of 16-bit Service Class UUIDs.                 */
    AD_TYPE_SERVICE_UUID_16BIT_LIST     = 0x03,   /*!< Complete list of 16-bit Service Class UUIDs.                   */
    AD_TYPE_SERVICE_UUID_32BIT          = 0x04,   /*!< Incomplete list of 32-bit Service Class UUIDs.                 */
    AD_TYPE_SERVICE_UUID_32BIT_LIST     = 0x05,   /*!< Complete list of 32-bit Service Class UUIDs.                   */
    AD_TYPE_SERVICE_UUID_128BIT         = 0x06,   /*!< Incomplete list of 128-bit Service Class UUIDs.                */
    AD_TYPE_SERVICE_UUID_128BIT_LIST    = 0x07,   /*!< Complete list of 128-bit Service Class UUIDs.                  */
    AD_TYPE_LOCAL_NAME_SHORT            = 0x08,   /*!< Shortened local device name. This supplied data must match the
                                                       first <I>n</I> characters of the complete name. See CSS v4
                                                       Part A Section 1.2.                                            */
    AD_TYPE_LOCAL_NAME_COMPLETE         = 0x09,   /*!< Complete local device name. See CSS v4 Part A Section 1.2.     */
    AD_TYPE_TX_POWER                    = 0x0A,   /*!< Transmitted power level. May be used with received RSSI to
                                                       estimate the RF path loss, see CSS v4 Part A Section 1.5.      */
    AD_TYPE_OOB_DEVICE_CLASS            = 0x0D,   /*!< Class of Device. See Bluetooth Assigned Numbers,
                                                       https://www.bluetooth.org/assigned-numbers.                    */
    AD_TYPE_OOB_SSP_HASH_C_192          = 0x0E,   /*!< Out of Band simple pairing hash, (P-192 elliptic curve). To be
                                                       sent OOB only, not over the air.                               */
    AD_TYPE_OOB_SSP_RANDOM_R_192        = 0x0F,   /*!< Out of Band simple pairing randomizer, (P-192 elliptic curve).
                                                       To be sent OOB only, not over the air.                         */
    AD_TYPE_SM_TK                       = 0x10,   /*!< Security Manager TK value. To be sent OOB only, not over the
                                                       air. See CSS v4 Part A Section 1.8.                            */
    AD_TYPE_SM_FLAGS                    = 0x11,   /*!< Security Manager Out of Band Flags. To be sent OOB only, not
                                                       over the air. See CSS v4 Part A Section 1.7.                   */
    AD_TYPE_SLAVE_CONN_INTERVAL         = 0x12,   /*!< Slave connection interval range. See CSS v4 Part A Section 1.9.*/
    AD_TYPE_SIGNED_DATA                 = 0x13,
    AD_TYPE_SERVICE_SOLICIT_UUID_16BIT  = 0x14,   /*!< List of 16-bit Service Solicitation UUIDs.                     */
    AD_TYPE_SERVICE_SOLICIT_UUID_128BIT = 0x15,   /*!< List of 128-bit Service Solicitation UUIDs.                    */
    AD_TYPE_SERVICE_DATA_UUID_16BIT     = 0x16,   /*!< Service Data - 16 bit UUID. See CSS v4 Part A Section 1.11.    */
    AD_TYPE_PUBLIC_TARGET_ADDRESS       = 0x17,   /*!< Public target address. See CSS v4 Part A Section 1.13.         */
    AD_TYPE_RANDOM_TARGET_ADDRESS       = 0x18,   /*!< Random target address. See CSS v4 Part A Section 1.14.         */
    AD_TYPE_APPEARANCE                  = 0x19,   /*!< The Appearance data type defines the external appearance
                                                       of the device. See CSS v4 Part A Section 1.12.                 */
    AD_TYPE_ADVERTISING_INTERVAL        = 0x1A,   /*!< Advertising interval. See CSS v4 Part A Section 1.15.          */
    AD_TYPE_LE_BT_ADDRESS               = 0x1B,   /*!< Local device's Bluetooth address and type. To be sent OOB only,
                                                       not over the air. See CSS v4 Part A Section 1.16.              */
    AD_TYPE_LE_ROLE                     = 0x1C,   /*!< Role of the local device. To be sent OOB only,
                                                       not over the air. See CSS v4 Part A Section 1.17.              */
    AD_TYPE_OOB_SSP_HASH_C_256          = 0x1D,   /*!< Out of Band simple pairing hash, (P-256 elliptic curve). To be
                                                       sent OOB only, not over the air.                               */
    AD_TYPE_OOB_SSP_RANDOM_R_256        = 0x1E,   /*!< Out of Band simple pairing randomizer, (P-256 elliptic curve).
                                                       To be sent OOB only, not over the air.                         */
    AD_TYPE_SERVICE_SOLICIT_UUID_32BIT  = 0x1F,   /*!< List of 32-bit Service Solicitation UUIDs.                     */
    AD_TYPE_SERVICE_DATA_UUID_32BIT     = 0x20,   /*!< Service Data - 32 bit UUID. See CSS v4 Part A Section 1.11.    */
    AD_TYPE_SERVICE_DATA_UUID_128BIT    = 0x21,   /*!< Service Data - 128 bit UUID. See CSS v4 Part A Section 1.11.   */
    AD_TYPE_MANUF                       = 0xFF    /*!< Manufacturer specific data. See CSS v4 Part A Section 1.4.     */
} ke_advType;


/*! local connection status enum */
typedef enum
{
    KE_CONN_ISSUED,
    KE_CONN_PROGRESS,    
    KE_CONNECTED,    
    KE_CONN_CLOSED
    
} connectionStatus_et;


void send_BLE_data_packet ( void );
void 	HandleAccessWrite			(GATT_ACCESS_IND_T *p_ind);
void    set_BLE_Advertising_Parameters ( void );

void 	SerialHandleAccessWrite		(GATT_ACCESS_IND_T *p_ind);
void 	HandleAccessRead			(GATT_ACCESS_IND_T *p_ind);

/*******************************************************************************
    --------------- Local structure declaration -------------- 
 ******************************************************************************/


/*! BLE connection data */
typedef struct
{
    char DualconnBuff[MAX_BLEin_DATALENGTH];
    
} LeDataBuff_st;


/*! Server services characteristics data */
typedef struct
{
    uint16              ui16Handle;
    uint8               ui8Properties;
} st_ServiceChar;


/*! Server services data */
typedef struct
{
    uint16              ui16StartHandle;
    uint16              ui16EndHandle;
    uint8               ui8NextFreeCharIndex; /* number of characteristics found on the service, 5=0,1,2,3,4 chars */
} st_FoundServices;







/*! BLE connection data */
typedef struct
{
    uint16              ui16ConnectionID;
    
    typed_bdaddr        stConnectedDeviceAddress ;
    connectionStatus_et eConnStatus;
    
        
    uint16              ui16ConnFlags;
    uint16              ui16ConnMTU;
    
} BLEConnectionData_st;





/*! BLE info data */
typedef struct
{
    bool                bGATTInitStatus;                        
    /* master/slave reconnectability, GATT initialisation only once in lifespan*/
    bool                WhiteListEnable;                        
    /* true= whiteListEnabled, False=whiteListDiasbled, Default=B_FALSE */
    
    bool                BLEautoScan;
    bool                BLEautoConnect;
    bool                Serial_Service_Found;
    bool                BLEApplyFilter;
	
	uint8               BLE_Role;       /* 1=Central,  2=Peripheral */
    
    ble_local_addr_type  Peri_addr_type;

} BLEInfoData_st;

typedef struct 
{
    uint16      CharStartHandle;
    uint16      CharEndHandle;
    
    uint16      SerialCharHandle  ;  
    bool        WR_and_CCFG_char_found;
    
    bool        Serial_CCFG_enabled;
    

    
} SerialServiceData_st;

extern bool        BLE_Adaper_ready;

/****************************************************************************************************************************/
/* --------------- variables for BLE -------------- */

/*! BLE connection data */     
extern      BLEConnectionData_st    *stBLEConnectionData;


extern      BLEInfoData_st          *stBLEInfoData;

extern      SerialServiceData_st     *SerialServiceData; /* memory allocated only if Serial service is found */

extern const ble_connection_params      stDefaultConnAdvertParams;

/****************************************************************************************************************************/

void BLE_Initialise     ( void );

void handle_BLE_message             ( Task task, MessageId messageId, Message message );
void handle_DM_BLE_message          (            MessageId messageId, Message message );
void handle_at_BLE_event_message    ( Task task, MessageId messageId, Message message );

void set_BLEadvertising_report_filter( void );

void Read_LE_Security_fromPS ( void );
void Read_LE_AutoScan_fromPS ( void );
void Read_LE_AutoConnect_fromPS (void );
void Read_LE_AddFilter_fromPS (void );


void Reset_BLEScannedDev    ( void );

void handle_GapCentral_autoConn (void);
void handleGapCentralAutoConnect_AfterDisconnet( void );

void BLE_Auto_FindServices( void );

bool request_GATTservice_scan ( uint8 connID,  uint8 servID, bool all );



/*ble_math.c*/
bool convert_string_to_uint16 ( const uint8 *str_ptr, uint16* num_ptr );






void handle_at_ble_conn     (event_at_ble_ConDiscon_st *event);
void handle_at_ble_disconn  (event_at_ble_ConDiscon_st *event);







/*******************************************************************************
 *******************************************************************************
    Variables for ring buffer
 *******************************************************************************
 *******************************************************************************/

#define     CBUFF_BUFFER_SIZE_254       100
#define     CBUFF_BUFFER_SIZE_100       50

#define     RING_BUFFER_SIZE            (2*CBUFF_BUFFER_SIZE_254)    /* need to consider the 254+254+100 */


typedef struct codebuff_st_
{
        uint8  buff[CBUFF_BUFFER_SIZE_254]; 
}      codebuff_st;
extern codebuff_st      *codebuff_1, *codebuff_2;


typedef struct codebuff100_st_
{
        uint8  buff[CBUFF_BUFFER_SIZE_100]; 
}      codebuff100_st;
/**/extern codebuff100_st      *codebuff100_3;


typedef struct  {
    
    uint8 *buffer;
    
    uint8 head;
    uint8 tail; 

    bool full;

}circular_buf_st;


typedef struct  {
    
    circular_buf_st *C1;  /*linked to codebuff_1*/
    circular_buf_st *C2;  /*linked to codebuff_2*/
    circular_buf_st *C3;  /*linked to codebuff100_3which is 100_bytes */ 
        
    uint8   full_count ;
    uint8   pointer ;
    uint8   Read_pointer ;
    bool    Clear_flag ;
        
}Ring_st;


void Ring_buf_reset(Ring_st *rRing);

/*******************************************************************************
 ******************************************************************************/




#endif /* BLEcore_h */


