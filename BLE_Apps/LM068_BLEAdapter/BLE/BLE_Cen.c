
#include "../CSR_LIB.h"
#include "../common.h"
#include "../events.h"
#include "../AT_parse/AT_Parse.h"
#include "../config/config.h"

#include "../BT_core/bt_core.h"

#include "../debug.h"

#include "../LED/LED.h"


#include "BLE_Cen_core.h"




uint16 BLE_Scan_InProcess = 0xff;

/*******************************************************************************
  handlers for AT command requests for BLE 
  ******************************************************************************/

static void handle_at_ble_AddFilter_advertiseReport(event_BLE_WList_st *event)
{
    
   status_et status = FALSE ;

    LE_INFO(("\n handle_at_ble_AddFilter_advertiseReport ")); 
      
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LE_FILTER", "b",stBLEInfoData->BLEApplyFilter));
        return;
    }
    else
    {
        if( event->enable != stBLEInfoData->BLEApplyFilter )
        {
            stBLEInfoData->BLEApplyFilter = event->enable;
            status = Store_memory_Location ( LCN_LE_ADD_FILTER, (const uint16*)&stBLEInfoData->BLEApplyFilter, sizeof(stBLEInfoData->BLEApplyFilter) );
        }
        
                if(stBLEInfoData->BLEApplyFilter == TRUE)
                            {
                                if(BLE_Scan_InProcess == FALSE)
                                set_BLEadvertising_report_filter(); 
                                else
                                {
                                    MOCK( at_parser_send_err_response() );   
                                    return;
                                }
                            }        
        
        MOCK(at_parser_send_ok_response());               
        return;
    }

    MOCK( at_parser_send_err_response() );    
}



static void handle_at_ble_discovery( event_BLE_discovery_st *event)
{
    LE_INFO(("\n ble_Find = "));  
    
        /* if scanning is requested */
        if( event->enable==TRUE  && BLE_Scan_InProcess!=0x01 )
        {
                LE_INFO(("ON "));
                BLE_Scan_InProcess = 0x01 ;
                
                if(stBLEInfoData->BLEApplyFilter == TRUE)        
                set_BLEadvertising_report_filter(); 
            
                ConnectionDmBleSetScanEnable( TRUE ); /* Only Scan is enabled here, GATT inited already */
            
                MOCK(at_parser_send_ok_response());
                return;
        }
        /* stop scanning */
        else if( event->enable==FALSE && BLE_Scan_InProcess == 0x01 ) /* do not process blefind- command if not in blefind+ */
        {            
                /* be sure to disable an eventual scanning operation */        
                ConnectionDmBleSetScanEnable( FALSE );
                LE_INFO(("OFF "));
                
                Reset_BLEScannedDev();
                /* BLE_Scan_InProcess = 0x00;    stop scanning */ 
                
                MOCK(at_parser_send_ok_response());
                return;
        }
     
    MOCK(at_parser_send_err_response());
}


void handle_at_ble_conn(event_at_ble_ConDiscon_st *event)
{
    typed_bdaddr  taddr;
    taddr.type = 0;
    LE_INFO(("\n handle_at_ble_conn"));
    
    if( stBLEConnectionData->eConnStatus == KE_CONN_CLOSED ) 
    {
            memcpy(&taddr.addr, &event->addr, sizeof(event->addr) );
            LE_DEBUG(("\n sending LE_conn to bd_addr=%04x-%02x-%06lx ", taddr.addr.nap, taddr.addr.uap, taddr.addr.lap ));
            
            /* need to stop scanning before getting connected, 
                if user does not isue LEfind=off, we must do it here */
            ConnectionDmBleSetScanEnable( FALSE );
            Reset_BLEScannedDev();
            
            GattConnectRequest( app_data.task,
                                (const typed_bdaddr *) &(taddr),
                                gatt_connection_ble_master_directed,
                                TRUE);
            
            stBLEConnectionData->eConnStatus =   KE_CONN_ISSUED;  
             
            MOCK(at_parser_send_ok_response()); 
    }
    else    
    MOCK(at_parser_send_err_response());
}

void handleGapCentralAutoConnect_AfterDisconnet( void )
{
    bdaddr addr;
    event_at_ble_ConDiscon_st *msg = (event_at_ble_ConDiscon_st *)malloc(sizeof(*msg));
    event_BLE_discovery_st *find_msg = (event_BLE_discovery_st *)malloc(sizeof(*find_msg));
     
     LE_INFO(("\n handleGapCentralAutoConnect_AfterDisconnet "));
     
    BdaddrSetZero (&addr);
    BdaddrSetZero (&(msg->addr));
    
    if( stBLEInfoData->BLEautoConnect == TRUE && stBLEInfoData->bGATTInitStatus == TRUE )
    {
         read_BDaddr ( LCN_LE_BOND_BDADDR, &(addr) );
         
         if ( BdaddrIsZero( (const bdaddr *)&addr)	== TRUE ) /* TRUE if the address passed is zero */
         {
                                      
                    LE_INFO(("\n LE_Bond_adr=0, start_LeFind  >>>>>>>>> "));
                    find_msg->enable = TRUE;
                    handle_at_ble_discovery(find_msg);
         }
         else
         {
                    memcpy(&(msg->addr), &addr,  sizeof(addr) );
                    LE_INFO(("\n sending LE_conn to bd_addr=%04x-%02x-%06lx ", msg->addr.nap, msg->addr.uap, msg->addr.lap ));
                    handle_at_ble_conn(msg);
                    MessageCancelAll(app_data.task, KE_WAIT_BEFORE_GAPCENTRAL_AUTOCONN_MSG);
         }
    } 
    
    free(msg);
    free(find_msg);
}

void handle_GapCentral_autoConn (void)
{
  LE_DEBUG(( "\n handle_GapCentral_autoConn " ));
  
  
   if( stBLEInfoData->BLEautoConnect == TRUE && stBLEInfoData->bGATTInitStatus == TRUE ) 
  {
       MessageSendLater(app_data.task,  KE_WAIT_BEFORE_GAPCENTRAL_AUTOCONN_MSG, 0, KE_GAPCENTRAL_AUTOCONN_WAIT_MS );  /*  */
        LE_DEBUG(( "\n trigger KE_WAIT_BEFORE_GAPCENTRAL_AUTOCONN_MSG " ));
   }
}

 

void handle_at_ble_disconn(event_at_ble_ConDiscon_st *event)
{

    PRINT_BDADDR(event->addr)   ;     
    PRINT_BDADDR(stBLEConnectionData->stConnectedDeviceAddress.addr);

    if( TRUE == BdaddrIsSame ( &stBLEConnectionData->stConnectedDeviceAddress.addr , &event->addr ) ) 
    {
        if( KE_CONNECTED == stBLEConnectionData->eConnStatus || KE_CONN_PROGRESS == stBLEConnectionData->eConnStatus)
        {
            LE_DEBUG(( "\n issue Dis-conn req for stBLEConnectionData->ui16ConnectionID = 0x%x ", stBLEConnectionData->ui16ConnectionID ));
        
            GattDisconnectRequest( stBLEConnectionData->ui16ConnectionID );
        
            MOCK(at_parser_send_ok_response());  
            return;
        }
    }
   
    MOCK(at_parser_send_err_response());
}



static void handle_at_ble_findserv(event_BLE_findServ_st *event)
{   
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )
    {     
        GattDiscoverAllPrimaryServicesRequest(app_data.task, stBLEConnectionData->ui16ConnectionID);
        MOCK(at_parser_send_ok_response());         
    }
    else
    MOCK(at_parser_send_STATEerr_response());   
}


/* 
  at*findchar=conn-ID, start-handle, end-handle
  AT*findCHAR=1,0000,0002
  at*findChar=2,001b,002C
*/
static void handle_at_ble_findchar ( event_BLE_findchar_st *event )
{
    LE_INFO(("\n handle_at_ble_findchar"));   

    /*BLE parser check s the validity of connection ID and is the 
      start-handle and end-handle are valid numbers.
     It does not check w.r.t  valid rabnge as it does not store anything in its own memory */    
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )
    {
         LE_DEBUG(("\n GATT scann-chars for conn-ID=%d, start=%d  end=%d ", event->connID, event->start, event->end));
    
         GattDiscoverAllCharacteristicsRequest( app_data.task, stBLEConnectionData->ui16ConnectionID, event->start, event->end );
    
         MOCK(at_parser_send_ok_response()); 
    }
    else
    MOCK(at_parser_send_STATEerr_response());      
   
}

static void handle_at_ble_ReadcharValue( event_BLE_rdcharval_st *event )
{
    LE_INFO(("\n handle_at_ble_ReadcharValue"));   

    /* at*rdCharVal=connID,char-Handle
        at*rdCharVal=1,0018  (0x0018)*/    
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )
    {
         LE_DEBUG(("\n GATT scann-chars for conn-ID=%d, char-handle=%d ", event->connID, event->char_handle ));
         
         GattReadCharacteristicValueRequest( app_data.task, stBLEConnectionData->ui16ConnectionID, event->char_handle );

            MOCK(at_parser_send_ok_response()); 
    }
   else 
    MOCK(at_parser_send_STATEerr_response());
}


static void handle_at_ble_ReadLongcharValue( event_BLE_rdcharval_st *event )
{
    LE_INFO(("\n handle_at_ble_ReadLongcharValue"));   

    /*
        at*rdCharVal=connID,char-Handle
        at*rdCharVal=1,0018  (0x0018)
    */    
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )
    {
         LE_DEBUG(("\n GATT scann-chars for conn-ID=%d, char-handle=%d ", event->connID, event->char_handle ));
         
         GattReadLongCharacteristicValueRequest( app_data.task, stBLEConnectionData->ui16ConnectionID, event->char_handle );

            MOCK(at_parser_send_ok_response()); 
    }
    else
    MOCK(at_parser_send_STATEerr_response());
}


static void handle_at_ble_EnableCCFG( event_BLE_rdcharval_st *event )
{
    uint8 value[2] = {0,0};  /* for enb v[1]=0, v[0]=1*/
#define CCFG_DATA_LENGTH    0x0002  /* as per protocol, CCFG value is 2 butes=16bit, with LSB=01_for_enable*/ 
        
    
    LE_INFO(("\n handle_at_ble_EnableCCFG"));   

    /*  at*enbCCFG=connID,char-Handle
        at*rdCharVal=1,0018  (0x0018)
        we need to write 2 bytes at (handle+1) 0and1 */    
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )
    {
         LE_DEBUG(("\n enb ccfg for conn-ID=%d, char-handle=%d ", event->connID, event->char_handle ));
         
        value[0] = 1;
        GattWriteCharacteristicValueRequest ( app_data.task,
                                              stBLEConnectionData->ui16ConnectionID, 
                                              (event->char_handle+1),
                                              CCFG_DATA_LENGTH,
                                              value ); 
        
        MOCK(at_parser_send_ok_response()); 
    }
   else 
    MOCK(at_parser_send_STATEerr_response());
}


static void handle_at_ble_WritecharValue( event_BLE_wrcharval_st *event )
{   
    uint8 i=0;
    LE_INFO(("\n handle_at_ble_WritecharValue"));   
    
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )    
    {
       LE_DEBUG(("\n write_char_val ID=%d, handle=%d, size=%d ", event->connID, event->char_handle, event->size ));
       
       for(i=0; i<(event->size); i++ )
           LE_DEBUG((" %c", event->val_buff[i] ));
       
       GattWriteCharacteristicValueRequest(	app_data.task, stBLEConnectionData->ui16ConnectionID, event->char_handle,
                                            event->size, event->val_buff );
       
        MOCK(at_parser_send_ok_response()); 
    }
    else
    MOCK(at_parser_send_STATEerr_response());
}           


static void handle_at_ble_Write_without_resp( event_BLE_wrcharval_st *event )
{   
    LE_INFO(("\n handle_at_ble_Write_without_resp"));   
    
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )    
    {
       LE_DEBUG(("\n write_char_val ID=%d, handle=%d, size=%d ", event->connID, event->char_handle, event->size ));
       
        GattWriteWithoutResponseRequest	(	app_data.task, 
                                            stBLEConnectionData->ui16ConnectionID, 
                                            event->char_handle,
                                            event->size, 
                                            event->val_buff );
       
        MOCK(at_parser_send_ok_response()); 
    }
    else
    MOCK(at_parser_send_STATEerr_response());
} 


static void handle_at_ble_signed_Write_without_resp( event_BLE_wrcharval_st *event )
{   
    uint8 i=0;
    LE_INFO(("\n handle_at_ble_signed_Write_without_resp "));   
    
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )    
    {
       LE_DEBUG(("\n write_char_val ID=%d, handle=%d, size=%d ", event->connID, event->char_handle, event->size ));
       
       for(i=0; i<(event->size); i++ )
           LE_DEBUG((" %c", event->val_buff[i] ));
       
        GattSignedWriteWithoutResponseRequest	(	app_data.task, 
                                            stBLEConnectionData->ui16ConnectionID, 
                                            event->char_handle,
                                            event->size, 
                                            event->val_buff );
       
        MOCK(at_parser_send_ok_response()); 
    }
    else
    MOCK(at_parser_send_STATEerr_response());
} 

static void handle_at_BLE_IndicatioResponse( event_BLE_IndicationResp_st *event )
{
    LE_INFO(("\n handle_at_BLE_IndicatioResponse"));   
    
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )    
    {      
         GattIndicationResponse	( stBLEConnectionData->ui16ConnectionID );
       
        MOCK(at_parser_send_ok_response()); 
    }
    else
    MOCK(at_parser_send_STATEerr_response());    
    
}


static void handle_at_ble_ReadMultipleCharValue( event_BLE_RdMultChar_st *event )
{
    uint16 i=0;
    LE_INFO(("\n handle_at_ble_ReadMultipleCharValue"));   

    
    LE_DEBUG(("\n for ConnID=%d read_%d_Values for handles 1=%d, 2=%d, 3=%d, 4=%d, 5=%d", 
                event->size,                 
                 event->size, 
                 event->char_handle[0], event->char_handle[1], event->char_handle[2], event->char_handle[3], event->char_handle[4] ));
  
           
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )    
    {
       for(i=0; i<(event->size); i++ )
           LE_DEBUG((" %d", event->char_handle[i] ));
       
        GattReadMultipleCharacteristicValuesRequest	(	app_data.task, 
                                            stBLEConnectionData->ui16ConnectionID, 
                                            event->size, 
                                            event->char_handle );
       
        MOCK(at_parser_send_ok_response()); 
    }
    else
    MOCK(at_parser_send_STATEerr_response());   
}




static void handle_at_ble_Whitelist(event_BLE_WList_st *event)
{
    status_et status = FALSE ;

    LE_INFO(("\n handle_at_ble_Whitelist")); 
      
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LE_SEC", "b",stBLEInfoData->WhiteListEnable));
                return;
    }
    else
    {
        if( event->enable != stBLEInfoData->WhiteListEnable )
        {
            stBLEInfoData->WhiteListEnable = event->enable;
        
            if(stBLEInfoData->BLE_Role == GAP_CENTRAL_ROLE)
            {
                    ConnectionDmBleSetScanParametersReq ( TRUE,	
                                                  TRUE,	
                                                ( (stBLEInfoData->WhiteListEnable == TRUE ) ? TRUE : FALSE ),	
                                                stDefaultConnAdvertParams.scan_interval,    
                                                stDefaultConnAdvertParams.scan_window    );
            }
            else
            {
                 set_BLE_Advertising_Parameters();    
            }
            
            status = Store_memory_Location ( LCN_BLE_WHITE_LIST_ENB, (const uint16*)&stBLEInfoData->WhiteListEnable, sizeof(stBLEInfoData->WhiteListEnable) );
        }
        MOCK(at_parser_send_ok_response());
        return;
    }

    MOCK( at_parser_send_err_response() );    
}


static void handle_at_ble_AutoConnectPeripheral(event_BLE_AutoScan_st *event)
{
   status_et status = FALSE ;

    LE_INFO(("\n handle_at_ble_AutoConnectPeripheral")); 
      
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LE_AUTOCONN_PERIPH", "b", stBLEInfoData->BLEautoConnect));
        return;
    }
    else
    {
        if( event->enable != stBLEInfoData->BLEautoConnect )
        {
            stBLEInfoData->BLEautoConnect = event->enable;
            
            status = Store_memory_Location ( LCN_BLE_CLIENT_AUTOCONN_PERIPHERAL, (const uint16*)&stBLEInfoData->BLEautoConnect, sizeof(stBLEInfoData->BLEautoConnect) );
        }
        MOCK(at_parser_send_ok_response());
        return;
    }  
        
    
    MOCK( at_parser_send_err_response() );  
}       

static void handle_at_ble_AutoScanServer(event_BLE_AutoScan_st *event)
{
   status_et status = FALSE ;

    LE_INFO(("\n handle_at_ble_AutoScanServer")); 
      
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LE_AUTOSCAN_SERVER", "b", stBLEInfoData->BLEautoScan));
        return;
    }
    else
    {
        if( event->enable != stBLEInfoData->BLEautoScan )
        {
            stBLEInfoData->BLEautoScan = event->enable;
            
            status = Store_memory_Location ( LCN_BLE_CLIENT_AUTOSCAN_SERVER, (const uint16*)&stBLEInfoData->BLEautoScan, sizeof(stBLEInfoData->BLEautoScan) );
        }
        MOCK(at_parser_send_ok_response());
        return;
    }  
        
    
    MOCK( at_parser_send_err_response() );  
}

static void handle_BLEConnInfo_request( void *ptr )
{   
    MOCK(at_parser_send_ok_response());
    
    uart_tx( (const char*)"REP*:BLE_CONN \n", strlen("REP*:BLE_CONN \n") );


        if( KE_CONN_CLOSED == stBLEConnectionData->eConnStatus)
        {
                        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "", " u : s ",
                                              (uint32) (1),
                                              "Open" ,
                                              strlen("Open")  ));
        }
        else
        {
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "", " u : B : y ",
                                              (uint32) (1),
                                              &stBLEConnectionData->stConnectedDeviceAddress.addr,
                                              (uint32) stBLEConnectionData->ui16ConnectionID   )); 
        }

    uart_tx( (const char*)"REP*:BLE_CONN END\n", strlen("REP*:BLE_CONN END\n") );
        
}

static void handle_At_BLE_getCidFromBdadr_request( event_at_del_st *event )
{
    uint32 Stack_CID = 0xffffffff ;
    typed_bdaddr taddr;
    
    /* 
    TODO : We may need to update the at command to receive the Type of bdaddress,
              If the type does not matches, the CID receives error evenif the BDaddr is correct 
    */    
    
    MOCK(at_parser_send_ok_response());
   
    memcpy(&(taddr.addr), &(event->addr), sizeof (event->addr));
    taddr.type = 0;
    
    Stack_CID = GattGetCidForBdaddr	( (const typed_bdaddr*)	&taddr );		
    LE_INFO(("\n CID from Stack is Stack_CID=0x%ld", Stack_CID )); 
    LE_INFO(("\n (bd_addr=%04x-%02x-%06lx)\n", taddr.addr.nap, taddr.addr.uap, taddr.addr.lap));    
    
    if(Stack_CID != 0 )
    {
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "GETCID", "B:y", 
                                          &event->addr, 
                                          (uint32) Stack_CID ));
    }
    else
    {
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "GETCID", "B:s", 
                                          &event->addr, 
                                          "Err",
                                          strlen("Err")  ));        
    }
    
}




static void handle_at_ble_ClrWhitelist(event_at_start_stop_discovery_st *event)
{
    LOG_INFO(("\n handle_at_ble_ClrWhitelist"));
    
    ConnectionDmBleClearWhiteListReq();
    MOCK(at_parser_send_ok_response()); 
}



static void handle_at_bleAddressQuery( void )
{
    bdaddr  addr;

    LOG_INFO(("\n handle_at_ble_Whitelist")); 
    
    BdaddrSetZero (&addr);
    
        read_BDaddr( LCN_BLE_ADVERT_ADDRESS,  &addr);
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "BLE_ADDR", "B", &addr ));  
        
}


static void handle_at_ble_GAPRole( event_at_GapRoleType_st *event)
{
    LE_DEBUG(("\n handle_at_ble_GAPRole ")); 
 
    
    if (event->type == OP_GET)
    {
        /* 1= UART, 2=OTA */
        MOCK(at_parser_send_ok_response());
        
         if( stBLEInfoData->BLE_Role == GAP_CENTRAL_ROLE )
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "GAPROLE", "s",  "GAP_CENTRAL", strlen("GAP_CENTRAL")   ));
        
        else if( stBLEInfoData->BLE_Role == GAP_PERIPHERAL_ROLE )
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "GAPROLE", "s",  "GAP_PERIPHERAL", strlen("GAP_PERIPHERAL")   ));  
        
    }
    else
    {
        if( stBLEInfoData->BLE_Role == event->GapRole )
        {
                MOCK(at_parser_send_err_response()); 
        }
        else
        {
                stBLEInfoData->BLE_Role = event->GapRole ;
                Store_memory_Location ( LCN_BLE_ROLE, (const uint16*)&stBLEInfoData->BLE_Role, sizeof(stBLEInfoData->BLE_Role) );
                MOCK(at_parser_send_ok_response());
                
                uart_tx( (const char*)" Module will restart", strlen(" Module will restart") );            

                Initiate_device_reset_delay();
        }
    }     
}
/*******************************************************************************
  messages from At commands from User for BLE
  ******************************************************************************/
void handle_at_BLE_event_message( Task task, MessageId id, Message message )
{
    LE_DEBUG(("\n BLE_AT_msg = 0x%x ", id));   
        switch (id)
    {
        case EventAtLEdiscovery:
            handle_at_ble_discovery((event_BLE_discovery_st *)message);
        break;

        case EventAtLEaddFilter:
            handle_at_ble_AddFilter_advertiseReport((event_BLE_WList_st *)message);
        break;    
        
        case EventAtGapRoleType:
            handle_at_ble_GAPRole((event_at_GapRoleType_st *)message);
        break;        
        
        case EventAtLEconn:
            handle_at_ble_conn((event_at_ble_ConDiscon_st *)message);
        break;
        
        case EventAtLEdisconn:
            handle_at_ble_disconn((event_at_ble_ConDiscon_st *)message);
        break;
        
        case EventAtLEConnInfo:
            handle_BLEConnInfo_request( (void *)message);
        break;
        case EventAtLEfindServ:
            handle_at_ble_findserv((event_BLE_findServ_st *)message);
        break;
        
        case EventAtLEfindChar:
            handle_at_ble_findchar((event_BLE_findchar_st *)message);
        break;
       
        case EventAtLEReadCharValue:
            handle_at_ble_ReadcharValue((event_BLE_rdcharval_st *)message);
        break;
        
        case EventAtLEReadLongCharValue:
            handle_at_ble_ReadLongcharValue((event_BLE_rdcharval_st *)message);
        break;


        case EventAtLEnbCCFG:
            handle_at_ble_EnableCCFG ((event_BLE_rdcharval_st *)message);
        break;
        
        
        case EventAtLEWriteCharValue:
            handle_at_ble_WritecharValue((event_BLE_wrcharval_st *)message);
        break;
        
        case EventAtLEWriteWoResp:
            handle_at_ble_Write_without_resp((event_BLE_wrcharval_st *)message);
        break;
        
        case EventAtLEsignedWriteWoResp:
            handle_at_ble_signed_Write_without_resp((event_BLE_wrcharval_st *)message);
        break;
        
        case EventAtLEIndicationResp:
            handle_at_BLE_IndicatioResponse((event_BLE_IndicationResp_st *)message);
        break;
        
        case EventAtGetCIDfromBdaddr:
            handle_At_BLE_getCidFromBdadr_request((event_at_del_st *)message);
        break;
        
        case EventAtLERdMultCharVal:
            handle_at_ble_ReadMultipleCharValue((event_BLE_RdMultChar_st *)message);
        break;
        
        case EventAtLEClearWhitelist:
            handle_at_ble_ClrWhitelist((event_at_start_stop_discovery_st *)message);
        break;
        
        case EventAtLEWhitelist:
            handle_at_ble_Whitelist((event_BLE_WList_st *)message);
        break;


        case EventAtLEAutoConnectPeripheral:
            handle_at_ble_AutoConnectPeripheral((event_BLE_AutoScan_st *)message);
        break;
        
        case EventAtLEAutoScanServer:
            handle_at_ble_AutoScanServer((event_BLE_AutoScan_st *)message);
        break;
        
/***********GAP Peri Funcitons ************/       
        case EventAtBLEaddr:
            handle_at_bleAddressQuery();
        break;
              
/******************************************/        
        
    }
}


