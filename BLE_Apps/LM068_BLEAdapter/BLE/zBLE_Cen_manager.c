#include <connection.h>
#include "../CSR_LIB.h"
#include "../common.h"
#include "../events.h"
#include "../AT_parse/AT_Parse.h"
#include "../config/config.h"



#include "../BT_core/bt_core.h"

#include "../debug.h"

#include "../LED/LED.h"


#include "BLE_Cen_core.h"


#include "../BLE_Periph/BLE_Peri.h"
#include "../BLE_Periph/app_gatt_db.h"


         
#define     SERIL_OVER_GATT_128BIT_UUID     0x00005500d10211e19b2300025b00a5a5
#define     SERIAL_UUID_ZERO_32BIT          ((uint32)0x00005500)
#define     SERIAL_UUID_ONE_32BIT           ((uint32)0xd10211e1)
#define     SERIAL_UUID_TWO_32BIT           ((uint32)0x9b230002)
#define     SERIAL_UUID_THREE_32BIT         ((uint32)0x5b00a5a5)

                                  
/***************************************************************************************************************/
const char lackofmem[19] = {'\n',' ','L','a','c','k',' ','O','f',' ','M','e','m','o','r','y',' ','\n','\0'} ;

const char report[21] = {'R','E','P','*',':','C','H','A','R',' ','o','f',' ','S','E','R','V','-','I','D','\0'};
const char report2[7] = {'S','T','A','R','T','\n','\0'};


const char bleauthstart[16] = {'B','L','E',' ','A','u','t','h',' ','s','t','a','r','t','\n','\0'} ; 


/***************************************************************************************************************/


extern uint16 BLE_Scan_InProcess;
extern uint8 start_data ;
extern Ring_st  *Ring; 


/*! BLE advertising data */  /*kpui8AdvertisingData[UC_ADV_DATA_LENGTH]*/
const uint8 kpui8AdvertisingData[25] = 
{
    0x02,
    ble_ad_type_flags,
    BLE_FLAGS_SINGLE_MODE,   /*BLE_FLAGS_DUAL_CONTROLLER ,*/
    
    3,
    AD_TYPE_MANUF,
    0x01,
    0xB6,
    
    9,
    AD_TYPE_LOCAL_NAME_SHORT,
    0x74, 
    0x6f, 
    0x67, 
    0x67, 
    0x6c, 
    0x65, 
    0x5f, 
    0x31,
    /**/
    7,
    AD_TYPE_SERVICE_UUID_16BIT_LIST,
    0x0F,
    0x18,
    0x0A,
    0x18,
    0x15,
    0x18,
    
};



/* --------------- Local variables declarations -------------- */

/*! BLE connection data */     
BLEConnectionData_st    *stBLEConnectionData;

BLEInfoData_st      *stBLEInfoData;

SerialServiceData_st     *SerialServiceData;

bool        BLE_Adaper_ready = FALSE ;

LeDataBuff_st           *p_lebuff;




/* when LM961 performs Scan, it stores device-address, yes/no nameofdevice_found and
   number of devices found */
typedef struct 
{
    bdaddr  scannedBLE[UC_MAX_NUM_OF_BLE_SCAN_DEVICES]; /* 10  devices */
    uint8   name[UC_MAX_NUM_OF_BLE_SCAN_DEVICES];
}scanned_ble_dev_st;
scanned_ble_dev_st  *bledev;
static uint16       BleDevScanned = 0;


/* --------------- Local constant variables declarations -------------- */

/*! BLE default connection and advertising parameters */  
/* TODO: implement proper defines */
const ble_connection_params stDefaultConnAdvertParams = 
{
    0x0800,     /* LE scan interval */  /* Scan interval in units of 0.625 ms. The allowed range is between 0x0004 (2.5 ms) and 0x4000 (10240 ms) */
    0x0400,     /* LE scan window */    /* Scan interval in units of 0.625 ms. The allowed range is between 0x0004 (2.5 ms) and 0x4000 (10240 ms) */
    0x08, /*0x0100,*/     /* Minimum value for the connection event interval */   /* Connection interval in units of 1.25 ms. The allowed range is between 0x0006 (7.5 ms) and 0x0c80 (4 s) */
    0x08, /*0x0C80,*/     /* Maximum value for the connection event interval */   /* Connection interval in units of 1.25 ms. The allowed range is between 0x0006 (7.5 ms) and 0x0c80 (4 s) */
    0x0,       /* 0x0100,*/     /* Slave latency for the connection in number of connection events */   /* The allowed range is between 0x0000 and 0x01f4 */
    0x0C80,     /* Supervision timeout for the LE Link */   /* Supervision timeout in units of 10 ms. The allowed range is between 0x000a (100 ms) and 0x0c80 (32 s) */
    0x0100,     /* TODO! */ /* LE connection attempt timeout */ /* Equivalent of Page Timeout in BR/EDR */
    0x0600,     /* Minimum advertising interval for non-directed advertising */ /* Advertising interval in units of 0.625 ms. The allowed range is between 0x0020 (20 ms) and 0x4000 (10.24 s) */
    0x2000,     /* Maximum advertising interval for non-directed advertising */ /* Advertising interval in units of 0.625 ms. The allowed range is between 0x0020 (20 ms) and 0x4000 (10.24 s) */
    0x0100,     /* Maximum allowed slave latency */ /* The maximum allowed slave latency that is accepted if slave requests connection parameter update once connected */
    0x0100,     /* Minimum allowed supervision timeout */   /* The minimum allowed supervision timeout that is accepted if slave requests connection parameter update once connected */
    0x0C80,     /* Maximum allowed supervision timeout */   /* The maximum allowed supervision timeout that is accepted if slave requests connection parameter update once connected */
    TYPED_BDADDR_RANDOM /* Own Address type used in LE connnect requests by the device */ /* Available types are TYPED_BDADDR_PUBLIC or TYPED_BDADDR_RANDOM */
};
   


/*****************************************************************************************************
  BLE security manager messages from  CL_library
  ****************************************************************************************************/
static void handleBLESimplePairingCompleteIndic( CL_SM_BLE_SIMPLE_PAIRING_COMPLETE_IND_T *ind )
{    
    /* if pairing success */
    
    /* 
    if( ind->status == success )
    {
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "BLE_PAIR", "B,s",
                                          &ind->taddr.addr,
                                          (const char)"Success",
                                          strlen("Success") ));

        ConnectionDmBleAddDeviceToWhiteListReq( ind->taddr.type, &ind->taddr.addr );
    }
    else
    {
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "BLE_PAIR", "B,s",
                                          &ind->taddr.addr,
                                          (const char)"Fail",
                                          strlen("Fail") ));
    }*/
    
}



/*! BLE MASTER AND SLAVE MODE: BLE I/O capability indication */ 
static void handleBLEIOCapabilityIndic( CL_SM_BLE_IO_CAPABILITY_REQ_IND_T *cfm )
{
    /* TODO: implement as needed...
       - cl_sm_io_cap_display_only: Display Only
       - cl_sm_io_cap_display_yes_no: Display Yes/No
       - cl_sm_io_cap_keyboard_only: Keyboard Only
       - cl_sm_io_cap_no_input_no_output: No IO
       - cl_sm_io_cap_keyboard_display: Keyboard and numeric output
       - cl_sm_reject_request: Use this to reject the IO capability request
    */
    /* send response */

    LE_DEBUG(("\n BLE IO capability request = No-IO "));
    ConnectionSmBleIoCapabilityResponse(    (const typed_bdaddr *)&cfm->taddr,  /* the remote device address */
                                            cl_sm_io_cap_no_input_no_output,    /* TODO: implement it properly, NOT FIXED! */
                                            KEY_DIST_INITIATOR_ENC_CENTRAL,     /* Indicates the keys to be distributed. ENC_CENTRAL is required for bonding and need only be set for the initiator */
                                            FALSE,  /* Set this to TRUE to force MITM protection when pairing. This may cause pairing to fail if the remote device cannot support the required io */
                                            0); /* The remote device's out of band hash value. This should be set to 0 if out of band data is not to be used */
    
    /* there is an additional parameter called "bonding" in the documentation; it is a mistake, the function has 5 parameters only */
}


static void handleBLESecurityCfm ( CL_DM_BLE_SECURITY_CFM_T *cfm )
{
    if( cfm->status == success )
        LE_DEBUG(("   success "));        
    else
        LE_DEBUG(("\n Failure???  "));
    
}




/*******************************************************************************
  This functio is called twice: after power_on and 
                                every time we disconnect from BLE device
    parameter - 0x01 - PowerON : no need to free the memory as it is not allocated
              - 0x00 - After disconnect - need to free the allocated memory
 ******************************************************************************/
static void    Reset_Connection_Parameters ( void  )
{

        LE_DEBUG(("\n reset param for connection"));
        
        stBLEConnectionData->eConnStatus      = KE_CONN_CLOSED;
    
        stBLEConnectionData->ui16ConnectionID    = 0xffff;
        
        BdaddrTypedSetEmpty( &stBLEConnectionData->stConnectedDeviceAddress  );
}

static void Reset_BLE_Adapter_parameter ( void )
{
    LE_INFO(("\n Reset the SerialService nad parameters after disconnect"));
    
    free((SerialServiceData_st * )SerialServiceData);
}
        
   
static void BLE_GATT_Initialise ( void ) 
{
    uint16 *db;
    uint16 size_db;
        
    LM_BLE_DEBUG(("\n  BLE_GATT_Initialise "));
    
    if( stBLEInfoData->BLE_Role == GAP_PERIPHERAL_ROLE )
    {
        /* TODO: init GATT... maybe... */
        db = GattGetDatabase(&size_db);
        GattInit(app_data.task, size_db, db);
        
        LM_BLE_DEBUG((" == Reuest_Database Init "));
    }
    else if( stBLEInfoData->BLE_Role == GAP_CENTRAL_ROLE )
    {
        LM_BLE_DEBUG((" == Reuest_Central Init "));
        		GattInit(app_data.task, 0, 0);
    }
    else
    {
        LM_BLE_DEBUG((" Invalid value for stBLEInfoData->BLE_Role  "));
    }
    
}

static void setBLELocalAddress( void )
{
    typed_bdaddr address;
           
    /* use devices BD-address here */

    BdaddrSetZero (&address.addr); 
    read_BDaddr( LCN_LOCAL_BD_ADDRESS,  &address.addr);
    address.type = ble_local_addr_write_static;
    
    ConnectionDmBleConfigureLocalAddressReq(ble_local_addr_write_static, (const typed_bdaddr *)&address);        
    /* A CL_DM_BLE_CONFIGURE_LOCAL_ADDRESS_CFM message will be sent as confirmation */
}


void set_BLEadvertising_report_filter( void )
{
    
    LE_INFO((" \n enable filter here set_BLEadvertising_report_filter "));
    
    /*
bool ConnectionBleAddAdvertisingReportFilter	(	ble_ad_type 	ad_type, 
uint16 	interval,
uint16 	size_pattern,
const uint8 * 	pattern )
      */

/*
    128/8 = 16 bytes 
    const uint8 UUID_Filter_pattern[16] = {0x00, 0x00, 0x55, 0x00, 0xd1, 0x02, 0x11, 0xe1, 0x9b, 0x23, 0x00, 0x02, 0x5b, 0x00, 0xa5, 0xa5};  
    
    ConnectionBleAddAdvertisingReportFilter	(	ble_ad_type_complete_uuid128 , 100, 16, (const uint8 *) 	UUID_Filter_pattern );
     */         

}


static void Initiate_Ring_Buffer ( void )
{
        uint8 i=0;
        
            codebuff_1    = (codebuff_st*)   PanicUnlessNew (codebuff_st); 
            codebuff_2    = (codebuff_st*)   PanicUnlessNew (codebuff_st); 
            codebuff100_3 = (codebuff100_st*)    PanicUnlessNew (codebuff100_st); 
            
            for(i=0; i< CBUFF_BUFFER_SIZE_254 ; i++)
            {
                codebuff_1->buff[i]=0;   
                codebuff_2->buff[i]=0;
            }
            
           Ring = (Ring_st*)    PanicUnlessNew(Ring_st); 
           Ring->C1 =  (circular_buf_st* )        PanicUnlessNew(circular_buf_st);
           Ring->C2 =  (circular_buf_st* )        PanicUnlessNew(circular_buf_st);
           Ring->C3 =  (circular_buf_st* )        PanicUnlessNew(circular_buf_st);
           
           Ring_buf_reset(Ring);
}


void BLE_Initialise ( void )
{

    
    LE_INFO((" \n BLE_Initialise "));
    
    stBLEInfoData       = (BLEInfoData_st *)        PanicUnlessNew(BLEInfoData_st);   
    stBLEConnectionData = (BLEConnectionData_st *)  PanicUnlessNew(BLEConnectionData_st);
    p_lebuff            = (LeDataBuff_st *)         PanicUnlessNew(LeDataBuff_st); 

    
    Reset_Connection_Parameters();

    Read_LE_Security_fromPS ();

    
        Read_BLE_ROLE_fromPS ();
        stBLEInfoData->bGATTInitStatus      = FALSE;  /* GATT init should be called only once in the application */
		BLE_GATT_Initialise ();    
    
    if( stBLEInfoData->BLE_Role == GAP_CENTRAL_ROLE )    
    {
 		/* configurations for BLE GAP Central */
		ConnectionDmBleSetScanEnable( FALSE ); 
        
            Read_LE_AutoScan_fromPS();
            Read_LE_AutoConnect_fromPS();
            
		/* set scan parameters before start scanning. ATTENTION: fixed parameters are used at the moment */
		ConnectionDmBleSetScanParametersReq (   TRUE,	/* If TRUE SCAN_REQ packets may be sent (default: FALSE).*/
                                                TRUE,	/* If TRUE, then use a Random own address (default: FALSE). */
                                                ( (stBLEInfoData->WhiteListEnable == TRUE ) ? TRUE : FALSE ),	/* If TRUE then advertising packets from devices that are not on the White List for this device will be ignored (default: FALSE).*/
                                                stDefaultConnAdvertParams.scan_interval,    /* TODO: make a proper define at least */ /* Scan interval in steps of 0.625ms, range 0x0004 (2.5 ms) to 0x4000 (10.24 s).*/
                                                stDefaultConnAdvertParams.scan_window    );     /* TODO: make a proper define at least */ /* Scan window in steps of 0.625ms, range 0x0004 (2.5ms) to 0x4000 (10.24 s). Must be less than or equal to the scan_interval parameter value.*/
		/* a CL_DM_BLE_SET_SCAN_PARAMETERS_CFM message will be sent to trig the scan start */
        
        /*for BLE-only-LM068, set the advertising report filter for GAP-Central */
        Read_LE_AddFilter_fromPS();
        
        if( stBLEInfoData->BLEApplyFilter == TRUE )        
        set_BLEadvertising_report_filter();     
        
        stBLEInfoData->Serial_Service_Found = FALSE ;
        BleDevScanned = 0; 
    } 
    else if( stBLEInfoData->BLE_Role == GAP_PERIPHERAL_ROLE )
        {
                /* General Configurations */
                setBLELocalAddress(); 
                /* if device is in Gap-Peripheral need to set advertising name also */
                
                    ConnectionDmBleSetAdvertiseEnable( FALSE ); 
    
                    set_BLE_Advertising_Parameters(); 
                     ConnectionDmBleSetAdvertisingDataReq( sizeof(kpui8AdvertisingData), (const uint8 *)kpui8AdvertisingData);
                    Add_DevName_in_ScanResp_data(); 
                    
                    ConnectionDmBleSetAdvertiseEnable( TRUE ); 
                    Initialise_Sink_for_Peri_Rcv_Data();
            }

        Initiate_Ring_Buffer();
    
}


void Ring_buf_reset (Ring_st* rRing )
{
    rRing->pointer = 0 ;
    rRing->full_count = 0;
    rRing->Read_pointer = 0;
    rRing->Clear_flag = FALSE;             
                      
    rRing->C1->buffer = codebuff_1->buff;
    rRing->C1->head = 0;
    rRing->C1->tail = 0;
    rRing->C1->full = FALSE;
    
    rRing->C2->buffer = codebuff_2->buff;    
    rRing->C2->head = 0;
    rRing->C2->tail = 0;
    rRing->C2->full = FALSE;

    rRing->C3->buffer = codebuff100_3->buff;
    rRing->C3->head = 0;
    rRing->C3->tail = 0;   
    rRing->C3->full = FALSE;
    
    LOG_RING((" \n RingBuffer Parameters Reset"));
}


/*******************************************************************************
  messages from Connection library  "CL_DM_BLE_xxxxx"
 *******************************************************************************/


void Reset_BLEScannedDev    ( void )
{
    BleDevScanned = 0;
    BLE_Scan_InProcess = 0x00;   /* stop scanning */
    
    if(bledev == NULL )
    {
        LE_DEBUG(("\n not allocated")); 
    }
    else
    {
        free((scanned_ble_dev_st * )bledev);
        bledev = NULL;
        LE_DEBUG(("\n ********  Released bledev-scanning memory :: scanned_ble_dev_st "));
    }
    
}


/* this device returns TRUE if the match of address is found,
   If it finds match, then it shall updat ehte index_ptr value equal to location of the address so that caller can check the name is found or not
 */
static bool is_knownBLEdev ( bdaddr address, uint16 *index_ptr )
{
    uint16 Index = 0, found = 0;
    
        for( Index=0; Index <= BleDevScanned ; Index++)
        {
            found = BdaddrIsSame( (const bdaddr *) &(bledev->scannedBLE[Index]) ,  (const bdaddr *)&address ); 
            /* Returns TRUE if they are the same */
            if(found)
            {
                LE_DEBUG(("\n Index=%d", Index ));
                *index_ptr = Index;
                return TRUE ;
            }
        }
        
    return FALSE;    
}    


/* To get the name from BLE Advertising packets, need "ble_adv_event_scan_response" packet.
   while performing BLE-Scan we should enable Scan-responses.
   there is special format to get the name from response paacket. 
 */
/*! BLE MASTER MODE: connection library advertising report indication */  
static void handleBLEAdvertisingReportInd( CL_DM_BLE_ADVERTISING_REPORT_IND_T *ind )
{
    uint16 total =0, i=0;    
    uint16 namefound=0, match_index=0 , add=0, nameprint=0;
    bool known = FALSE;
     
  /* LE_DEBUG(("\n CL_DM_BLE_ADVERTISING_REPORT_IND_T :  "));  
  PRINT_BDADDR(ind->permanent_taddr.addr);*/
  
  
  
    if(BLE_Scan_InProcess==TRUE && BleDevScanned<UC_MAX_NUM_OF_BLE_SCAN_DEVICES )
    {
        /* check if it is a known event */
        if( ind->event_type != ble_adv_event_unknown  && 
		    ind->num_reports>0                        &&  
			ind->size_advertising_data!=0              )
        {
          /*
            LE_DEBUG(("\n addr=%04x-%02x-%06lx ", ind->permanent_taddr.addr.nap, ind->permanent_taddr.addr.uap, ind->permanent_taddr.addr.lap));
            LE_DEBUG(("\n e_type=%d ", ind->event_type ));
            LE_DEBUG((" ad_data_size=%d \n actual data is bleow \n >>>>>   ", ind->size_advertising_data));
			
			
            for(i=0; i<ind->size_advertising_data; i++)
            {
                LE_DEBUG(("%d ", ind->advertising_data[i]));
                if( ind->advertising_data[i]==AD_TYPE_LOCAL_NAME_COMPLETE)
                    LE_DEBUG(("(L) "));
                else if( ind->advertising_data[i]==AD_TYPE_LOCAL_NAME_SHORT )
                    LE_DEBUG(("(S) "));
            }*/
            


			while(total < ind->size_advertising_data)
			{
					if( ind->advertising_data[total+1]==AD_TYPE_LOCAL_NAME_COMPLETE || 
						ind->advertising_data[total+1]==AD_TYPE_LOCAL_NAME_SHORT )
					{ 
						LE_DEBUG(("\n name found in event_type=%d at %d, length=%d \n", ind->event_type,(total+2), (ind->advertising_data[total]-1) ));
						namefound=1;
						break;
					}
					else
					{
						LE_DEBUG(("\n total=%d ,ind->advertising_data[total]=%d ", total, ind->advertising_data[total] ));
						total = total + ind->advertising_data[total] + 1 ;
					}
			}/*while*/

            
			if( BleDevScanned==0 && bledev==NULL )
			{
				bledev = (scanned_ble_dev_st*) PanicUnlessNew (scanned_ble_dev_st); 
                 /*   LE_DEBUG(("\n  **********  Allocated mem dynamically :: scanned_ble_dev_st "));                */
                
				MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "BLE_SCAN_REP", "s", "START", lstrlen("START")));
				for(i=0;i<UC_MAX_NUM_OF_BLE_SCAN_DEVICES;i++)
					bledev->name[i]=FALSE;
			}
			
			known = is_knownBLEdev(ind->permanent_taddr.addr , &match_index) ;
			
			if(  known == FALSE )
			{
				/*device might have already added by its 0th packet, but update name here */
				bledev->scannedBLE[BleDevScanned] = ind->permanent_taddr.addr;
				add=1;
				
				if(namefound==1)
				{
					bledev->name[BleDevScanned] = TRUE;
					nameprint=1;
				}
				
				BleDevScanned++;
			}
			else
			{
				add=0;
				
				if( ( bledev->name[match_index] == FALSE ) && (namefound==1) )
				{
				  /* device was known but name is discoverd now, just print name */
					bledev->name[match_index] = TRUE;
					nameprint=1;
				}
				else
					return;
			}				
			
			if(add==1 )
			{
				if(nameprint==1)
				{
					MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "", " u :B :u :s", 
                                                              (uint32)BleDevScanned,
                                                              &(ind->permanent_taddr.addr),
                                                              (uint32)ind->event_type,
                                                              &(ind->advertising_data[total+2]), 
                                                              (ind->advertising_data[total]-1) ));
				}
				else
				{
					MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "", " u :B :u",
                                                              (uint32)BleDevScanned,
                                                              &(ind->permanent_taddr.addr),
                                                              (uint32)ind->event_type
																			   ));
				}
			}
            else if(nameprint==1)
            {
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "", " u: B, s", 
                                                              (uint32)0,
                                                              &(ind->permanent_taddr.addr),
                                                              &(ind->advertising_data[total+2]), 
                                                              (ind->advertising_data[total]-1) )); 
            }
            
            if(BleDevScanned==UC_MAX_NUM_OF_BLE_SCAN_DEVICES)
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "BLE_SCAN_REP", "s", "END", lstrlen("END")));
			

            LE_DEBUG(("\n "));
        }/* event_type */

    }/*in progress, <max dev*/
}


static void check_BLEAutoConn ( void )
{
    event_at_ble_ConDiscon_st *msg = (event_at_ble_ConDiscon_st *)malloc(sizeof(*msg));    
    
    LE_DEBUG(("\n check_BLEAutoConn "));   
    
    if(BleDevScanned)
    {
            if( (stBLEInfoData->BLEautoConnect == TRUE)  ) 
            {
                    /* for BLE based adapters there is only one connection possible  and 
                        It is open */
                /* send connection req to first found device */
                
                LE_INFO(("\n Initiate BLE conn to 1st found device  "));
                
                memcpy(&(msg->addr), &(bledev->scannedBLE[0]),  sizeof((msg->addr)) );
                handle_at_ble_conn(msg);
                Reset_BLEScannedDev();
                ConnectionDmBleSetScanEnable( FALSE );
            }
    }
   
    free(msg);
}


/*****************************************************************************************************
  ****************************************************************************************************/

/*! GATT initialisation confirmation */    
static void handleGATTInitConfirm ( GATT_INIT_CFM_T *cfm )
{
    char *string_ptr[4] = {"write_static", "generate_static", "non_resolvable", "resolvable" };  
    char *adr_typr[2] = {"Public", "Random"};
      
    bdaddr  addr;

    if( cfm->status == success )
    {
        stBLEInfoData->bGATTInitStatus     = TRUE ;
        
        if( stBLEInfoData->BLE_Role == GAP_PERIPHERAL_ROLE )
        {
            BdaddrSetZero (&addr); 
            read_BDaddr( LCN_LOCAL_BD_ADDRESS,  &addr);
            
            if ( TRUE == Is_Response_Set() ) 
             uart_tx("LM068_LE-Peri_GATT_Initialised\r\n", lstrlen("LM068_LE-Peri_GATT_Initialised\r\n"));           
            
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "Peri_ADV_ADR", "s,s,B",
                                              string_ptr[stBLEInfoData->Peri_addr_type],
                                              strlen(string_ptr[stBLEInfoData->Peri_addr_type]),
                                              adr_typr[stBLEInfoData->Peri_addr_type],
                                              strlen(adr_typr[stBLEInfoData->Peri_addr_type]),
                                              &addr ));  
        }
        else
        {
            if ( TRUE == Is_Response_Set() ) 
                uart_tx("LM068_LE-Central_GATT_Initialised\r\n", lstrlen("LM068_LE-Central_GATT_Initialised\r\n")); 
            
        }
    }
    else
    {
        uart_tx("\n GATT Failure", lstrlen("\n GATT Failure"));
        stBLEInfoData->bGATTInitStatus     = FALSE;
        /* Control should never come here, not decided how to handle this */
    }
}


/*! handle a connection confirmation. It could be from an accepted one or a requested one */  
static void handleGATTConnectionConfirm( GATT_CONNECT_CFM_T *cfm  )
{
    LE_DEBUG(("\n bd_addr=%04x-%02x-%06lx ", cfm->taddr.addr.nap, cfm->taddr.addr.uap, cfm->taddr.addr.lap ));  
    
        if( (gatt_status_initialising==cfm->status) || (gatt_status_success_sent==cfm->status) ) 
        {
                stBLEConnectionData->ui16ConnectionID = cfm->cid;
                stBLEConnectionData->eConnStatus = KE_CONN_PROGRESS ;
                LE_DEBUG(("\n connection in progress " ));  
        }    
        else if( (gatt_status_success == cfm->status ) || (gatt_status_success_more == cfm->status) )
        {
            LE_DEBUG(("\n BLE connected bd_addr=%04x-%02x-%06lx ", cfm->taddr.addr.nap, cfm->taddr.addr.uap, cfm->taddr.addr.lap )); 
            
            if( cfm->cid == stBLEConnectionData->ui16ConnectionID )
            {
                stBLEConnectionData->eConnStatus = KE_CONNECTED;   
                
                stBLEConnectionData->stConnectedDeviceAddress = cfm->taddr;
                
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "BLE_CONNECTED:", "B-u : u : y : y : y ", 
                                                  &(stBLEConnectionData->stConnectedDeviceAddress.addr) ,
                                                  (uint32)(cfm->taddr.type),
                                                  (uint32)(1),
                                                  (uint32)stBLEConnectionData->ui16ConnectionID,
                                                  (uint32)(cfm->mtu),
                                                  (uint32)(cfm->flags)));  
                
                if ( FALSE == stBLEInfoData->WhiteListEnable  )
                {
                    uart_tx( &bleauthstart[0], strlen(bleauthstart));
                    ConnectionDmBleSecurityReq( app_data.task, 
                                                &cfm->taddr,
                                                ble_security_encrypted_bonded, 
                                                gatt_connection_ble_master_directed );
                    LE_DEBUG(("\n BLE-Sec-Req for above device "));  
                }
                
                MessageCancelAll(app_data.task, KE_WAIT_BEFORE_GAPCENTRAL_AUTOCONN_MSG);
                return;
            }
            else
                GattDisconnectRequest( cfm->cid );
        }
        else
        {
            if( cfm->cid == stBLEConnectionData->ui16ConnectionID )
            {
                stBLEConnectionData->eConnStatus = KE_CONN_CLOSED;
                stBLEConnectionData->ui16ConnectionID = 0xffff;
                
                           MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "BLE_CONN_FAIL:", "B,u s:y", 
                                              &(cfm->taddr.addr),
                                              (uint32)(1),
                                              "err_code", strlen("err_code"), (uint32) cfm->status ));
                           
                handle_GapCentral_autoConn();           
            }
        }

}

/*********************************************************************************************************************
 After getting disconnected from the device we need to release memory
 accoupied by its services and characteristics if were scanned  
  ********************************************************************************************************************/
static void release_services_memory ( void )
{
    
}


static void handleGATTDisconnectionIndic( GATT_DISCONNECT_IND_T *ind )
{
    if( ind->cid == stBLEConnectionData->ui16ConnectionID)
    {      
        if( gatt_status_success == ind->status  ||  gatt_status_link_loss == ind->status )
        {
            LE_DEBUG(("\n BLEDis_connect with SUCCESS  "));
            stBLEConnectionData->eConnStatus = KE_CONN_CLOSED ;
            
            MOCK( at_parser_send_event_message( AT_EVENT_TYPE_REP, "BLE_DIS_CONN:", "B,u s:y", 
                                              &stBLEConnectionData->stConnectedDeviceAddress.addr,
                                              (uint32)(1),
                                              "err_code", strlen("err_code"), (uint32) ind->status ));

            Reset_Connection_Parameters();
            
            BLE_Adaper_ready = FALSE;
            start_data = FALSE;
            Ring_buf_reset(Ring);
            
            if(stBLEInfoData->BLE_Role == GAP_CENTRAL_ROLE )
            {
                release_services_memory();
                
                if(  stBLEInfoData->Serial_Service_Found == TRUE )
                {
                    stBLEInfoData->Serial_Service_Found = FALSE ;
                    Reset_BLE_Adapter_parameter();
                }
                
                handle_GapCentral_autoConn();
            }
            else
            {
                Start_DualAdvert_AfterDisconnect();  
            }
        }
     } 
}




/*! BLE MASTER MODE: connection library connection parameters update indication */    
static void handleBLEConnParUpdateIndic( CL_DM_BLE_ACCEPT_CONNECTION_PAR_UPDATE_IND_T *ind )
{
    LE_DEBUG(("\n Update BLE Connection para "));
    /* accept connection and confirm all connection parameter as received */
    ConnectionDmBleAcceptConnectionParUpdateResponse(   TRUE,
                                                        (const typed_bdaddr *)&ind->taddr,
                                                        ind->id,
                                                        ind->conn_interval_min,
                                                        ind->conn_interval_max,
                                                        ind->conn_latency,
                                                        ind->supervision_timeout);
}


/*********************************************************************************************
  ********************************************************************************************/


static bool handleGATTDiscoveryAllPrimServicesCfm( GATT_DISCOVER_ALL_PRIMARY_SERVICES_CFM_T *cfm )
{
    static uint8    ui8ServiceIndex=0;
           
    if( gatt_status_success == cfm->status )
    {
        if( cfm->cid != stBLEConnectionData->ui16ConnectionID)
            return FALSE;
        
                    if(ui8ServiceIndex == 0)
                        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP,  
                                                          "SERVICE START: ", "B", 
                                                          &stBLEConnectionData->stConnectedDeviceAddress.addr ));
                    
                    
                    if( gatt_uuid128 == cfm->uuid_type )
                    {      
                        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "", " u : xxxx : y : y ",   
                                                  (uint32) (ui8ServiceIndex+1),                                                       
                                                  (uint32) cfm->uuid[0],
                                                  (uint32) cfm->uuid[1],
                                                  (uint32) cfm->uuid[2],
                                                  (uint32) cfm->uuid[3],
                                                  (uint32) cfm->handle,
                                                  (uint32) cfm->end )  );
                        
                            /* check UUID for Serial over GATT service */
                            LE_DEBUG(("\n 0=%lx, 1=%lx, 2=%lx, 3=%lx",  (uint32) cfm->uuid[0], (uint32) cfm->uuid[1], (uint32) cfm->uuid[2], (uint32) cfm->uuid[3]  ));
                           
                            if(stBLEInfoData->Serial_Service_Found == FALSE ) /* if dev has multiple 128 UUID service, keep compsring till finds serial */
                            {
                                    if( ( cfm->uuid[0] == SERIAL_UUID_ZERO_32BIT ) && ( cfm->uuid[1] == SERIAL_UUID_ONE_32BIT ) && ( cfm->uuid[2] == SERIAL_UUID_TWO_32BIT ) && ( cfm->uuid[3] == SERIAL_UUID_THREE_32BIT ) )
                                        {
                                            stBLEInfoData->Serial_Service_Found = TRUE;                                            
                                            LE_DEBUG(("\n stBLEInfoData->Serial_Service_Found = TRUE "));

                                            SerialServiceData       = (SerialServiceData_st *)        PanicUnlessNew(SerialServiceData_st);
                                            
                                            LE_DEBUG(("\n stBLEInfoData->Serial_Service_Found = TRUE "));
                                            SerialServiceData->CharStartHandle = cfm->handle;
                                            SerialServiceData->CharEndHandle = cfm->end; 
                                        }
                                    else
                                        {
                                            stBLEInfoData->Serial_Service_Found = FALSE;
                                            LE_DEBUG(("\n stBLEInfoData->Serial_Service_Found = FALSE "));
                                        }    
                            }
                    }
                    else if( gatt_uuid16 == cfm->uuid_type )
                    {                
                        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "", " u : y : y : y",
                                                  (uint32)(ui8ServiceIndex+1),
                                                  (uint32)cfm->uuid[0],
                                                  (uint32) cfm->handle,
                                                  (uint32) cfm->end )  );
                    }
                    
                    ui8ServiceIndex++;
            
                    
            if( FALSE == cfm->more_to_come )
            {
                    ui8ServiceIndex=0;
                    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "SERVICE END: ", "B", &stBLEConnectionData->stConnectedDeviceAddress.addr ));        
                    return TRUE;
            }

    }/* cfm-success */

    return FALSE;
} 





/*******************************************************************************
  Scan a service for its characteristics.
  a service can have multiple characteristics
  ******************************************************************************/

/*! GATT discovery all characteristics confirmation */    
static bool handleGATTDiscoveryAllCharCfm( GATT_DISCOVER_ALL_CHARACTERISTICS_CFM_T *cfm )
{
    uint8   properties =  0xff;
    static uint8 ui8CharIndex = 0;
    bool   WRcmd=FALSE, CCFG=FALSE;
    
        if( gatt_status_success == cfm->status)
        {  
                LE_INFO(("\n CHARACTERISTICS_CFM >> cfm->handle=%d, cfm->properties=%d  ", cfm->handle, cfm->properties)); 
        
            if( cfm->cid != stBLEConnectionData->ui16ConnectionID)
                return FALSE;      

                    if(ui8CharIndex == 0)
                    {
                        uart_tx( (const char*)"REP*:CHAR start \n", strlen("REP*:CHAR start \n") );
                    }
                    
                    if( gatt_uuid16 == cfm->uuid_type )
                    {
                       MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "", "u: y : y : z : y ",
                                          (uint32)(ui8CharIndex+1),
                                          (uint32)cfm->uuid[0],
                                          (uint32)cfm->handle,
                                          (uint32)cfm->properties,
                                          (uint32) cfm->declaration ));
                    }
                    else
                    {
                      MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "", "u: x : y : z : y ",
                                          (uint32)(ui8CharIndex+1),
                                          (uint32)cfm->uuid[0],  /* show only lower 32 bits */
                                          (uint32)cfm->handle,
                                          (uint32) cfm->properties,
                                          (uint32) cfm->declaration ));
                    }
                    
                    ui8CharIndex++;
                    properties = cfm->properties ;
                    
/* #if 0 */
                        uart_tx( (const char*)"\b[ ", strlen("\b[ ")  );
                            
                        if( (properties & CHAR_PROP_BROADCAST) == CHAR_PROP_BROADCAST ) /* 0b0000 0001 */
                            uart_tx( (const char*)"Bd ", strlen("Bd ")  );
                                 
                        if( (properties & CHAR_PROP_READ) == CHAR_PROP_READ ) /* 0b0000 0010 */
                            uart_tx( (const char*)"Rd ", strlen("Rd ")  ); 
                               
                        if( (properties &  CHAR_PROP_WRITECMD) == CHAR_PROP_WRITECMD ) /* three types of write */
                        {
                            uart_tx( (const char*)"Wr_cmd ", strlen("Wr_cmd ")  );
                            WRcmd =TRUE;
                        }
                        
                        if( (properties &  CHAR_PROP_WRITEREQ) == CHAR_PROP_WRITEREQ ) /* three types of write */
                             uart_tx( (const char*)"Wr_req ", strlen("Wr_req ")  );
                                 
                        if( (properties & CHAR_PROP_CCFG) == CHAR_PROP_CCFG ) /* 0b0001 0000 */
                        {
                            uart_tx( (const char*)"CCFG ", strlen("CCFG ")  );
                            CCFG =TRUE ;
                        }
                        
                        if( (properties & CHAR_PROP_INDICATION) == CHAR_PROP_INDICATION ) /* 0b0010 0000 */
                            uart_tx( (const char*)"Indi ", strlen("Indi ")  );
                              
                        if( (properties &  CHAR_PROP_WRITE_SIGNED) == CHAR_PROP_WRITE_SIGNED ) /* three types of write */
                            uart_tx( (const char*)"Wr_signed ", strlen("Wr_signed ")  );
                                 
                        if( (properties & CHAR_PROP_EXTENDED) == CHAR_PROP_EXTENDED ) /* 0b1000 0000 */
                            uart_tx( (const char*)"Ext ", strlen("Ext ")  );
                             
                        uart_tx( (const char*)"]\n", strlen("]\n")  );     
                        
/* #endif */
                if( FALSE == cfm->more_to_come )
                {       
                        uart_tx( (const char*)"REP*:CHAR END \n", strlen("REP*:CHAR END \n") );
                        ui8CharIndex = 0;
                        
                                if(CCFG==TRUE && WRcmd==TRUE )
                                {
                                    SerialServiceData->WR_and_CCFG_char_found = TRUE;
                                    SerialServiceData->SerialCharHandle = cfm->handle;
                                    
                                    LE_DEBUG(("\n  Found WR and CCFG at char hanle %x",SerialServiceData->SerialCharHandle ));
                                }
                                else
                                     SerialServiceData->WR_and_CCFG_char_found = FALSE;     
                                
                        return TRUE;
                }
        }
        else if( gatt_status_attr_not_found == cfm->status ) 
        {
            uart_tx( (const char*)"\n No CHAR found.\n", strlen("\n No CHAR found.\n") );  
            ui8CharIndex = 0;
            return TRUE;
        }

    
    if(CCFG==TRUE && WRcmd==TRUE )
    {
        SerialServiceData->WR_and_CCFG_char_found = TRUE;
        SerialServiceData->SerialCharHandle = cfm->handle;
        
        LE_DEBUG(("\n  Found WR and CCFG at char hanle %x",SerialServiceData->SerialCharHandle ));
    }
    else
         SerialServiceData->WR_and_CCFG_char_found = FALSE;

    
    return FALSE;
}


/*******************************************************************************
  ******************************************************************************/
/*! GATT write character confirmation */    
static bool handleGATTWriteCharCfm( GATT_WRITE_CHARACTERISTIC_VALUE_CFM_T *cfm )
{  
    
    LE_DEBUG(("\n ++ GATT_WRITE_CHARACTERISTIC_VALUE_CFM_T  = CID=%x Handle=%x ", cfm->cid, cfm->handle ));    

    
    if( cfm->cid == stBLEConnectionData->ui16ConnectionID)
    {
        if(cfm->handle == (SerialServiceData->SerialCharHandle+1) )
        {
           SerialServiceData->Serial_CCFG_enabled = TRUE; 
           BLE_Adaper_ready = TRUE;
           LE_DEBUG(("\n ++ BLE_Adaper_ready for Transactions " )); 
           
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "BLE_ADAPTER", "s", "Ready For USE", strlen("Ready For USE") ));
        }
    }
    return FALSE;
    
}



/*! GATT read character confirmation */   
static void handleGATTReadCharCfm( GATT_READ_CHARACTERISTIC_VALUE_CFM_T *cfm )
{

}


static void handleGATT_signedWriteWoResp_Cfm ( const GATT_SIGNED_WRITE_WITHOUT_RESPONSE_CFM_T *cfm )
{
    /*gatt_status_t status=gatt_status_failure; 
error code of gatt_status_t should be printed as 0xffff and not 0xff */
    
    if( cfm->cid != stBLEConnectionData->ui16ConnectionID)
        return;
            
    if( cfm->status == gatt_status_success_sent )
        LE_DEBUG(("\n gatt_status_success_sent"  ));


    if( cfm->status == gatt_status_invalid_cid ) LE_DEBUG(("\n gatt_status_invalid_cid"  ));	


    if( cfm->status == gatt_status_invalid_db 	) LE_DEBUG(("\n gatt_status_invalid_db"  ));


    if( cfm->status == gatt_status_db_full ) LE_DEBUG(("\n gatt_status_db_full"  ));


    if( cfm->status == gatt_status_invalid_phandle )  LE_DEBUG(("\n gatt_status_invalid_phandle"  ));	


    if( cfm->status == gatt_status_invalid_permissions ) LE_DEBUG(("\n gatt_status_invalid_permissions"  ));
    
    
    if( cfm->status == gatt_status_busy  ) LE_DEBUG(("\n gatt_status_busy"  ));

    if( cfm->status == gatt_status_value_mismatch  ) LE_DEBUG(("\n gatt_status_value_mismatch"  ));   
    
            
    if( gatt_status_success == cfm->status )
    {
         MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "SIWRWORESP", "u:y:s ", 
                                          (uint32) (1),
                                          (uint32) cfm->handle,
										  "Success",
                                          strlen("Success")  ));
    }
    else
    {
         MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "SIWRWORESP", "u:y:s:z",
                                           (uint32) (1),
										   (uint32) cfm->handle,
                                          "Err_code",
                                          strlen("Err_code"),
                                          (uint32) (cfm->status&0x00ff) ));
    }    
}




static void Handle_GATTNotificationIndication	( GATT_NOTIFICATION_IND_T * ind )
{
        uint16 datalen = 0;
        uint8 i=0;
 
    LE_DEBUG(("\n from CID=%x,  handle=%x,  rcvd=%d ", ind->cid, ind->handle, ind->size_value ));

        if( ind->size_value > MAX_BLEin_DATALENGTH )
            datalen = MAX_BLEin_DATALENGTH ;
        else
            datalen = ind->size_value;

        if(datalen==0)
        {
            uart_tx( (const char*)" NO Data \n", strlen(" NO Data \n") );
        }
        else
        {  
            LM_DEBUG(("\n datalength = %d \n actaulData is = ", datalen ));

            memcpy(p_lebuff->DualconnBuff, ind->value, datalen);
            
                for(i=0;i<datalen;i++)
                LM_DEBUG((" %x ", ind->value[i] ));
                LM_DEBUG(("\n dualconnBuff data is = "));
                for(i=0;i<datalen;i++)
                LM_DEBUG((" %x ", p_lebuff->DualconnBuff[i] ));
                
            
            uart_tx( (const char*)p_lebuff->DualconnBuff , datalen  );
        }
}
         
static void handle_GattIndicationResponse( GATT_INDICATION_IND_T *ind )
{
  
        
}



static void handleGATT_ReadMultipleCharValues( const GATT_READ_MULTIPLE_CHARACTERISTIC_VALUES_CFM_T *cfm )
{

}





static void handleGattReadLongCharValcfm( GATT_READ_LONG_CHARACTERISTIC_VALUE_CFM_T *cfm )
{
}


static void handle_GATT_LeSec_CFM( CL_DM_BLE_SET_SCAN_PARAMETERS_CFM_T *cfm )
{

        if(stBLEInfoData->bGATTInitStatus  == TRUE)
        {
            if( gatt_status_success == cfm->status )
            {
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LESEC", "s", 
                                          "Success",
                                          strlen("Success") 
                                          ));
            }
            else
            {
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LESEC", "s:z", 
                                          "Err_code",
                                          strlen("Err_code"),
                                          (uint32) cfm->status ));
            }
        }        
}



static void Initiate_disconnection_NoSerialService( void )
{
    event_at_ble_ConDiscon_st *msg = (event_at_ble_ConDiscon_st *)malloc(sizeof(*msg));
    
    LE_DEBUG(("\n Initiate_disconnection_NoSerialService "));
    
    memcpy(&(msg->addr), &(stBLEConnectionData->stConnectedDeviceAddress.addr),  sizeof((msg->addr)) );
    
    PRINT_BDADDR(msg->addr)   ;     
    PRINT_BDADDR(stBLEConnectionData->stConnectedDeviceAddress.addr); 
    
    handle_at_ble_disconn(msg);
    
    free(msg);   
}


static void Initiate_Scan_Serial_Characteristic ( void )
{
    LE_DEBUG(("\n Initiate_Scan_Serial_Characteristic "));
    
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )
    {
         LE_DEBUG(("\n GATT scann-chars for conn-ID=%d, start=%d  end=%d ", 0, SerialServiceData->CharStartHandle, SerialServiceData->CharEndHandle ));
         
         GattDiscoverAllCharacteristicsRequest( app_data.task, stBLEConnectionData->ui16ConnectionID, SerialServiceData->CharStartHandle, SerialServiceData->CharEndHandle );
     }
}


static void Initiate_enable_CCFG ( void )
{
        uint8 value[2] = {1,0};  /* for enb v[1]=0, v[0]=1*/
#define CCFG_DATA_LENGTH    0x0002  /* as per protocol, CCFG value is 2 butes=16bit, with LSB=01_for_enable*/ 
        
    
    LE_INFO(("\n Initiate_enable_CCFG "));   

    /*  at*enbCCFG=connID,char-Handle
        at*rdCharVal=1,0018  (0x0018)
        we need to write 2 bytes at (handle+1) 0and1 */    
    if( KE_CONNECTED == stBLEConnectionData->eConnStatus )
    {
         LE_DEBUG(("\n enb ccfg for conn-ID=%d, char-handle=%d ", 0, ((SerialServiceData->SerialCharHandle)+1)  ));
         
        GattWriteCharacteristicValueRequest ( app_data.task,
                                              stBLEConnectionData->ui16ConnectionID, 
                                              ((SerialServiceData->SerialCharHandle)+1),
                                              CCFG_DATA_LENGTH,
                                              value ); 

    }
    
}

/*******************************************************************************
  Handler for GATT connection Library messages 
 ******************************************************************************/

void handle_BLE_message( Task task, MessageId messageId, Message message )
{  
    bool   Report_end = FALSE;
    
    LE_INFO(("\n BLE_Lib_msg = 0x%x ", messageId));

    switch( messageId )
    {  
        /* GATT */
        case GATT_INIT_CFM:
            LE_INFO(("\n GATT_INIT_CFM"));
            handleGATTInitConfirm( (GATT_INIT_CFM_T *)message );
            if( stBLEInfoData->bGATTInitStatus == TRUE )
                handle_GapCentral_autoConn();
            break;
            
            
        case GATT_CONNECT_CFM:
            {
                LE_INFO(("\n GATT_CONNECT_CFM")); 
                handleGATTConnectionConfirm( (GATT_CONNECT_CFM_T *)message );
             
                if( stBLEInfoData->BLEautoScan == TRUE)
                    BLE_Auto_FindServices();  
            }
            break;
                      
        case GATT_DISCONNECT_IND: 
            LE_INFO(("\n GATT_DISCONNECT_IND"));
            handleGATTDisconnectionIndic((GATT_DISCONNECT_IND_T *)message);

            break;          
            
        case GATT_DISCOVER_ALL_PRIMARY_SERVICES_CFM: 
                LE_INFO(("\n GATT_DISCOVER_ALL_PRIMARY_SERVICES_CFM_T")); 
                Report_end = handleGATTDiscoveryAllPrimServicesCfm( (GATT_DISCOVER_ALL_PRIMARY_SERVICES_CFM_T *)message);
                if( (Report_end == TRUE)  )
                {
                    if( stBLEInfoData->Serial_Service_Found == FALSE) 
                        {
                            LE_DEBUG(("\n At the end of service scan, Serial not found "));
                            Initiate_disconnection_NoSerialService();
                        }
                    else
                    {
                            Initiate_Scan_Serial_Characteristic();
                    }
                }
            break;
            
            
        case GATT_DISCOVER_ALL_CHARACTERISTICS_CFM: 
                LE_INFO(("\n GATT_DISCOVER_ALL_CHARACTERISTICS_CFM")); 
                Report_end = handleGATTDiscoveryAllCharCfm( (GATT_DISCOVER_ALL_CHARACTERISTICS_CFM_T *)message );
                if( (SerialServiceData->WR_and_CCFG_char_found == TRUE) && (stBLEInfoData->Serial_Service_Found == TRUE) )
                {
                    LE_INFO(("\n WRcmd_CCFG_found = true & serialservicefound=true >> inititate ENB-CCFG"));
                    Initiate_enable_CCFG();
                }
                else
                {
                     LE_INFO(("\n WRcmd_CCFG_not found Cant initiate Enb_ccfg???????  "));
                }
            break;
            
        case GATT_WRITE_CHARACTERISTIC_VALUE_CFM:
                LE_INFO(("\n GATT_WRITE_CHARACTERISTIC_VALUE_CFM"));
                Report_end = handleGATTWriteCharCfm( (GATT_WRITE_CHARACTERISTIC_VALUE_CFM_T *)message );
        break;
        
        case GATT_READ_CHARACTERISTIC_VALUE_CFM:
            LE_INFO(("\n GATT_READ_CHARACTERISTIC_VALUE_CFM")); 
            handleGATTReadCharCfm( (GATT_READ_CHARACTERISTIC_VALUE_CFM_T *)message );
            break;
            
        case GATT_READ_LONG_CHARACTERISTIC_VALUE_CFM:
            LE_INFO(("\n GATT_READ_LONG_CHARACTERISTIC_VALUE_CFM_T")); 
            handleGattReadLongCharValcfm((GATT_READ_LONG_CHARACTERISTIC_VALUE_CFM_T *) message);
            break;

        case GATT_INDICATION_IND :
        	LE_DEBUG(("\n GATT_INDICATION_IND nohandler"));
            handle_GattIndicationResponse((GATT_INDICATION_IND_T *) message );
        break; 
        
        case GATT_NOTIFICATION_IND :
            LE_DEBUG (("GATT_NOTIFICATION_IND"));
			Handle_GATTNotificationIndication ( (GATT_NOTIFICATION_IND_T *) message );
        break;

       /* case GATT_WRITE_WITHOUT_RESPONSE_CFM :
            LE_INFO(("\n GATT_WRITE_WITHOUT_RESPONSE_CFM"));
            handleGATT_WriteWoResp_Cfm( (const GATT_WRITE_WITHOUT_RESPONSE_CFM_T *)message );
        break;*/
        
        case GATT_SIGNED_WRITE_WITHOUT_RESPONSE_CFM :
            LE_INFO(("\n GATT_SIGNED_WRITE_WITHOUT_RESPONSE_CFM"));
            handleGATT_signedWriteWoResp_Cfm( (const GATT_SIGNED_WRITE_WITHOUT_RESPONSE_CFM_T *)message );
        break;
        
        
        case GATT_READ_MULTIPLE_CHARACTERISTIC_VALUES_CFM :
            LE_INFO(("\n GATT_READ_MULTIPLE_CHARACTERISTIC_VALUES_CFM "));
            handleGATT_ReadMultipleCharValues( (const GATT_READ_MULTIPLE_CHARACTERISTIC_VALUES_CFM_T *)message );                
        break;
        
        
        
        case GATT_RELIABLE_WRITE_PREPARE_CFM :
            LE_INFO (("GATT_RELIABLE_WRITE_PREPARE_CFM"));
        break;
           
/*********** GAP Peri Messges *******************/
        
        case GATT_CONNECT_IND:
            LM_BLE_DEBUG(("\n GATT_CONNECT_IND")); 
            handleGATTConnectionIndic( (GATT_CONNECT_IND_T *)message);
            break;

        case GATT_ACCESS_IND:
            LM_BLE_DEBUG(("\n GATT_ACCESS_IND")); 
            handle_gatt_access_indication((GATT_ACCESS_IND_T *)message);
            break;
            
        case GATT_EXCHANGE_MTU_IND:
            LM_BLE_DEBUG(("\n GATT_EXCHANGE_MTU_IND ")); 
            handle_exchangeMTUindication((GATT_EXCHANGE_MTU_IND_T *) message);
            break;
                        
        case GATT_DISCOVER_ALL_CHARACTERISTIC_DESCRIPTORS_CFM:
            LM_BLE_DEBUG(("\n GATT_DISCOVER_ALL_CHARACTERISTIC_DESCRIPTORS_CFM")); 
            break;
                    
        case GATT_DISCOVER_PRIMARY_SERVICE_CFM : /* no need to handle for each service, cfm of all services is handled */
        break;
        
        case GATT_DISCOVER_BREDR_SERVICE_CFM : /* no need to handle this if cfm of all_bredr is hanlded */
        break;
        
        case GATT_DISCOVER_ALL_BREDR_SERVICES_CFM :
            LM_BLE_DEBUG(("\n GATT_DISCOVER_ALL_BREDR_SERVICES_CFM_T"));            
        break;
         
        case GATT_INDICATION_CFM :
        	LM_BLE_DEBUG(("\n GATT_INDICATION_CFM nohandler"));
        break;
        
        case GATT_NOTIFICATION_CFM :
            LM_BLE_DEBUG(("\n GATT_NOTIFICATION_CFM ")); 
            Handle_GattNotificationConfirmation((GATT_NOTIFICATION_CFM_T *) message);
        break;
        
        case GATT_WRITE_WITHOUT_RESPONSE_CFM :
            LM_BLE_DEBUG(("\n GATT_WRITE_WITHOUT_RESPONSE_CFM"));
            /* handleGATT_WriteWoResp_Cfm( (GATT_WRITE_WITHOUT_RESPONSE_CFM_T *)message );*/
        break;

        case GATT_RELIABLE_WRITE_EXECUTE_CFM :  
            LM_BLE_DEBUG (("GATT_RELIABLE_WRITE_EXECUTE_CFM"));
        break;

/******************************/        
        
        default: 
            LE_INFO(("\n  Not managed message ID in BLE manager ! "));
        break;
    }
     
}


void handle_DM_BLE_message ( MessageId messageId, Message message )
{
    LE_INFO(("\n  DM_BLE_MSG 0x%x ", messageId ));
    
    switch ( messageId )
    {
        case CL_DM_BLE_ADVERTISING_REPORT_IND:
            LE_INFO(("\n CL_DM_BLE_ADVERTISING_REPORT_IND"));
            handleBLEAdvertisingReportInd( (CL_DM_BLE_ADVERTISING_REPORT_IND_T *)message );
            check_BLEAutoConn();
        break;

        case CL_DM_BLE_SECURITY_CFM :
            LE_INFO(("\n CL_DM_BLE_SECURITY_CFM"));
            handleBLESecurityCfm( (CL_DM_BLE_SECURITY_CFM_T *)message );
        break;
                                                             
            case CL_DM_BLE_SET_SCAN_PARAMETERS_CFM:
            LE_INFO(("\n CL_DM_BLE_SET_SCAN_PARAMETERS_CFM"));
            handle_GATT_LeSec_CFM((CL_DM_BLE_SET_SCAN_PARAMETERS_CFM_T *)message);
            break;
            
                        
        case CL_SM_BLE_SIMPLE_PAIRING_COMPLETE_IND:
            LE_INFO(("\n CL_SM_BLE_SIMPLE_PAIRING_COMPLETE_IND"));
            handleBLESimplePairingCompleteIndic( (CL_SM_BLE_SIMPLE_PAIRING_COMPLETE_IND_T *)message );            
        break;
                        
        case CL_DM_BLE_ACCEPT_CONNECTION_PAR_UPDATE_IND:
            LE_INFO(("\n CL_DM_BLE_ACCEPT_CONNECTION_PAR_UPDATE_IND "));
            handleBLEConnParUpdateIndic( (CL_DM_BLE_ACCEPT_CONNECTION_PAR_UPDATE_IND_T *)message );
        break;
            
        case CL_SM_BLE_IO_CAPABILITY_REQ_IND:    
            LE_INFO(("\n CL_SM_BLE_IO_CAPABILITY_REQ_IND "));
            handleBLEIOCapabilityIndic( (CL_SM_BLE_IO_CAPABILITY_REQ_IND_T *)message );
        break;

       
        case CL_DM_BLE_SET_SCAN_RESPONSE_DATA_CFM:
            LM_BLE_DEBUG(("\n CL_DM_BLE_SET_SCAN_RESPONSE_DATA_CFM"));
        break;
            
        case CL_DM_BLE_SET_ADVERTISING_PARAMS_CFM:
            LM_BLE_DEBUG(("\n CL_DM_BLE_SET_ADVERTISING_PARAMS_CFM"));
            handle_AdvertisingParametersCfm((CL_DM_BLE_SET_ADVERTISING_PARAMS_CFM_T *)message);
        break;
            
        case CL_DM_BLE_SET_ADVERTISING_DATA_CFM:
            LM_BLE_DEBUG(("\n CL_DM_BLE_SET_ADVERTISING_DATA_CFM"));
            handle_BLE_Adv_datacfm((CL_DM_BLE_SET_ADVERTISING_DATA_CFM_T *)message);
        break;
                       
        case CL_DM_BLE_SET_CONNECTION_PARAMETERS_CFM:
            LM_BLE_DEBUG(("\n CL_DM_BLE_SET_CONNECTION_PARAMETERS_CFM"));
        break;
                        
        case CL_DM_BLE_READ_WHITE_LIST_SIZE_CFM:
            LM_BLE_DEBUG(("\n CL_DM_BLE_READ_WHITE_LIST_SIZE_CFM"));
            handle_ReadWhitelistSizeCfm(( CL_DM_BLE_READ_WHITE_LIST_SIZE_CFM_T *)message);
        break;
            
        case CL_DM_BLE_REMOVE_DEVICE_FROM_WHITE_LIST_CFM :
            LM_BLE_DEBUG(("\n CL_DM_BLE_REMOVE_DEVICE_FROM_WHITE_LIST_CFM"));
        break;
            
        case CL_DM_BLE_CLEAR_WHITE_LIST_CFM:
            LM_BLE_DEBUG(("\n CL_DM_BLE_CLEAR_WHITE_LIST_CFM"));
            handle_BLE_ClearWhitelist_cfm((CL_DM_BLE_CLEAR_WHITE_LIST_CFM_T *) message);
        break;
       
        case CL_DM_BLE_CONNECTION_PARAMETERS_UPDATE_CFM :
            LM_BLE_DEBUG(("\n CL_DM_BLE_CONNECTION_PARAMETERS_UPDATE_CFM"));
            handle_BLE_Connection_parameters_Update_Request_cfm((CL_DM_BLE_CONNECTION_PARAMETERS_UPDATE_CFM_T *) message);
        break;
        
        case CL_DM_BLE_ADD_DEVICE_TO_WHITE_LIST_CFM: 
            LM_BLE_DEBUG(("\n CL_DM_BLE_ADD_DEVICE_TO_WHITE_LIST_CFM"));
            handle_BLE_AddDevWhitelistcfm((CL_DM_BLE_ADD_DEVICE_TO_WHITE_LIST_CFM_T *) message);
        break;
                        
        case CL_DM_BLE_CONFIGURE_LOCAL_ADDRESS_CFM:
            LM_BLE_DEBUG(("\n CL_DM_BLE_CONFIGURE_LOCAL_ADDRESS_CFM"));
            handle_configure_local_address_cfm( (CL_DM_BLE_CONFIGURE_LOCAL_ADDRESS_CFM_T *)message );      
        break;
                       
        default:
            LE_INFO((" unhanled ?????? "));
        break;
    }
    
}    
/*************************************************************************************************************
  ************************************************************************************************************/

/*************************************************************************************
  After getting connected with BLE slave, 
  Services and characteristics for each service are scanned automatically by device
  ************************************************************************************/



/*! Function to set the GAP role */
void BLE_Auto_FindServices( void  )
{
    event_BLE_findServ_st *msg = (event_BLE_findServ_st *)malloc(sizeof(*msg));
    
        LE_DEBUG(("\n BLE_Auto_FindServices "));

    send_message(EventAtLEfindServ, (void *)msg);    
}




