

#ifndef  CSR_lib_h
#define  CSR_lib_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include  <memory.h>   /* memcmp8() */

/* used CSR libraries */
#include <bdaddr.h>
#include <boot.h>

#include <connection_no_ble.h>
#include <connection.h>
#include <gatt.h>
#include <library.h>
#include <led.h>
#include <message.h>
#include <panic.h>


#include <aio.h>
#include <adc.h>


#include <pio.h>
#include <ps.h>

#include <sink.h>
#include <source.h>

#include <spp_common.h>
#include <spps.h>       /* SPP server */
#include <sppc.h>       /* SPP client */

#include <stream.h>
#include <transform.h>

#include <util.h>
#include <vm.h>

#include <stdarg.h>

#include <partition.h>


#endif /*csr_lib_h*/

