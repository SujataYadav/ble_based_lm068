#ifndef SPP_COMMON_H
#define SPP_COMMON_H




/* Escape timeouts values */
#define LED_DATA_UPDATE_TIME                200   
#define ESC_DATA_GUARD_TIME		            1000 






#define LINK_SUPERVISION_TIMEOUT  			8000  /*0.625*8000 = 5 seconds */

#define US_MAX_SPP_PAYLOAD_SIZE             ( 0x0064 )

#define SPP_CONNECTED_TRUE                  ( 0x01 )
#define SPP_CONNECTED_FALSE                 ( 0x00 )


/******************************************************************************
  *****************************************************************************/

typedef enum 
{
    SPP_INIT,           /*server_init, client_init*/        
    SPP_READY,          /*ready for master or server connection, no need to be either or */
    
    SPP_OUT_CONNECTING,
    SPP_OUT_CONNECTED,

    SPP_IN_CONNECTING,
    SPP_IN_CONNECTED,

    SPP_DISCONNECTING,
    SPP_DISCONNECTED,
    
    SPP_IDLE
    
}spp_conn_state_st;


typedef struct 
{
    spp_conn_state_st        spp_state;
    
    SPP*            pSppLink;                 /*!< Pointer to SPP link structure */

    Sink            SppSink;                  /*!< SPP sink structure */
    
    bdaddr          spp_remote_dev;
     
}spp_data_st;




void Handle_UART_OTA_Data( MessageMoreData *UARTdata);


















     
        



/*******************************************************************************
  Functions related to OTA functionality
  ******************************************************************************/
void Close_OTA_Partition_Sink   ( void );
void handle_at_OTA_request      ( event_firmware_upgrade_st *event );

void print_16bitInteger         (const uint16 number);


void handleStreamParttionVerify ( MessageStreamPartitionVerify *msg );
        
void handle_OTA_XFER_IDLE_TIMEOUT_MSG ( void );

/******************************************************************************/








void Restore_Escape_Settings ( void );


#endif /* SPP_COMMON_H */

