
#include "../CSR_LIB.h"
#include "../common.h"
#include "../events.h"
#include "../AT_parse/AT_Parse.h"
#include "../config/config.h"

#include "../BT_core/bt_core.h"

#include "../debug.h"

#include "../LED/LED.h"

#include "spp_common.h"
#include "spp_core.h"
#include "../GPIO_define.h"

#include "../BLE/BLE_Cen_core.h"



/*******************************************************************************
  Variables for OTA and partitioning
 *******************************************************************************/                               

typedef struct
{
    uint16 data[66];   /* 64(signature) +  2(CRC) */  
}sign_array ;

typedef struct
{
    uint8 value[140];   /* 4(sf_rfd1u) + 64(signature) +  2(CRC) */  
}dummy_array ;

sign_array *psign;
dummy_array *pdarray;

Sink sqif_sink;
uint8 partition_number = 0xff ; 

/************************************************************
    Application needs to reset following paramertes when
    SPP connection is lost after started writing on SQIF-partition 
 ************************************************************/

static bool FirstTransferOTA=FALSE; 
static uint32 total_bytes = 0;
static uint16 imageflag = 0, sign_array_flag = 0;

static uint16 last_signindex = 0;
/***************************************************************/


/* always have three partitions for code and one for PS-Keys in ext-Flash
0 = Default code
1 = PSkeys
2 = OTA_1 (not mounted in default code)
3 = OTA_2 (not mounted in default code) 
** application should check if the PTN-2 or PTN-3 is available for OTA? 
   it should mount either 2 or 3, but should never un-mount the 0x1000, i.e. default code **
*/


uint16 upgade_debug = FALSE;

/*******************************************************************************/










void Reset_OTA_Progress_Paramters( void );











/*********************************************************************************************************************************************************************************************************
   OTA functionS here Onwards
  ********************************************************************************************************************************************************************************************************/


void handle_OTA_XFER_IDLE_TIMEOUT_MSG ( void )
{
    bool boolSppToPTNSinktBusy=FALSE;
    
    DEBUG_OTA(("\n handle_OTA_XFER_IDLE_TIMEOUT_MSG "));
    
    /* TRUE if the transform exists and has processed data, FALSE otherwise. */        
    if( app_config.FW_upgrade_interface == 2 )
    boolSppToPTNSinktBusy = TransformPollTraffic( TransformFromSink(sqif_sink ) );
    else if( app_config.FW_upgrade_interface == 1 )
    boolSppToPTNSinktBusy = TransformPollTraffic( TransformFromSource(StreamUartSource()) );
    
    
    
    /* if there has been no activity on both streams */
    if ( boolSppToPTNSinktBusy == TRUE && FirstTransferOTA == FALSE )
    {
            DEBUG_OTA(("\n Received FirstTransferOTA=%d ", FirstTransferOTA ));
            FirstTransferOTA = TRUE;
            MessageSendLater( app_data.task, KE_APP_OTA_XFER_IDLE_TIMEOUT_MSG, 0, US_OTA_XFER_IDLE_TIMEOUT_MS ); 
            DEBUG_OTA(("\n  FirstTransferOTA= %d , start_timer", FirstTransferOTA ));
    }    
    else if ( boolSppToPTNSinktBusy == FALSE && FirstTransferOTA == TRUE )
    {
            DEBUG_OTA(("\n Close_OTA_Partition_Sink(); , FirstTransferOTA=%d ", FirstTransferOTA ));
            Close_OTA_Partition_Sink(); /**/ 
    }
    else
    {
            DEBUG_OTA(("\n MessageSendLater >> US_OTA_XFER_IDLE_TIMEOUT_MS  "));
            /* control should never come here */
            MessageSendLater( app_data.task, KE_APP_OTA_XFER_IDLE_TIMEOUT_MSG, 0, US_OTA_XFER_IDLE_TIMEOUT_MS );
    }
}


 
static void Trigger_OTA_IDLE_check_timer( void )
{
    DEBUG_OTA(("\n ++Trigger_OTA_IDLE_check_timer() "));
    
    MessageCancelFirst( app_data.task, KE_APP_OTA_XFER_IDLE_TIMEOUT_MSG );
    
    MessageSendLater( app_data.task, KE_APP_OTA_XFER_IDLE_TIMEOUT_MSG, 0, US_OTA_XFER_IDLE_TIMEOUT_MS );
}



/*******************************************************************************
  OTA related functions 
  >> for OTA 
    >> 1. The host first gets connected to LM961.
    >> 2. Then LM961 enters in command mode by entring the escape sequence
    >> 3. LM961-local host enters the AT command with password to enter in OTA receive mode
    >> 4. at the 3rd step, check the states and give user notification "Ready for OTA"
  for receiving OTA file, remote-Spp-Sink is not connected till the signature is received.
  after receiving signature, sink and partitions are connected.  
  ******************************************************************************/

/* this function prints low level debug messages to UART only if 
    enabled by command "AT*OTADEBUG=ON"
    This is not stored in memory after Power-off
*/
static void uart_tx_update (const char *message, uint16 length)
{
    /*if( app_config.FW_upgrade_interface == 2 )*/
    if( upgade_debug == TRUE )
    {
        uart_tx ( message, length);
    }
}




void handle_at_OTA_request ( event_firmware_upgrade_st *event )
{
    uint16 upgrade_entry= 0xff ;
        
    DEBUG_OTA(("\n handle_at_OTA_request "));

    if(event->upgrade == TRUE )
    {

    }

    
    
    if(upgrade_entry == 0xff)
    {
        MOCK(at_parser_send_STATEerr_response());
        return;        
    }
    else if(upgrade_entry == 0x01)
    {
        app_data.spp_online_state = SPP_UART_OTA_INITIATED ;
    }


    uart_tx( "\r\n Ready For OTA file receive...\n", lstrlen("\r\n Ready For OTA file receive...\n") ); 

    Trigger_OTA_IDLE_check_timer();                
    
    
    
}


static void adjust_number( char *p )
{
    uint8 data = 0;
    data = *p;
    
    if( data <= 9 )
    *p = data + 0x30 ;
    else
    *p = data + 0x40 - 9 ;   
}
void print_16bitInteger ( const uint16 number )
{
    char array[4] = {0x31, 0x31, 0x31, 0x31 };    
    uint8 index = 0;
    
    array[0] =(char) ( ( ( ( number & 0xff00)>>8 )  & 0xf0 ) >> 4 );
    array[1] =(char) ( ( (   number & 0xff00)>>8 )  & 0x0f );
    array[2] =(char) ( ( ( ( number & 0x00ff) )     & 0xf0 ) >> 4 );
    array[3] =(char) ( ( (   number & 0x00ff) )     & 0x0f );
                        
        for(index=0; index<4; index++ )
            adjust_number ( &array[index] );
                        
        uart_tx_update( (const char*)array , 4 );
}




static void enterOTAFileRecieveMode( void )
{
    DEBUG_OTA(("\n+Open partition-Sink for write \n"));
        
    sqif_sink  = StreamPartitionOverwriteSink ( PARTITION_SERIAL_FLASH , 2 );
    
    if( sqif_sink == NULL )
    {
        DEBUG_OTA(("\n Error in opeinig PTN-2, try opening PTN-3 "));
        sqif_sink  = StreamPartitionOverwriteSink ( PARTITION_SERIAL_FLASH , 3 );
        
            if( sqif_sink == NULL )
            {
                 DEBUG_OTA(("\n Error in opeinig PTN-3, No partitions for OTA image "));
            }
            else
            {        
                DEBUG_OTA(("\n Success for PTN_3, created partition"));
                partition_number = 3; 
                uart_tx("\nPTN3" , lstrlen("\nPTN3"));
             }
    }
    else
    {
        DEBUG_OTA(("\n Success for PTN_2, created partition"));
        partition_number = 2; 
        uart_tx("\nPTN2" , lstrlen("\nPTN2"));
    }
    
    
    if( partition_number != 0xff ) 
    {
        MessageSinkTask( sqif_sink, app_data.task );
        
        if( app_config.FW_upgrade_interface == 1)
        StreamConnect( StreamUartSource(),  sqif_sink ); 
        
        
        
    }
    else
    {
        uart_tx("\nPTN-ERR" , lstrlen("\nPTN-ERR"));
        Panic();
    }
}



/*
static void discard_data( const SPP_MESSAGE_MORE_DATA_T *msg )
{
    Source stUartSource = msg->source;
    const uint8* pui8SartSourceMap = SourceMap( stUartSource ); 

    SourceDrop( stUartSource, SourceSize( stUartSource ) );
				
    pui8SartSourceMap = SourceMap( stUartSource );
    
}
*/


/*******************************************************************************
  till the time we are not received the signature bytes, 
  we don't connect SPP-Source/UART-Source to partition-sink.
  We receive signature bytes data in SPPMoredata and then connect soource to Partition sink
  ******************************************************************************/

void Handle_UART_OTA_Data( MessageMoreData *more_data)
{
 

    Source stUartSource = StreamUartSource();
	uint32  bytecount = 0;
	uint16  current_bytes = 0;
    
	uint16  required_bytes = 0;
    
    uint16 index=0; 
	const uint8* pui8SartSourceMap = SourceMap( stUartSource ); 

    
  	DEBUG_OTA(("\n handleSPPMoreData "));
         
	current_bytes = SourceSize( stUartSource );
	DEBUG_OTA(("\n current_bytes = %d , total_bytes = %ld", current_bytes, total_bytes ));

    print_16bitInteger( (const uint16) current_bytes );
                
 
    
/***********************/
            
    if( current_bytes ==0)
	{}
	else
    {    
		if( !imageflag)
		{
				bytecount = current_bytes + total_bytes ;
				/* LM_DEBUG(("\n bytecount = %ld  ", bytecount  )); */

                if(sign_array_flag == 0)
                {
                    pdarray = (dummy_array*) PanicUnlessNew (dummy_array);
                    sign_array_flag = 1 ;
                    uart_tx_update("\n Ar1_OK", lstrlen("\n Ar1_OK"));
                }
                
                
                if(bytecount<140)
                {
                    required_bytes = current_bytes;
                }
                else
                {
                   required_bytes = 140 - total_bytes ; 
                }
                    /* move the current bytes to signature array */
                    for(index=0; index < required_bytes ; index++)
                    {     
                        pdarray->value [ last_signindex + index ] =  *(pui8SartSourceMap+index)  ;    
                        /* LM_DEBUG(("\n pdarray->value[%x] = %x  ", last_signindex+index, pdarray->value[last_signindex+index] )); */
                    }
                    last_signindex =  last_signindex + index ;       
                    
                    /* LM_DEBUG(("\n last_signindex  for next run is %x ", last_signindex ));*/

                    SourceDrop( stUartSource, required_bytes );
                    pui8SartSourceMap = SourceMap( stUartSource );

                    /* check here if there are more than 140 byts and need to open the partiotion for write and write remaining bytes there */
                    if( bytecount >= 140 )
                    {
                        uart_tx_update("\n 140_OK", lstrlen("\n 140_OK"));
                        
                        /*here required bytes are bytes to go in partition */
                        required_bytes = bytecount - 140;
                        
                        psign = (sign_array*) PanicUnlessNew (sign_array);
                        uart_tx_update("\n AR2_OK", lstrlen("\n AR2_OK"));
                                                      
                        /* move the current bytes to signature array */
                        for(index=0; index < 66 ; index++)
                        {   
                            psign->data[index] = (uint16) ( ( *(pdarray->value+(2*index)+8) ) << 8 ) + (uint16) ( *(pdarray->value+((2*index)+1)+8) ) ; 
                             /* DEBUG_OTA(("\n psign->data[%x] = %x  ", index, psign->data[index] )); */
                        }
                        free( (dummy_array *) pdarray );
                        pdarray = NULL;
                        uart_tx_update("\nFill_ok", lstrlen("\nFill_ok"));
                        
                        /* for(index=0; index<66; index++)                        
                        DEBUG_OTA(("\n psign->data[%x] = %x  ", index, psign->data[index] )); */

                        if( required_bytes )
                        {        
                            imageflag = 1;
                            uart_tx_update("\n IMG_Flag_OK" , lstrlen("\n IMG_Flag_OK"));
                            app_data.spp_online_state = SPP_OTA_RECEIVING ;
                            /* LM_DEBUG(("\n dropped remaining_bytes=%d in Sink_partition ", required_bytes ));*/
                            enterOTAFileRecieveMode();
                        
                            SourceDrop( stUartSource, required_bytes );
					        pui8SartSourceMap = SourceMap( stUartSource );
				        }
                    }
        
                    total_bytes = total_bytes + current_bytes ;
					/* LM_DEBUG(("\n total_bytes = %ld ", total_bytes ));*/
		}/*imageflag*/
		else
		{
			SourceDrop( stUartSource, current_bytes );
				
			/* update UART source data pointer */
			pui8SartSourceMap = SourceMap( stUartSource );
		
			/* LM_DEBUG(("\n imageflag == 1, dropped current %d bytes ", current_bytes ));*/
				
			total_bytes = total_bytes + current_bytes ;
		}
    }
/***********************/    
}














void Close_OTA_Partition_Sink ( void )
{
    uint8 signature_set = 0xff ;  	
    bool sinkclose_status = 0;      
    
    DEBUG_OTA(("\nClose_OTA_Partition_Sink "));
    
    if( app_data.spp_online_state == SPP_OTA_RECEIVING )
        app_data.spp_online_state = SPP_OTA_SINK_CLOSED;
    else
    {
        FirstTransferOTA = FALSE;
        uart_tx("\nSPP-Lost" , lstrlen("\nSPP-Lost"));     
        return;
    }
    /*if device lost connection while receiving file, return from here */

    
    StreamDisconnect( NULL , sqif_sink );
            
    signature_set = PartitionSetMessageDigest( sqif_sink, PARTITION_MESSAGE_DIGEST_APP_SIGNATURE, (psign->data) , 66);            

            if(  signature_set == TRUE )
            {
                    uart_tx("\n Sign_OK" , lstrlen("\n Sign_OK"));
                    LM_DEBUG(("\n Applied DFU-Sign = %d " , signature_set) );

                    sinkclose_status = SinkClose ( sqif_sink  );
                    
                    if( sinkclose_status == TRUE )
                    {
                        /*  disconnect the stream with partion-sink; */
                        DEBUG_OTA(("\n closed PTN_WRITE_sink , PTN_NUM = %d ", partition_number));
                        uart_tx("\n SinkClosed" , lstrlen("\n SinkClosed"));
                   
                        uart_tx_update("\n rcvd CRC: " , lstrlen("\n rcvd CRC: ")); 
                        /* CRC is at 0x44 and 0x45, it is 16bit   */
                        print_16bitInteger ( *(psign->data+64) );
                        print_16bitInteger ( *(psign->data+65) );
                        
                        free( (sign_array *) psign );
                    }
                    else
                    {
                        DEBUG_OTA(("\n error in closing the partition write sink   "));
                    }
            }/*signature_set = true*/
        
}




void handleStreamParttionVerify ( MessageStreamPartitionVerify *msg )
{
    uint16 FSTAB_BUFFER_PTN2[3] = {  0x1002, 0x1000, 0x0000 };
    uint16 FSTAB_BUFFER_PTN3[3] = {  0x1003, 0x1000, 0x0000 };
    bool PSstoreStatus = 0; /* true if operation success */
    event_at_reset_st *preset_msg = NULL;

    preset_msg = (event_at_reset_st *)malloc(sizeof(*preset_msg));
        
    DEBUG_OTA((" for %d type device , with Patition_number = %d .", msg->device, msg->partition ));
    
    if( msg->verify_result == PARTITION_VERIFY_PASS )
    {
        DEBUG_OTA((" PARTITION_VERIFY_PASS "));

                if( partition_number == 2)
                {
                    PSstoreStatus = PsStoreFsTab ( FSTAB_BUFFER_PTN2, sizeof (FSTAB_BUFFER_PTN2), TRUE );
                    uart_tx("\n  V_Pass_2" , lstrlen("\n  V_Pass_2"));
                }
                else if( partition_number == 3)
                {
                    PSstoreStatus = PsStoreFsTab ( FSTAB_BUFFER_PTN3, sizeof (FSTAB_BUFFER_PTN3), TRUE );
                    uart_tx("\n  V_Pass_3" , lstrlen("\n  V_Pass_3"));
                }
                else
                {
                    uart_tx("\n  V_Pass_Err" , lstrlen("\n  V_Pass_Err"));
                }
                
                if( PSstoreStatus )
                {
                   /*  LED_SetPreconfLed(LED_KE_WARNING); */
                    preset_msg->reset_level = 2;
                    DEBUG_OTA((" sucesss PSstoreStatus  "));
                }
                else
                {
                    uart_tx("\n PS-Str-err" , lstrlen("\n PS-Str-err"));
                    preset_msg->reset_level = 1;
                    DEBUG_OTA(("\n PS_store Failed "));  
                }
    }
    else if( msg->verify_result == PARTITION_VERIFY_CRC_FAILED  )
    {
        DEBUG_OTA((" PARTITION_VERIFY_CRC_FAILED ")); 
                            preset_msg->reset_level = 1;
       /*  uart_tx("\n failed-CRC" , lstrlen("\n failed-CRC"));   */
       uart_tx("E1" , lstrlen("E1"));
    }
    else if ( msg->verify_result == PARTITION_VERIFY_SIGNATURE_FAILED  )
    {
        DEBUG_OTA((" PARTITION_VERIFY_SIGNATURE_FAILED "));
                            preset_msg->reset_level = 1;
        /*uart_tx("\n failed-Sign" , lstrlen("\n failed-Sign"));*/
        uart_tx("E2" , lstrlen("E2")); 
    }
    else if ( msg->verify_result == PARTITION_VERIFY_OVERFLOW   )
    {
        DEBUG_OTA((" PARTITION_VERIFY_OVERFLOW ")); 
                            preset_msg->reset_level = 1;
        /*uart_tx("\n failed-OVF" , lstrlen("\n failed-OVF"));  */ 
        uart_tx("E3" , lstrlen("E3"));                 
    }
    
        
        send_message(EventAtReset, (void *)preset_msg);
        
        free(preset_msg);  /*Need to test this in OTA  */
}




void Reset_OTA_Progress_Paramters( void )
{
  /**/  bool fail_sinkclose_sqif = 0;  
    
    DEBUG_OTA(("\n disconnected in OTA receiving "));
    
    MessageCancelAll( app_data.task, KE_APP_OTA_XFER_IDLE_TIMEOUT_MSG );
    
    FirstTransferOTA = FALSE;
    total_bytes = 0;
    imageflag = 0;
     sign_array_flag = 0; /*no need to reset this flag as it creates the array using dynamic memory */
    last_signindex = 0;
    
    
    if(pdarray == NULL )
    {
        DEBUG_OTA(("\n pdarray not allocated")); 
    }
    else
    {
        free( (dummy_array *) pdarray );   
        pdarray = NULL;
        DEBUG_OTA(("\n Released dummy_array "));
    }

            
    /*
      This array is deleted/freed after sink is closed after receiving file successfully */
    if(psign == NULL )
    {
        DEBUG_OTA(("\n psign not allocated")); 
    }
    else
    {
        free( (sign_array *) psign );   
        psign = NULL;
        DEBUG_OTA(("\n Released sign_array "));
    }

    /* close the sink and terminate the stream to avoid further wrong bytes being written to SQIF */
        StreamDisconnect( NULL , sqif_sink );
        fail_sinkclose_sqif = SinkClose ( sqif_sink  );
           DEBUG_OTA(("\n fail_sinkclose_sqif = %d ", fail_sinkclose_sqif));
     
        
}


/*******************************************************************************************************
  ******************************************************************************************************/





