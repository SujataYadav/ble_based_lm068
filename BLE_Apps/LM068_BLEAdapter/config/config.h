
#ifndef config_h
#define config_h



/******************************************************************************/



/********************************** Typedef Stucts here ***********************/

    static const char* const baud_array[10] = { "NotApplicable", "9600(1)", "19200(2)" , "38400(3)" , "57600(4)" , "115200(5)" , "230400(6)" , "460800(7)" , "921600(8)" , "1382400(9)" };   
    static const char* const StopBit_array[3] = { "Stop_One(0)", "Stop_Two(1)" , "Stop_Same(2)"};   
    static const char* const ParityBit_array[4] = { "None(0)", "Odd(1)" , "Even(2)", "Same(3)"};   
    static const char* const UARTconfig_array[2] = { "UART_LATENCY", "UART_THROUGHPUT" };
    
typedef struct 
{
    uint16      Baud;
    uint16      StopBits;
    uint16      Parity;
    
} uart_config_t;


/** Application-specific configuration data structure. */
typedef struct 
{
    /** The length of the device name */
    uint16       name_len;
    
    /** The local device name as read from PS or as set in AT commands */
    uint8       dev_name[BT_AT_DEVICE_NAME_LENGTH];
    
} app_devname_t;



/** Application version configuration data structure. */
typedef struct 
{
    /** Major version number */
    uint16 major;
    /** Minor version number */
    uint16 minor;
} version_config_t;


/** Application version configuration data structure. */
typedef struct 
{
    /** Major version number */
    uint16 PIN[BT_AT_DEVICE_PIN_LENGTH];
               
} dev_pin_t;

/*************************************** Enumerations here *************************************************/



/* Serial Role enum */
typedef enum
{
    KE_SPP_ROLE_SLAVE = 0,
    KE_SPP_ROLE_MASTER,
    KE_SPP_DUAL_ROLE
} app_SerialRole;

/* Modem Mode enum */
typedef enum
{
    APP_KE_MODEM_SIG_NONE,
    APP_KE_MODEM_SIG_LOOPBACK, /* Local Flowcontrol for modem-handshake */
    APP_KE_MODEM_SIG_REMOTE,     /* Remote transferred FlowControl for modem-handshake */
    APP_KE_MODEM_MODES_MAX_NUM
} app_ModemMode;


/***************************************** App config structure ***********************************************/

/** Application-specific configuration data structure. */
typedef struct 
{

    dev_pin_t           PIN;
    uint16              PIN_len;
    cl_sm_io_capability iotype;    
    bool                mitm;
    bool                dpin;
   
    
    /* */
    uart_config_t       UartCFG; 
    
    bool                flow;
    bool                echo;
    bool                resp;
    
    /**/
    bool                dcov;
    bool                pair;
    
    
    version_config_t   ver; 

    
    uint16              AddressBytesInName;
    
    uint16          FW_upgrade_interface;
    
} app_config_st;




extern  app_config_st        app_config;




/*******************************************************************************
  Function declaration here
  ******************************************************************************/
  
uint16 Read_UART_STREAM_CONFIG_fromPS ( void );

void ReadnBoot_with_FlowControl ( void );

void Config_Init (void );

void Clear_memory_Location ( uint16 key );

bool Store_memory_Location ( uint16 key, const uint16* ptr, uint16 words);

void Initiate_device_reset_delay( void );
void resetSppApp ( void );


void write_BDaddr(uint16 location, bdaddr* far_addr);
void clear_BDaddr(uint16 location);
void read_BDaddr(uint16 location , bdaddr* far_addr );

void Read_DevName_FromPSkey (void);

void handle_at_GetRSSI ( void );

void Read_AddAddressBytesInNameSetting_fromPskey ( void );
void get_updated_name( app_devname_t  *pname );
void get_device_name( app_devname_t  *pname );

uint16 Read_UPGARDE_INTERFACE_fromPS ( void );

/* -------------------------------------------------------------------------
 *                              PSKEY numbers
 * -------------------------------------------------------------------------
 * Non-USR PSKEY numbers
 */

#define PSKEY_BDADDR           0x0001
#define PSKEY_UART_BITRATE     0x01ea
#define PSKEY_UART_CONFIG_USR  0x01c2

/****************************************************************
    below locations mentions the USer keys in Persistent storage
 ****************************************************************/
/* Actual PS-key Location is base + offset */
#define USER_PSKEY_0        (0x028a)

#define LCN_NAME_LENGTH     (0)
#define LCN_NAME_STRING     (1)
/*#define LCN_SERIAL_ROLE     (2)
#define LCN_ACON            (3)
#define LCN_MODEM_TYPE      (4)*/
#define LCN_PIN             (5)
#define LCN_IOTYPE          (6)
#define LCN_MITM            (7)
#define LCN_DPIN            (8)
#define LCN_UART_CONFIG     (9)
#define LCN_FLOW            (10)
#define LCN_ECHO            (11)
#define LCN_RESP            (12)
#define LCN_DCOV            (13)
#define LCN_PAIR            (14)
#define LCN_FW_VERSION      (15)
/*#define LCN_MASTER_ACON_BDADDR  (16)*/
#define LCN_BLE_WHITE_LIST_ENB  (17)
/* This is used for auto scan of the slave device for BLE */
#define LCN_BLE_CLIENT_AUTOSCAN_SERVER  (18)
#define LCN_LOCAL_BD_ADDRESS  (19)
 
#define LCN_BLE_ADVERT_ADDRESS  	(21)

#define LCN_PIN_LENGTH        (22)

#define LCN_LE_BOND_BDADDR  (23)

#define LCN_ADD_ADDRESS_BYTES_IN_NAME (25)

#define LCN_UART_STREAM_CONFIG (26)
#define LCN_UPGRADE_INTERFACE  (27)

#define LCN_BLE_CLIENT_AUTOCONN_PERIPHERAL  (28)
#define LCN_LE_ADD_FILTER           (30)


#define LCN_BLE_ROLE              	(31)

#define LCN_MAXIMUM_PS_STORE   ( LCN_BLE_ROLE )



#endif /*config_h*/
