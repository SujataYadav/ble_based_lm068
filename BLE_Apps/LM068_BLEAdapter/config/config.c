
#include "../CSR_LIB.h"
#include "../common.h"

#include "../debug.h"

#include "config.h"



#include "../BLE/BLE_Cen_core.h"

#include "../BLE_Periph/BLE_Peri.h"


app_config_st       app_config ;



static void Read_Version_fromPSkey( void );
static void Read_DevicePIN_fromPSkey( void );
static void Read_UARTConfig_fromPSkey( void );


/************************************************************************************
  ***********************************************************************************/
const app_config_st Default_App_config =
{ 

    {
        {'1','2','3','4'}
    },           /* PIN; */
    4,  /* PIN_len */

    cl_sm_io_cap_keyboard_only,  /* iotype; */   
    TRUE,               /*  mitm;*/
    TRUE,               /*  dpin;*/
      
    
    {
        VM_UART_RATE_19K2,
        VM_UART_STOP_ONE,
        VM_UART_PARITY_NONE
    },                      /*UartCFG;*/

    FALSE,               /*  flowcontrol;*//*defalt initial_boot_mode = 2 = BOOT_MODE_FLOW_CONTROL_OFF */
    TRUE,                /* echo;*/
    TRUE,                /* resp;*/
    
   
    FALSE,                /* dcov;*/
    
    FALSE,              /* pair;*/
    
    {
        0x02,
        0x01
    },                /*FW version,
                        01-0a fixed the bug for Flow control ON with RTS line*/

    TRUE,              /* AddressBytesInName; */
    
    /* 1= UART, 2=OTA */
    1,           /* FW_upgrade_interface */

    
};


/*LM961 provides 
    Bootmode=1 flow control ON
    Bootmode=2 flow control OFF
    Read the Flow control setting at power up and boot in that mode
*/
void ReadnBoot_with_FlowControl ( void )
{
    uint16 size = 0;  
    
    size = sizeof(app_config.flow);
    if ( size == PsRetrieve( LCN_FLOW , &app_config.flow , size ) )
    {
         LM_INFO_PS(("CONF: flow=%s \n",&app_config.flow ? "flow+" : "flow-" )); 
    }
    else
    {
        LM_INFO_PS(("\n flow Load_default"));
        app_config.flow = Default_App_config.flow ;
        PsStore( LCN_FLOW, &app_config.flow, sizeof(app_config.flow) );
    }
    
    if( ( BootGetMode() == BOOT_MODE_FLOW_CONTROL_ON ) &&  ( app_config.flow == FALSE ) )
    {
        BootSetMode(BOOT_MODE_FLOW_CONTROL_OFF);
    }
    else if( ( BootGetMode() == BOOT_MODE_FLOW_CONTROL_OFF ) &&  ( app_config.flow == TRUE ) )
    {
        BootSetMode(BOOT_MODE_FLOW_CONTROL_ON);
    }
        
}


void Config_Init (void)
{
  
    uint16 size = 0;  
 
    /* Local BD addr    
    size = sizeof(app_config.myaddr);
    if (size == PsFullRetrieve(PSKEY_BDADDR, &app_config.myaddr, size ))
    {
        LM_INFO_PS(("CONF: size=%d , PSKEY_BDADDR [%04x %02x %06lx]\n", size,
            app_config.myaddr.nap,
            app_config.myaddr.uap,
            app_config.myaddr.lap));

    }*/ 
    
    
    Read_UARTConfig_fromPSkey();    
    
   Read_DevName_FromPSkey (); /**/
    

/**/
    size = sizeof(app_config.mitm);
    if ( size == PsRetrieve( LCN_MITM, &app_config.mitm , size ) )
    {
        LM_INFO_PS(("CONF: mitm=%s \n", &app_config.mitm ? "MITM+": "MITM-" ));
    }
    else
    {
        LM_INFO_PS(("\n  MITM Load_default"));
        app_config.mitm = Default_App_config.mitm  ;
        PsStore( LCN_MITM, &app_config.mitm, sizeof(app_config.mitm) );
    }
    
    size = sizeof(app_config.dpin);
    if (size == PsRetrieve( LCN_DPIN, &app_config.dpin , size ))
    {
        LM_INFO_PS(("CONF: DPIN=%s \n", &app_config.dpin ? "DPIN+": "DPIN-" ));
    }
    else
    {
        LM_INFO_PS(("\n DPIN Load_default"));
        app_config.dpin = Default_App_config.dpin ;
        PsStore( LCN_DPIN, &app_config.dpin, sizeof(app_config.dpin) );
    }
    
    Read_DevicePIN_fromPSkey();    

    
    size = sizeof(app_config.iotype);
    if ( size == PsRetrieve( LCN_IOTYPE , &app_config.iotype , size ) )
    {
        LM_INFO_PS(("CONF: IOtype=%d \n", app_config.iotype ));
    }
    else
    {
        LM_INFO_PS(("\n IOTYPE Load_default"));
        app_config.iotype = Default_App_config.iotype ;
        PsStore( LCN_IOTYPE, &app_config.iotype, sizeof(app_config.iotype) );
    }
    
    
    size = sizeof(app_config.echo);
    if ( size == PsRetrieve( LCN_ECHO , &app_config.echo , size ) )
    {
        LM_INFO_PS(("CONF: ECHO=%s \n", app_config.echo ? "ECHO+" : "ECHO-" ));
    }
    else
    {
        LM_INFO_PS(("\n ECHO Load_default"));
        app_config.echo = Default_App_config.echo ;
        PsStore( LCN_ECHO, &app_config.echo, sizeof(app_config.echo) );
    }

    
    size = sizeof(app_config.resp);
    if ( size == PsRetrieve( LCN_RESP , &app_config.resp , size ))
    {
        LM_INFO_PS(("CONF: RESP=%s \n", app_config.resp ? "RESP+" : "RESP-" ));
    }
    else
    {
        LM_INFO_PS(("\n resp Load_default"));
        app_config.resp = Default_App_config.resp ;
        PsStore( LCN_RESP, &app_config.resp, sizeof(app_config.resp) );
    }  

/* */
    size = sizeof(app_config.dcov);
    if ( size == PsRetrieve( LCN_DCOV , &app_config.dcov , size ))
    {
        LM_INFO_PS(("CONF: DCOV=%s \n", app_config.dcov ? "DCOV+" : "DCOV-" ));
    }
    else
    {
        LM_INFO_PS(("\n DCOV Load_default"));
        app_config.dcov = Default_App_config.dcov ;
        PsStore( LCN_DCOV, &app_config.dcov, sizeof(app_config.dcov) );
    }

    size = sizeof(app_config.pair);
    if ( size == PsRetrieve( LCN_PAIR , &app_config.pair , size ))
    {
        LM_INFO_PS(("CONF: PAIR=%s \n", app_config.pair ? "PAIR+" : "PAIR-" ));
    }
    else
    {
        LM_INFO_PS(("\n PAIR Load_default"));
        app_config.pair = Default_App_config.pair ;
        PsStore( LCN_PAIR, &app_config.pair, sizeof(app_config.pair) );
    }

    
    Read_Version_fromPSkey();

    app_config.FW_upgrade_interface = Read_UPGARDE_INTERFACE_fromPS(); 
    
    
}

void Read_AddAddressBytesInNameSetting_fromPskey ( void )
{
    uint16 size;
    
    size = sizeof(&app_config.AddressBytesInName);
    if (size == PsFullRetrieve( (USER_PSKEY_0+LCN_ADD_ADDRESS_BYTES_IN_NAME), &app_config.AddressBytesInName, sizeof(&app_config.AddressBytesInName) ))
    {
        LM_INFO_PS(("Size=%d,,  AddressBytesInName = %d  (1=True, 0=False)\n",size ,  app_config.AddressBytesInName ));
    }
    else
    {
        LM_INFO_PS(("AddressBytesInName  = load defult  \n" ));
        app_config.AddressBytesInName = Default_App_config.AddressBytesInName;
        PsStore( LCN_ADD_ADDRESS_BYTES_IN_NAME, &app_config.AddressBytesInName, sizeof(app_config.AddressBytesInName) );
    }
}

void Read_DevName_FromPSkey (void)
{
    uint16 i=0, success = 0;    
    uint16 size, read_bytes = 0; 
    
    app_devname_t       name;
    
     
    const app_devname_t Default = { strlen("LM068_BLE_Adapter"),  "LM068_BLE_Adapter" };

        Read_AddAddressBytesInNameSetting_fromPskey();    
        
    size = sizeof(name.name_len);
    if (size == PsFullRetrieve( (USER_PSKEY_0+LCN_NAME_LENGTH), &name.name_len, sizeof(name.name_len) ))
    {
        if( name.name_len < BT_AT_DEVICE_NAME_LENGTH )
        {
            success = 1;
            LM_INFO_PS(("CONF: Valid Name_length = %d \n", name.name_len ));        
        }
    }

    if ( success )
    {
            /*read Dev-name*/
            read_bytes = PsFullRetrieve( (USER_PSKEY_0+LCN_NAME_STRING), &name.dev_name, name.name_len ); /*returns 0 if unsuccess */
            if( read_bytes )
            {
                LM_INFO_PS(("\n CONF: Dev-Name = "));
                for (i=0; i<(name.name_len); i++ )
                    LM_INFO_PS((" %c=%d ",  name.dev_name[i], name.dev_name[i] )); 
                
                LM_INFO_PS(("END.."));
            }  
            else
                success =0;
     }
    
    if( success == 0 )  /*if(1)*/ 
    {
        /* name not read successfully, load default */
        LM_INFO_PS((" Namebytes = %d ", read_bytes ));
                
        memcpy( &name.name_len, &Default.name_len, sizeof(Default.name_len) );
        PsStore( LCN_NAME_LENGTH, &name.name_len, sizeof(name.name_len) );   
                
        memcpy(  name.dev_name, Default.dev_name, Default.name_len );
        PsStore( LCN_NAME_STRING, &name.dev_name, name.name_len ); 
    }
    
   /* ConnectionChangeLocalName(name.name_len, name.dev_name ); */
    
}

static void Read_UARTConfig_fromPSkey( void )
{
/**/
    uint16 size=0, buff[3], actual_read=0; 

    size = sizeof(app_config.UartCFG);
    actual_read = PsFullRetrieve( (USER_PSKEY_0+LCN_UART_CONFIG), buff, sizeof(app_config.UartCFG) ) ;
    if ( size == actual_read  )
    {
        app_config.UartCFG.Baud = buff[0];
        app_config.UartCFG.StopBits = buff[1];
        app_config.UartCFG.Parity = buff[2];
        LM_INFO_PS(("valid UCFG  Baud=%d , STOP-bit=%d , Parity=%d \n", app_config.UartCFG.Baud, app_config.UartCFG.StopBits, app_config.UartCFG.Parity  ));  
    }
    else
    {
        LM_INFO_PS(("invalid UARTCFG actual-length=%d , size=%d \n", actual_read , size ));
        app_config.UartCFG.Baud = Default_App_config.UartCFG.Baud;
        app_config.UartCFG.StopBits = Default_App_config.UartCFG.StopBits;
        app_config.UartCFG.Parity = Default_App_config.UartCFG.Parity;
        buff[0] = app_config.UartCFG.Baud ;
        buff[1] = app_config.UartCFG.StopBits ;
        buff[2] = app_config.UartCFG.Parity ;
        PsStore( LCN_UART_CONFIG, buff, sizeof(app_config.UartCFG) );
    }
    
}

static void Read_Version_fromPSkey( void )
{
    uint16 size=0, buff[2]; 

    size = sizeof(app_config.ver); LM_INFO_PS(("sizeof Version = %d \n", size )); 
    
    if ( size == PsFullRetrieve( (USER_PSKEY_0+LCN_FW_VERSION), &buff, sizeof(app_config.ver) ) )
    {
        app_config.ver.major = buff[0]; 
        app_config.ver.minor = buff[1];
        LM_INFO_PS(("FW Version major=%d, minor=%d", app_config.ver.major, app_config.ver.minor ));
    }
    else
    {
        LM_INFO_PS(("invalid FW Version  = load defult  \n" ));
        app_config.ver.major = Default_App_config.ver.major;
        app_config.ver.minor = Default_App_config.ver.minor;
        buff[0] = app_config.ver.major ;
        buff[1] = app_config.ver.minor ;
        PsStore( LCN_FW_VERSION, buff, sizeof(app_config.ver) );
    }
}

static void Read_DevicePIN_fromPSkey( void )
{
    uint16 size=0, ReadBytes=0,success = 0 ;
    uint16 i=0 ; 
   /* uint16 *ptr=NULL;*/
    
	
	size = sizeof(Default_App_config.PIN_len);
    if (size == PsFullRetrieve( (USER_PSKEY_0+LCN_PIN_LENGTH), &app_config.PIN_len, sizeof(app_config.PIN_len) ))
    {
        if( app_config.PIN_len < BT_AT_DEVICE_PIN_LENGTH )
        {
            success = 1;
            LM_INFO_PS(("CONF: Valid PIN_Length = %d \n", app_config.PIN_len));        
        }
    }
	
	
	if ( success )
    {
            ReadBytes = PsFullRetrieve( (USER_PSKEY_0+LCN_PIN), app_config.PIN.PIN , app_config.PIN_len );
            if( ReadBytes )
            {
                LM_INFO_PS(("\n CONF: Dev-Pin = "));
                for (i=0; i<=(app_config.PIN_len); i++ )
                    LM_INFO_PS((" %c ",  app_config.PIN.PIN[i] )); 
            }  
            else
			{
				LM_INFO_PS(("\n Need to Load default PIN "));
                success =0;
			}
     }
	
	
    if( success == 0 )
    {
        /* name not read successfully, load default */
        LM_INFO_PS((" PIN Bytes = %d \n Load Default PIN", ReadBytes ));
                
        memcpy( &app_config.PIN_len, &Default_App_config.PIN_len, sizeof(Default_App_config.PIN_len) );
        PsStore( LCN_PIN_LENGTH, &app_config.PIN_len, sizeof(Default_App_config.PIN_len) );   
                
        memcpy(  app_config.PIN.PIN, Default_App_config.PIN.PIN , Default_App_config.PIN_len );
        PsStore( LCN_PIN, app_config.PIN.PIN , Default_App_config.PIN_len ); 
		
    }

	
}



/* 
    Clear_memory_LOcations 
    words = number of words o clear
**/
void Clear_memory_Location ( uint16 	key_location )
{
    /*
    A Persistent store user key can be deleted by calling the PsStore function 
    with "key" to be deleted and "words" argument being zero.
    uint16 PsStore	(uint16 key, const void* buff, uint16 words)	
    */
    if(  key_location <= LCN_MAXIMUM_PS_STORE   /* LCN_ENABLE_GAPCENTRAL */  )
            PsStore ( key_location, NULL, 0);

}

bool Store_memory_Location ( uint16 key, const uint16* ptr, uint16 words)
{
    uint16 written = 0;

            written  = PsStore ( key, ptr, words);

    if(written != 0)
        return TRUE;
    else
        return FALSE;
}


/*******************************************************************************
  Read, write and clear of BT adress stored in PS_memory
  ******************************************************************************/
       
/* Manipulate far_addr, keeping it in step with the copy in Persistent Store */
void write_BDaddr(uint16 location, bdaddr* far_addr)
{ 
    bdaddr bd_addr;
   
    memcpy( &(bd_addr), far_addr, sizeof(bd_addr) );
    LM_DEBUG(("\n   Write far addr: %4x-%2x-%6lx  \n",bd_addr.nap,bd_addr.uap,bd_addr.lap));
        
    PsStore(location, (void *)&bd_addr, sizeof(bd_addr)); 
}

void clear_BDaddr(uint16 location)
{
    bdaddr addr;

    uint16 read_bytes=0;
    
    BdaddrSetZero (&addr);        
        
    (void) PsStore(location, 0, 0);
    LM_DEBUG(("Clear far addr:\n"));
    
      read_bytes = PsRetrieve(location, (&addr), sizeof(bdaddr)) ;  
      LM_DEBUG(("\n bdaddress_size=%d  , read_bytes=%d ",sizeof(bdaddr),   read_bytes ));
    if (sizeof(bdaddr) == read_bytes )
    {
        LM_DEBUG(("\n address_read_success after clearing it  "));
        LM_DEBUG(("Read addreess after clearing it as : %04x-%02x-%06lx", addr.nap, addr.uap, addr.lap));
    }
}

void read_BDaddr(uint16 location , bdaddr* far_addr )
{
    uint16 read_bytes=0;
    read_bytes = PsRetrieve(location, far_addr, sizeof(bdaddr)) ;
    
        LM_DEBUG(("\n bdaddress_size=%d  , read_bytes=%d ",sizeof(bdaddr),   read_bytes ));    
    
    if (sizeof(bdaddr) == read_bytes )
    {
        LM_DEBUG(("\n address_read_success "));
        LM_DEBUG(("Read far addr: %04x-%02x-%06lx", far_addr->nap, far_addr->uap, far_addr->lap));
    }
    
}

/*******************************************************************************
  Read BLE parameters form PS configurations at power up
  *****************************************************************************/ 
  
  
void Read_LE_Security_fromPS ( void )
{
    uint16 size = 0;
            
    size = sizeof(stBLEInfoData->WhiteListEnable);
    if ( size == PsRetrieve( LCN_BLE_WHITE_LIST_ENB , &stBLEInfoData->WhiteListEnable , size ))
    {
        LM_INFO_PS(("CONF: BLE_WHITE-LIST=%s \n", stBLEInfoData->WhiteListEnable ? "TRUE" : "FALSE" ));
    }
    else
    {
        LM_INFO_PS(("\n BLE_WHITE-LIST default "));
        stBLEInfoData->WhiteListEnable = FALSE ; /* BLE whitelist-enable by default is off */
        PsStore( LCN_BLE_WHITE_LIST_ENB, (const uint16*)&stBLEInfoData->WhiteListEnable, sizeof(stBLEInfoData->WhiteListEnable) );
    }
}



void Read_LE_AutoScan_fromPS ( void )
{
    uint16 size = 0;
            
    size = sizeof(stBLEInfoData->BLEautoScan);
    if ( size == PsRetrieve( LCN_BLE_CLIENT_AUTOSCAN_SERVER , &stBLEInfoData->BLEautoScan , size ))
    {
        LM_INFO_PS(("CONF: BLE-Auto-Scan=%s \n", stBLEInfoData->BLEautoScan ? "TRUE" : "FALSE" ));
    }
    else
    {
        LM_INFO_PS(("\n BLE-Auto-Scan default = FALSE "));
        stBLEInfoData->BLEautoScan = FALSE; /* BLE AutoScan server is default TRUE */
        PsStore( LCN_BLE_CLIENT_AUTOSCAN_SERVER, (const uint16*)&stBLEInfoData->BLEautoScan, sizeof(stBLEInfoData->BLEautoScan) );
    }
    
}

void Read_LE_AutoConnect_fromPS (void )
{
    uint16 size = 0;
            
    size = sizeof(stBLEInfoData->BLEautoConnect);
    if ( size == PsRetrieve( LCN_BLE_CLIENT_AUTOCONN_PERIPHERAL , &stBLEInfoData->BLEautoConnect , size ))
    {
        LM_INFO_PS(("CONF: BLE_BLEautoConnect=%s \n", stBLEInfoData->BLEautoConnect ? "TRUE" : "FALSE" ));
    }
    else
    {
        LM_INFO_PS(("\n BLE_BLEautoConnect default "));
        stBLEInfoData->BLEautoConnect = FALSE; 
        PsStore( LCN_BLE_CLIENT_AUTOCONN_PERIPHERAL, (const uint16*)&stBLEInfoData->BLEautoScan, sizeof(stBLEInfoData->BLEautoScan) );
    }
    
}

void Read_LE_AddFilter_fromPS (void )
{
    uint16 size = 0;
            
    size = sizeof(stBLEInfoData->BLEApplyFilter);
    if ( size == PsRetrieve( LCN_LE_ADD_FILTER , &stBLEInfoData->BLEApplyFilter , size ))
    {
        LM_INFO_PS(("CONF: BLE_BLEApplyFilter=%s \n", stBLEInfoData->BLEApplyFilter ? "TRUE" : "FALSE" ));
    }
    else
    {
        LM_INFO_PS(("\n BLE_BLEApplyFilter default = FALSE "));
        stBLEInfoData->BLEApplyFilter = FALSE; 
        PsStore( LCN_LE_ADD_FILTER, (const uint16*)&stBLEInfoData->BLEApplyFilter, sizeof(stBLEInfoData->BLEApplyFilter) );
    }
    
}


void Read_BLE_ROLE_fromPS ( void )
{
    uint16 size = 0;
            
    /* 1=Central,  2=Peripheral */    
    
    size = sizeof( stBLEInfoData->BLE_Role );
    if ( size == PsRetrieve( LCN_BLE_ROLE , &stBLEInfoData->BLE_Role , size ))
    {
        LM_INFO_PS(("CONF: BLE_Role=%s \n", (stBLEInfoData->BLE_Role==1) ? "CENTRAL" : "PERIPHERAL" ));
    }
    else
    {
        LM_INFO_PS(("\n BLE_Role default = Central "));
        stBLEInfoData->BLE_Role = GAP_CENTRAL_ROLE ;  /* 1=Central,  2=Peripheral */ 
        PsStore( LCN_BLE_ROLE, (const uint16*)&stBLEInfoData->BLE_Role, sizeof(stBLEInfoData->BLE_Role) );
    }
    
}





/****************************************************************************************************************************************
 ****************************************************************************************************************************************/



uint16 Read_UART_STREAM_CONFIG_fromPS ( void )
{
    /*    static const char* const UARTconfig_array[2] = { "UART_LATENCY", "UART_THROUGHPUT" }; */
    
    uint16 size = 0;
    uint16 uart_stream_config=0x55;
    
    size = sizeof(uart_stream_config);
    if ( size == PsRetrieve( LCN_UART_STREAM_CONFIG , &uart_stream_config , size ))
    {
        LM_INFO_PS(("CONF: UART-Stream =%s \n", uart_stream_config ? "TRUE" : "FALSE" ));
    }
    else
    {
        LM_INFO_PS(("\n default configuration is VM_STREAM_UART_THROUGHPUT=1 for Mettler=0=VM_STREAM_UART_LATENCY"));
        uart_stream_config = TRUE; 
        PsStore( LCN_UART_STREAM_CONFIG, (const uint16*)&uart_stream_config, sizeof(uart_stream_config) );
    }
    
    return uart_stream_config;
}


uint16 Read_UPGARDE_INTERFACE_fromPS ( void )
{
 /* 1= UART, 2=OTA */
    
    uint16 size = 0;
    uint16 upgrade_interface=0x55;
    
    size = sizeof(upgrade_interface);
    if ( size == PsRetrieve( LCN_UPGRADE_INTERFACE , &upgrade_interface , size ))
    {
        LM_INFO_PS(("CONF: UPGRADE_INTF=%s = %d \n", upgrade_interface ? "UART" : "OTA",   upgrade_interface));
    }
    else
    {
        LM_INFO_PS(("\n default UPGARDE_INTERFACE = OTA=2, UART=1  "));
        upgrade_interface = 1; 
        PsStore( LCN_UPGRADE_INTERFACE, (const uint16*)&upgrade_interface, sizeof(upgrade_interface) );
    }
    
    return upgrade_interface;
    
}













