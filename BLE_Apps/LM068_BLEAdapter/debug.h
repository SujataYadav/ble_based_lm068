
#ifndef debug_h
#define debug_h


/*******************************************************************************
  Make sure while creating the production image, 
    all the Debug messages are disabled
 *******************************************************************************/

/* ring buffer print messgaes 
#define CFG_RING_PRINT 1 */


/*    
Define this if needed to print the LED sequence messages  // LED.c file
#define CFG_ENABLE_LED_PRINT 1 */


/* detailed Information messages, for all files 
#define CFG_ENABLE_DEBUG    
*/


/* debug messages, for all files 
#define CFG_ENABLE_INFO_MSG 
*/ 


/* debug only for PS read-write "Confog.c"  
#define CFG_ENABLE_PS_READ
*/


/* debug print_bluetooth_device_address 
#define  PRINT_BLUETOOTH_ADDR 
 */


/* Info Log messages only for LE files 
#define CFG_ENABLE_LE_INFO 
*/


/* debug messages only for LE files 
#define CFG_ENABLE_LE_DEBUG  */



/* debug messages only for LE files 
#define CFG_ENABLE_BLE_PERI         */



/* debug OTA messages  
#define  CFG_DEBUG_OTA
   */ 



#ifdef  CFG_RING_PRINT 
#define LOG_RING(x)             printf x
#else
#define LOG_RING(x)  
#endif




/* all files */
#ifdef  CFG_ENABLE_DEBUG
#define LM_DEBUG(x)             printf x
#else
#define LM_DEBUG(x)  
#endif


/* all files */
#ifdef  CFG_ENABLE_INFO_MSG
#define LOG_INFO(x)             printf x
#else
#define LOG_INFO(x)
#endif


/* Local define for reading settings from PS area, Config.c */
#ifdef  CFG_ENABLE_PS_READ
#define LM_INFO_PS(x)             printf x
#else
#define LM_INFO_PS(x)  
#endif

/* INFO messages for LE files only */
#ifdef  CFG_ENABLE_LE_INFO
#define LE_INFO(x)             printf x
#else
#define LE_INFO(x)  
#endif


/* Debug messages for LE files only */
#ifdef  CFG_ENABLE_LE_DEBUG
#define LE_DEBUG(x)             printf x
#else
#define LE_DEBUG(x)  
#endif


/* Local define for debug messages in BLE peripheral files */
#ifdef  CFG_ENABLE_BLE_PERI
#define LM_BLE_DEBUG(x)             printf x
#else
#define LM_BLE_DEBUG(x)  
#endif

      


#ifdef  PRINT_BLUETOOTH_ADDR
#define PRINT_BDADDR(x)                (void)printf("\n (bd_addr=%04x-%02x-%06lx)", x.nap, x.uap, x.lap )
#else
#define PRINT_BDADDR(x) 
#endif 



#ifdef  CFG_DEBUG_OTA
#define DEBUG_OTA(x)             printf x
#else
#define DEBUG_OTA(x) 
#endif


#endif 


