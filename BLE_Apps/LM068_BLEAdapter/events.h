
#ifndef EVENTS_H
#define EVENTS_H



extern bool Dev_Initiated;


#define US_OTA_XFER_IDLE_TIMEOUT_MS             ((uint16)15000)   /*  10_seconds */
#define US_WAIT_BEFORE_AUTO_CONNECT_TIME_MS     ((uint16)500)     /* Made change to make it similar to LM048, LM048 issues Conn request without dalay */
#define KE_GAPCENTRAL_AUTOCONN_WAIT_MS          ((uint16)2000) 

#define KE_SEND_BLE_DATA_PACKET_TIME            ((uint16)50)   /* 100msec */



#define EVENTS_MESSAGE_BASE (0x4000)
#define EVENTS_USR_MESSAGE_BASE (EVENTS_MESSAGE_BASE)
#define EVENTS_AT_SYS_MESSAGE_BASE  (EVENTS_MESSAGE_BASE + 0x0100)
#define EVENTS_AT_SPP_MESSAGE_BASE  (EVENTS_MESSAGE_BASE + 0x0200)   /*spp has hardly 4 messages at this point*/
#define EVENTS_AT_BT_MESSAGE_BASE   (EVENTS_MESSAGE_BASE + 0x0300)
#define EVENTS_AT_UART_MESSAGE_BASE (EVENTS_MESSAGE_BASE + 0x0400 )
#define EVENTS_AT_BLE_MESSAGE_BASE  (EVENTS_MESSAGE_BASE + 0x0500 )


#define EVENTS_SYS_MESSAGE_BASE (EVENTS_USR_MESSAGE_BASE + 0x0700)
#define EVENTS_LAST_EVENT   (EventUsrLast)

/* This enum is used to determine whether an event is a get or set operation */
typedef enum op_set_get
{
    OP_SET,     /**< Operation is SET */
    OP_GET      /**< Operation is GET */
} op_set_get_et;


/*This enum is used as an index in an array - do not edit - without thinking*/
typedef enum events
{
    /* USER EVENTS */
/*0x4000*/  EventInvalid     = EVENTS_USR_MESSAGE_BASE,
/*0x4001*/  EventUsrLed,                    /**< This is a temporary event for LED switching */
/*0x4002*/  EventBtInitComplete,            /**< This event is triggered once Bluetooth initialisation has completed */
            

    /* USER EVENTS for AT commands */
            EventAtPower = EVENTS_AT_SYS_MESSAGE_BASE,
            EventAtReset,
            KE_APP_OTA_XFER_IDLE_TIMEOUT_MSG,
            EventAtUpgrade,
            KE_SPP_APP_RESET_DELAY_MSG,
			US_POWER_ON_SEQUENCE_TIMEOUT_MSG,
            US_SWITCH_PRESSED_TIMEOUT_MSG,
            US_MASTER_STATE_LED_TIMEOUT_MSG,
            KE_WAIT_BEFORE_GAPCENTRAL_AUTOCONN_MSG,
            US_SEND_BLE_DATA_PACKET_MSG,
            US_SEND_BLE_CENTRAL_DATA_PACKET_MSG,
            
            SPP_APP_DATA_GUARD_TIMEOUT = EVENTS_AT_SPP_MESSAGE_BASE,
            SPP_APP_SWITCH_DATA_MODE,
            KE_WAIT_BEFORE_AUTO_CONNECT_MSG,
            EventAtSppConnect, /* = EVENTS_AT_SPP_MESSAGE_BASE,*/
            EventAtSppDrop,
            EventAutoConnect,
            EventSPPEscapeAUTO,
            EventSPPRole,
            EventAtModemType, 
                        EventAtCTSline, 
                        EventAtRTSline,
                        EventAtDTRline,
                        EventAtDSRline,
            
            EventAtGetState = EVENTS_AT_BT_MESSAGE_BASE, 
			
			KE_DEV_INQUIRY_TIMEOUT_MSG,  
            EventAtGetRSSI,            
            
            EventAtDiscovery,  /*start discovery*/
            EventAtPairable,
            EventAtPaired,
            EventAtPair,

            EventAtStopPair,
            EventAtPairList,
            EventAtDel,
    
            EventAtBondAddr,
            EventAtLEBondAddr,
            
            EventAtStartStopDiscovery, /*stop discovery*/

            EventAtName,
            
            EventAtdpin,
            EventAtMitm,
            EventAtIotype,
            EventAtPin,
            EventAtPasskey,
            EventAtPassCFM,
            
            EventAtenbGAP,
            EventAtRICD,
            
            EventAtUpdateName,
                        
            EventAtFlow  = EVENTS_AT_UART_MESSAGE_BASE,
            EventAtEcho,
            EventAtResp,
            EventAtuartBaud,
            EventAtStopbit,
            EventAtParitybit,  
            EventAtUartConfigType,
            EventAtUpgradeInterface,
                     
            
            EventAtLEdiscovery = EVENTS_AT_BLE_MESSAGE_BASE ,
            EventAtGapRoleType,
            EventAtLEaddFilter,
            EventAtLEconn,
            EventAtLEConnInfo,
            EventAtLEdisconn,        
            EventAtLEfindServ,
            EventAtLEfindChar,
            EventAtLEReadCharValue,
            EventAtLEReadLongCharValue,
            EventAtLEBREDRsearch,
                       
            EventAtLEnbCCFG,
            
            EventAtLEIndicationResp,   
            EventAtLERdMultCharVal,
            EventAtGetCIDfromBdaddr,
            
            EventAtLEWriteCharValue,
            EventAtLEWriteWoResp,
            EventAtLEsignedWriteWoResp,           

            EventAtLEClearWhitelist,
            EventAtLEWhitelist,
            EventAtLEAutoScanServer,
            EventAtLEAutoConnectPeripheral,
			
			EventAtBLEaddr,
            
            EventUsrLast
            
} events_et;


/**
 * Data structure for \ref EventAtPasskey event. This event is triggered
 * when an AT*PASSK command is received.
 *
 * This is used for Secure Simple Pairing (BT 2.1+).
 *
 * See:
 * - SWD v4.0, Section 5.9.1
 * - BT v4.2, Volume 3, Part C (Generic Access Profile), Section 3.2.3.
 */
typedef struct
{
    /** The passkey value. */
    uint32 passkey;
} event_at_passkey_st;

typedef struct
{
    /** Indicates whether this is a set or get event */
    op_set_get_et type;

    /** Enable/disable mitm */
    bool mitm;
} event_at_mitm_st;

typedef struct
{
    /** Indicates whether this is a set or get event */
    op_set_get_et type;

    /** Enable/disable dpin*/
    bool dpin;
} event_at_dpin_st;

typedef struct
{
    /** Indicates whether this is a set or get event */
    op_set_get_et type;

    /** Enable/disable dpin*/
    bool updatename;
} event_at_updatename_st;


typedef struct
{
    /** Indicates whether this is a set or get event */
    op_set_get_et type;

    /**  GapRole =1=Central, 2=Peripheral */
    uint8 GapRole;
    
} event_at_GapRoleType_st;


typedef struct
{
    /** Indicates whether this is a set or get event */
    op_set_get_et type;

    /** Enable/disable dpin*/
    bool uartconfig;
}event_at_uartconfig_st;


typedef struct
{
    /** Indicates whether this is a set or get event */
    op_set_get_et type;

    /** Enable/disable mitm */
    cl_sm_io_capability  iotype;
} event_at_iotype_st;

/**
 * Data structure for \ref EventAtDiscovery event. This event is triggered
 * when an AT*DCOV command is received.
 *
 * See SWD v4.0, Section 5.5.
 */
typedef struct
{
    /** Indicates whether this is a set or get event */
    op_set_get_et type;

    /** Enable/disable discoverability */
    bool discoverable;
} event_at_discovery_st;


/**
 * Data structure for \ref EventAtPairable event. This event is triggered
 * when an AT*PAIR command is received.
 *
 * See SWD v4.0, Section 5.5.
 */
typedef struct
{
    /** Indicates whether this is a set or get event */
    op_set_get_et type;

    /** Enable/disable pairability (valid when \ref type is \ref OP_SET) */
    bool pairable;
} event_at_pairable_st;


/**
 * Data structure for \ref EventAtPaired event. This event is triggered
 * when an AT*PAIR=?{BD_ADDR} command is received to test whether a
 * given device is paired.
 *
 * See SWD v4.0, Section 5.5.
 */
typedef struct
{
    /** Address to get pairing status of */
    bdaddr peer_bdaddr;
} event_at_paired_st;



/*set bonded device address, or set to zero, or query */
typedef struct
{
    op_set_get_et type;
    
    bdaddr addr;
    
} event_at_BondAddr_st;    




/**
 * Data structure for \ref EventAtPair event. This event is triggered when
 * an AT*PAIR={bd_addr} or AT*PAIR={bd_addr},{acc/rej} command is received.
 *
 * See SWD v4.0, Section 5.9.
 */
typedef struct
{
    /**
     * This event can take on several functions. It can be used to
     * initiate pairing to another device  by setting cmd_stype to
     * \ref PAIR_INITIATE. Alternatively, it can be used to accept or
     * reject a pairing request (\ref PAIR_ACCEPT or \ref PAIR_REJECT).
     */
    enum
    {
        PAIR_INITIATE,  /**< Initiate pairing with a device */
        PAIR_ACCEPT,    /**< Accept a pairing request from a device */
        PAIR_REJECT     /**< Reject a pairing request from a device */
    } cmd_type;

    /** Address of the device that this event pertains to */
    bdaddr peer_bdaddr;
} event_at_pair_st;

typedef struct
{
    bool cfm_yn; /* TRUE = yes, FALSE = no */

    /** Address of the device that this event pertains to */
    typed_bdaddr peer_bdaddr;
    
} event_Passk_CFM_yn_st;


/**
 * Data structure for \ref EventAtPin event. This event is triggered
 * when an AT*PIN command is received.
 *
 * This is used for Legacy Pairing (pre BT 2.1).
 *
 * See:
 * - SWD v4.0, Section 5.9.1
 * - BT v4.2, Volume 3, Part C (Generic Access Profile), Section 3.2.3.
 */
typedef struct
{
    /** The pin value. */
    uint8 pin[16];   /* Maximum length is 16, considering null = 17 */
    /** The length of \ref pin */
    uint16 pin_length;
    
    /** Indicates whether this is a set or get event */
    op_set_get_et type;
    
} event_at_pin_st;


/**
 * Data structure for \ref EventAtStopPair event. This event is triggered
 * when an AT*STOPPAIR={BD_ADDR} command is received.
 *
 * See SWD v4.0, Section 5.9.1.
 */
typedef struct
{
    /** Address of device to stop pairing with. */
    bdaddr peer_bdaddr;
} event_at_stoppair_st;



/**
 * Data structure for \ref EventAtDel event. This event is triggered
 * when an AT*DEL command is received.
 *
 * See SWD v4.0, Section 5.9.3.
 */
typedef struct
{
    /** BD_ADDR of the device to delete from the paired devices list. */
    bdaddr addr;
} event_at_del_st;

/**
 * Data structure for \ref EventAtName event. This event is triggered
 * when an AT*NAME command is received.
 *
 * See SWD v4.0, Section 5.5.
 */
typedef struct
{
    /** Indicates whether this is a set or get event */
    op_set_get_et type;

    /** The name to set for the device */
    uint8 name[BT_AT_DEVICE_NAME_LENGTH];

    /** The length of \ref name */
    uint16 name_length;
} event_at_name_st;



/**
 * Data structure for \ref EventAtStartStopDiscovery event. This event is
 * triggered when an AT*DISC command is received.
 *
 * See SWD v4.0, Section 5.8.1.
 */
typedef struct
{
    /* Whether to enable or disable discovery */
    bool enable;

} event_at_start_stop_discovery_st;





typedef struct
{
    op_set_get_et type;

    bool flow;
} 
event_at_flow_st;

typedef struct
{
    op_set_get_et type;

    uint16 paritybit;
} 
event_at_paritybit_st;

typedef struct
{
    op_set_get_et type;

    uint16 stopbit;
} 
event_at_stopbit_st;

typedef struct
{
    op_set_get_et type;

    uint16 baud;
} 
event_at_baud_st;



typedef struct
{
    op_set_get_et    type;
    uint16    ModemType;
    
} event_at_ModemType_st;


typedef struct
{
    op_set_get_et    type;
    uint16    interface;
    
}event_at_UpgradeIntf_st;



typedef struct
{
    op_set_get_et   type;
    bool            onoff;
} event_at_LineControl_st;






/**
 * Data structure for \ref EventAtReset event. This event is triggered
 * when an AT*RESET command is received.
 *
 * See SWD v4.0, Section 5.2.
 */
typedef struct
{
    /** The type of reset requested */
    uint32 reset_level;
} event_at_reset_st;






/*SPP related events*/
typedef struct
{
    bdaddr addr;
    
} event_at_ConDiscon_st;

typedef struct
{
    op_set_get_et    type;
    bool            AutoConn;
    
} event_at_SPPAutoConn_st;

typedef enum
{
    SPP_ROLE_SLAVE = 0,
    SPP_ROLE_MASTER,
    SPP_DUAL_ROLE  
} spp_role_type_st;

typedef struct
{
    op_set_get_et    type;
    spp_role_type_st SPProle;
    
} event_at_SPProle_st;

typedef struct
{
    bool            EscSeq;

} event_SPP_EscapeSeq_st;


typedef struct
{
    bool            upgrade;

} event_firmware_upgrade_st;


/************************** BLE related event types  ***************************/
typedef struct
{
    op_set_get_et   type;
    bool            enable;

} event_BLE_AutoScan_st;

typedef struct
{
    op_set_get_et   type;
    bool            enable;

} event_BLE_WList_st;

typedef struct
{
    bool        enable;

} event_BLE_discovery_st;


typedef struct
{
    bdaddr addr;
    
} event_at_ble_ConDiscon_st;

typedef struct
{
    /*bool        findserv;*/
    uint8       connID;   /*find services of 1 or 2*/
} event_BLE_findServ_st;
/* find services of connected BLE-device */

typedef struct
{
    uint8       connID;
    uint16      start;
    uint16      end;

} event_BLE_findchar_st;
/* find characteristic of servID provided for connected BLE-device */



/* the same structure is used for read characteristic value and 
 to enable the CCFG flasg of the characteristic*/
typedef struct
{
    uint8       connID;
    uint16      char_handle;
} event_BLE_rdcharval_st;



/* the same structure is used for three GATT commands 
    1. Write Char value                 At*WRCHARVAL
    2. WRsigned request                 At*WRwiRESP 
    3. Write without response request   AT*WRwoResp
*/
typedef struct
{
    uint8       connID;
    uint16      char_handle;
    uint16      size;
    uint8      val_buff[20];      
} event_BLE_wrcharval_st;



typedef struct
{
    uint8       connID;
    
} event_BLE_IndicationResp_st;


typedef struct
{
    uint8       connID;
    uint16      char_handle[5];
    uint16      size;
} event_BLE_RdMultChar_st;


/*******************************************************************************/



void handle_at_Reset(event_at_reset_st *event);


/**
 * Send an event Message to the Application Task after the given delay.
 *
 * \param event     ID of the event to send (see \ref events_t)
 * \param delay     Delay before the event should be sent (see
 *                  D_IMMEDIATE, D_SEC, D_MIN, D_HOUR).
 */
void send_later(events_et event, uint32 delay);


/**
 * Send an event Message to the Application Task.
 *
 * \param event     ID of the event to send (see \ref events_t)
 * \param msg       Message data to send
 */
void send_message(events_et event, void *msg);


/**
 * Cancel all event messages of the given type
 *
 * \param event     ID of the event message to cancel.
 */
void send_cancel(events_et event);


#endif


