#ifndef UTILS_H__
#define UTILS_H__

/**
 * Macro to test if parameter x matches a given letter (disregarding case).
 * The letter to match (parameter ucase) must be passed in upper case for
 * this to work.
 */
#define MATCH_LETTER(x, ucase) \
    ((x & ~(uint8)0x20) == ucase)


/**
 * Case insensitive match of a string containing only letters at the
 * start of a given buffer.
 *
 * \param buffer            The buffer to look for the word at the start of.
 * \param buffer_length     The length of buffer.
 * \param word              The word to match at the start of the buffer.
 *                          Must only contain upper case letters.
 * \param word_length       Length of word.
 *
 * \return TRUE if buffer starts with the given word (case insensitive),
 *         else FALSE.
 */
bool match_word(const uint8 *buffer, uint16 buffer_length,
                const char *word, uint16 word_length);


/**
 * Find the first occurrence of word in the given buffer.  The match
 * is case insensitive and word must only contain upper case letters.
 *
 * \param buffer        The buffer to search in.
 * \param buffer_length The length of buffer.
 * \param word          The word to search for.  Must only contain upper
 *                      case letters.
 * \param word_length   Length of word.
 *
 * \return the index of the start of the first occurrence word in buffer
 *         if present else -1 if not present.
 */
int32 find_word(const uint8 *buffer, uint16 buffer_length,
                const char *word, uint16 word_length);

#endif
