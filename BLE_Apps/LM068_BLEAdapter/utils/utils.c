
#include "../CSR_LIB.h"
#include "utils.h"


bool match_word(const uint8 *buffer, uint16 buffer_length,
                const char *word, uint16 word_length)
{
    if (buffer_length < word_length)
        return FALSE;

    while (word_length--)
    {
        if (!MATCH_LETTER(*buffer++, *word++))
            return FALSE;
    }
    return TRUE;
}


int32 find_word(const uint8 *buffer, uint16 buffer_length,
                const char *word, uint16 word_length)
{
    int32 pos;

    if (word_length > buffer_length)
        return -1;

    for (pos = 0; pos <= (buffer_length - word_length); pos++)
    {
        if (match_word(buffer + pos, buffer_length - pos, word, word_length))
            return pos;
    }

    return -1;
}
