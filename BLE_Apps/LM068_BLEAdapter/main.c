/*
 Created a project for LM068_BLEAdapter to merge the Peripheral and Central in ine project
*/



#include <stdio.h>
#include <connection.h>

#include "CSR_LIB.h"
#include "common.h"
#include "events.h"

#include "debug.h"

#include "config/config.h"
#include "uart/uart.h"
#include "bt_core/bt_core.h"
#include "AT_parse/AT_Parse.h"
#include "spp/spp_common.h"
#include "spp/spp_core.h"
#include "GPIO_define.h" 
#include "LED/LED.h"

#include "BLE/BLE_Cen_core.h"

#include "BLE_Periph/BLE_Peri.h"
#include "BLE_Periph/app_gatt_db.h"

#include <connection.h>


/*  Added UART interface for firmware upgrade on CSRb5348 chipset */

/*there are no revisions numbered as 11, 12, 13 for master branch,
 Mettler and masteer branch is kept same with version_number 01.14 */

const char *upgrade_password = "LMUPDATEV_068BLE_CENTRAL_0201";






/*******************************************************************************
  Enable below define to pdate the device name as namestring added with 
  two bytes added from the BD-address of the device.
 *******************************************************************************/
/* #define ADD_ADDRESS_BYTES_IN_NAME    1 */ 
/* "This define needs to be set in the Project>>Properties>>BuildSystems>>Define_symbols"  */
        
        
/*******************************************************************************
 *******************************************************************************/



/*
Added #define for LM961/LM068. Switch press for LM961 board
*/
                               

/*uint8 Power_On_Seq_Timeout = FALSE;*/

/***********************************/
static uint8 button_flag = 0;
#ifdef LM961 
static uint8 button2_flag = 0;    
#endif    
static uint8 press_count = 0;
/***********************************/




/** Application Task */
TaskData app_task;
TaskData PIO_task;

app_data_st app_data;


codebuff_st     *codebuff_1, *codebuff_2;
codebuff100_st      *codebuff100_3;        
Ring_st  *Ring; 
uint8 start_data = FALSE;

bool Dev_Initiated = FALSE;

/*******************************************************************************
 *******************************************************************************/


bool uart_tx(const char *message, uint16 length)
{
    uint16 offset;
    uint8 *dest;
    /* Get the sink for the uart, panic if not available */
    Sink sink = StreamUartSink();
    PanicNull(sink);

    if (length == 0)
        length = strlen(message);

    /* Claim space in the sink, getting the offset to it */
    offset = SinkClaim(sink, length);
    if(offset == 0xFFFF)
        return FALSE; /* Space not available */

    /* Map the sink into memory space */
    dest = SinkMap(sink);
    (void) PanicNull(dest);

    /* Copy the message into the claimed space */
    memcpy(dest+offset, message, length);

    /* Flush the data out to the uart */
    PanicZero(SinkFlush(sink, length));

    return TRUE;
}


static void Central_send_BLE_data_packet ( void )
{
    
#define BLE_MTU_size 23
    
    uint8 fresh_bytes = 0;
/*  */  uint8 i=0;
     
        uint8   full = 0;

    if( BLE_Adaper_ready == FALSE )
        return;
    

    if( Ring->Clear_flag == TRUE )
    {
        Ring->Clear_flag = FALSE;
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "RING_CLEAR:", "s", 
                                                                "Ready_for_SPP_IN_Data",
                                                                strlen("Ready_for_SPP_IN_Data"))); 
    }
    
    
    if( Ring->Read_pointer == 0 )
    {
        if( Ring->C1->full == TRUE )
        {
            full = 1;
            fresh_bytes  = CBUFF_BUFFER_SIZE_254 - Ring->C1->tail;
            /*  LOG_RING((" FULL_1 fresh_bytes = %d ", fresh_bytes));*/
        }
        else
        {
            fresh_bytes =  Ring->C1->head - Ring->C1->tail;
            /*  LOG_RING(("  fresh_bytes_1 = %d ", fresh_bytes));*/
        }
    }
    else if( Ring->Read_pointer == 1 )
    {
        if( Ring->C2->full == TRUE )
        {
            full = 2;
            fresh_bytes  = CBUFF_BUFFER_SIZE_254 - Ring->C2->tail; 
            /* LOG_RING(("\n FULL_2 fresh_bytes = %d ", fresh_bytes)); */
        }
        else
        {
            fresh_bytes =  Ring->C2->head - Ring->C2->tail;
           /* LOG_RING(("\n fresh_bytes_2 = %d ", fresh_bytes)); */
        }
    }
     
            if(fresh_bytes)
            {
                    LOG_RING(("\n fresh_bytes = %d ", fresh_bytes)); 
                    
                    if( ( fresh_bytes ) > (BLE_MTU_size-3) ) 
                    {
                            if( Ring->Read_pointer == 0 )
                            {
                                GattWriteWithoutResponseRequest ( app_data.task,
                                                  stBLEConnectionData->ui16ConnectionID, 
												  SerialServiceData->SerialCharHandle,
                                                  (BLE_MTU_size-3),
                                                  (uint8 * ) &Ring->C1->buffer[Ring->C1->tail] );
                                
                                                
                                                        LOG_RING(("\n BLE_1 read ::\t"));
                                                          for(i=0; i<(BLE_MTU_size-3) ; i++)
                                                                 LOG_RING(("%c", Ring->C1->buffer[Ring->C1->tail+i] ));/* */
                                
                                
                                Ring->C1->tail =  Ring->C1->tail + (BLE_MTU_size-3);
                               /*  LOG_RING(("\t BLE_1 Sent %d , tail = %d ", (BLE_MTU_size-3)  , Ring->C1->tail ));   */
                            }
                            else
                            {
                                GattWriteWithoutResponseRequest ( app_data.task,
                                                  stBLEConnectionData->ui16ConnectionID, 
											      SerialServiceData->SerialCharHandle,
                                                  (BLE_MTU_size-3),
                                                  (uint8 * ) &Ring->C2->buffer[Ring->C2->tail] );
                                
                                                
                                                        LOG_RING(("\n BLE_2 read ::\t"));
                                                          for(i=0; i<(BLE_MTU_size-3) ; i++)
                                                                 LOG_RING(("%c", Ring->C2->buffer[Ring->C2->tail+i] )); /**/
                                
                                
                                Ring->C2->tail =  Ring->C2->tail + (BLE_MTU_size-3);
                               /*  LOG_RING(("\t BLE_2 Sent %d , tail = %d ", (BLE_MTU_size-3)  , Ring->C2->tail )); */                                
                            }
                                
                    }
                    else
                    {
                            if( Ring->Read_pointer == 0 )
                            {
                                GattWriteWithoutResponseRequest ( app_data.task,
                                                  stBLEConnectionData->ui16ConnectionID, 
												  SerialServiceData->SerialCharHandle,
                                                  fresh_bytes,
                                                  (uint8 * ) &Ring->C1->buffer[Ring->C1->tail] ); 
                                
                                                       LOG_RING(("\n BLE_1 read ::\t"));
                                                          for(i=0; i<fresh_bytes ; i++)
                                                                 LOG_RING(("%c", Ring->C1->buffer[Ring->C1->tail+i] ));     /*  */
                               /* LOG_RING(("\t BLE_1 Sent %d ", fresh_bytes ));  */
                            }
                            else
                            {
                                GattWriteWithoutResponseRequest ( app_data.task,
                                                  stBLEConnectionData->ui16ConnectionID, 
                                                  SerialServiceData->SerialCharHandle,
                                                  fresh_bytes,
                                                  (uint8 * ) &Ring->C2->buffer[Ring->C2->tail] ); 
                                
                                                      LOG_RING(("\n BLE_2 read ::\t"));
                                                          for(i=0; i<fresh_bytes ; i++)
                                                                 LOG_RING(("%c", Ring->C2->buffer[Ring->C2->tail+i] ));   /*    */ 
                              /*   LOG_RING(("\t BLE_2 Sent %d ", fresh_bytes )); */
                            }
                                                                
                                if(full == 0)
                                {
                                    if ( Ring->Read_pointer == 0 )
                                    {
                                        Ring->C1->tail = Ring->C1->tail + fresh_bytes;
                                       /* LOG_RING(("\t tail_1>> %d ", Ring->C1->tail ));  */ 
                                    }
                                    else
                                    {
                                        Ring->C2->tail = Ring->C2->tail + fresh_bytes;
                                        /* LOG_RING(("\t tail_2>> %d ", Ring->C2->tail )); */ 
                                    }
                                }
                                else if(full == 1)
                                {
                                    Ring->C1->full = FALSE;
                                    Ring->C1->tail = 0;
                                    Ring->Read_pointer = 1;
                                  /*  LOG_RING(("\t tail_1>> %d ", Ring->C1->tail ));  */ 
                                }
                                else if(full == 2)
                                {
                                    Ring->C2->full = FALSE;
                                    Ring->C2->tail = 0;
                                    Ring->Read_pointer = 0;
                                   /* LOG_RING(("\t tail_2>> %d ", Ring->C2->tail ));  */ 
                                    
                                    if(Ring->full_count == 1)
                                    {
                                        Ring->full_count = 0;
                                        Ring->Clear_flag = TRUE;
                                    }
                                }     
                    }
            }
               
    MessageCancelFirst( app_data.task, US_SEND_BLE_CENTRAL_DATA_PACKET_MSG );
    MessageSendLater( app_data.task, US_SEND_BLE_CENTRAL_DATA_PACKET_MSG, 0, KE_SEND_BLE_DATA_PACKET_TIME ) ; 

}





/**
 * General handler for system event messages.  This invoke the appropriate
 * event handler.
 */
static void handle_at_sys_event_message(Task task, MessageId id,
                                        Message message)
{
       /* LOG_INFO(("\n +handle_at_sys_event_message"));
        
        
          1. Power - not in LM951
          2. Reset - yes "EventAtReset" 
          3. Upgrade -Not in LM951
          4. KE_SPP_APP_RESET_DELAY_MSG - delay before reset
        */
        
    switch (id)
        {
            case EventAtReset:
			    handle_at_Reset((event_at_reset_st *)message);
			    break;
                
           case US_POWER_ON_SEQUENCE_TIMEOUT_MSG:
                LOG_INFO(("\n US_POWER_ON_SEQUENCE_TIMEOUT_MSG"));
                handle_PowerOnSequence_timeout();
                break;
                
            case US_MASTER_STATE_LED_TIMEOUT_MSG:
                enable_master_role_toggle();
                break;
                
            case US_SWITCH_PRESSED_TIMEOUT_MSG:
                 handle_SwitchPress_Timeout();/**/
                break;   
                
                            
            case KE_WAIT_BEFORE_GAPCENTRAL_AUTOCONN_MSG:
                handleGapCentralAutoConnect_AfterDisconnet();
            break;
            
            case US_SEND_BLE_DATA_PACKET_MSG:
               /**/ send_BLE_data_packet ();
            break;
            
            case US_SEND_BLE_CENTRAL_DATA_PACKET_MSG:
               /**/ Central_send_BLE_data_packet ();
            break;            
            
            case KE_SPP_APP_RESET_DELAY_MSG:
                LOG_INFO(("\n KE_SPP_APP_RESET_DELAY_MSG"));
                resetSppApp ();
                break;

           case KE_APP_OTA_XFER_IDLE_TIMEOUT_MSG:
                if (app_data.spp_online_state < SPP_OTA_INITIATED)
                {
                    DEBUG_OTA(("\n cancelled this messgae due to connectio-Lost error "));
                    MessageCancelAll( app_data.task, KE_APP_OTA_XFER_IDLE_TIMEOUT_MSG );
                } 
                else   
                {
                    DEBUG_OTA(("\n OTA-Idle timer retrigger request "));
                    handle_OTA_XFER_IDLE_TIMEOUT_MSG ();
                }
                break;
                
           case EventAtUpgrade:
                LOG_INFO(("\n EventAtUpgrade"));
                handle_at_OTA_request ((event_firmware_upgrade_st *)message);
                break;
                
            default:
                LOG_INFO(("\n Invalid message (id=%u)", id));
                break;
        }
}



/** Handler for a User Event on the Application Task */
static void handle_user_event_message(Task task, MessageId id, Message message)
{
    UNUSED(task);

    if (id < EVENTS_AT_SPP_MESSAGE_BASE)
    {
        handle_at_sys_event_message(task, id, message);
    }
    else if (id < EVENTS_AT_UART_MESSAGE_BASE )
    {
        handle_at_bt_event_message(task, id, message);
    }
    else if( id < EVENTS_AT_BLE_MESSAGE_BASE )
    {
        handle_at_uart_event_message(task, id, message);
    }
    else
    {
        handle_at_BLE_event_message(task, id, message);
    }
    
}


/** Handle a MORE_DATA message */
static void handle_more_data (Message message)
{
    MessageMoreData *more_data = (MessageMoreData *)message;
    Source uart_source = StreamUartSource();

    uint16 size;
    const uint8 *data;
    uint16 consume;


    
    uint16   current_bytes = 0;
    uint16   now_sending = 0; 
    uint8    excess_bytes = 0;
    
    
    if (more_data->source != uart_source)
    {
        LOG_RING(("Received MORE_DATA message from unknown source"));
        return;
    }
    
        size = SourceSize(uart_source);
        data = SourceMap(uart_source);
          
    
    
    if( BLE_Adaper_ready == TRUE )        
    {
         LOG_RING(("\n BLE_Adaper_ready >> send = %d ", size)); 
         
         if(size>0)       
         {
             current_bytes = size;
                                        if(current_bytes==0)
                                        return;
                                
                                        if( current_bytes > RING_BUFFER_SIZE )
                                        {
                                          MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "SPP_OVF", "s", "SPP IN OVERFLOW" , sizeof("SPP IN OVERFLOW") ));
                                          Panic();
                                        }   
                                         
                                        if( (Ring->C1->full == TRUE) && (Ring->C2->full == TRUE) && (Ring->full_count == 0) )
                                        {
                                            Ring->full_count =1;
                                            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "RING_OVF:", "s", 
                                                                                                        "Stop_SPP_IN_data",
                                                                                                        strlen("Stop_SPP_IN_data")));  
                                            
                                            return; /*dont drop these bytes from source so that we can handle it later on*/
                                        }  
                                        
                                        
                                       while(current_bytes>0)
                                       {            
                                            now_sending = current_bytes;
                                
                                            if( (start_data == FALSE) )
                                            {
                                                start_data = TRUE;
                                                
                                                if( stBLEInfoData->BLE_Role == GAP_CENTRAL_ROLE )
                                                     MessageSendLater( app_data.task, US_SEND_BLE_CENTRAL_DATA_PACKET_MSG, 0, KE_SEND_BLE_DATA_PACKET_TIME ); /*  );*/                                                
                                                else
                                                     MessageSendLater( app_data.task, US_SEND_BLE_DATA_PACKET_MSG, 0, KE_SEND_BLE_DATA_PACKET_TIME ); /*  );*/
                                            }
                                            
                                            if( Ring->pointer == 0 )
                                            {
                                                    if(   ( Ring->C1->head + now_sending ) < CBUFF_BUFFER_SIZE_254 )
                                                    {
                                                        memcpy(&Ring->C1->buffer[Ring->C1->head], data, now_sending);
                                                    
                                                            /*  LOG_RING(("\n SPP_1 write is ::     "));
                                                                  for(i=0; i<now_sending ; i++)
                                                                         LOG_RING(("%c", Ring->C1->buffer[Ring->C1->head+i] ));   */
                                                                  
                                                        Ring->C1->head = Ring->C1->head + now_sending;
                                                       /* LOG_RING(("\t now_sending = %d ", now_sending )); */
                                                    }
                                                    else
                                                    {
                                                        excess_bytes = CBUFF_BUFFER_SIZE_254 - Ring->C1->head ;  
                                                         /* LOG_RING(("\t excess_bytes = %d ", excess_bytes )); */
                                                    
                                                        memcpy(&Ring->C1->buffer[Ring->C1->head], data, excess_bytes );
                                                                          /*   LOG_RING(("\n SPP_1 write is ::     "));
                                                                  for(i=0; i<excess_bytes ; i++)
                                                                         LOG_RING(("%c", Ring->C1->buffer[Ring->C1->head+i]  ));    */ 
                                                                  
                                                        Ring->C1->full  = TRUE;
                                                        now_sending = current_bytes - excess_bytes ;
                                                        /*LOG_RING(("\t now_sending = %d ", now_sending )); */
                                                        Ring->C2->head = 0;
                                                    
                                                        memcpy(&Ring->C2->buffer[Ring->C2->head], data+excess_bytes, now_sending );
                                                        /* LOG_INFO(("\n C1_Full C2::     "));
                                                                  for(i=0; i<now_sending ; i++)
                                                                         LOG_RING(("%c", Ring->C2->buffer[Ring->C2->head+i]  )); */
                                    
                                                        Ring->C2->head = now_sending ;
                                                        Ring->pointer   = 1 ;
                                                    }
                                                
                                            }
                                            else if( Ring->pointer == 1 )
                                            {
                                                        if((Ring->C2->head + now_sending) < CBUFF_BUFFER_SIZE_254 )
                                                        {
                                                            memcpy(&Ring->C2->buffer[Ring->C2->head], data, now_sending);
                                                        
                                                                    /* LOG_RING(("\n SPP_2 write is ::     "));
                                                                      for(i=0; i<now_sending ; i++)
                                                                             LOG_RING(("%c", Ring->C2->buffer[Ring->C2->head+i] ));*/
                                                                      
                                                            Ring->C2->head = Ring->C2->head + now_sending;
                                                           /* LOG_RING(("\t now_sending = %d ", now_sending )); */
                                                        }
                                                        else
                                                        {                          
                                                            excess_bytes = CBUFF_BUFFER_SIZE_254 - Ring->C2->head ;  
                                                            /* LOG_RING(("\t excess_bytes = %d ", excess_bytes ));               */
                                                        
                                                            memcpy(&Ring->C2->buffer[Ring->C2->head], data, excess_bytes );
                                                                       /*            LOG_RING(("\n SPP_2_excess ::     "));
                                                                      for(i=0; i<excess_bytes ; i++)
                                                                             LOG_RING(("%c", Ring->C2->buffer[Ring->C2->head+i]  ));*/ 
                                                                      
                                                            /*  LOG_RING(("\t C2 FULL" )); */
                                                            Ring->C2->full = TRUE;
                                                            now_sending = current_bytes - excess_bytes ;
                                                            /* LOG_RING(("\t now_sending = %d ", now_sending ));*/
                                                            Ring->C1->head = 0;
                                                        
                                                            memcpy(&Ring->C1->buffer[Ring->C1->head], data+excess_bytes, now_sending );
                                                            /* LOG_INFO(("\n C2_FULL C1 ::     "));
                                                                      for(i=0; i<now_sending ; i++)
                                                                             LOG_RING(("%c", Ring->C1->buffer[Ring->C1->head+i]  )); */
                                        
                                                            Ring->C1->head = now_sending ;
                                                            Ring->pointer   = 0 ;
                                                        }
                                                        
                                            }  
                                                
                                            SourceDrop( uart_source, current_bytes );
                                            data = SourceMap( uart_source );
                                            current_bytes = 0;  
                                                
                                        } 
        }
    }/* if ble_adapter_is_ready */
    else if( app_data.spp_online_state == SPP_UART_OTA_INITIATED ) 
    {     
            DEBUG_OTA(("\n data in SPP_UART_OTA_RECEIVING "));
           Handle_UART_OTA_Data((MessageMoreData *)more_data);
    }
    else if( app_data.spp_online_state == SPP_OTA_RECEIVING )    
    {
            DEBUG_OTA(("\n app_data.spp_online_state == SPP_OTA_RECEIVING   discard_this_data "));
            SourceDrop(uart_source, size);  
    }
    else if( app_data.spp_online_state == SPP_ONLINE_COMMAND )
    {  
        /* LM_DEBUG(("\n Dev-CMD-mode"));  */
        	
            if (app_config.echo && (size - app_data.echo_offset) > 0)
            {
                uart_tx((const char *)data + app_data.echo_offset,
                        size - app_data.echo_offset);
            }
        
            app_data.echo_offset = size;
        
            consume = at_parser_process_data(data, size);
            if (consume > 0)
            {
                SourceDrop(uart_source, consume);
                app_data.echo_offset -= consume;
            }
    }

}




/*******************************************************************************
   PIO Handler is different for LM068 and LM961 due to differnce in hardware
   On LM961 -
        switches are on GPIO-28/29 i.e. SCL/SDA lines and 
        are normal read is High and read as zero when pressed.
   On LM068/LM048 -
       Reset switch is on GPIO-17 and 
       normal read low, Press is read high.
********************************************************************************/

/* Hercules untick is read as pin-high in SW */

/** Top level handler for the PIO_Task */
static void PIO_handler(Task task, MessageId id, Message message)
{   
    const MessagePioChanged *changed = NULL;



    
    LOG_INFO(("\n PIO_change_handler = 0x%x  >>  ", id));
   
    
    switch( id )
    { 
        case MESSAGE_PIO_CHANGED:
            changed = (const MessagePioChanged *) message;
            LOG_INFO((" state = 0x%lx ", changed->state ));            
            

       
          
        break;
        
        default:
        break;
    }
}    


/** Top level event handler for the Application Task */
static void app_handler(Task task, MessageId id, Message message)
{
  /*  const MessageAdcResult *adc;
    const char *AdcSource[9] = {  "adcsel_aio0", "adcsel_aio1", "adcsel_aio2", "adcsel_aio3", "adcsel_vref", "adcsel_vdd_bat", "adcsel_byp_vregin" , "adcsel_vdd_sense" , "adcsel_vregen" };
   */
   
    /* LOG_INFO(("\n app_handler = 0x%x ", id));*/
    
    if( id == MESSAGE_STREAM_PARTITION_VERIFY ) 
    { 
        handleStreamParttionVerify((MessageStreamPartitionVerify *)message); 
    }
    if ((id >= EVENTS_MESSAGE_BASE) && (id < EVENTS_LAST_EVENT))
    {
        handle_user_event_message(task, id, message);
    }
    else if ((id >= GATT_MESSAGE_BASE  && id < GATT_MESSAGE_TOP ))
    {
        handle_BLE_message(task, id, message);
    }
    else if ((id >= CL_MESSAGE_BASE && id < CL_MESSAGE_TOP))
    {
        handle_cl_message(task, id, message);
    }
    else
    {
        switch (id)
        {
            case MESSAGE_MORE_DATA:
                handle_more_data(message);
                break;

            case MESSAGE_MORE_SPACE:
                break;              
            case MESSAGE_STREAM_DISCONNECT : /* MessageStreamDisconnect:) */
                LOG_INFO(("\n MESSAGE_STREAM_DISCONNECT 0x%x ", id));
                LOG_INFO(("Source - 0x%x ",(uint8)((MessageStreamDisconnect*)message)->source));
		        LOG_INFO(("Sink - 0x%x\n",(uint8)((MessageStreamDisconnect*)message)->sink));
                break;
                
            default:
                LE_DEBUG(("Invalid app message (id=0x%x)\n", id));
            break;
        }
    }
}

void send_later(events_et event, uint32 delay)
{
    MessageCancelAll(&app_task, event);
    MessageSendLater(&app_task, event, NULL, delay);
}


void send_message(events_et event, void *msg)
{
    MessageCancelAll(&app_task, event);
    MessageSend(&app_task, event, msg);
}


void send_cancel(events_et event)
{
    MessageCancelAll(&app_task, event);
}


static void App_init ( void )
{
    app_task.handler = app_handler;

    app_data.task = &app_task; 
    
    PIO_task.handler = PIO_handler;
    app_data.pio_task = &PIO_task;
    
    /* Initialise the Connection Library with the options */
    ConnectionInitEx2(app_data.task , NULL, NUMBER_OF_TRUSTED_DEVICES );
    
    MessagePioTask( app_data.pio_task );
    
    app_data.device_state = POWER_ON_STATE ;
    Update_LED_state();   
    
}




/*******************************************************************************
  ******************************************************************************/

int main(void)
{   
    LOG_INFO(("\n Config_Init "));
    
    Initiate_LED(); 
    
    ReadnBoot_with_FlowControl();
    Config_Init();
   
    UART_Init();

    App_init();
    
    at_parser_init();
    MessageSendLater(app_data.task, US_POWER_ON_SEQUENCE_TIMEOUT_MSG, 0, KE_POWER_ON_SEQUENCE_TIMEOUT_MS );  /*  */
    
    


    
    MessageLoop();
    
    return 0;
}

/******************************************************************************************************************************/
/* On LM961 development board LM55x rev-F , 
                Switches SW1 and SW2 are on SCL and SDA lines and goes to Ground when pressed. 
                There is no GPIO-17 on LM961/LM55x dev-Board.
   
  ON LM068 expected hardware similar to current LM048 with LM072.
                SCL and SDA are made as inputs and are for MODEM Transfer functionality.
                Reset-Button is On GPIO-17 and goes to Level-High when pressed.
                when S2 pressed, read the port as 10020000
                When S1 pressed, read the port as 20020000

  Software developed for LM068 cannot be used for LM961 for Reset switch functionality.
*/
/******************************************************************************************************************************/


void handle_SwitchPress_Timeout ( void )
{
    uint32 port_read = 0;
   
    port_read = PioGet32();
    
    LOG_INFO(("\n SwitchPress_Timeout >> port_read = 0x%lx", port_read ));  
       
    if( ((port_read & PIO_BUTTON_MASK__HIGH_MASK) == PIO_BUTTON_MASK__HIGH_MASK) && (button_flag==1)  )
    {
        /* switch is still pressed */
        press_count++;
        if(press_count == 10)
        {
            LOG_INFO(("\n Call Reset >> press_count = %d ", press_count ));
            press_count = 0;
            Delete_Pairing_and_Settings();
            resetSppApp();/* */
        }
        else
        {
            MessageSendLater(app_data.task, US_SWITCH_PRESSED_TIMEOUT_MSG, 0, KE_SWITCH_PRESSED_TIMEOUT_MS );    
            LOG_INFO(("\n Continue delay  >> press_count = %d ", press_count ));
        }
    }
    else/* if( press_count <3 )*/
    {
        button_flag = 0 ;
        press_count =0;
        /*(stop and return)*/
        LOG_INFO(("\n Button Released shut-off, reset_press-count "));
    }
    
}



/*************/

/*************/

