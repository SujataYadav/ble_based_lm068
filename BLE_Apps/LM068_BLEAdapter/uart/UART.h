
#ifndef uart_h
#define uart_h


#define VALID_BAUD_MIN  1
#define VALID_BAUD_MAX  9

#define VALID_STOPBIT_MIN 0
#define VALID_STOPBIT_MAX 1

#define VALID_PARITY_MIN 0
#define VALID_PARITY_MAX 2



void    UART_Init           ( void );

void    set_UARTBaud_rate   ( uint16 value );

void    set_UART_Parity     ( uint16 value );       
void    set_UART_StopBit    ( uint16 value );   


 

void handle_at_uart_event_message(Task task, MessageId id, Message message);



uint16  get_UART_PARITY_value   ( void );
uint16  get_UART_StopBit_value  ( void );
uint16  get_UART_BAUD_value     ( void );


#endif /* uart_h */


