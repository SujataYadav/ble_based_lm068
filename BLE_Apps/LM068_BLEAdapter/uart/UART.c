
#include "../CSR_LIB.h"
#include "../common.h"
#include "../events.h"
#include "../AT_parse/AT_Parse.h"




#include "../debug.h"

#include "../config/config.h"

#include "uart.h"




#if  defined LM068
#include "../spp/spp_common.h"
#include "../GPIO_define.h" 
#endif




static void update_UART_FLOWstate( void );


/* 
    user PSKey location 9 is default is "01d8 0000 0000" which refers to "115200, NoParity, 1-StopBit"
*/

/*
The UART baud rate - Enumerator
  VM_UART_RATE_SAME = 0,     -- 0 --
  VM_UART_RATE_9K6 = 39,     -- 1 -- 9600 baud.    = 0x0027
  VM_UART_RATE_19K2 = 79,    -- 2 -- 19200 baud.   = 0x004f
  VM_UART_RATE_38K4 = 157,   -- 3 -- 38400 baud.   = 0x009d
  VM_UART_RATE_57K6 = 236,   -- 4 -- 57600 baud.   = 0x00EC
  VM_UART_RATE_115K2 = 472,  -- 5 -- 115200 baud.  = 0x01d8
  VM_UART_RATE_230K4 = 944,  -- 6 -- 230400 baud.  = 0x03B0
  VM_UART_RATE_460K8 = 1887, -- 7 -- 460800 baud.  = 0x075F
  VM_UART_RATE_921K6 = 3775, -- 8 -- 921600  baud. = 0x0EbF
  VM_UART_RATE_1382K4 = 5662 -- 9 -- 1382400 baud. = 0x161e
*/
const uint16 baud_values[10] = 
{
  VM_UART_RATE_SAME,
  VM_UART_RATE_9K6,
  VM_UART_RATE_19K2,
  VM_UART_RATE_38K4,
  VM_UART_RATE_57K6,
  VM_UART_RATE_115K2,
  VM_UART_RATE_230K4,
  VM_UART_RATE_460K8,
  VM_UART_RATE_921K6,
  VM_UART_RATE_1382K4
}; 


/*
enum vm_uart_stop The number of stop bits.
VM_UART_STOP_ONE 	- One. = 0
VM_UART_STOP_TWO 	- Two. = 1
VM_UART_STOP_SAME 	- The same.
*/



const uint16 stop_bit_value[3] = 
{
    VM_UART_STOP_ONE,
    VM_UART_STOP_TWO,
    VM_UART_STOP_SAME
};


/*
enum vm_uart_parity The parity to use.
Enumerator
VM_UART_PARITY_NONE - None. = 0 
VM_UART_PARITY_ODD 	- Odd.  = 1
VM_UART_PARITY_EVEN - Even. = 2
VM_UART_PARITY_SAME - The same.
*/

const uint16 parity_bit_value [4] = 
{
    VM_UART_PARITY_NONE, 
    VM_UART_PARITY_ODD,
    VM_UART_PARITY_EVEN,
    VM_UART_PARITY_SAME
};




void UART_Init (void)
{
    LM_DEBUG(("\n UART_Init \n"));   
    
    if( Read_UART_STREAM_CONFIG_fromPS() == 0)
        StreamConfigure( VM_STREAM_UART_CONFIG, VM_STREAM_UART_LATENCY ); 
    else if( Read_UART_STREAM_CONFIG_fromPS() == 1)
        StreamConfigure (VM_STREAM_UART_CONFIG ,VM_STREAM_UART_THROUGHPUT );   
    
    StreamUartConfigure( app_config.UartCFG.Baud, app_config.UartCFG.StopBits, app_config.UartCFG.Parity ); 

    
}



uint16 get_UART_BAUD_value ( void )
{
    /**/
    if( app_config.UartCFG.Baud == VM_UART_RATE_9K6 )
        return 1;
    else if( app_config.UartCFG.Baud == VM_UART_RATE_19K2 )  
        return 2;
    else if( app_config.UartCFG.Baud == VM_UART_RATE_38K4 )  
        return 3;
    else if( app_config.UartCFG.Baud == VM_UART_RATE_57K6 )  
        return 4;
    else if( app_config.UartCFG.Baud == VM_UART_RATE_115K2 ) 
        return 5;
    else if( app_config.UartCFG.Baud == VM_UART_RATE_230K4 ) 
        return 6;
    else if( app_config.UartCFG.Baud == VM_UART_RATE_460K8 ) 
        return 7;
    else if( app_config.UartCFG.Baud == VM_UART_RATE_921K6 ) 
        return 8;
    else if( app_config.UartCFG.Baud == VM_UART_RATE_1382K4 )
        return 9;
    else 
        return 0xffff;
}

uint16 get_UART_StopBit_value ( void )
{
    /**/
    if( app_config.UartCFG.StopBits == VM_UART_STOP_ONE )
        return 0;
    else if( app_config.UartCFG.StopBits == VM_UART_STOP_TWO )  
        return 1;
    else
        return 0xffff; 
    
}
    
uint16 get_UART_PARITY_value ( void )
{
    /**/
    if( app_config.UartCFG.Parity == VM_UART_PARITY_NONE )
        return 0;
    else if( app_config.UartCFG.Parity == VM_UART_PARITY_ODD )  
        return 1;
    else if( app_config.UartCFG.Parity == VM_UART_PARITY_EVEN )  
        return 2;
    else
        return 0xffff;
        
}        
/* */
static void Store_and_Update_UARTConfig ( void )
{
    uint16 buff[3], status = FALSE ;
    
    buff[0] = app_config.UartCFG.Baud ;
    buff[1] = app_config.UartCFG.StopBits ;
    buff[2] = app_config.UartCFG.Parity ;
    
    status = Store_memory_Location ( LCN_UART_CONFIG, buff, sizeof(app_config.UartCFG) );
    
    StreamUartConfigure( app_config.UartCFG.Baud, app_config.UartCFG.StopBits, app_config.UartCFG.Parity );
  
    

  

}
 
/*******************************************************************************
  ******************************************************************************/


static void handle_at_uart_flow( event_at_flow_st *event)
{

    LOG_INFO(("\n handle_at_uart_flow "));
    
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "FLOW", "b",
                                          app_config.flow ));
    }
    else
    {

        MOCK(at_parser_send_ok_response());
        if(app_config.flow != event->flow)
        {   
            app_config.flow = event->flow;
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "FLOW_CHANGE", "s",
                                          (" IN_Progress"), lstrlen(" IN_Progress")));
            update_UART_FLOWstate();
        }
    }   
}



static void handle_at_uart_baud( event_at_baud_st *event)
{
   /**/
    uint16 value = 0;
  
    LOG_INFO(("\n handle_at_uart_baud "));
    
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        value = get_UART_BAUD_value();
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "BAUD", "s",  baud_array[value] , strlen(baud_array[value])  ));
    }
    else
    {
        app_config.UartCFG.Baud = baud_values[ event->baud ] ;
        Store_and_Update_UARTConfig ();
        MOCK(at_parser_send_ok_response());
        LM_DEBUG(("\n Baud updated to %d ", app_config.UartCFG.Baud ));
   }
}


static void handle_at_uart_stopbit( event_at_stopbit_st *event)
{
    
    uint16 value = 0;
    /* uint32 pio_read = 0;    */
    
    LOG_INFO(("\n handle_at_uart_stopbit "));
            
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        value = get_UART_StopBit_value();
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "STOP", "s",  StopBit_array[value] , strlen(StopBit_array[value])  ));
    }
    else
    {
        app_config.UartCFG.StopBits = stop_bit_value[event->stopbit] ;
        Store_and_Update_UARTConfig ();
        MOCK(at_parser_send_ok_response());
        LM_DEBUG(("\n Stop updated to %d ", app_config.UartCFG.StopBits ));     
    }      
    /*
    pio_read = PioGet32();
    LM_MODEM(("\n pio_read is 0x%lx ", pio_read )); 
    */
}



static void handle_at_uart_paritybit(event_at_paritybit_st *event)
{
  /*  */
    uint16 value = 0;
    
    LOG_INFO(("\n handle_at_uart_paritybit "));
        
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        value = get_UART_PARITY_value();
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "PARITY", "s",  ParityBit_array[value] , strlen(ParityBit_array[value])  ));
    }
    else
    {
        app_config.UartCFG.Parity = parity_bit_value[event->paritybit];
        Store_and_Update_UARTConfig ();
        MOCK(at_parser_send_ok_response());
        LM_DEBUG(("\n Parity updated to %d ", app_config.UartCFG.Parity ));
    }
    
}
  

static void handle_at_UartConfigType(event_at_uartconfig_st *event)
{
    uint16 value = 0;
    
    LOG_INFO(("\n handle_at_UartConfigType "));
    
    value = Read_UART_STREAM_CONFIG_fromPS();   
        
    if (event->type == OP_GET)
    {
        MOCK(at_parser_send_ok_response());
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "UART_CONFIG", "s",  UARTconfig_array[value] , strlen(UARTconfig_array[value])  ));
    }
    else
    {
        if( value == event->uartconfig )
        {
                MOCK(at_parser_send_err_response()); 
        }
        else
        {
                Store_memory_Location ( LCN_UART_STREAM_CONFIG, (const uint16*)&event->uartconfig, sizeof(value) );
                MOCK(at_parser_send_ok_response());
                LM_DEBUG(("\n UART-Config updated to %d ", event->uartconfig ));
                uart_tx("Device will Reboot\r\n", lstrlen("Device will Reboot\r\n"));
                Initiate_device_reset_delay();
        }
        
    } 
    
}

static void handle_at_UpggradeInterface( event_at_UpgradeIntf_st *event )
{  
    LOG_INFO(("\n handle_at_UpggradeInterface "));
     
    if (event->type == OP_GET)
    {
        /* 1= UART, 2=OTA */
        MOCK(at_parser_send_ok_response());
        
        if( app_config.FW_upgrade_interface==1 )
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "UPGRADEINT", "s",  "UART", strlen("UART")   ));
        
        else if( app_config.FW_upgrade_interface==2 )
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "UPGRADEINT", "s",  "OTA", strlen("OTA")   ));   
        
    }
    else
    {
        if( app_config.FW_upgrade_interface == event->interface )
        {
                MOCK(at_parser_send_err_response()); 
        }
        else
        {
                app_config.FW_upgrade_interface =  event->interface ;
                Store_memory_Location ( LCN_UPGRADE_INTERFACE, (const uint16*)&app_config.FW_upgrade_interface, sizeof(app_config.FW_upgrade_interface) );
                MOCK(at_parser_send_ok_response());
        }
    } 
       
}
/*******************************************************************************
  Events generated by AT command parser
  ******************************************************************************/
void handle_at_uart_event_message(Task task, MessageId id, Message message)
{
    switch (id)
    {
        case EventAtFlow:
            handle_at_uart_flow((event_at_flow_st *)message);
            break;
            
        case EventAtuartBaud:
            handle_at_uart_baud((event_at_baud_st *)message);
            break;
            
        case EventAtStopbit:
            handle_at_uart_stopbit((event_at_stopbit_st *)message);
            break;
            
        case EventAtParitybit:
            handle_at_uart_paritybit((event_at_paritybit_st *)message);
            break;
            
        case EventAtUartConfigType:
            handle_at_UartConfigType((event_at_uartconfig_st *)message);
            break;
            
        case EventAtUpgradeInterface:
            handle_at_UpggradeInterface((event_at_UpgradeIntf_st *)message);
            break;
            
        default:
            LM_DEBUG(("Invalid AT UART Event message (id=%u)\n", id));
            break;
    }    
    
}

/********************************************************************************
    Change of Flow control requires reboot with Bootmode UART=0xa808 setting.
  *******************************************************************************/
static void update_UART_FLOWstate ( void )
{
   /*this function is called only when Flow-Control change is required.*/ 
    
    PsStore( LCN_FLOW, &app_config.flow, sizeof(app_config.flow) );
    
    Initiate_device_reset_delay();
    
}
        

