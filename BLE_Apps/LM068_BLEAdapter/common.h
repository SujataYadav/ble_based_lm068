
#ifndef common_h
#define common_h

/** Length of the local_name array */
#define BT_AT_DEVICE_NAME_LENGTH (31)  /*chars=31, null=32*/ 
#define MAX_DEVICE_NAME_ATTRIB_LENGTH (32)  /*remote dev name search*/
#define BT_AT_DEVICE_PIN_LENGTH   (17)

#define NUMBER_OF_TRUSTED_DEVICES (8)


/* ----------------------------------------------------------------------------- */
/** This macro is used to execute mocked functions in unit tests. When
 *  not unit testing the normal implementation will be executed. */
#ifdef UNITTEST

#include "mock.h"
#define MOCK(x) mock_ ## x

#else

/** This macro is used to execute mocked functions in unit tests. When
 *  not unit testing the normal implementation will be executed. */
#define MOCK(x) x

#endif
/* ----------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------- */


#ifndef UNUSED
#define UNUSED(x) (void)x
#endif



/** Length of a literal string. Must only be used for literals (e.g., "abc")
 *  and never for variables (use strlen() instead). */
#define lstrlen(str) (sizeof(str) - 1)


/** Get the number of elements in an array. This must be passed a direct
 *  reference to an array and not a pointer. */
#define NUM_ELEMENTS(x) (sizeof(x)/sizeof(x[0]))


/**
 * Find the minimum of two numbers.
 *
 * \note the expansion of this macro will evaluate the selected value
 * twice.
 */
#define MIN(x, y) ((x) < (y) ? (x) : (y))


/**
 * Enumeration of status codes
 */
typedef enum status
{
    STATUS_OK                                           = 0,
    STATUS_INSUFFICIENT_SPACE                           = 1,
    STATUS_MEMORY_ALLOCATION_FAILURE                    = 2,
    STATUS_PS_READ_FAILURE                              = 3,
    STATUS_PS_WRITE_FAILURE                             = 4,
    STATUS_INCOMPLETE                                   = 5,
    STATUS_OTHER_ERROR                                  = 0xFF
} status_et;

typedef enum
{
	SPP_ONLINE_EXIT,			/* not connected to any device */		
	
    SPP_ONLINE_COMMAND,			/* connected to remote dev, but in command mode */
    SPP_ONLINE_DATA,			/* connected to remote dev AND in DATA mode */
    SPP_ONLINE_ESCAPE_WAITING,	/* connected and Waiting for escape seq */
    
    SPP_OTA_INITIATED,
    SPP_OTA_RECEIVING,
    SPP_OTA_SINK_CLOSED,
    SPP_OTA_SINK_VERYFYING,

        SPP_UART_OTA_INITIATED,
        SPP_UART_OTA_RECEIVING,
        
	SPP_ONLINE_MAX
	
}spp_online_mode_et;





/*******************************************************************************
 *******************************************************************************/

/*! local LED status enum */
typedef enum
{
    POWER_ON_STATE,
    PAIRING_STATE,
    MASTER_ACON_OFF_STATE,    
    MASTER_ACON_ON_STATE,
    SLAVE_DISCOVERABLE_STATE,
    CONNECTED_STATE
    
} device_state_et;

/** Global application data type. */
typedef struct app_data
{
    /** Tracks whether power is on 
    bool power_on;*/

    /** Keeps track of how much data in the UART receive buffer has
     *  already been echoed. */
    uint16 echo_offset;

    /** Application Task */
    Task task;
    	
    /*PIO task*/    
    Task pio_task;
    
    
	spp_online_mode_et	spp_online_state;  /*spp command, escape_waiting or data mode */
    
    device_state_et  device_state;
    
} app_data_st;


/** Global application data */
extern app_data_st app_data;

extern uint8 Power_On_Seq_Timeout ;


/**
 * Send the given message via the UART.
 *
 * \param message   The message to send.
 * \param length    Length of message. If this value is zero then the length
 *                  will be automatically determined using strlen().
 * \return  TRUE if the message was sent, else FALSE if there was not enough
 *          buffer space.
 */
bool uart_tx(const char *message, uint16 length);





/* -------------------------------------------------------------------------
    Settings for Flow-Control 
  ------------------------------------------------------------------------- */
/* NOTE:
	BOOT MODE 2 PS Settings must have  
    PSKEY_UART_CONFIG_USR(450) as 0x08a0
	BOOT MODE 1 PS Setttings have 
	PSKEY_UART_CONFIG_USR(450) as 0x08a8 by default
    
    Dont forget to "Initial_Boot_mode" PS-KEY as per default Flow control setting
    for Default flow off, initial boot mode = 2
    for Default Flow ON,  initial boot mode = 1 
 */
#define     BOOT_MODE_FLOW_CONTROL_ON       1
#define     BOOT_MODE_FLOW_CONTROL_OFF      2
#define     BOOT_MODE_FW_UPGRADE            0       /* Loader always uses mode 0 */

/* Call the function "BootSetMode(mode)"  with valid parameter

   When device is required to re-boot, for role change or change of flow control 
   use parameters from BOOT_MODE_FLOW_CONTROL_ON or BOOT_MODE_FLOW_CONTROL_OFF.

   When the device is required to enter in DFU mode, pass the parameter BOOT_MODE_FW_PGRADE.
*/


void handle_SwitchPress_Timeout ( void );
#define KE_SWITCH_PRESSED_TIMEOUT_MS          ((uint16)1200)  /*mili-seconds*/





#endif 

