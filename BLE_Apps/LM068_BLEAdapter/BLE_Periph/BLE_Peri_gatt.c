
#include "../CSR_LIB.h"
#include "../common.h"
#include "../events.h"
#include "../AT_parse/AT_Parse.h"
#include "../config/config.h"

#include "../BT_core/bt_core.h"

#include "../debug.h"

#include "../LED/LED.h"


#include "BLE_Peri.h"
#include "app_gatt_db.h"



extern Ring_st  *Ring ;  


uint16  BufReadUint16(uint8 *ptr);



uint16 BufReadUint16(uint8 *ptr)
{
/*return the number pointed by buffer as a uint16, buff=lo, buff+1=hi, number = hi,lo */
    uint16 number =0;
    
    number =  ( 4 << (*(ptr+1))  ) + *ptr; ;
    LM_BLE_DEBUG(("\n  *(ptr+1)=0x%x, *ptr=0x%x,  number=0x%x ", (*(ptr+1)), *ptr, number ));
    return number;    
    
}

void SerialHandleAccessWrite(GATT_ACCESS_IND_T *p_ind)
{
    uint16 client_config;
    uint8*  p_value = p_ind->value;
    uint16 result = gatt_status_success ;

    switch(p_ind->handle)
    {
              
        case HANDLE_SERIAL_DATA_TRANSFER:
        break;
       
        case HANDLE_SERIAL_DATA_C_CFG:
        {
            client_config = BufReadUint16( (uint8*)p_value); 
            
            
            if(*p_value == 1)
            {
                    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "SER_CCFG", "s",
                                           "Enabled",
                                           strlen("Enabled")  )); 
                     MOCK(at_parser_send_event_message(AT_EVENT_TYPE_INFO, "BLE_ADAPTER", "s", "Ready For USE", strlen("Ready For USE") ));
                     
                }
            else
                    MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "SER_CCFG", "s",
                                           "Disabled",
                                           strlen("Disabled")  ));  
                    
             
        }
        break;
        
        default:
        {
            result = gatt_status_write_not_permitted;
        }
        break;
    }


    if( stBLEConnectionData->eConnStatus == KE_CONNECTED)
            GattAccessResponse(p_ind->cid, p_ind->handle, result, 0, NULL);

}
 
static bool SerialCheckHandleRange(uint16 handle)
{
    return ((handle >= HANDLE_SERIAL_SERVICE) &&
            (handle <= HANDLE_SERIAL_SERVICE_END))
            ? TRUE : FALSE;
}


static void SerialHandleAccessRead(GATT_ACCESS_IND_T *p_ind)
{
    /* Initialise to 2 octets for Client Configuration */
    uint16 length = 2;
    uint8  value[2];
    uint16 rc = gatt_status_success;

    switch(p_ind->handle)
    {
        
        case HANDLE_SERIAL_DATA_C_CFG:
        {
            LM_BLE_DEBUG(("\n  serial CCFG read by remote-device  "));
        }
        break;
      
        case HANDLE_SERIAL_DATA_TRANSFER:
        {
            LM_BLE_DEBUG(("\n  HANDLE_SERIAL_DATA_TRANSFER  Read sometihning"));
        }
        break;
        default:
        {
            rc = gatt_status_read_not_permitted;
        }
        break;
    }

    /* Send GATT Response for the received request */
    if( stBLEConnectionData->eConnStatus == KE_CONNECTED)
    GattAccessResponse(p_ind->cid, p_ind->handle, rc,
                          length, value);

}







void HandleAccessRead(GATT_ACCESS_IND_T *p_ind)
{
    if(SerialCheckHandleRange(p_ind->handle))
    {
        SerialHandleAccessRead(p_ind);
    }
    else
    {
        /* Application doesn't support 'Read' operation on received attribute
         * handle, so return 'gatt_status_read_not_permitted' status.
         */
        if( stBLEConnectionData->eConnStatus == KE_CONNECTED)
        GattAccessResponse(p_ind->cid, p_ind->handle,
                      gatt_status_read_not_permitted,
                      0, NULL);
    }
}




void HandleAccessWrite(GATT_ACCESS_IND_T *p_ind)
{
    /* For the received attribute handle, check all the services that support
     * attribute 'Write' operation handled by application.
     */
    /* More services may be added here to support their write operations */
    if(SerialCheckHandleRange(p_ind->handle))
    {
        /* Attribute handle belongs to Serial service */
        SerialHandleAccessWrite(p_ind);
    }
    else
    {
        /* Application doesn't support 'Write' operation on received  attribute
         * handle, so return 'gatt_status_write_not_permitted' status
         */
        if( stBLEConnectionData->eConnStatus == KE_CONNECTED)
        GattAccessResponse(p_ind->cid, p_ind->handle,
                      gatt_status_write_not_permitted,
                      0, NULL);
    }
}






 void send_BLE_data_packet ( void )
{
    
#define BLE_MTU_size 23
    
    uint8 fresh_bytes = 0;
/**/    uint8 i=0;
     
        uint8   full = 0;

    if( BLE_Adaper_ready == FALSE )
        return;
    

    if( Ring->Clear_flag == TRUE )
    {
        Ring->Clear_flag = FALSE;
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "RING_CLEAR:", "s", 
                                                                "Ready_for_SPP_IN_Data",
                                                                strlen("Ready_for_SPP_IN_Data"))); 
    }
    
    
    if( Ring->Read_pointer == 0 )
    {
        if( Ring->C1->full == TRUE )
        {
            full = 1;
            fresh_bytes  = CBUFF_BUFFER_SIZE_254 - Ring->C1->tail;
            /* LOG_RING((" FULL_1 fresh_bytes = %d ", fresh_bytes)); */
        }
        else
        {
            fresh_bytes =  Ring->C1->head - Ring->C1->tail;
            /* LOG_RING(("  fresh_bytes_1 = %d ", fresh_bytes)); */
        }
    }
    else if( Ring->Read_pointer == 1 )
    {
        if( Ring->C2->full == TRUE )
        {
            full = 2;
            fresh_bytes  = CBUFF_BUFFER_SIZE_254 - Ring->C2->tail; 
            /* LOG_RING(("\n FULL_2 fresh_bytes = %d ", fresh_bytes)); */
        }
        else
        {
            fresh_bytes =  Ring->C2->head - Ring->C2->tail;
           /* LOG_RING(("\n fresh_bytes_2 = %d ", fresh_bytes)); */
        }
    }
     
            if(fresh_bytes)
            {
                    if( ( fresh_bytes ) > (BLE_MTU_size-3) ) 
                    {
                            if( Ring->Read_pointer == 0 )
                            {
                                GattNotificationRequest ( app_data.task,
                                                  stBLEConnectionData->ui16ConnectionID, 
                                                  HANDLE_SERIAL_DATA_TRANSFER,
                                                  (BLE_MTU_size-3),
                                                  (const uint8 * ) &Ring->C1->buffer[Ring->C1->tail] );
                                
                                                
                                                        LOG_RING(("\n BLE_1 read ::\t"));
                                                          for(i=0; i<(BLE_MTU_size-3) ; i++)
                                                                 LOG_RING(("%c", Ring->C1->buffer[Ring->C1->tail+i] ));/* */
                                
                                
                                Ring->C1->tail =  Ring->C1->tail + (BLE_MTU_size-3);
                                 LOG_RING(("\t BLE_1 Sent %d , tail = %d ", (BLE_MTU_size-3)  , Ring->C1->tail ));  /* */
                            }
                            else
                            {
                                GattNotificationRequest ( app_data.task,
                                                  stBLEConnectionData->ui16ConnectionID, 
                                                  HANDLE_SERIAL_DATA_TRANSFER,
                                                  (BLE_MTU_size-3),
                                                  (const uint8 * ) &Ring->C2->buffer[Ring->C2->tail] );
                                
                                                
                                                        LOG_RING(("\n BLE_2 read ::\t"));
                                                          for(i=0; i<(BLE_MTU_size-3) ; i++)
                                                                 LOG_RING(("%c", Ring->C2->buffer[Ring->C2->tail+i] )); /**/
                                
                                
                                Ring->C2->tail =  Ring->C2->tail + (BLE_MTU_size-3);
                                 LOG_RING(("\t BLE_2 Sent %d , tail = %d ", (BLE_MTU_size-3)  , Ring->C2->tail )); /**/                                
                            }
                                
                    }
                    else
                    {
                            if( Ring->Read_pointer == 0 )
                            {
                                GattNotificationRequest ( app_data.task,
                                                  stBLEConnectionData->ui16ConnectionID, 
                                                  HANDLE_SERIAL_DATA_TRANSFER,
                                                  fresh_bytes,
                                                  (const uint8 * ) &Ring->C1->buffer[Ring->C1->tail] ); 
                                
                                                       LOG_RING(("\n BLE_1 read ::\t"));
                                                          for(i=0; i<fresh_bytes ; i++)
                                                                 LOG_RING(("%c", Ring->C1->buffer[Ring->C1->tail+i] ));     /*  */
                               LOG_RING(("\t BLE_1 Sent %d ", fresh_bytes ));  /* */
                            }
                            else
                            {
                                GattNotificationRequest ( app_data.task,
                                                  stBLEConnectionData->ui16ConnectionID, 
                                                  HANDLE_SERIAL_DATA_TRANSFER,
                                                  fresh_bytes,
                                                  (const uint8 * ) &Ring->C2->buffer[Ring->C2->tail] ); 
                                
                                                      LOG_RING(("\n BLE_2 read ::\t"));
                                                          for(i=0; i<fresh_bytes ; i++)
                                                                 LOG_RING(("%c", Ring->C2->buffer[Ring->C2->tail+i] ));   /*    */ 
                                LOG_RING(("\t BLE_2 Sent %d ", fresh_bytes )); /* */
                            }
                                                                
                                if(full == 0)
                                {
                                    if ( Ring->Read_pointer == 0 )
                                    {
                                        Ring->C1->tail = Ring->C1->tail + fresh_bytes;
                                        LOG_RING(("\t tail_1>> %d ", Ring->C1->tail )); /* */ 
                                    }
                                    else
                                    {
                                        Ring->C2->tail = Ring->C2->tail + fresh_bytes;
                                        LOG_RING(("\t tail_2>> %d ", Ring->C2->tail )); /* */ 
                                    }
                                }
                                else if(full == 1)
                                {
                                    Ring->C1->full = FALSE;
                                    Ring->C1->tail = 0;
                                    Ring->Read_pointer = 1;
                                    LOG_RING(("\t tail_1>> %d ", Ring->C1->tail )); /* */ 
                                }
                                else if(full == 2)
                                {
                                    Ring->C2->full = FALSE;
                                    Ring->C2->tail = 0;
                                    Ring->Read_pointer = 0;
                                    LOG_RING(("\t tail_2>> %d ", Ring->C2->tail )); /* */ 
                                    
                                    if(Ring->full_count == 1)
                                    {
                                        Ring->full_count = 0;
                                        Ring->Clear_flag = TRUE;
                                    }
                                }     
                    }
            }
               
            
    MessageCancelFirst( app_data.task, US_SEND_BLE_DATA_PACKET_MSG );
    MessageSendLater( app_data.task, US_SEND_BLE_DATA_PACKET_MSG, 0, KE_SEND_BLE_DATA_PACKET_TIME ) ; 

}



/*******************************************************************************
  handlers for AT command requests for BLE 
  ******************************************************************************/





