
#ifndef BLE_PERIPH__H   
#define BLE_PERIPH__H

#include "gatt.h"
#include "../BLE/BLE_Cen_core.h"


#define         GAP_CENTRAL_ROLE        1
#define         GAP_PERIPHERAL_ROLE     2



void 	BLE_Peri_Initialise 		( void );
void    Read_BLE_ROLE_fromPS        ( void );




/****************************************************************************************************************************/
/* --------------- variables for BLE -------------- */

void handleGATTConnectionIndic( GATT_CONNECT_IND_T *ind );
void handle_gatt_access_indication( GATT_ACCESS_IND_T *ind );
void handle_exchangeMTUindication( GATT_EXCHANGE_MTU_IND_T *ind );
void Handle_GattNotificationConfirmation ( GATT_NOTIFICATION_CFM_T *cfm );
void handle_configure_local_address_cfm( CL_DM_BLE_CONFIGURE_LOCAL_ADDRESS_CFM_T *cfm );
void handle_BLE_AddDevWhitelistcfm( CL_DM_BLE_ADD_DEVICE_TO_WHITE_LIST_CFM_T *cfm );
void handle_BLE_Connection_parameters_Update_Request_cfm( CL_DM_BLE_CONNECTION_PARAMETERS_UPDATE_CFM_T *cfm );
void handle_BLE_ClearWhitelist_cfm( CL_DM_BLE_CLEAR_WHITE_LIST_CFM_T *cfm );
void handle_AdvertisingParametersCfm( CL_DM_BLE_SET_ADVERTISING_PARAMS_CFM_T *cfm );
void handle_BLE_Adv_datacfm(CL_DM_BLE_SET_ADVERTISING_DATA_CFM_T *cfm);
void  handle_ReadWhitelistSizeCfm( CL_DM_BLE_READ_WHITE_LIST_SIZE_CFM_T *cfm );
        
void Add_DevName_in_ScanResp_data( void );   
void Initialise_Sink_for_Peri_Rcv_Data ( void );



#endif   /* end BLE_PERIPH__H */


