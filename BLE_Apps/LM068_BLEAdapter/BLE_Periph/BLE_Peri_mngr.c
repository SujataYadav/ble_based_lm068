#include <connection.h>
#include "../CSR_LIB.h"
#include "../common.h"
#include "../events.h"
#include "../AT_parse/AT_Parse.h"
#include "../config/config.h"

#include "../BT_core/bt_core.h"

#include "../debug.h"

#include "../LED/LED.h"


#include "BLE_Peri.h"
#include "app_gatt_db.h"




extern Ring_st  *Ring;  
extern uint8 start_data ;


static Sink Uart_sink;
static uint16 Uart_offset;
static uint8 *Uart_dest;



extern const char bleauthstart;


/*******************************************************************************
  ******************************************************************************/


/*******************************************************************************
  ******************************************************************************/

void Initialise_Sink_for_Peri_Rcv_Data ( void )
{              
        Uart_sink = StreamUartSink();  
        Uart_dest = SinkMap(Uart_sink);
}


void set_BLE_Advertising_Parameters ( void )
{
    ble_adv_params_t stAdvertParams;

    /* Adv interval range: 0x0020..0x4000 Default: 0x0800 */ 
    /*0x800 = 1.28 seconds*/
    stAdvertParams.undirect_adv.adv_interval_min = (uint16)0x00A0;  
    stAdvertParams.undirect_adv.adv_interval_max = (uint16)0x0100;
 
    
/*
ble_filter_none 	    Allow scan and connect request from any
ble_filter_scan_only    Allow scan request from white list only, allow any connect
ble_filter_connect_only Allow any scan, Allow connect from white list only
ble_filter_both 	    Allow scan and connect request from white list only
*/    
    if ( FALSE == stBLEInfoData->WhiteListEnable  ) 
    {
        LM_BLE_DEBUG(("\n Advertising_filter_policy = ble_filter_none  "));
        stAdvertParams.undirect_adv.filter_policy = ble_filter_none ;
    }
    else 
    {
        LM_BLE_DEBUG(("\n Advertising_filter_policy = ble_filter_both  "));
        stAdvertParams.undirect_adv.filter_policy = ble_filter_both ; 
    }
    stAdvertParams.undirect_adv.filter_policy = ble_filter_none ;
    
    ConnectionDmBleSetAdvertisingParamsReq( ble_adv_ind,        /* Advertising packet type used for advertising */ 
                                            FALSE,               /* FALSE for public device address */
                                            0xFF,               /* Advertising channels to be used. At least one should be used. If none are set, all channels shall be used by default */
                                            (const ble_adv_params_t *)&stAdvertParams ); /* undirected or directed advertising specific params. If NULL and ble_adv_type is 'ble_adv_direct_ind' then the CL will Panic. For other ble_adv_type, parameters are validated by BlueStack */
    
}


void Add_DevName_in_ScanResp_data( void )
{
#define MAX_NAME_BUFFER  ( 31 )  /* name, +size(1) + AD_type(1) */
    /* size_sr_data	The length of the scan response data, maximum is 31 octets. */
    
    uint8 buff[MAX_NAME_BUFFER];

    app_devname_t       name; 
    
    PsFullRetrieve( (USER_PSKEY_0+LCN_NAME_LENGTH), &name.name_len, sizeof(name.name_len) );
    PsFullRetrieve( (USER_PSKEY_0+LCN_NAME_STRING), &name.dev_name, name.name_len ); /*returns 0 if unsuccess */
    
    buff[0] = ( name.name_len  + 2) ;  
    buff[1] = AD_TYPE_LOCAL_NAME_COMPLETE;        

    if(( name.name_len  + 2) > 31 )
    {
        memcpy( (buff+2), &name.dev_name,  (31-2) ); 
        buff[0]  = 31 ;
    }
    else
    memcpy( (buff+2), &name.dev_name,  name.name_len );     
        
    /* use APPconfig.name for BLE advertising <same name for BT and BLE > */
    ConnectionDmBleSetScanResponseDataReq(  buff[0], (const uint8 *)buff);
    
}
  

/****************************************************************************************
  Messages from GATT Library
  ***************************************************************************************/

void Handle_GattNotificationConfirmation ( GATT_NOTIFICATION_CFM_T *cfm )
{     
        LM_BLE_DEBUG(("\n GATT_NOTIFICATION_CFM_T "));
        
        if(cfm->status == gatt_status_success  )
            LM_BLE_DEBUG(("Success "));
        else
            LM_BLE_DEBUG(("Error "));
}


static void send_Conn_update_request( void )
{ 
    ConnectionDmBleConnectionParametersUpdateReq	(	app_data.task, 
                                                        &stBLEConnectionData->stConnectedDeviceAddress,
                                                        0x08,/* uint16 	min_interval, */
                                                        0x08,/*uint16 	max_interval,*/
                                                        0, /*uint16 	latency,*/
                                                        0x0168, /* uint16 	timeout, */
                                                        0 , /*uint16 	min_ce_length,*/
                                                        0 /*uint16 	max_ce_length */
                                                    );	    
                                                        
    
}

void handleGATTConnectionIndic( GATT_CONNECT_IND_T *ind )
{ 
    /* GAP Peripheral role does not receive CFM message for the connection */
    LM_DEBUG(("\n Conn_Ind from device = bd_addr=%04x-%02x-%06lx   flags=0x%x, type =0x%x ", 
              ind->taddr.addr.nap, 
              ind->taddr.addr.uap, 
              ind->taddr.addr.lap, 
              ind->flags,
              ind->taddr.type ));
    
   if( stBLEConnectionData->eConnStatus == KE_CONN_CLOSED  )  
    {       
                        GattConnectResponse( app_data.task, 
                         ind->cid,  
                         ind->flags, 
                         TRUE);
                        
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "BLE_CONNECTED:", "B:u", 
                                          &(ind->taddr.addr), (uint32)ind->mtu ));
        
            stBLEConnectionData->ui16ConnectionID = ind->cid;
            
            stBLEConnectionData->stConnectedDeviceAddress = ind->taddr;
            stBLEConnectionData->ui16ConnMTU = ind->mtu;
            stBLEConnectionData->ui16ConnFlags = ind->flags;
            
            stBLEConnectionData->eConnStatus = KE_CONNECTED;      
        
            if ( FALSE == stBLEInfoData->WhiteListEnable  )
            {
               /* uart_tx( (const char*)&bleauthstart[0], strlen((const char*)bleauthstart));*/
                
                ConnectionDmBleSecurityReq( app_data.task, 
                                            &ind->taddr,
                                            ble_security_encrypted_bonded, 
                                            gatt_connection_ble_master_directed );
                
            }
            
            /* Stop both advertising */
            Stop_advertising_after_connect(); 
            
            BLE_Adaper_ready = TRUE;
          
            send_Conn_update_request();
            
    }
   else
   {
                GattConnectResponse( app_data.task, 
                         ind->cid,  
                         ind->flags, 
                         FALSE);
   }
   
}


void handle_gatt_access_indication( GATT_ACCESS_IND_T *ind )
{
      /*  uint8 i = 0;*/
    uint16  free_space = 0;
    
    
    if(ind->flags == 
                (ATT_ACCESS_WRITE |
                 ATT_ACCESS_PERMISSION |
                 ATT_ACCESS_WRITE_COMPLETE))
            {                
                            if(ind->handle == HANDLE_SERIAL_DATA_TRANSFER )
                                {
                                        if(ind->size_value)
                                           {     
                                                 
                                               /* DEBUG_OTA(("\n Rcvd = %d  ", ind->size_value ));*/
                                                
                                                    free_space = SinkSlack(Uart_sink);
                                                   /* DEBUG_OTA(("\n Free spcae in Sink is = %d  ", free_space )); */
                                                    
                                                    /* Claim space in the sink, getting the offset to it */
                                                    Uart_offset = SinkClaim(Uart_sink, ind->size_value);
                                                    if(Uart_offset == 0xFFFF)
                                                    return;
                                                    
                                                    /* Copy the message into the claimed space */
                                                    memcpy(Uart_dest+Uart_offset, ind->value, ind->size_value);
                                                    
                                                    /* Flush the data out to the uart */
                                                    PanicZero(SinkFlush(Uart_sink, ind->size_value));
                                                    
                                                    /*
                                                    LOG_RING(("\n rcvd data %d = ", ind->size_value));
                                                    for(i=0; i<ind->size_value ; i++)
                                                                         LOG_RING(("%c", ind->value[0+i] ) ); */
                                                    
                                            }/*if data*/
                                            GattAccessResponse(ind->cid, ind->handle, gatt_status_success, 0, NULL);
                                            
                                    }/* if Serial data Xfer handle */
                                    else
                                        HandleAccessWrite(ind);
            }
            /* Received GATT ACCESS IND with read access */
            else if(ind->flags == (ATT_ACCESS_READ | ATT_ACCESS_PERMISSION) )
            {
                HandleAccessRead(ind);
            }
            else
            {
                /* No other request is supported */
                if( stBLEConnectionData->eConnStatus == KE_CONNECTED)
                GattAccessResponse(ind->cid, ind->handle,
                              gatt_status_request_not_supported,
                              0, NULL);
            }

}

void handle_exchangeMTUindication( GATT_EXCHANGE_MTU_IND_T *ind )
{
    LM_BLE_DEBUG(("\n handle_exchangeMTUindication : rcvd_MTU =0x%x ", ind->mtu ));
    
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "MTU_EXCH_IND:", "u", (uint32)ind->mtu ));
    
   if( stBLEConnectionData->eConnStatus == KE_CONNECTED) 
   GattExchangeMtuResponse	(	ind->cid , 20 ); 	 
}




/*****************************************************************************************************
  BLE security manager messages from  CL_library
  ****************************************************************************************************/


void handle_configure_local_address_cfm( CL_DM_BLE_CONFIGURE_LOCAL_ADDRESS_CFM_T *cfm )
{
        if( cfm->status == success )
        {
            LM_BLE_DEBUG(("   >>> success "));       
            stBLEInfoData->Peri_addr_type = cfm->addr_type;
            
           /* stBLEInfoData->st_myBLETypedStaticAddress.type = address.type ;  */
           /* memcpy( &(stBLEInfoData->st_myBLETypedStaticAddress.addr), &address.addr, sizeof(address.addr)  );
            write_BDaddr(LCN_BLE_ADVERT_ADDRESS,  &address.addr ); */
        }
        else        
        {
            uart_tx("\n Error BLE Address ", lstrlen("\n Error BLE Address "));
        }
        
}


void handle_AdvertisingParametersCfm( CL_DM_BLE_SET_ADVERTISING_PARAMS_CFM_T *cfm )
{
    if(stBLEInfoData->bGATTInitStatus == FALSE) 
        return;     
    
    if( cfm->status == success )
    {
        LM_BLE_DEBUG((" >>>> Success "));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LE_ADV_PARA", "s", 
                                            "Success", strlen("Success") ));
    }
    else
    {
        LM_BLE_DEBUG((" >>>> Error ERR_Code = %d ?????  ", cfm->status ));   
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LE_ADV_PARA", "s", 
                                            "Error", strlen("Error") ));
    }
}


void handle_BLE_Adv_datacfm(CL_DM_BLE_SET_ADVERTISING_DATA_CFM_T *cfm)
{
      if( stBLEInfoData->bGATTInitStatus == FALSE ) 
        return; 
      
     if( cfm->status == success )
    {
        LM_BLE_DEBUG((" >>>> Success "));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LE_Adv_data", "s", 
                                            "Success", strlen("Success") ));
    }
    else
    {
        LM_BLE_DEBUG((" >>>> Error ERR_Code = %d ?????  ", cfm->status ));   
                MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "LE_Adv_data", "s", 
                                            "Error", strlen("Error") ));
    }   
}        


void handle_BLE_ClearWhitelist_cfm( CL_DM_BLE_CLEAR_WHITE_LIST_CFM_T *cfm )
{
    if( cfm->status == success )
    {
        LM_BLE_DEBUG((" >>>> Success "));
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "CLR_WHITE", "s", 
                                            "Success", strlen("Success") ));
    }
    else
    {
        LM_BLE_DEBUG((" >>>> Error ERR_Code = %d ?????  ", cfm->status ));  
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "CLR_WHITE", "s", 
                                            "Error", strlen("Error") ));
    }
}




void  handle_ReadWhitelistSizeCfm( CL_DM_BLE_READ_WHITE_LIST_SIZE_CFM_T *cfm )
{
    if( cfm->status == success )
    {
        LM_BLE_DEBUG((" >>>> Success , size=%d ", cfm->white_list_size ));
    }
    else
    {
        LM_BLE_DEBUG((" >>>> Error ERR_Code = %d ?????  ", cfm->status ));  
    }
}

void handle_BLE_AddDevWhitelistcfm( CL_DM_BLE_ADD_DEVICE_TO_WHITE_LIST_CFM_T *cfm )
{
  /*  if( cfm->status == success )
    {
        LM_BLE_DEBUG((" >>>> Success " ));
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "ADD_DevWhite", "s", 
                                            "Success", strlen("Success") ));
    }
    else
    {
        LM_BLE_DEBUG((" >>>> Error ERR_Code = %d ?????  ", cfm->status ));  
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "ADD_DevWhite", "s", 
                                            "Error", strlen("Error") ));
    }*/
}


void handle_BLE_Connection_parameters_Update_Request_cfm( CL_DM_BLE_CONNECTION_PARAMETERS_UPDATE_CFM_T *cfm )
{
    if( cfm->status == success )
    {
        MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "CONN_PAR_UPDATE", "s", 
                                            "Success", strlen("Success") ));
    }
    else
    {
            MOCK(at_parser_send_event_message(AT_EVENT_TYPE_REP, "CONN_PAR_UPDATE", "s", 
                                            "Error", strlen("Error") ));
    }
}

/*************************************************************************************************************
  ************************************************************************************************************/
/*************************************************************************************************************
  ************************************************************************************************************/







