/* 
    LED module inclusion file
*/


#ifndef INC_LED 
#define INC_LED 1


/* --------------- Inclusions ------------- */



/* --------------- Exported typedefs ------------- */

/* Functional application LEDs */
typedef enum
{
    LED_KE_INIT,
    LED_KE_SPP_ESCAPE_WAIT,
    LED_KE_PANIC,
    LED_KE_MAX_NUM
} LED_ke_LedType;




/* --------------- Exported functions prototypes ---------------- */

void LED_SetPreconfLed   ( LED_ke_LedType eLedType );
void LED_ResetPreconfLed ( LED_ke_LedType eLedType );



#define KE_POWER_ON_SEQUENCE_TIMEOUT_MS       ((uint16)3000)  /* Mili_Seconds */

void Update_LED_state ( void );

void Initiate_LED ( void );
void handle_PowerOnSequence_timeout( void );
void Device_Connected_LedState ( void );
void ready_for_advert_and_connect ( void );


void enable_master_role_toggle( void );

#endif

