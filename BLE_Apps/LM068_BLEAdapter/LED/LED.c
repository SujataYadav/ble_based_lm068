/* 
    LED module source file
*/

#include <connection.h>

#include <stdio.h>
/* --------------- Inclusions ------------- */
#include "../CSR_LIB.h"
#include "../common.h"

#include "../debug.h"
#include "LED.h"
#include "../events.h"





/*    
Define this if needed to print the LED sequence messages
#define CFG_ENABLE_LED_PRINT 1
   */



#ifdef  CFG_ENABLE_LED_PRINT
#define LOG_LED(x)             printf x
#else
#define LOG_LED(x)  
#endif



/* -------------- Local definitions ---------------- */

/* LEDs fixed values used as parameters for LedConfigure function */
#define LED_ENABLE_VALUE                ((uint8)1)
#define LED_DISABLE_VALUE               ((uint8)0)
#define LED_COMMON_PERIOD_VALUE         ((uint16)1000)


#define KE_MASTER_STATE_LED_TIMEOUT_MS  (1000)

/* --------------- Local variables declaration */

/* Array to store association between module LEDs and functional application LEDs */  
static const uint8 aui8LedMatchArray[LED_KE_MAX_NUM] =
{
    LED_0,  /* LED_KE_INIT */
    LED_1,  /* LED_KE_WARNING */
    LED_2   /* LED_KE_PANIC */
};


/* Array to store LEDs duty cycle values */  
static const uint8 aui8LedDutyCycleArray[LED_KE_MAX_NUM] =
{
    8,      /* LED_KE_INIT */
    12,     /* LED_KE_WARNING */
    15      /* LED_KE_PANIC */
};


/* Array to store LEDs flash rate values */  
static const uint8 aui8LedFlashRateArray[LED_KE_MAX_NUM] =
{
    250,    /* LED_KE_INIT */
    200,    /* LED_KE_WARNING */
    80      /* LED_KE_PANIC */
};




/* ------------- Exported functions -------------- */

/* Function to set a preconfigured LED status */
void LED_SetPreconfLed( LED_ke_LedType eLedType )
{
        LedConfigure(eLedType, LED_ENABLE, LED_ENABLE_VALUE);
}


/* Function to reset a LED status */
void LED_ResetPreconfLed( LED_ke_LedType eLedType )
{
        LedConfigure(eLedType, LED_ENABLE, LED_DISABLE_VALUE);
}






/*****************************************************************************/


void static disable_Flash_and_Switchoff ( led_id led )
{
    /* Disable flashing and switch off the LED */
    LedConfigure(led, LED_FLASH_ENABLE, 0);      
    LedConfigure(led, LED_ENABLE, 0);
}


static void LED_PowerOn_state( led_id led )
{
    /* flashing seq for showing the module is powerd on and runnig functions */
    LedConfigure(led, LED_ENABLE, 1);
    LedConfigure(led, LED_FLASH_RATE, 250);  
    LedConfigure(led, LED_DUTY_CYCLE , 15);  
    LedConfigure(led, LED_PERIOD , 500); 
    LedConfigure(led, LED_FLASH_ENABLE, 1);      
}



void Initiate_LED ( void )
{
    LED_PowerOn_state(LED_0);
    LED_PowerOn_state(LED_1);
    LED_PowerOn_state(LED_2);
}

void handle_PowerOnSequence_timeout( void )
{
    disable_Flash_and_Switchoff(LED_0);
    disable_Flash_and_Switchoff(LED_1);
    disable_Flash_and_Switchoff(LED_2);   
    
    LedConfigure(LED_0, LED_ENABLE, 1);
    LedConfigure(LED_0, LED_FLASH_RATE, 10);   
    LedConfigure(LED_0, LED_FLASH_ENABLE, 1);    
}

void Device_Connected_LedState ( void )
{
    disable_Flash_and_Switchoff(LED_1);
    disable_Flash_and_Switchoff(LED_2);

    LedConfigure(LED_2, LED_ENABLE, 1);
    LedConfigure(LED_2, LED_FLASH_RATE, 10);      
    LedConfigure(LED_2, LED_FLASH_ENABLE, 1);  
    
        
    MessageCancelAll( app_data.task, US_MASTER_STATE_LED_TIMEOUT_MSG );    
}

static void Device_Slave_Discoverable_LED_State ( void )
{

    disable_Flash_and_Switchoff(LED_1);
    disable_Flash_and_Switchoff(LED_2);
    
    LedConfigure(LED_2, LED_ENABLE, 1);
    LedConfigure(LED_2, LED_FLASH_RATE, 125);           
    LedConfigure(LED_2, LED_FLASH_ENABLE, 1);      
           
    MessageCancelAll( app_data.task, US_MASTER_STATE_LED_TIMEOUT_MSG );    
    
    LOG_LED(("\n Slave_Discoverable_LED_STATE   "));
    
}


static void Device_Pairing_LED_State ( void )
{
    disable_Flash_and_Switchoff(LED_1);
    disable_Flash_and_Switchoff(LED_2);
    
    LedConfigure(LED_1, LED_ENABLE, 1);
    LedConfigure(LED_1, LED_FLASH_RATE, 175);     
    
    LedConfigure(LED_2, LED_ENABLE, 1);
    LedConfigure(LED_2, LED_FLASH_RATE, 175);        

    LedConfigure(LED_2, LED_FLASH_ENABLE, 1);          
    LedConfigure(LED_1, LED_FLASH_ENABLE, 1);     
    
    MessageCancelAll( app_data.task, US_MASTER_STATE_LED_TIMEOUT_MSG );    
    
    LOG_LED(("\n PAIRING_STATE" ));
}


void enable_master_role_toggle( void )
{
    static uint8 toggle_flag = 0;
    
        LOG_LED(("\n enable_master_role_toggle " ));
        
    if(toggle_flag ==  1)
    {
        toggle_flag =0;
        /*ON*/
            LedConfigure(LED_2, LED_ENABLE, 1);
            LedConfigure(LED_2, LED_FLASH_RATE, 10);      
            LedConfigure(LED_2, LED_FLASH_ENABLE, 1);   
                LOG_LED(("\n Flag=ON" ));
    }
    else
    {        
            /*OFF*/
            toggle_flag = 1;
            disable_Flash_and_Switchoff(LED_2);
                LOG_LED(("\n Flasg=off" ));
    }
    
    MessageSendLater(app_data.task, US_MASTER_STATE_LED_TIMEOUT_MSG, 0, KE_MASTER_STATE_LED_TIMEOUT_MS );
    
}


static void Device_Master_ACON_OFF_LED_state ( void )
{
    disable_Flash_and_Switchoff(LED_1);
    disable_Flash_and_Switchoff(LED_2); 
    
    enable_master_role_toggle();
       
    LOG_LED(("\n MASTER_ACON_OFF_STATE" )); 
}

static void Device_Master_ACON_ON_LED_state ( void )
{
    disable_Flash_and_Switchoff(LED_1);
    disable_Flash_and_Switchoff(LED_2); 
    
    enable_master_role_toggle();
        
    LOG_LED(("\n MASTER_ACON_ON_STATE" )); 
}

/*******************************************************************************/


void Update_LED_state ( void ) 
{
    LOG_LED(("\n Update_LED_state" ));
    
    switch( app_data.device_state )
    {
    case POWER_ON_STATE:
        LOG_LED(("\n POWER_ON_STATE" ));
        Initiate_LED();
        break;
        
    case PAIRING_STATE:
        LOG_LED(("\n PAIRING_STATE" ));
        Device_Pairing_LED_State();
        break;
        
    case MASTER_ACON_OFF_STATE:
       LOG_LED(("\n MASTER_ACON_OFF_STATE" )); 
       Device_Master_ACON_OFF_LED_state();
        break;
        
    case MASTER_ACON_ON_STATE:
        LOG_LED(("\n MASTER_ACON_ON_STATE" ));
        Device_Master_ACON_ON_LED_state ();
        break;
        
    case SLAVE_DISCOVERABLE_STATE:
        LOG_LED(("\n SLAVE_DISCOVERABLE_STATE" ));
        Device_Slave_Discoverable_LED_State();
        break;
        
    case CONNECTED_STATE:
        LOG_LED(("\n CONNECTED_STATE" ));
        Device_Connected_LedState();
        break;
        
    }
        
}





/* End of file */




