#ifndef GPIO_DEFINE_H
#define GPIO_DEFINE_H

#define PIO_BIT_HIGH_MASK(pio)      (0x01UL << (pio))


#if 0
#define PIO0_SCL_SW1_PIN28          (28UL)      /*switch-1, SCL share the same GPIO-0 */ /*input pin*/
#define PIO1_SDA_SW2_PIN29          (29UL)      /*switch-2, SDA share the same GPIO-1 */
#define PIO0_SCL_SW1_PIN28__HIGH_MASK  PIO_BIT_HIGH_MASK(PIO0_SCL_SW1_PIN28)   /* input pin */
#define PIO1_SDA_SW2_PIN29__HIGH_MASK  PIO_BIT_HIGH_MASK(PIO1_SDA_SW2_PIN29)
#endif 


/* MBAUD is used to control the higher Baud-rates, MBAUD=High if Baud>=460K8 */
/* HANDSHAKE_MASK is used to control the the CTS/RTS signals as per DTE/DCE switch */






#ifdef LM068
        #define PIO_DTR_MASK            (18UL)  /* output */
        #define PIO_RTS_MASK            (16UL)  /* output */
        #define PIO_DSR_MASK            (28UL)  /* INput */    
        #define PIO_CTS_MASK            (29UL)  /* INput */
        #define PIO_BUTTON_MASK         (17UL)  /* INput */ 
        #define PIO_MBAUD_ENABLED       (12UL)  /* output */
        #define PIO_HANDSHAKE_MASK      (13UL)  /* output */
        #define CONTROL_MODEM_RTC_MASK  0x04    /* 0x01 */ /* Ready to communicate: DTR/DSR status transfer bit */
        #define CONTROL_MODEM_RTR_MASK  0x08    /* 0x02 */ /* Ready to receive: status transfer bit */

    #define PIO_MBAUD_ENABLED__HIGH_MASK        PIO_BIT_HIGH_MASK(PIO_MBAUD_ENABLED)
    #define PIO_HANDSHAKE_MASK__HIGH_MASK       PIO_BIT_HIGH_MASK(PIO_HANDSHAKE_MASK)
    #define PIO_DTR_MASK__HIGH_MASK             PIO_BIT_HIGH_MASK(PIO_DTR_MASK)
    #define PIO_DSR_MASK__HIGH_MASK             PIO_BIT_HIGH_MASK(PIO_DSR_MASK)
    #define PIO_CTS_MASK__HIGH_MASK             PIO_BIT_HIGH_MASK(PIO_CTS_MASK)
    #define PIO_RTS_MASK__HIGH_MASK             PIO_BIT_HIGH_MASK(PIO_RTS_MASK)
    #define PIO_BUTTON_MASK__HIGH_MASK          PIO_BIT_HIGH_MASK(PIO_BUTTON_MASK)
#endif


#define IO_DEBOUNCE_COUNT           ((uint16)10)
#define IO_DEBOUNCE_PERIOD          ((uint16)2)


void Initiate_Input_Pins_LM961 ( void );
void Set_GPIO_LOW ( uint32 PIO_HIGH_MASK  );
void Set_GPIO_HIGH ( uint32 PIO_HIGH_MASK  );


#endif


